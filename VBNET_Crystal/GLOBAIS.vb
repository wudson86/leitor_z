﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Threading.Thread
Imports System.Globalization
Imports FirebirdSql.Data.FirebirdClient
Imports System.Data.Odbc
Imports System.Data.OleDb
Imports System.Security.Cryptography
Imports MySql.Data.MySqlClient
Module GLOBAIS
    'VARIAVEIS FIREBIRD
    Public fa, xa As FbDataAdapter
    Public fs, xs As DataSet
    Public FIREBIRD_SERVIDOR_TEOREMA, FIREBIRD_SERVIDOR, FIREBIRD_USUARIO, FIREBIRD_SENHA, FIREBIRD_PORTA, FIREBIRD_BANCO, TABELA As String
    Public WB_XML_NFCE_SERVIDOR, WB_XML_NFCE_USUARIO, WB_XML_NFCE_SENHA, WB_XML_NFCE_PORTA, WB_XML_NFCE_BANCO, WB_XML_NFCE_TABELA As String
    Public TIPO_CONEXAO As Char
    Public FIREBIRD_SERVIDOR_ENTREGA As String


    'VARIAVEIS ODBC
    Public Oda As OdbcDataAdapter
    Public Ods As DataSet
    Public ODBC_SERVIDOR, ODBC_USUARIO, ODBC_SENHA, ODBC_PORTA, ODBC_BANCO As String
    Public WB_ODBC_SERVIDOR, WB_ODBC_USUARIO, WB_ODBC_SENHA, WB_ODBC_PORTA, WB_ODBC_BANCO As String

    'Variaveis INI Importador
    Public ORIGEM_VENDAS As String
    Public ORIGEM_RZ As String
    Public DESTINO_VENDAS As String
    Public DESTINO_SCANNTECH As String
    Public DESTINO_RZ As String
    Public DESTINO_NFCE As String
    Public DESTINO_ERROS As String
    Public EXTENSAO As String
    Public TEMPO_IMPORTADOR As String
    Public DATA_INICIAL As String
    Public DATA_FINAL As String
    Public HORA_INICIAL As String
    Public HORA_FINAL As String
    Public NMR_ARQUIVOS As String
    Public HABILITADO_IMPORTADOR As String
    Public HABILITADO_DATA As String
    Public HABILITADO_CONVERTER_XML As String
    Public SENHA As String
    Public ECF_PADRAO As String
    Public RZ_CONSOLIDADO As String
    Public CAMINHO_NFCE As String
    Public CONVERTER_ONLINE As String
    Public HABILITAR_Z As String
    Public HABILITAR_Z_REPETIDA As String
    Public HABILITAR_Z_EMPORIUM As String
    Public HABILITAR_EMAIL As String
    Public HABILITAR_MONITORAMENTO As String
    Public HABILITAR_IMPORTAR_NFCE As String
    Public HABILITAR_IMPORTAR_SCANNTECH As String
    Public HABILITAR_IMPORTAR_ENTREGA As String
    Public HABILITAR_NFCE_PROCESSADA As String
    Public HABILITAR_Z_RECARGA As String
    Public DIRETORIO_COTABILIDADE As String
    Public HABILITAR_DIRETORIO_COTABILIDADE As String
    Public HABILITAR_VENDAS_TEOREMA As String
    Public VENDAS_HORA_INICIAL As String
    Public VENDAS_HORA_FINAL As String
    Public VENDAS_HORA_INICIAL_2 As String
    Public VENDAS_HORA_FINAL_2 As String
    Public VENDAS_DIA_SEMANA As String

    Public RETORNO_CANCELADA As String

    'Variaveis de configuração de E-mail
    Public EMAIL_SMTP As String
    Public EMAIL_PORTA As String
    Public EMAIL_SEGURANCA As String
    Public EMAIL_TEMPO_MONITOR As String
    Public EMAIL_USUARIO As String
    Public EMAIL_REMETENTE As String
    Public EMAIL_DESTINATARIO As String
    Public EMAIL_ASSUNTO As String
    Public EMAIL_SENHA As String

    ''''
    '''' MYSQL
    ''''
    Public WB_MYSQL_SERVIDOR As String
    Public WB_MYSQL_USUARIO As String
    Public WB_MYSQL_SENHA As String
    Public WB_MYSQL_PORTA As String
    Public WB_MYSQL_BANCO As String
    'Public TABELA As String
    Public SQL As String
    ' VARIAVEIS MySQL
    Public myconn As New MySqlConnection
    Public myds As New DataSet
    Public myda As New MySqlDataAdapter
    Public mydt As New DataTable

    Public ACCESS_BANCO As String
    Public CONTINUAR As Boolean

    ''' <summary>
    ''' VARIAVEIS NFCe
    ''' </summary>
    Public CHAVE_NFCE As String
    Public SEFAZ_AMBIENTE As String
    Public CERTIFICADO_DIGITAL As String
    Public HABILITAR_VENDAS_CANCELADAS As String
    Public AMBIENTE As String


    Sub CONEXAO_MySQL(ByVal SQL As String)
        CARREGAR_INI_IMPORTADOR()

        myconn = New MySqlConnection("SERVER=" & WB_MYSQL_SERVIDOR & ";port=" & WB_MYSQL_PORTA & ";user id=" & WB_MYSQL_USUARIO & ";password=" & WB_MYSQL_SENHA & ";database=" & WB_MYSQL_BANCO & ";default command timeout=600;convert zero datetime=True")
        Try
            myconn.Open()
            myda = New MySqlDataAdapter(SQL, myconn) 'Dado1 string SQL
            myds = New DataSet()
            myda.Fill(myds, TABELA)
            Try
            Catch erro As MySqlException
                MessageBox.Show("erro ao ler o banco de dados:" & erro.Message)
            End Try
        Catch erro As MySqlException
            MessageBox.Show("Erro ao tentar conectar ao banco de dados" & erro.Message)
        Finally
            myconn.Dispose()
        End Try
    End Sub


    'Limpar Variáveis
    Sub LIMPAR_VARIAVEIS_CONEXÃO()
        FIREBIRD_SERVIDOR = ""
        FIREBIRD_SERVIDOR_TEOREMA = ""
        FIREBIRD_USUARIO = ""
        FIREBIRD_SENHA = ""
        FIREBIRD_PORTA = ""
        FIREBIRD_BANCO = ""
        WB_XML_NFCE_BANCO = ""

        WB_MYSQL_BANCO = ""
        WB_MYSQL_SERVIDOR = ""
        WB_MYSQL_PORTA = ""
        WB_MYSQL_USUARIO = ""
        WB_MYSQL_SENHA = ""

        ACCESS_BANCO = ""
    End Sub

    Sub CONEXAO_FIREBIRD(ByVal sql As String) ', ByVal conexao As String)
        ' Texto de ligação à base de dados
        ' Dim myConnectionString As String = "User=SYSDBA;Password=masterkey;Database=c:\teorema\windados\teorema_padrao_2_5.fdb;DataSource=localhost;Port=3050;Dialect=3;"
        CARREGAR_INI_TEOREMA()

        '***********String conexao com DLL
        Dim myConnectionString As String = "User=SYSDBA;Password=masterkey;Database=" & FIREBIRD_SERVIDOR & ";DataSource=localhost;Port=3050;Dialect=3;"
        'Dim myConnectionString As String = "User=" & FIREBIRD_USUARIO & ";Password=" & FIREBIRD_SENHA & ";Database=" & FIREBIRD_BANCO & ";DataSource=" & FIREBIRD_SERVIDOR & ";Port=" & FIREBIRD_PORTA & ";Dialect=3;"


        ' Cria uma nova ligação à base de dados
        Using connection As New FbConnection(myConnectionString)

            ' Cria um novo SqlDataAdapter que servirá para atualizar o DataSet
            fa = New FbDataAdapter(sql, connection)

            ' Cria um DataSet, ou seja, uma representação em memória da informação
            fs = New DataSet

            ' Coloca a informação da tabela definida no DataSet
            fa.Fill(fs, TABELA)
            connection.Close()
            connection.Dispose()

            LIMPAR_VARIAVEIS_CONEXÃO()
        End Using
    End Sub

    Sub CONEXAO_FIREBIRD_ENTREGA(ByVal sql As String) ', ByVal conexao As String)
        ' Texto de ligação à base de dados
        ' Dim myConnectionString As String = "User=SYSDBA;Password=masterkey;Database=c:\teorema\windados\teorema_padrao_2_5.fdb;DataSource=localhost;Port=3050;Dialect=3;"
        CARREGAR_INI_TEOREMA()

        '***********String conexao com DLL
        Dim myConnectionString As String = "User=SYSDBA;Password=masterkey;Database=" & FIREBIRD_SERVIDOR_ENTREGA & ";DataSource=localhost;Port=3050;Dialect=3;"
        'Dim myConnectionString As String = "User=" & FIREBIRD_USUARIO & ";Password=" & FIREBIRD_SENHA & ";Database=" & FIREBIRD_BANCO & ";DataSource=" & FIREBIRD_SERVIDOR & ";Port=" & FIREBIRD_PORTA & ";Dialect=3;"


        ' Cria uma nova ligação à base de dados
        Using connection As New FbConnection(myConnectionString)

            ' Cria um novo SqlDataAdapter que servirá para atualizar o DataSet
            fa = New FbDataAdapter(sql, connection)

            ' Cria um DataSet, ou seja, uma representação em memória da informação
            fs = New DataSet

            ' Coloca a informação da tabela definida no DataSet
            fa.Fill(fs, TABELA)
            connection.Close()
            connection.Dispose()

            LIMPAR_VARIAVEIS_CONEXÃO()
        End Using
    End Sub


    '=================================================
    '====            CONEXAO ODBC             ===
    '=================================================

    Sub CONEXAO_ODBC(ByVal sql As String) ', ByVal conexao As String)
        CARREGAR_INI_TEOREMA()

        Using conn As New OdbcConnection("DRIVER=Firebird/InterBase(r) driver;UID=" & ODBC_USUARIO & ";PWD=" & ODBC_SENHA & ";DBNAME=" & ODBC_SERVIDOR & "/" & ODBC_PORTA & ":" & ODBC_BANCO & "")

            Try
                conn.Open()
                Oda = New OdbcDataAdapter(sql, conn) 'Dado1 string SQL
                Ods = New DataSet()
                Oda.Fill(Ods, TABELA)
                Try
                Catch erro As OleDbException
                    MessageBox.Show("erro ao ler o banco de dados:" & erro.Message)
                End Try
            Catch erro As OleDbException
                MessageBox.Show("Erro ao tentar conectar ao banco de dados" & erro.Message)
            Finally
                conn.Close()
                conn.Dispose()
            End Try
            LIMPAR_VARIAVEIS_CONEXÃO()
        End Using
    End Sub

    '=================================================
    '====            CONEXAO WB_XML_NFCE           ===
    '=================================================

    Sub CONEXAO_WB_XML_NFCE(ByVal sql As String)
        CARREGAR_INI_TEOREMA()
        WB_XML_NFCE_SERVIDOR = WB_ODBC_BANCO
        Dim myConnectionString As String = "User=SYSDBA;Password=masterkey;Database=" & WB_XML_NFCE_SERVIDOR & ";DataSource=localhost;Port=3050;Dialect=3;"
        Try
            'CARREGAR_INI_TEOREMA()
            'WB_XML_NFCE_SERVIDOR = Form1.tx_Banco_WB_Xml_Nfce.Text
            'Dim myConnectionString As String = "User=SYSDBA;Password=masterkey;Database=" & WB_XML_NFCE_SERVIDOR & ";DataSource=localhost;Port=3050;Dialect=3;"

            Using connection As New FbConnection(myConnectionString)

                xa = New FbDataAdapter(sql, connection)

                xs = New DataSet
                xa.Fill(xs, "WB_XML_NFCE")
                connection.Close()
                connection.Dispose()

                LIMPAR_VARIAVEIS_CONEXÃO()
            End Using
        Catch
            'MsgBox(myConnectionString)
            'MsgBox(sql)
        End Try
    End Sub
    Sub CONEXAO_WBPORT_ODBC(ByVal sql As String) ', ByVal conexao As String)

        Using conn As New OdbcConnection("DRIVER=Firebird/InterBase(r) driver;UID=" & WB_ODBC_USUARIO & ";PWD=" & WB_ODBC_SENHA & ";DBNAME=" & WB_ODBC_SERVIDOR & "/" & WB_ODBC_PORTA & ":" & WB_ODBC_BANCO & "")
            Try
                conn.Open()
                Oda = New OdbcDataAdapter(sql, conn) 'Dado1 string SQL
                Ods = New DataSet()
                Oda.Fill(Ods, TABELA)
                Try
                Catch erro As OleDbException
                    MessageBox.Show("Erro ao ler o banco de dados:" & erro.Message)
                End Try
            Catch erro As OleDbException
                MessageBox.Show("Erro ao tentar conectar ao banco de dados" & erro.Message)
            Finally
                conn.Close()
                conn.Dispose()
            End Try
            LIMPAR_VARIAVEIS_CONEXÃO()
        End Using
    End Sub


    Public MCon As OleDbConnection
    Public MDa As OleDbDataAdapter
    Public MDs As DataSet
    Public Sub ACCESS_CONEXAO(ByVal SQL As String, ByVal BANCO As String)

        MCon = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BANCO) 'Conexão com banco Access
        MDa = New OleDbDataAdapter(SQL, MCon) 'Defino o Objeto "Adapatdor";
        MDs = New DataSet ' Variável que armazena todas as informações do Banco (RecordSet do "VB-6" melhorado);

        ' Utilizando o Adaptador preencho o DataSet definindo a tabela ("Clientes");
        MDa.Fill(MDs, "clientes")

    End Sub


    '============================================================
    '===               CONEXAO DO ARQUIVO .INI                ===
    '============================================================

    'Agora Declare as Funções das API's que ultilizaremos
    Private Declare Auto Function GetPrivateProfileString Lib "Kernel32" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As StringBuilder, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Auto Function WritePrivateProfileString Lib "Kernel32" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer

    'Para Ler os dados do arquivo INI, usaremos a função LerArquivoDados()

    Function LeArquivoINI(ByVal nome_do_arquivo As String, ByVal sessão As String, ByVal key As String, ByVal valor_Padrão As String) As String
        Const MAX_LENGTH As Integer = 500
        Dim string_builder As New StringBuilder(MAX_LENGTH)
        GetPrivateProfileString(sessão, key, valor_Padrão, string_builder, MAX_LENGTH, nome_do_arquivo)
        Return string_builder.ToString()
    End Function

    Sub CARREGAR_INI_TEOREMA()
        'Ler Arquivo INI
        Dim nome_arquivo_ini_teorema As String = nomeArquivoINI_TEOREMA()
        If Not File.Exists(nome_arquivo_ini_teorema) Then
            MsgBox("Será carregado os valores padrão do sistema.")
            'Form3.HABILITAR()
        End If

        'FIREBIRD
        FIREBIRD_SERVIDOR = LeArquivoINI("C:\Windows\teorema.ini", "Direcionamento de Banco", "Pitágoras", 0)

    End Sub

    Sub CARREGAR_INI_IMPORTADOR()
        'Ler Arquivo INI

        Dim nome_arquivo_ini As String = nomeArquivoINI_IMPORTADOR()
        If Not File.Exists(nome_arquivo_ini) Then
            MsgBox("Será carregado os valores padrão do sistema.")
        End If

        'IMPORTADOR
        ORIGEM_VENDAS = LeArquivoINI("c:\WBPORT.ini", "ORIGEM_VENDAS", "ORIGEM_VENDAS", 0)
        ORIGEM_RZ = LeArquivoINI("c:\WBPORT.ini", "ORIGEM_VENDAS", "ORIGEM_VENDAS", 0)
        DESTINO_VENDAS = LeArquivoINI("c:\WBPORT.ini", "DESTINO_VENDAS", "DESTINO_VENDAS", 0)
        DESTINO_SCANNTECH = LeArquivoINI("c:\WBPORT.ini", "DESTINO_SCANNTECH", "DESTINO_SCANNTECH", 0)
        DESTINO_RZ = LeArquivoINI("c:\WBPORT.ini", "DESTINO_RZ", "DESTINO_RZ", 0)
        DESTINO_NFCE = LeArquivoINI("c:\WBPORT.ini", "DESTINO_NFCE", "DESTINO_NFCE", 0)
        DESTINO_ERROS = LeArquivoINI("c:\WBPORT.ini", "DESTINO_ERROS", "DESTINO_ERROS", 0)
        EXTENSAO = LeArquivoINI("c:\WBPORT.ini", "EXTENSAO", "EXTENSAO", 0)
        TEMPO_IMPORTADOR = LeArquivoINI("c:\WBPORT.ini", "TEMPO", "TEMPO", 0)
        DATA_INICIAL = LeArquivoINI("c:\WBPORT.ini", "DATA_INICIAL", "DATA_INICIAL", 0)
        DATA_FINAL = LeArquivoINI("c:\WBPORT.ini", "DATA_FINAL", "DATA_FINAL", 0)
        HORA_INICIAL = LeArquivoINI("c:\WBPORT.ini", "HORA_INICIAL", "HORA_INICIAL", 0)
        HORA_FINAL = LeArquivoINI("c:\WBPORT.ini", "HORA_FINAL", "HORA_FINAL", 0)
        NMR_ARQUIVOS = LeArquivoINI("c:\WBPORT.ini", "NMR_ARQUIVOS", "NMR_ARQUIVOS", 0)
        HABILITADO_IMPORTADOR = LeArquivoINI("c:\WBPORT.ini", "HABILITADO_IMPORTADOR", "HABILITADO_IMPORTADOR", 0)
        HABILITADO_DATA = LeArquivoINI("c:\WBPORT.ini", "HABILITADO_DATA", "HABILITADO_DATA", 0)
        HABILITADO_CONVERTER_XML = LeArquivoINI("c:\WBPORT.ini", "HABILITADO_CONVERTER_XML", "HABILITADO_CONVERTER_XML", 0)
        HABILITAR_Z_REPETIDA = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_Z_REPETIDA", "HABILITAR_Z_REPETIDA", 0)
        HABILITAR_Z_EMPORIUM = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_Z_EMPORIUM", "HABILITAR_Z_EMPORIUM", 0)
        HABILITAR_Z_RECARGA = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_Z_RECARGA", "HABILITAR_Z_RECARGA", 0)
        CONVERTER_ONLINE = LeArquivoINI("c:\WBPORT.ini", "HABILITADO_CONVERTER_XML", "ONLINE", 0)
        HABILITAR_EMAIL = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_EMAIL", "HABILITAR_EMAIL", 0)
        HABILITAR_MONITORAMENTO = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_MONITORAMENTO", "HABILITAR_MONITORAMENTO", 0)
        HABILITAR_IMPORTAR_NFCE = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_IMPORTAR_NFCE", "HABILITAR_IMPORTAR_NFCE", 0)
        HABILITAR_IMPORTAR_SCANNTECH = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_IMPORTAR_SCANNTECH", "HABILITAR_IMPORTAR_SCANNTECH", 0)
        HABILITAR_IMPORTAR_ENTREGA = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_IMPORTAR_ENTREGA", "HABILITAR_IMPORTAR_ENTREGA", 0)
        HABILITAR_NFCE_PROCESSADA = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_NFCE_PROCESSADA", "HABILITAR_NFCE_PROCESSADA", 0)
        HABILITAR_DIRETORIO_COTABILIDADE = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_DIRETORIO_COTABILIDADE", "HABILITAR_DIRETORIO_COTABILIDADE", 0)
        DIRETORIO_COTABILIDADE = LeArquivoINI("c:\WBPORT.ini", "DIRETORIO_COTABILIDADE", "DIRETORIO_COTABILIDADE", 0)
        CERTIFICADO_DIGITAL = LeArquivoINI("c:\WBPORT.ini", "CERTIFICADO_DIGITAL", "CERTIFICADO_DIGITAL", 0)
        HABILITAR_VENDAS_CANCELADAS = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_VENDAS_CANCELADAS", "HABILITAR_VENDAS_CANCELADAS", 0)
        HABILITAR_VENDAS_TEOREMA = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_VENDAS_TEOREMA", "HABILITAR_VENDAS_TEOREMA", 0)
        VENDAS_HORA_INICIAL = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_VENDAS_TEOREMA", "VENDAS_HORA_INICIAL", 0)
        VENDAS_HORA_FINAL = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_VENDAS_TEOREMA", "VENDAS_HORA_FINAL", 0)
        VENDAS_HORA_INICIAL_2 = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_VENDAS_TEOREMA", "VENDAS_HORA_INICIAL_2", 0)
        VENDAS_HORA_FINAL_2 = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_VENDAS_TEOREMA", "VENDAS_HORA_FINAL_2", 0)
        VENDAS_DIA_SEMANA = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_VENDAS_TEOREMA", "VENDAS_DIA_SEMANA", 0)

        AMBIENTE = LeArquivoINI("c:\WBPORT.ini", "AMBIENTE", "AMBIENTE", 0)

        'WritePrivateProfileString("HABILITADO_CONVERTER_XML", "ONLINE", CONVERTER_ONLINE, "c:\WBPORT.ini")
        SENHA = LeArquivoINI("c:\WBPORT.ini", "SENHA", "SENHA", 0)
        RZ_CONSOLIDADO = LeArquivoINI("c:\WBPORT.ini", "RZ_CONSOLIDADO", "RZ_CONSOLIDADO", 0)
        ECF_PADRAO = LeArquivoINI("c:\WBPORT.ini", "ECF_PADRAO", "ECF_PADRAO", 0)
        HABILITAR_Z = LeArquivoINI("c:\WBPORT.ini", "HABILITAR_Z", "HABILITAR_Z", 0)

        'Caminho banco NFC-e
        WB_XML_NFCE_BANCO = LeArquivoINI("c:\WBPORT.ini", "WB_NFCE_XML", "WB_XML_NFCE_BANCO", 0)

        'Tipo de Conexao
        TIPO_CONEXAO = LeArquivoINI("c:\WBPORT.ini", "CONEXAO", "TIPO", 0)

        'Dados Conexao Firebird

        ODBC_BANCO = LeArquivoINI("c:\WBPORT.ini", "CONEXAO_ODBC", "BANCO", 0)
        ODBC_SERVIDOR = LeArquivoINI("c:\WBPORT.ini", "CONEXAO_ODBC", "SERVIDOR", 0)
        ODBC_PORTA = LeArquivoINI("c:\WBPORT.ini", "CONEXAO_ODBC", "PORTA", 0)
        ODBC_USUARIO = LeArquivoINI("c:\WBPORT.ini", "CONEXAO_ODBC", "USUARIO", 0)
        ODBC_SENHA = LeArquivoINI("c:\WBPORT.ini", "CONEXAO_ODBC", "SENHA", 0)

        WB_ODBC_BANCO = LeArquivoINI("c:\WBPORT.ini", "WB_CONEXAO_ODBC", "BANCO", 0)
        WB_ODBC_SERVIDOR = LeArquivoINI("c:\WBPORT.ini", "WB_CONEXAO_ODBC", "SERVIDOR", 0)
        WB_ODBC_PORTA = LeArquivoINI("c:\WBPORT.ini", "WB_CONEXAO_ODBC", "PORTA", 0)
        WB_ODBC_USUARIO = LeArquivoINI("c:\WBPORT.ini", "WB_CONEXAO_ODBC", "USUARIO", 0)
        WB_ODBC_SENHA = LeArquivoINI("c:\WBPORT.ini", "WB_CONEXAO_ODBC", "SENHA", 0)


        'DADOS CONEXAO MYSQL
        WB_MYSQL_BANCO = LeArquivoINI("c:\WBPORT.ini", "WB_CONEXAO_MYSQL", "BANCO", 0)
        WB_MYSQL_SERVIDOR = LeArquivoINI("c:\WBPORT.ini", "WB_CONEXAO_MYSQL", "SERVIDOR", 0)
        WB_MYSQL_PORTA = LeArquivoINI("c:\WBPORT.ini", "WB_CONEXAO_MYSQL", "PORTA", 0)
        WB_MYSQL_USUARIO = LeArquivoINI("c:\WBPORT.ini", "WB_CONEXAO_MYSQL", "USUARIO", 0)
        WB_MYSQL_SENHA = LeArquivoINI("c:\WBPORT.ini", "WB_CONEXAO_MYSQL", "SENHA", 0)


        'Variaveis FDB Entrega
        FIREBIRD_SERVIDOR_ENTREGA = LeArquivoINI("c:\WBPORT.ini", "FIREBIRD_SERVIDOR_ENTREGA", "BANCO", 0)

        'Variaveis de configuração de E-mail
        EMAIL_SMTP = LeArquivoINI("c:\WBPORT.ini", "CONFIG_EMAIL", "EMAIL_SMTP", 0)
        EMAIL_PORTA = LeArquivoINI("c:\WBPORT.ini", "CONFIG_EMAIL", "EMAIL_PORTA", 0)
        EMAIL_SEGURANCA = LeArquivoINI("c:\WBPORT.ini", "CONFIG_EMAIL", "EMAIL_SEGURANCA", 0)
        EMAIL_TEMPO_MONITOR = LeArquivoINI("c:\WBPORT.ini", "CONFIG_EMAIL", "EMAIL_TEMPO_MONITOR", 0)
        EMAIL_USUARIO = LeArquivoINI("c:\WBPORT.ini", "CONFIG_EMAIL", "EMAIL_USUARIO", 0)
        EMAIL_REMETENTE = LeArquivoINI("c:\WBPORT.ini", "CONFIG_EMAIL", "EMAIL_REMETENTE", 0)
        EMAIL_ASSUNTO = LeArquivoINI("c:\WBPORT.ini", "CONFIG_EMAIL", "EMAIL_ASSUNTO", 0)
        EMAIL_DESTINATARIO = LeArquivoINI("c:\WBPORT.ini", "CONFIG_EMAIL", "EMAIL_DESTINATARIO", 0)
        EMAIL_SENHA = LeArquivoINI("c:\WBPORT.ini", "CONFIG_EMAIL", "EMAIL_SENHA", 0)

    End Sub

    Sub ESCREVER_INI_IMPORTADOR()
        'Gravar Arquivo ini
        Dim nome_arquivo_ini As String = nomeArquivoINI_IMPORTADOR()

        'IMPORTADOR
        WritePrivateProfileString("ORIGEM_VENDAS", "ORIGEM_VENDAS", ORIGEM_VENDAS, "c:\WBPORT.ini")
        WritePrivateProfileString("ORIGEM_RZ", "ORIGEM_RZ", ORIGEM_RZ, "c:\WBPORT.ini")
        WritePrivateProfileString("DESTINO_VENDAS", "DESTINO_VENDAS", DESTINO_VENDAS, "c:\WBPORT.ini")
        WritePrivateProfileString("DESTINO_SCANNTECH", "DESTINO_SCANNTECH", DESTINO_SCANNTECH, "c:\WBPORT.ini")
        WritePrivateProfileString("DESTINO_RZ", "DESTINO_RZ", DESTINO_RZ, "c:\WBPORT.ini")
        WritePrivateProfileString("DESTINO_NFCE", "DESTINO_NFCE", DESTINO_NFCE, "c:\WBPORT.ini")
        WritePrivateProfileString("DESTINO_ERROS", "DESTINO_ERROS", DESTINO_ERROS, "c:\WBPORT.ini")
        WritePrivateProfileString("EXTENSAO", "EXTENSAO", EXTENSAO, "c:\WBPORT.ini")
        WritePrivateProfileString("TEMPO", "TEMPO", TEMPO_IMPORTADOR, "c:\WBPORT.ini")
        WritePrivateProfileString("DATA_INICIAL", "DATA_INICIAL", DATA_INICIAL, "c:\WBPORT.ini")
        WritePrivateProfileString("DATA_FINAL", "DATA_FINAL", DATA_FINAL, "c:\WBPORT.ini")
        WritePrivateProfileString("HORA_INICIAL", "HORA_INICIAL", HORA_INICIAL, "c:\WBPORT.ini")
        WritePrivateProfileString("HORA_FINAL", "HORA_FINAL", HORA_FINAL, "c:\WBPORT.ini")
        WritePrivateProfileString("NMR_ARQUIVOS", "NMR_ARQUIVOS", NMR_ARQUIVOS, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITADO_IMPORTADOR", "HABILITADO_IMPORTADOR", HABILITADO_IMPORTADOR, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITADO_DATA", " HABILITADO_DATA", HABILITADO_DATA, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITADO_CONVERTER_XML", "HABILITADO_CONVERTER_XML", HABILITADO_CONVERTER_XML, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITADO_CONVERTER_XML", "ONLINE", CONVERTER_ONLINE, "c:\WBPORT.ini")
        WritePrivateProfileString("SENHA", "SENHA", SENHA, "c:\WBPORT.ini")
        WritePrivateProfileString("ECF_PADRAO", "ECF_PADRAO", ECF_PADRAO, "c:\WBPORT.ini")
        WritePrivateProfileString("RZ_CONSOLIDADO", "RZ_CONSOLIDADO", RZ_CONSOLIDADO, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_Z", "HABILITAR_Z", HABILITAR_Z, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_Z_REPETIDA", "HABILITAR_Z_REPETIDA", HABILITAR_Z_REPETIDA, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_Z_EMPORIUM", "HABILITAR_Z_EMPORIUM", HABILITAR_Z_EMPORIUM, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_Z_RECARGA", "HABILITAR_Z_RECARGA", HABILITAR_Z_RECARGA, "c:\WBPORT.ini")
        WritePrivateProfileString("WB_NFCE_XML", "WB_XML_NFCE_BANCO", WB_XML_NFCE_BANCO, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_EMAIL", "HABILITAR_EMAIL", HABILITAR_EMAIL, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_MONITORAMENTO", "HABILITAR_MONITORAMENTO", HABILITAR_MONITORAMENTO, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_IMPORTAR_NFCE", "HABILITAR_IMPORTAR_NFCE", HABILITAR_IMPORTAR_NFCE, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_IMPORTAR_SCANNTECH", "HABILITAR_IMPORTAR_SCANNTECH", HABILITAR_IMPORTAR_SCANNTECH, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_IMPORTAR_ENTREGA", "HABILITAR_IMPORTAR_ENTREGA", HABILITAR_IMPORTAR_ENTREGA, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_NFCE_PROCESSADA", "HABILITAR_NFCE_PROCESSADA", HABILITAR_NFCE_PROCESSADA, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_DIRETORIO_COTABILIDADE", "HABILITAR_DIRETORIO_COTABILIDADE", HABILITAR_DIRETORIO_COTABILIDADE, "c:\WBPORT.ini")
        WritePrivateProfileString("DIRETORIO_COTABILIDADE", "DIRETORIO_COTABILIDADE", DIRETORIO_COTABILIDADE, "c:\WBPORT.ini")
        WritePrivateProfileString("CERTIFICADO_DIGITAL", "CERTIFICADO_DIGITAL", CERTIFICADO_DIGITAL, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_VENDAS_CANCELADAS", "HABILITAR_VENDAS_CANCELADAS", HABILITAR_VENDAS_CANCELADAS, "c:\WBPORT.ini")

        WritePrivateProfileString("HABILITAR_VENDAS_TEOREMA", "HABILITAR_VENDAS_TEOREMA", HABILITAR_VENDAS_TEOREMA, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_VENDAS_TEOREMA", "VENDAS_HORA_INICIAL", VENDAS_HORA_INICIAL, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_VENDAS_TEOREMA", "VENDAS_HORA_FINAL", VENDAS_HORA_FINAL, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_VENDAS_TEOREMA", "VENDAS_HORA_INICIAL_2", VENDAS_HORA_INICIAL_2, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_VENDAS_TEOREMA", "VENDAS_HORA_FINAL_2", VENDAS_HORA_FINAL_2, "c:\WBPORT.ini")
        WritePrivateProfileString("HABILITAR_VENDAS_TEOREMA", "VENDAS_DIA_SEMANA", VENDAS_DIA_SEMANA, "c:\WBPORT.ini")

        WritePrivateProfileString("AMBIENTE", "AMBIENTE", AMBIENTE, "c:\WBPORT.ini")

        'Variaveis Conexão Normal
        WritePrivateProfileString("CONEXAO", "TIPO", TIPO_CONEXAO, "c:\WBPORT.ini")

        'Variaveis Conexão ODBC
        WritePrivateProfileString("CONEXAO_ODBC", "BANCO", ODBC_BANCO, "c:\WBPORT.ini")
        WritePrivateProfileString("CONEXAO_ODBC", "SERVIDOR", ODBC_SERVIDOR, "c:\WBPORT.ini")
        WritePrivateProfileString("CONEXAO_ODBC", "PORTA", ODBC_PORTA, "c:\WBPORT.ini")
        WritePrivateProfileString("CONEXAO_ODBC", "USUARIO", ODBC_USUARIO, "c:\WBPORT.ini")
        WritePrivateProfileString("CONEXAO_ODBC", "SENHA", ODBC_SENHA, "c:\WBPORT.ini")

        WritePrivateProfileString("WB_CONEXAO_ODBC", "BANCO", WB_ODBC_BANCO, "c:\WBPORT.ini")
        WritePrivateProfileString("WB_CONEXAO_ODBC", "SERVIDOR", WB_ODBC_SERVIDOR, "c:\WBPORT.ini")
        WritePrivateProfileString("WB_CONEXAO_ODBC", "PORTA", WB_ODBC_PORTA, "c:\WBPORT.ini")
        WritePrivateProfileString("WB_CONEXAO_ODBC", "USUARIO", WB_ODBC_USUARIO, "c:\WBPORT.ini")
        WritePrivateProfileString("WB_CONEXAO_ODBC", "SENHA", WB_ODBC_SENHA, "c:\WBPORT.ini")

        'Variaveis Mysql
        WritePrivateProfileString("WB_CONEXAO_MYSQL", "BANCO", WB_MYSQL_BANCO, "c:\WBPORT.ini")
        WritePrivateProfileString("WB_CONEXAO_MYSQL", "SERVIDOR", WB_MYSQL_SERVIDOR, "c:\WBPORT.ini")
        WritePrivateProfileString("WB_CONEXAO_MYSQL", "PORTA", WB_MYSQL_PORTA, "c:\WBPORT.ini")
        WritePrivateProfileString("WB_CONEXAO_MYSQL", "USUARIO", WB_MYSQL_USUARIO, "c:\WBPORT.ini")
        WritePrivateProfileString("WB_CONEXAO_MYSQL", "SENHA", WB_MYSQL_SENHA, "c:\WBPORT.ini")

        'Variaveis FDB Entrega
        WritePrivateProfileString("FIREBIRD_SERVIDOR_ENTREGA", "BANCO", FIREBIRD_SERVIDOR_ENTREGA, "c:\WBPORT.ini")

        'Variaveis de configuração de E-mail
        WritePrivateProfileString("CONFIG_EMAIL", "EMAIL_SMTP", EMAIL_SMTP, "c:\WBPORT.ini")
        WritePrivateProfileString("CONFIG_EMAIL", "EMAIL_PORTA", EMAIL_PORTA, "c:\WBPORT.ini")
        WritePrivateProfileString("CONFIG_EMAIL", "EMAIL_SEGURANCA", EMAIL_SEGURANCA, "c:\WBPORT.ini")
        WritePrivateProfileString("CONFIG_EMAIL", "EMAIL_TEMPO_MONITOR", EMAIL_TEMPO_MONITOR, "c:\WBPORT.ini")
        WritePrivateProfileString("CONFIG_EMAIL", "EMAIL_USUARIO", EMAIL_USUARIO, "c:\WBPORT.ini")
        WritePrivateProfileString("CONFIG_EMAIL", "EMAIL_REMETENTE", EMAIL_REMETENTE, "c:\WBPORT.ini")
        WritePrivateProfileString("CONFIG_EMAIL", "EMAIL_ASSUNTO", EMAIL_ASSUNTO, "c:\WBPORT.ini")
        WritePrivateProfileString("CONFIG_EMAIL", "EMAIL_DESTINATARIO", EMAIL_DESTINATARIO, "c:\WBPORT.ini")
        WritePrivateProfileString("CONFIG_EMAIL", "EMAIL_SENHA", EMAIL_SENHA, "c:\WBPORT.ini")

        MsgBox("Configurações Salvas!", MsgBoxStyle.Information)
    End Sub

    ' Retorna o nome do arquivo INI TEOREMA
    Function nomeArquivoINI_TEOREMA() As String
        Dim nome_arquivo_ini_teorema As String = "c:\windows\" 'Application.StartupPath
        Return nome_arquivo_ini_teorema & "TEOREMA.ini"
    End Function

    ' Retorna o nome do arquivo INI IMPORTADOR
    Function nomeArquivoINI_IMPORTADOR() As String
        Dim nome_arquivo_ini As String = "C:\" 'Application.StartupPath
        Return nome_arquivo_ini & "WBPORT.ini"
    End Function

    Public Class MemoryManagement
        Private Declare Function SetProcessWorkingSetSize Lib "kernel32.dll" ( _
        ByVal process As IntPtr, _
        ByVal minimumWorkingSetSize As Integer, _
        ByVal maximumWorkingSetSize As Integer) As Integer

        Public Shared Sub FlushMemory()
            GC.Collect()
            GC.WaitForPendingFinalizers()
            If (Environment.OSVersion.Platform = PlatformID.Win32NT) Then
                SetProcessWorkingSetSize(Process.GetCurrentProcess().Handle, -1, -1)
            End If
        End Sub

    End Class


    'Funcão Criptografar e descriptografar
    Public Class cls_crypto

        Private Shared TripleDES As New TripleDESCryptoServiceProvider
        Private Shared MD5 As New MD5CryptoServiceProvider

        ' Definição da chave de encriptação/desencriptação 
        Private Const key As String = "WBPort2017"

        ''' <summary> 
        ''' Calcula o MD5 Hash  
        ''' </summary> 
        ''' <param name="value">Chave</param> 
        Public Shared Function MD5Hash(ByVal value As String) As Byte()

            ' Converte a chave para um array de bytes  
            Dim byteArray() As Byte = ASCIIEncoding.UTF8.GetBytes(value)
            Return MD5.ComputeHash(byteArray)

        End Function

        ''' <summary> 
        ''' Encripta uma string com base em uma chave 
        ''' </summary> 
        ''' <param name="stringToEncrypt">String a encriptar</param> 
        Public Function Encrypt(ByVal stringToEncrypt As String) As String

            Try

                ' Definição da chave e da cifra (que neste caso é Electronic 
                ' Codebook, ou seja, encriptação individual para cada bloco) 
                TripleDES.Key = cls_crypto.MD5Hash(key)
                TripleDES.Mode = CipherMode.ECB

                ' Converte a string para bytes e encripta 
                Dim Buffer As Byte() = ASCIIEncoding.ASCII.GetBytes(stringToEncrypt)
                Return Convert.ToBase64String _
                    (TripleDES.CreateEncryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))

            Catch ex As Exception

                MessageBox.Show(ex.Message, My.Application.Info.Title, _
                                MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return String.Empty

            End Try

        End Function

        ''' <summary> 
        ''' Desencripta uma string com base em uma chave 
        ''' </summary> 
        ''' <param name="encryptedString">String a decriptar</param> 
        Public Function Decrypt(ByVal encryptedString As String) As String

            Try

                ' Definição da chave e da cifra 

                If encryptedString = String.Empty Then Return String.Empty
                TripleDES.Key = cls_crypto.MD5Hash(key)
                TripleDES.Mode = CipherMode.ECB

                ' Converte a string encriptada para bytes e decripta 
                Dim Buffer As Byte() = Convert.FromBase64String(encryptedString)
                Return ASCIIEncoding.ASCII.GetString _
                    (TripleDES.CreateDecryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))

            Catch ex As Exception

                MessageBox.Show(ex.Message, My.Application.Info.Title,
                                MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return String.Empty

            End Try

        End Function



    End Class
End Module
