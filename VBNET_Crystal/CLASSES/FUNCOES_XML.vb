﻿Imports System.Security.Cryptography
Imports System.Security.Cryptography.Xml
Imports System.Security.Cryptography.X509Certificates
Imports System.IO
Imports System.Xml
Imports System.Threading
Public Class FUNCOES_XML

    ''' <summary>
    ''' Verifica Horario de Verao
    ''' </summary>
    Public Shared Function HORARIO_VERAO() As Boolean
        Dim tz As TimeZone = TimeZone.CurrentTimeZone
        Return tz.IsDaylightSavingTime(DateTime.Now).ToString()
    End Function

    ''' <summary>
    ''' Listar Certificados Na ComboBox
    ''' </summary>
    Public Shared Function LISTAR_CERTIFICADOS(cbcertificado As ComboBox)

        Dim ListaCertificados As New List(Of X509Certificate2)()

        Dim Store As X509Store = New X509Store(StoreName.My, StoreLocation.CurrentUser)
        Store.Open(OpenFlags.ReadOnly Or OpenFlags.OpenExistingOnly)

        Dim _certs As X509Certificate2Collection = Store.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, True).Find(X509FindType.FindByKeyUsage, X509KeyUsageFlags.DigitalSignature, True)

        For Each certificado In _certs

            If certificado.HasPrivateKey Then
                cbcertificado.Items.Add(certificado.Subject)
                certificado.GetExpirationDateString()
                cbcertificado.Sorted = True
            End If
        Next

        Return cbcertificado.Text
    End Function

    ''' <summary>
    ''' Gerar Chave XML NFC-e
    ''' </summary>
    Public Shared Function GerarChaveNfce(cnpj As String, serie As Integer, nfce As Integer, transacao As Integer)
        Dim CHAVE As String
        CHAVE = "41"                                    'UF
        CHAVE += Now.Year.ToString.Substring(2, 2)      'Data
        CHAVE += Format(Now.Month, "00")                'Hora
        CHAVE += cnpj                                   'CNPJ
        CHAVE += "65"                                   'Modelo
        CHAVE += Format(CInt(serie), "000")             'Série
        CHAVE += Format(CInt(nfce), "000000000")        'Número NFC-e
        CHAVE += "1"                                    'Tipo de Envio 1-Normal, 9-Contigência
        CHAVE += Format(CInt(transacao), "00000000")    'Número Qualquer
        Return CHAVE                                    'Retorna Chave Formada sem o dígito Verificador
    End Function

    ''' <summary>
    ''' Gerar Chave XML NFC-e Substituta
    ''' </summary>
    Public Shared Function GerarChaveSubstituta(CHAVE_NFCE As String)
        Dim CHAVE As String
        Dim NUMERO As Integer
        NUMERO = CHAVE_NFCE.ToString.Substring(25, 9) + 1

        CHAVE = CHAVE_NFCE.ToString.Substring(0, 25)

        CHAVE = CHAVE & NUMERO.ToString.PadLeft(9, "0")

        CHAVE = CHAVE & "9"
        CHAVE = CHAVE & CHAVE_NFCE.ToString.Substring(35, 8)
        CHAVE = CHAVE & Calculo_DV11(CHAVE)


        Return CHAVE                                    'Retorna Chave Formada sem o dígito Verificador
    End Function

    Public Shared Function LIMPAR_NFCE(xml As String)

        Dim Processo As String = String.Empty
        Dim xmlRet As New XmlDocument
        xmlRet.Load(xml)
        Try
            Processo = xmlRet.GetElementsByTagName("NFe")(0).OuterXml
        Catch
        End Try
        xmlRet.LoadXml(Processo)

        Dim root As XmlNode = xmlRet.DocumentElement

        'Remover QRCode.
        Try
            root.RemoveChild(root.Item("infNFeSupl"))
        Catch
        End Try

        'Remover Assinatura.
        Try
            root.RemoveChild(root.Item("Signature"))
        Catch
        End Try

        Return root.OuterXml

    End Function

    Public Shared Function SELECIONAR_CERTIFICADO(ByVal xnome As String) As X509Certificate2

        Dim vRetorna As Boolean
        Dim oX509Cert As New X509Certificate2
        Dim store As New X509Store
        Dim collection As New X509Certificate2Collection
        Dim collection1 As New X509Certificate2Collection
        Dim collection2 As New X509Certificate2Collection
        Dim scollection As New X509Certificate2Collection
        Dim msgResultado As String

        oX509Cert = New X509Certificate2()
        store = New X509Store("MY", StoreLocation.CurrentUser)
        store.Open(OpenFlags.ReadOnly Or OpenFlags.OpenExistingOnly)
        collection = store.Certificates
        collection1 = collection.Find(X509FindType.FindByTimeValid, DateTime.Now, False)
        collection2 = collection.Find(X509FindType.FindByKeyUsage, X509KeyUsageFlags.DigitalSignature, False)

        scollection = collection.Find(X509FindType.FindBySubjectDistinguishedName, xnome, False)


        If (scollection.Count = 0) Then

            msgResultado = "Nenhum certificado digital foi selecionado ou o certificado selecionado está com problemas."
            MessageBox.Show(msgResultado, "Advertência", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            vRetorna = False

        Else

            oX509Cert = scollection.Item(0) 'Para selecionar o item em Vb.NET
            vRetorna = True
            Return oX509Cert
        End If


    End Function

    Public Shared Function VALIDA_NFE(ByVal NfeAler As String, ByVal TAG_URI As String, ByVal TAG_INDEX As String, cbCertificado As ComboBox)

        Try

            'Abrir agora o arquivo XML criado e inserir o Protocolo encontrado!!
            Dim xDoc As New XmlDocument : xDoc.Load(NfeAler)

            'Assinar o XML
            Dim OAd As New AssinaturaDigitalClass()
            Dim pDadosNfe As String
            Dim vStringNfe As String

            OAd.Assinar(NfeAler, TAG_URI, SELECIONAR_CERTIFICADO(cbCertificado.Text))

            If OAd.vResultado = 0 Then 'Assinado corretamente

                pDadosNfe = OAd.vXMLStringAssinado
                vStringNfe = pDadosNfe.Substring(pDadosNfe.IndexOf(TAG_INDEX), pDadosNfe.Length - pDadosNfe.IndexOf(TAG_INDEX))

            Else 'Ocorreu algum erro na assinatura
                MessageBox.Show(OAd.vResultadoString, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Catch ex As Exception

            MsgBox(ex.Message)
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Calcula DV11
    ''' </summary>
    Public Shared Function Calculo_DV11(ByVal strNumero As String) As String
        'Calculo modulo 11
        Dim I As Integer : Dim IntCont As Integer : Dim Vlr As Integer
        Dim Resto As Integer
        IntCont = 2
        Vlr = 0
        For I = Len(strNumero) To 1 Step -1
            Vlr = Vlr + (Val(Mid(strNumero, I, 1) * IntCont))
            IntCont = IIf(IntCont >= 9, 2, IntCont + 1)
        Next
        Resto = Vlr Mod 11
        Select Case Resto
            Case 0
                Resto = 0
            Case 1
                Resto = 0
            Case Is > 1
                Resto = Str(Val(11 - Resto))
        End Select
        Calculo_DV11 = Resto
    End Function

    ''' <summary>
    ''' Transforma em HEXADECIMAL
    ''' </summary>
    Public Shared Function StringToHex(ByVal text As String) As String
        Dim hex As String = String.Empty
        For i As Integer = 0 To text.Length - 1
            hex &= Asc(text.Substring(i, 1)).ToString("x").ToUpper
        Next
        Return hex
    End Function

    'Função HEX to String
    Public Shared Function HexToString(ByVal hex As String) As String
        Dim text As New System.Text.StringBuilder(hex.Length \ 2)
        For i As Integer = 0 To hex.Length - 2 Step 2
            text.Append(Chr(Convert.ToByte(hex.Substring(i, 2), 16)))
        Next
        Return text.ToString
    End Function

    ''' <summary>
    ''' Criptografa Informações do QRCode
    ''' </summary>
    Public Shared Function EncryptSHA(ByVal password As String) As String
        Dim sha As New SHA1CryptoServiceProvider ' declare sha as a new SHA1CryptoServiceProvider
        Dim bytesToHash() As Byte ' and here is a byte variable

        bytesToHash = System.Text.Encoding.ASCII.GetBytes(password) ' covert the password into ASCII code

        bytesToHash = sha.ComputeHash(bytesToHash) ' this is where the magic starts and the encryption begins

        Dim encPassword As String = ""

        For Each b As Byte In bytesToHash
            encPassword += b.ToString("x2")
        Next

        Return encPassword ' boom there goes the encrypted password!

    End Function

    Public Shared Function GerarQRCode200(ByVal xml As String, ByVal ID_TOKEN As String, ByVal CSC As String)

        '========================== FIM Verificar XML ======================
        Dim CHAVE, AMB, DATA_EMISSAO, VALOR_NOTA,
            DIGEST, DIGEST_HEX, VERSAO_QRCODE, HASH_PARCIAL As String


        Dim Ds As New DataSet
        Ds.ReadXml(xml)

        Dim bdsAux As New BindingSource
        bdsAux.DataSource = Ds

        bdsAux.DataMember = "infNFe"
        'Coletar Valor da Chave
        CHAVE = (Replace((bdsAux.Current("Id").ToString()), "NFe", ""))


        bdsAux.DataMember = "ide"
        'Coletar Ambiente e data de Emissão
        AMB = bdsAux.Current("tpAmb").ToString()
        DATA_EMISSAO = bdsAux.Current("dhEmi").ToString()


        'Coletar Valor da Nota e do ICMS
        bdsAux.DataMember = "ICMSTot"
        VALOR_NOTA = bdsAux.Current("vNF").ToString()

        bdsAux.DataMember = "Reference"
        DIGEST = bdsAux.Current("DigestValue").ToString()

        bdsAux.Dispose()
        Ds.Dispose()
        '========================== FIM Verificar XML ======================


        '=======================INCICIO CRIAR QRCODE ======================

        DIGEST_HEX = LCase(StringToHex(DIGEST))

        VERSAO_QRCODE = "2"

        Dim URLNFCe As String = String.Empty


        If CHAVE.Substring(34, 1) = 1 Then

            'GERAR HASH QRCode 2.0 Emissão Normal
            URLNFCe = "http://www.fazenda.pr.gov.br/nfce/qrcode?p="
            HASH_PARCIAL = UCase(EncryptSHA(CHAVE & "|" & VERSAO_QRCODE & "|" & AMB & "|" & CInt(ID_TOKEN) & CSC))
            URLNFCe += CHAVE & "|" & VERSAO_QRCODE & "|" & AMB & "|" & CInt(ID_TOKEN) & "|" & HASH_PARCIAL

        ElseIf CHAVE.Substring(34, 1) = 9 Then

            Clipboard.SetText(CHAVE & "|" & VERSAO_QRCODE & "|" & AMB & "|" & DATA_EMISSAO.Substring(8, 2) & "|" & VALOR_NOTA & "|" & DIGEST_HEX & "|" & CInt(ID_TOKEN) & CSC)

            'GERAR HASH QRCode 2.0 Emissão Contigência
            URLNFCe = "http://www.fazenda.pr.gov.br/nfce/qrcode?p="
            HASH_PARCIAL = UCase(EncryptSHA(CHAVE & "|" & VERSAO_QRCODE & "|" & AMB & "|" & DATA_EMISSAO.Substring(8, 2) & "|" & VALOR_NOTA & "|" & DIGEST_HEX & "|" & CInt(ID_TOKEN) & CSC))
            URLNFCe += CHAVE & "|" & VERSAO_QRCODE & "|" & AMB & "|" & DATA_EMISSAO.Substring(8, 2) & "|" & VALOR_NOTA & "|" & DIGEST_HEX & "|" & CInt(ID_TOKEN) & "|" & HASH_PARCIAL

        End If


        '========================== FIM CRIAR QRCODE ======================
        Return URLNFCe
    End Function


    'Concerter em HEXADECIMAL para BASE64
    Public Shared Function ConvertFromStringToHex(ByVal inputHex As String) As Byte()
        Dim resultantArray As Byte() = New Byte(inputHex.Length / 2 - 1) {}

        For i As Integer = 0 To resultantArray.Length - 1
            resultantArray(i) = Convert.ToByte(inputHex.Substring(i * 2, 2), 16)
        Next

        Return resultantArray
    End Function

    'Criar HashCSRT Base64
    Public Shared Function hashCSRT(ByVal CSRT As String, CHAVE As String)

        '=======================INCICIO CRIAR hashCSRT ======================
        Dim hashCSRT_SHA As String

        'Aplica SHA1 na CSRT e Chave
        hashCSRT_SHA = EncryptSHA(CSRT & CHAVE)

        'Converter para Hexadecimal
        Dim hexstr As String = hashCSRT_SHA
        Dim data As Byte() = ConvertFromStringToHex(hexstr)

        'Converte para Base64
        Dim base64 As String = Convert.ToBase64String(data)
        Return base64

        '========================== FIM CRIAR hashCSRT ======================

    End Function


    ''' <summary>
    ''' Incluir QRCode no XML
    ''' </summary>
    Public Shared Function IncluirQRCode(ByVal xml As String, ByVal QRCode As String, Destino As String, Nome_arquivo As String)

        Dim fluxoTexto As IO.StreamWriter

        Dim xmlGerado As String
        Dim xmlTmp As XmlDocument = New XmlDocument
        xmlTmp.Load(xml)
        xmlGerado = Replace(xmlTmp.OuterXml, "</infNFe><Signature", "</infNFe><infNFeSupl><qrCode><![CDATA[" & QRCode & "]]></qrCode><urlChave>http://www.fazenda.pr.gov.br</urlChave></infNFeSupl><Signature")
        Dim xmlOK As New XmlDocument
        fluxoTexto = New IO.StreamWriter(Destino & Nome_arquivo & ".xml", False)
        fluxoTexto.Write(xmlGerado)
        fluxoTexto.Close()


        Return True
    End Function

    ''' <summary>
    ''' Criar XML Cancelamento
    ''' </summary>
    Public Shared Function XML_CANCELAMENTO(ByVal CHAVE As String, ByVal UF As String, ByVal AMBIENTE As String,
                              ByVal SEQ_EVENTO As String,
                              ByVal PROTOCOLO As String, ByVal JUSTIFICATIVA As String)
        Try

            If Directory.Exists(".\ENVIO") = False Then
                Directory.CreateDirectory(".\ENVIO")
            End If

            Dim writer As New XmlTextWriter(".\ENVIO/NFCeEvent.xml", System.Text.Encoding.UTF8)

            'inicia o documento xml
            writer.WriteStartDocument()

            'escreve o elmento raiz Evento
INI_EVENTO: writer.WriteStartElement("envEvento")       'Inicio envEvento
            writer.WriteAttributeString("xmlns", "http://www.portalfiscal.inf.br/nfe")
            writer.WriteAttributeString("versao", "1.00")

            '==================INICIO DADOS ================================
            'ESCREVE IdLote
            writer.WriteElementString("idLote", 1)

            'Escreve Evento
            writer.WriteStartElement("evento")          'inicio Evento
            writer.WriteAttributeString("versao", "1.00")

            writer.WriteStartElement("infEvento")       'Inicio infEvento
            writer.WriteAttributeString("Id", "ID110111" & CHAVE & "01")
            writer.WriteElementString("cOrgao", UF)
            writer.WriteElementString("tpAmb", AMBIENTE)
            writer.WriteElementString("CNPJ", CHAVE.ToString.Substring(6, 14))
            writer.WriteElementString("chNFe", CHAVE)

            Dim DATA_EVENTO As String = "" '2018-03-12T13:27:17-03:00

            DATA_EVENTO = Format(Now.Year, "0000")
            DATA_EVENTO += "-" & Format(Now.Month, "00")
            DATA_EVENTO += "-" & Format(Now.Day, "00")
            DATA_EVENTO += "T"
            DATA_EVENTO += Format(Now, "HH:mm:ss")

            If HORARIO_VERAO() = True Then
                DATA_EVENTO += "-02:00"
            Else
                DATA_EVENTO += "-03:00"
            End If


            writer.WriteElementString("dhEvento", DATA_EVENTO)

            writer.WriteElementString("tpEvento", "110111")
            writer.WriteElementString("nSeqEvento", SEQ_EVENTO)
            writer.WriteElementString("verEvento", "1.00")

            writer.WriteStartElement("detEvento")       'Inicio detEvento
            writer.WriteAttributeString("versao", "1.00")
            writer.WriteElementString("descEvento", "Cancelamento")
            writer.WriteElementString("nProt", PROTOCOLO)
            writer.WriteElementString("xJust", JUSTIFICATIVA)
            writer.WriteEndElement()                    'Fim detEvento

            writer.WriteEndElement()                    'Fim infEvento

            writer.WriteEndElement()                    'Fim Evento
            '==================FIM PASTAS ===================================

            'encerra o elemento raiz Evento
FIM_EVENTO: writer.WriteEndElement()                    'Fim envEvento


            'Escreve o XML para o arquivo e fecha o objeto escritor
            writer.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return True
    End Function

    ''' <summary>
    ''' Criar XML Cancelamento Substituicao
    ''' </summary>
    Public Shared Function XML_CANCELAMENTO_SUBSTITUICAO(ByVal CHAVE As String, ByVal UF As String, ByVal AMBIENTE As String,
                              ByVal SEQ_EVENTO As String,
                              ByVal PROTOCOLO As String, ByVal JUSTIFICATIVA As String,
                              ByVal CHAVE_REFERENCIADA As String, ByVal VERSAO_APLICATIVO As String)
        Try

            If Directory.Exists(".\ENVIO") = False Then
                Directory.CreateDirectory(".\ENVIO")
            End If

            Dim writer As New XmlTextWriter(".\ENVIO/NFCeEvent.xml", System.Text.Encoding.UTF8)

            'inicia o documento xml
            writer.WriteStartDocument()

            'escreve o elmento raiz Evento
INI_EVENTO: writer.WriteStartElement("envEvento")       'Inicio envEvento
            writer.WriteAttributeString("xmlns", "http://www.portalfiscal.inf.br/nfe")
            writer.WriteAttributeString("versao", "1.00")

            '==================INICIO DADOS ================================
            'ESCREVE IdLote
            writer.WriteElementString("idLote", 1)

            'Escreve Evento
            writer.WriteStartElement("evento")          'inicio Evento
            writer.WriteAttributeString("xmlns", "http://www.portalfiscal.inf.br/nfe")
            writer.WriteAttributeString("versao", "1.00")

            writer.WriteStartElement("infEvento")       'Inicio infEvento
            writer.WriteAttributeString("Id", "ID110112" & CHAVE & "01")
            writer.WriteElementString("cOrgao", UF)
            writer.WriteElementString("tpAmb", AMBIENTE)
            writer.WriteElementString("CNPJ", CHAVE.ToString.Substring(6, 14))
            writer.WriteElementString("chNFe", CHAVE)

            Dim DATA_EVENTO As String = "" '2018-03-12T13:27:17-03:00

            DATA_EVENTO = Format(Now.Year, "0000")
            DATA_EVENTO += "-" & Format(Now.Month, "00")
            DATA_EVENTO += "-" & Format(Now.Day, "00")
            DATA_EVENTO += "T"
            DATA_EVENTO += Format(Now, "HH:mm:ss")

            If HORARIO_VERAO() = True Then
                DATA_EVENTO += "-02:00"
            Else
                DATA_EVENTO += "-03:00"
            End If


            writer.WriteElementString("dhEvento", DATA_EVENTO)

            writer.WriteElementString("tpEvento", "110112")
            writer.WriteElementString("nSeqEvento", SEQ_EVENTO)
            writer.WriteElementString("verEvento", "1.00")

            writer.WriteStartElement("detEvento")       'Inicio detEvento
            writer.WriteAttributeString("versao", "1.00")
            writer.WriteElementString("descEvento", "Cancelamento por substituicao")
            writer.WriteElementString("cOrgaoAutor", "41")
            writer.WriteElementString("tpAutor", "1")
            writer.WriteElementString("verAplic", VERSAO_APLICATIVO)
            writer.WriteElementString("nProt", PROTOCOLO)
            writer.WriteElementString("xJust", JUSTIFICATIVA)
            writer.WriteElementString("chNFeRef", CHAVE_REFERENCIADA)

            writer.WriteEndElement()                    'Fim detEvento

            writer.WriteEndElement()                    'Fim infEvento

            writer.WriteEndElement()                    'Fim Evento
            '==================FIM PASTAS ===================================

            'encerra o elemento raiz Evento
FIM_EVENTO: writer.WriteEndElement()                    'Fim envEvento


            'Escreve o XML para o arquivo e fecha o objeto escritor
            writer.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return True
    End Function


    ''' <summary>
    ''' Gerar XML de CAncelamento
    ''' </summary>
    Public Shared Function ASSINAR_XML_CANCELAMENTO()
        Try
            Dim t1 As String = ""
            't1.Text = "<?xml version=""1.0"" encoding=""utf-8""?><envEvento xmlns=""http://www.portalfiscal.inf.br/nfe"" versao=""1.00""><idLote>@lote</idLote><evento versao=""1.00"">@EVENTO</evento></envEvento>"

            Dim xml As XmlDocument = New XmlDocument


            xml.Load(".\ENVIO/NFCeEvent.xml")
            Dim root As XmlNode = xml.DocumentElement

            t1 = Replace(Replace(root.OuterXml, "</infEvento></evento>", "</infEvento>"), "</Signature></envEvento>", "</Signature></evento></envEvento>")
            't1.Text = Replace(t1.Text, "@lote", 1)
            't1.Text = Replace(t1.Text, "@EVENTO", root.ChildNodes(0).OuterXml & root.ChildNodes(1).OuterXml)


            Dim xml_2 As New XmlDocument
            xml_2.LoadXml(t1)
            xml_2.Save(".\ENVIO/NFCeEvent.xml")
            Return True
        Catch

            MsgBox(Err.Description)
            Return False
        End Try

    End Function

    ''' <summary>
    ''' Gerar XML de Distribuição 4.00
    ''' </summary>
    Public Shared Function ProcNfce_400(xml_envio As String, protocolo As String)

        Dim Processo As String
        Processo = "<?xml version=""1.0"" encoding=""utf-8""?><nfeProc xmlns=""http://www.portalfiscal.inf.br/nfe"" versao=""4.00"">@nfce@protocolo</nfeProc>"

        Try
            Dim xmlRet As New XmlDocument
            xmlRet.Load(xml_envio)
            Processo = Replace(Processo, "@nfce", xmlRet.GetElementsByTagName("NFe")(0).OuterXml)


            Dim xmlRetProt As New XmlDocument
            xmlRetProt.LoadXml(protocolo)
            Processo = Replace(Processo, "@protocolo", xmlRetProt.GetElementsByTagName("protNFe")(0).OuterXml)

            Return Processo

            'Verirficar se Existe Diretório Autorizadas
            'If Directory.Exists("./AUTORIZADAS") = False Then
            'Directory.CreateDirectory("./AUTORIZADAS")
            'End If

            'Dim fluxoTexto = New IO.StreamWriter("./AUTORIZADAS\NFCe" & frmPrincipal.txChave.Text & "_" & frmPrincipal.txChave.Text.Substring(25, 9) & ".xml", False)
            'fluxoTexto.Write(Processo)
            'fluxoTexto.Close()

            'File.Delete("./ENVIO\NFCeEnvio.xml")
            'File.Delete("./ENVIO\RetEnv.xml")

        Catch
            'MsgBox(Err.Description)
        End Try
        'PATH_XML = "./AUTORIZADAS\NFCe" & frmPrincipal.txChave.Text & "_" & frmPrincipal.txChave.Text.Substring(25, 9) & ".xml"
        Return True

    End Function


    Public Shared Function RetornoEvento(ByVal chave As String, cbcertificado As ComboBox)

        Dim xmlRet As New XmlDocument
        Try
            xmlRet.LoadXml(WEBSERVICES_400.Homologacao.NFCeConsultaChave(chave, cbcertificado))
            xmlRet.Save("./ENVIO\RetEvent.xml")

            'MsgBox(xmlRet.GetElementsByTagName("xMotivo").Count)
            'frmCancelamento.txMsgStatus.Text = xmlRet.GetElementsByTagName("xMotivo")(2).InnerText
            'frmCancelamento.txNumProtocolo.Text = (xmlRet.GetElementsByTagName("nProt")(1).InnerText)
            'frmCancelamento.txProtocoloCancelamento.Text = (xmlRet.GetElementsByTagName("nProt")(2).InnerText)
            Return True
        Catch
            'MsgBox(frmCancelamento.txStatus.Text = xmlRet.GetElementsByTagName("xMotivo")(0).InnerText)
            MsgBox(Err.Description)
            Return False
        End Try


    End Function

    ''' <summary>
    ''' MODIFICAR XML NFC-e
    ''' </summary>
    Public Shared Function MODIFICAR_XML_CONTIGENCIA(xml As String, tipo_emissao As String)
        'Altear Chave para Contigencia

        Dim chave_contigencia As String
        chave_contigencia = ""


        Dim xEle As XElement = XElement.Load("./NFCe.xml")
        Dim x As String = "{http://www.portalfiscal.inf.br/nfe}"

        Dim fone = xEle.Elements.ToList()
        For Each pEle As XElement In fone
            'Pegar Chave
            chave_contigencia = pEle.Attribute("Id").Value
        Next pEle

        Dim chave_nova As String

        'MsgBox(chave_contigencia)

        chave_nova = Replace(chave_contigencia, "NFe", "")

        chave_nova = (chave_nova.Substring(0, 34) & "9" & chave_nova.Substring(35, 8) & Calculo_DV11(chave_nova.Substring(0, 34) & "9" & chave_nova.Substring(35, 8)))

        'Clipboard.SetText(chave_nova)

        'chave_contigencia += Calculo_DV11(chave_contigencia)

        CHAVE_NFCE = chave_nova

        For Each pEle As XElement In fone
            'Alterar Atributo
            pEle.SetAttributeValue("Id", "NFe" & chave_nova)
        Next pEle

        Dim ID As XElement = xEle.Element(x + "infNFe").Element(x + "ide")
        Dim FUSO_HORARIO As String = ""

        If HORARIO_VERAO() = True Then
            FUSO_HORARIO = "-02:00"
        Else
            FUSO_HORARIO = "-03:00"
        End If


        With ID

            ID.SetElementValue(x + "tpEmis", tipo_emissao)
            'ID.SetElementValue(x + "tpAmb", ambiente)
            ID.SetElementValue(x + "cDV", chave_nova.Substring(43, 1))
            ID.SetElementValue(x + "dhEmi", Now.Year & "-" & Format(Now.Month, "00") & "-" & Format(Now.Day, "00") _
                                   & "T" & Format(Now.Hour, "00") & ":" & Format(Now.Minute, "00") & ":" & Format(Now.Second, "00") & FUSO_HORARIO)
            Try
                ID.Element(x + "dhCont").Remove()
                ID.Element(x + "xJust").Remove()
            Catch
            End Try

        End With

        Try
            ID.Element(x + "dhCont").Remove()
            ID.Element(x + "xJust").Remove()
        Catch

        End Try

        If SEFAZ_AMBIENTE = 2 Then
            xEle.Element(x + "infNFe").Element(x + "det").Element(x + "prod").SetElementValue(x + "xProd", "NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL")
        End If

        If tipo_emissao = 9 Then
            Dim CONT As XElement = xEle.Element(x + "infNFe")

            With CONT
                .Element(x + "ide").Add(New XElement(x + "dhCont", Now.Year & "-" & Format(Now.Month, "00") & "-" & Format(Now.Day, "00") _
                                   & "T" & Format(Now.Hour, "00") & ":" & Format(Now.Minute, "00") & ":" & Format(Now.Second + 10, "00") & FUSO_HORARIO))
                .Element(x + "ide").Add(New XElement(x + "xJust", "SEM CONEXAO SEFAZ"))
            End With
        End If

        xEle.Save("./NFCe.xml")

        Dim fluxoTexto As IO.StreamWriter
        Dim xmlGerado As String
        Dim xmlTmp As XmlDocument = New XmlDocument
        xmlTmp.Load("./NFCe.xml")
        'xmlGerado = Replace(xmlTmp.OuterXml, "</emit>", "</emit><dest><CPF>91251206034</CPF><xNome>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome><indIEDest>9</indIEDest></dest>").Replace("</infNFe>", PAGAMENTO & "</infNFe>")
        xmlGerado = Replace(xmlTmp.OuterXml, "<vBCSTRet>0.00</vBCSTRet>", "<vBCSTRet>0.00</vBCSTRet><pST>0.00</pST>")
        fluxoTexto = New IO.StreamWriter("./NFCe.xml", False)
        fluxoTexto.Write(xmlGerado)
        fluxoTexto.Close()

        Return True

    End Function

    Public Shared Function ProcEventNfce()

        Dim Processo As String

        Try
            Dim xmlRetEvent As New XmlDocument
            xmlRetEvent.Load("./ENVIO\RetEvent.xml")
            Processo = xmlRetEvent.GetElementsByTagName("procEventoNFe")(0).OuterXml

            'frmCancelamento.txRetorno.Text = Processo

            'Dim fluxoTexto = New IO.StreamWriter("c:\teste\VALIDADAS\NFCe" & frmPrincipal.txChave.Text & "_" & frmPrincipal.txChave.Text.Substring(25, 9) & "_Even.xml", False)

            If Directory.Exists("./CANCELADAS") = False Then
                Directory.CreateDirectory("./CANCELADAS")
            End If

            'Dim fluxoTexto = New IO.StreamWriter("./CANCELADAS\NFCe" & frmCancelamento.txChave.Text & "_" & frmCancelamento.txChave.Text.Substring(25, 9) & "_Even.xml", False)
            'fluxoTexto.Write(Processo)
            'fluxoTexto.Close()

            'Excluir Arquivos Temporários
            File.Delete("./ENVIO\NFCeEvent.xml")
            File.Delete("./ENVIO\RetEvent.xml")
        Catch
            MsgBox(Err.Description)
        End Try

        Return True
    End Function

    Public Shared Function ConsultaCaptcha(chave As String, captcha As String, txchave As TextBox)
        txchave.Text = ""
        txchave.Text = chave

        Return captcha
    End Function

    ''' <summary>
    ''' Gravar XML Cancelamento
    ''' </summary>
    Public Shared Function Gravar_Cancelamento(ByVal xml As String, Destino As String, Nome_arquivo As String)
        Try
            Dim fluxoTexto As IO.StreamWriter

            fluxoTexto = New IO.StreamWriter(Destino & Nome_arquivo & ".xml", False)
            fluxoTexto.Write(xml)
            fluxoTexto.Close()

            Return True
        Catch

        End Try

        Return False

    End Function


    Public Shared Function ValidarCancelamento(xml As String)
        Try
            Dim xmlRet As New XmlDocument
            xmlRet.LoadXml(xml)

            With xmlRet
                Dim retorno As String = ""
                Dim protocolo As String = ""
                Dim status As String = ""

                Select Case .GetElementsByTagName("cStat").Count
                    Case 1
                        retorno = 0
                    Case 2
                        retorno = 1
                    Case 3
                        retorno = 2
                End Select

                Select Case .GetElementsByTagName("cStat")(retorno).InnerText
                    Case 100
                        status = "100"
                        protocolo = .GetElementsByTagName("nProt")(0).InnerText
                    Case 150
                        status = "150"
                        protocolo = .GetElementsByTagName("nProt")(0).InnerText
                    Case 135
                        status = "135"
                        protocolo = .GetElementsByTagName("nProt")(1).InnerText
                    Case Else
                        status = .GetElementsByTagName("cStat")(retorno).InnerText
                        protocolo = .GetElementsByTagName("xMotivo")(retorno).InnerText
                End Select

                Return status & "|" & protocolo

            End With
        Catch
            Return "|"
        End Try
        Return "|"
    End Function

End Class


