﻿Imports System.Net
Imports System.Xml
Public Class WEBSERVICES_400
    Public Class Homologacao

        ''' <summary>
        ''' Autorizar NFC-e Homologação
        ''' </summary>
        Public Shared Function NFCeAutorizacao(ByVal Caminho_Xml As String, cbcertificado As ComboBox)
            Dim icRetorno As String = String.Empty
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode

            ServicePointManager.CertificatePolicy = New MyPolicy()
            Try

                'Consulta AUTORIZAÇÃO
                Dim wsAut As New H_NFeAutorizacao4.NFeAutorizacao4
                wsAut.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

                'Xml Autorização
                xmlDados.LoadXml(Caminho_Xml)
                xmlResultado = wsAut.nfeAutorizacaoLote(xmlDados)

                Return xmlResultado.OuterXml

            Catch
                Return (Err.Description)
            End Try

        End Function

        ''' <summary>
        ''' Consulta Chave NFC-e Homologação
        ''' </summary>
        Public Shared Function NFCeConsultaChave(ByVal chave As String, cbcertificado As ComboBox) As String

            Dim icRetorno As String = String.Empty
            'Dim objCertificado As X509Certificate2
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode

            ServicePointManager.CertificatePolicy = New MyPolicy()
            Try

                'Xml Consulta Pela Chave
                xmlDados.LoadXml("<?xml version=""1.0"" encoding=""UTF-8"" ?><consSitNFe versao=""4.00"" xmlns=""http://www.portalfiscal.inf.br/nfe""><tpAmb>2</tpAmb><xServ>CONSULTAR</xServ><chNFe>" & chave & "</chNFe></consSitNFe>")

                'Consulta CHAVE
                Dim wsCons As New H_NFeConsultaProtocolo4.NFeConsultaProtocolo4
                wsCons.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

                xmlResultado = wsCons.nfeConsultaNF(xmlDados)
                Return (xmlResultado.OuterXml)

            Catch
                ' frmPrincipal.TextBox1.Text = Err.Description
                Return False
            End Try

        End Function

        ''' <summary>
        ''' Cancelamento NFC-e Homologação
        ''' </summary>
        Public Shared Function EventoNfce(ByVal Caminho_Xml As String, cbcertificado As ComboBox)
            Dim icRetorno As String = String.Empty
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode
            ServicePointManager.CertificatePolicy = New MyPolicy()

            Try

                'Consulta Inutilizacao
                Dim wsEvent As New H_NFeRecepcaoEvento4.NFeRecepcaoEvento4
                wsEvent.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

                'Xml Inutilizar
                xmlDados.Load(Caminho_Xml)
                xmlResultado = wsEvent.nfeRecepcaoEventoNF(xmlDados)
                Return (xmlResultado.OuterXml)

            Catch
                Return Err.Description
            End Try

        End Function

        ''' <summary>
        ''' Consulta de Protocolo Homolocação
        ''' </summary>
        Public Shared Function Consulta_Recibo(ByVal Numero_Retorno As String, cbcertificado As ComboBox) As String

            Dim icRetorno As String = String.Empty
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode

            ServicePointManager.CertificatePolicy = New MyPolicy()

            'Consulta Retorno Autorizacao
            Dim wsRetAut As New H_NFeRetAutorizacao4.NFeRetAutorizacao4
            wsRetAut.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

            'Xml Consulta Pela Autorização
            xmlDados.LoadXml("<?xml version=""1.0"" encoding=""UTF-8"" ?><consReciNFe versao=""4.00"" xmlns=""http://www.portalfiscal.inf.br/nfe""><tpAmb>2</tpAmb><nRec>" & Numero_Retorno & "</nRec></consReciNFe>")
            xmlResultado = wsRetAut.nfeRetAutorizacaoLote(xmlDados)

            Return xmlResultado.OuterXml

        End Function

        ''' <summary>
        ''' Consulta Status Homolocação
        ''' </summary>
        Public Shared Function ConsultaStatus(cbcertificado As ComboBox)

            'Consulta Status do Serviço
            Dim icRetorno As String = String.Empty
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode
            ServicePointManager.CertificatePolicy = New MyPolicy()

            Try
                'Consulta STATUS
                Dim wsMsg As New H_NFeStatusServico4.NFeStatusServico4
                wsMsg.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

                'Xml Status do Serviço
                xmlDados.LoadXml("<?xml version=""1.0"" encoding=""utf-8""?><consStatServ xmlns=""http://www.portalfiscal.inf.br/nfe"" versao=""4.00""><tpAmb>2</tpAmb><cUF>41</cUF><xServ>STATUS</xServ></consStatServ>")
                xmlResultado = wsMsg.nfeStatusServicoNF(xmlDados)
                Return xmlResultado.OuterXml
            Catch
                Return Err.Description
            End Try

        End Function

        ''' <summary>
        ''' Inutilizar Numeração Homologacao
        ''' </summary>
        Public Shared Function InutilizarNfce(ByVal Caminho_Xml As String, cbcertificado As ComboBox)

            'Envio de Numeração para Inutilização
            Dim icRetorno As String = String.Empty
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode
            ServicePointManager.CertificatePolicy = New MyPolicy()

            Try

                'Consulta Inutilizacao
                Dim wsInut As New H_NFeInutilizacao4.NFeInutilizacao4
                wsInut.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

                'Xml Inutilizar
                xmlDados.Load(Caminho_Xml)

                xmlResultado = wsInut.nfeInutilizacaoNF(xmlDados)
                Return xmlResultado.OuterXml

            Catch
                Return Err.Description
            End Try

        End Function

    End Class

    Public Class Producao
        ''' <summary>
        ''' Autorizar NFC-e Produção
        ''' </summary>
        Public Shared Function NFCeAutorizacao(ByVal xml As String, cbcertificado As ComboBox)
            Dim icRetorno As String = String.Empty
            'Dim objCertificado As X509Certificate2
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode

            ServicePointManager.CertificatePolicy = New MyPolicy()
            Try

                'Consulta AUTORIZAÇÃO
                Dim wsAut As New P_NFeAutorizacao4.NFeAutorizacao4
                wsAut.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

                'Xml Autorização
                xmlDados.LoadXml(xml)
                xmlResultado = wsAut.nfeAutorizacaoLote(xmlDados)

                Return xmlResultado.OuterXml
            Catch
                MsgBox(Err.Description)
                Return Err.Description
            End Try

        End Function

        ''' <summary>
        ''' Consulta Chave NFC-e Produção
        ''' </summary>
        Public Shared Function NFCeConsultaChave(ByVal chave As String, cbcertificado As ComboBox) As String

            Dim icRetorno As String = String.Empty
            'Dim objCertificado As X509Certificate2
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode

            ServicePointManager.CertificatePolicy = New MyPolicy()

            Try

                'Xml Consulta Pela Chave
                xmlDados.LoadXml("<?xml version=""1.0"" encoding=""UTF-8"" ?><consSitNFe versao=""4.00"" xmlns=""http://www.portalfiscal.inf.br/nfe""><tpAmb>1</tpAmb><xServ>CONSULTAR</xServ><chNFe>" & chave & "</chNFe></consSitNFe>")

                'Consulta CHAVE
                Dim wsCons As New P_NFeConsultaProtocolo4.NFeConsultaProtocolo4
                wsCons.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

                xmlResultado = wsCons.nfeConsultaNF(xmlDados)
                Return (xmlResultado.OuterXml)

            Catch
                MsgBox(Err.Description)
                'frmPrincipal.TextBox1.Text = Err.Description
                Return False
            End Try

        End Function

        ''' <summary>
        ''' Cancelamento NFC-e Produção
        ''' </summary>
        Public Shared Function EventoNfce(ByVal Caminho_Xml As String, cbcertificado As ComboBox)
            Dim icRetorno As String = String.Empty
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode

            ServicePointManager.CertificatePolicy = New MyPolicy()

            Try

                'Consulta Inutilizacao
                Dim wsEvent As New P_NFeRecepcaoEvento4.NFeRecepcaoEvento4
                wsEvent.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

                'Xml Inutilizar
                xmlDados.Load(Caminho_Xml)

                xmlResultado = wsEvent.nfeRecepcaoEventoNF(xmlDados)
                Return (xmlResultado.OuterXml)

            Catch
                Return Err.Description
            End Try

        End Function

        ''' <summary>
        ''' Consulta de Protocolo Produção
        ''' </summary>
        Public Shared Function Consulta_Recibo(ByVal Numero_Retorno As String, cbcertificado As ComboBox) As String

            Dim icRetorno As String = String.Empty
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode
            ServicePointManager.CertificatePolicy = New MyPolicy()

            'Consulta Retorno Autorizacao
            Dim wsRetAut As New P_NFeRetAutorizacao4.NFeRetAutorizacao4
            wsRetAut.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

            'Xml Consulta Pela Autorização
            xmlDados.LoadXml("<?xml version=""1.0"" encoding=""UTF-8"" ?><consReciNFe versao=""4.00"" xmlns=""http://www.portalfiscal.inf.br/nfe""><tpAmb>1</tpAmb><nRec>" & Numero_Retorno & "</nRec></consReciNFe>")
            xmlResultado = wsRetAut.nfeRetAutorizacaoLote(xmlDados)

            Return xmlResultado.OuterXml

        End Function

        ''' <summary>
        ''' Consulta Status Produção
        ''' </summary>
        Public Shared Function ConsultaStatus(cbcertificado As ComboBox)

            'Consulta Status do Serviço
            Dim icRetorno As String = String.Empty
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode
            ServicePointManager.CertificatePolicy = New MyPolicy()

            Try
                'Consulta STATUS
                Dim wsMsg As New P_NFeStatusServico4.NFeStatusServico4
                wsMsg.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

                'Xml Status do Serviço
                xmlDados.LoadXml("<?xml version=""1.0"" encoding=""utf-8""?><consStatServ xmlns=""http://www.portalfiscal.inf.br/nfe"" versao=""4.00""><tpAmb>1</tpAmb><cUF>41</cUF><xServ>STATUS</xServ></consStatServ>")
                xmlResultado = wsMsg.nfeStatusServicoNF(xmlDados)
                Return xmlResultado.OuterXml
            Catch
                Return Err.Description
            End Try

        End Function

        ''' <summary>
        ''' Inutilizar Numeração Produção
        ''' </summary>
        Public Shared Function InutilizarNfce(ByVal Caminho_Xml As String, cbcertificado As ComboBox)

            'Envio de Numeração para Inutilização
            Dim icRetorno As String = String.Empty
            Dim xmlDados As New XmlDocument
            Dim xmlRetorno_STR As String = String.Empty

            Dim xmlResultado As XmlNode
            ServicePointManager.CertificatePolicy = New MyPolicy()

            Try

                'Consulta Inutilizacao
                Dim wsInut As New P_NFeInutilizacao4.NFeInutilizacao4
                wsInut.ClientCertificates.Add(FUNCOES_XML.SELECIONAR_CERTIFICADO(cbcertificado.Text))

                'Xml Inutilizar
                xmlDados.Load(Caminho_Xml)

                xmlResultado = wsInut.nfeInutilizacaoNF(xmlDados)
                Return xmlResultado.OuterXml

            Catch
                Return Err.Description
            End Try

        End Function

    End Class

End Class
