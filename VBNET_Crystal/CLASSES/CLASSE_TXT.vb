﻿Imports System.IO
Imports System.Text
Imports System.Web
Imports System.Xml
Imports System.Text.RegularExpressions
Imports System.Xml.XPath

Public Class CLASSE_TXT

    'Verificar Arquivo em Uso
    'Protected Overridable Function IsFileLocked(ByVal file As FileInfo) As Boolean
    Public Shared Function ArquivoEmUso(ByVal file As FileInfo) As Boolean
        Dim stream As FileStream = Nothing

        Try
            stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None)
        Catch __unusedIOException1__ As IOException
            Return True
        Finally
            If stream IsNot Nothing Then stream.Close()
        End Try

        Return False
    End Function

    Public Shared Function LER_SALE(xml As String, xml_old As String, erro As String)
        Dim oFileInfo As New FileInfo(xml)

        If ArquivoEmUso(oFileInfo) = True Then
            Return False
        End If

        erro = erro & System.IO.Path.GetFileName(xml_old)
        Dim fs As FileStream = New FileStream(xml, FileMode.Open)
        Dim encoding__1 = Encoding.GetEncoding("ISO-8859-1")
        'Dim encoding__1 = Encoding.GetEncoding(1252)
        Dim sr = New StreamReader(fs, encoding__1)
        Dim bytes = encoding__1.GetBytes(System.Web.HttpUtility.HtmlDecode(sr.ReadToEnd()))
        Dim ms = New MemoryStream(bytes)
        Dim ds = New DataSet()


        Try

            'Ler Xml
            ds.ReadXml(ms)

            Dim dtab As DataTable = ds.Tables("ANSWER")
            Dim dview As New DataView(dtab)

            'Filtrar pelo ID 191
            dview.RowFilter = "ID='191'"

            If dview.Count > 1 Then
                sr.Dispose()
                ds.Dispose()
                ms.Dispose()
                fs.Dispose()
                CONTINUAR = True
                Return True
            Else
                sr.Dispose()
                ds.Dispose()
                ms.Dispose()
                fs.Dispose()
                CONTINUAR = True
                Return False
            End If
        Catch
            sr.Dispose()
            ds.Dispose()
            ms.Dispose()
            fs.Dispose()
            'MsgBox(Err.Description)
            Try
                File.Move(xml_old, erro)
            Catch
                File.Delete(erro)
                File.Move(xml_old, erro)
            End Try
            CONTINUAR = False
            Application.DoEvents()
        End Try

    End Function

    Public Shared Function Verificar_XML(xml As String)
        Dim fluxoTexto As IO.StreamReader
        Dim linhaTexto As String
        Dim xml_modificado As String = String.Empty
        Dim contador As Integer = 0


        fluxoTexto = New IO.StreamReader(xml)
        linhaTexto = fluxoTexto.ReadLine

        While linhaTexto <> Nothing
            'If linhaTexto Like "*<ANSWER><ID>191</ID><LABEL>*" Then
            If linhaTexto Like "*<ANSWER><ID>191</ID>*" Then
                If contador > 0 Then
                    'Pula linha
                    'fluxoTexto.ReadLine()
                Else
                    contador += 1
                    xml_modificado += linhaTexto & vbCrLf
                End If

                'xml_modificado += linhaTexto & vbCrLf
            Else
                xml_modificado += linhaTexto & vbCrLf
            End If
            linhaTexto = fluxoTexto.ReadLine()
        End While
        fluxoTexto.Close()

        Return xml_modificado

    End Function

    Public Shared Function ABRIR_TXT(xml As String)
        Try
            Dim fluxoTexto As IO.StreamReader
            Dim Texto As String
            fluxoTexto = New IO.StreamReader(xml)
            Texto = fluxoTexto.ReadToEnd

            fluxoTexto.Close()

            Return Texto
        Catch
            Return False
        End Try
        Return False

    End Function

    Public Shared Function Verificar_CARACTER_ESPECIAL(xml As String)

        Dim oFileInfo As New FileInfo(xml)

        If ArquivoEmUso(oFileInfo) = True Then
            Return False
        End If

        Dim fluxoTexto As IO.StreamReader
        Dim linhaTexto As String = String.Empty
        Dim xml_modificado As String = String.Empty
        Dim contador As Integer = 0
        Dim GRAVAR As IO.StreamWriter
        Try

            GRAVAR = New IO.StreamWriter("c:\teste\SALE_TMP.xml", False)

            fluxoTexto = New IO.StreamReader(xml)
            linhaTexto = fluxoTexto.ReadLine

            If linhaTexto = "False" Then
                Return False
            End If

            While linhaTexto <> Nothing
                linhaTexto = LIMPAR(linhaTexto)
                linhaTexto = CarRemove(linhaTexto, "&#;")

                If linhaTexto Like "*<DATA>CmdData=*" Then
                    linhaTexto = "    <DATA>0</DATA>"
                    'linhaTexto = fluxoTexto.ReadLine()
                    GRAVAR.WriteLine(linhaTexto)
                    'xml_modificado += linhaTexto
                    'ElseIf linhaTexto Like "*<DATA>Msg=*" Then

                    ' Clipboard.SetText(linhaTexto)
                    'linhaTexto = "    <DATA>0</DATA>"
                    'linhaTexto = fluxoTexto.ReadLine()
                    'GRAVAR.WriteLine(linhaTexto)
                    'xml_modificado += linhaTexto
                ElseIf linhaTexto Like "*<DATA>Data=*" Then
                    linhaTexto = "    <DATA>0</DATA>"
                    'linhaTexto = fluxoTexto.ReadLine()
                    GRAVAR.WriteLine(linhaTexto)
                    'xml_modificado += linhaTexto
                ElseIf linhaTexto Like "*<COMMENT>http:*" Then
                    linhaTexto = Replace(Replace(linhaTexto, "<COMMENT>http:", "<COMMENT><![CDATA["), "</COMMENT>", "]]></COMMENT>")
                    'linhaTexto = fluxoTexto.ReadLine()
                    GRAVAR.WriteLine(linhaTexto)
                    'xml_modificado += linhaTexto
                Else
                    GRAVAR.WriteLine(linhaTexto)
                    'xml_modificado += linhaTexto
                End If
                linhaTexto = LIMPAR(fluxoTexto.ReadLine())
            End While
            fluxoTexto.Close()

            '        GRAVAR.Write(linhaTexto)
            GRAVAR.Close()
            'Return Regex.Replace(Regex.Replace(xml_modificado, "(\r\n)+", ""), " +", " ").Replace("> <", "><")

            Return True
        Catch
            Return False
        End Try

    End Function

    Public Shared Function Gravar_XML(xml As String, caminho As String)
        Dim oFileInfo As New FileInfo(caminho)

        If ArquivoEmUso(oFileInfo) = True Then
            Return False
        End If

        Dim fluxoTexto As IO.StreamWriter
        Try
            fluxoTexto = New IO.StreamWriter(caminho, False)
            fluxoTexto.Write(xml)
            fluxoTexto.Close()

            Return True
        Catch
            Return False
        End Try


    End Function

    Public Shared Function LIMPAR(texto As String)
        texto = Replace(texto, "&Aacute;", "")
        texto = Replace(texto, "&aacute;", "")
        texto = Replace(texto, "&Acirc;", "")
        texto = Replace(texto, "&acirc;", "")
        texto = Replace(texto, "&Agrave;", "")
        texto = Replace(texto, "&agrave;", "")
        texto = Replace(texto, "&Aring;", "")
        texto = Replace(texto, "&aring;", "")
        texto = Replace(texto, "&Atilde;", "")
        texto = Replace(texto, "&atilde;", "")
        texto = Replace(texto, "&Auml;", "")
        texto = Replace(texto, "&auml;", "")
        texto = Replace(texto, "&AElig;", "")
        texto = Replace(texto, "&aelig;", "")
        texto = Replace(texto, "&Eacute;", "")
        texto = Replace(texto, "&eacute;", "")
        texto = Replace(texto, "&Ecirc;", "")
        texto = Replace(texto, "&ecirc;", "")
        texto = Replace(texto, "&Egrave;", "")
        texto = Replace(texto, "&egrave;", "")
        texto = Replace(texto, "&Euml;", "")
        texto = Replace(texto, "&euml;", "")
        texto = Replace(texto, "&ETH;", "")
        texto = Replace(texto, "&eth;", "")
        texto = Replace(texto, "&Iacute;", "")
        texto = Replace(texto, "&iacute;", "")
        texto = Replace(texto, "&Icirc;", "")
        texto = Replace(texto, "&icirc;", "")
        texto = Replace(texto, "&Igrave;", "")
        texto = Replace(texto, "&igrave;", "")
        texto = Replace(texto, "&Iuml;", "")
        texto = Replace(texto, "&iuml;", "")
        texto = Replace(texto, "&Oacute;", "")
        texto = Replace(texto, "&oacute;", "")
        texto = Replace(texto, "&Ocirc;", "")
        texto = Replace(texto, "&ocirc;", "")
        texto = Replace(texto, "&Ograve;", "")
        texto = Replace(texto, "&ograve;", "")
        texto = Replace(texto, "&Oslash;", "")
        texto = Replace(texto, "&oslash;", "")
        texto = Replace(texto, "&Otilde;", "")
        texto = Replace(texto, "&otilde;", "")
        texto = Replace(texto, "&Ouml;", "")
        texto = Replace(texto, "&ouml;", "")
        texto = Replace(texto, "&Uacute;", "")
        texto = Replace(texto, "&uacute;", "")
        texto = Replace(texto, "&Ucirc;", "")
        texto = Replace(texto, "&ucirc;", "")
        texto = Replace(texto, "&Ugrave;", "")
        texto = Replace(texto, "&ugrave;", "")
        texto = Replace(texto, "&Uuml;", "")
        texto = Replace(texto, "&uuml;", "")
        texto = Replace(texto, "&Ccedil;", "")
        texto = Replace(texto, "&ccedil;", "")
        texto = Replace(texto, "&Ntilde;", "")
        texto = Replace(texto, "&ntilde;", "")
        texto = Replace(texto, "&lt;", "")
        texto = Replace(texto, "&gt;", "")
        texto = Replace(texto, "&amp;", "")
        texto = Replace(texto, "&quot;", "")
        texto = Replace(texto, "&reg;", "")
        texto = Replace(texto, "&copy;", "")
        texto = Replace(texto, "&Yacute;", "")
        texto = Replace(texto, "&yacute;", "")
        texto = Replace(texto, "&THORN;", "")
        texto = Replace(texto, "&thorn;", "")
        texto = Replace(texto, "&szlig;", "")
        Dim i As Integer = 0
        For i = 1 To 410
            texto = Replace(texto, "&#" & i & ";", "")
        Next

        Return texto
    End Function


    Public Shared Function CarRemove(Texto As String, Caracteres As String) As String
        Dim Ic As Integer, It As Integer, Inicio As Integer, Pos As Integer, Caracter As String

        Try
            For Ic = 1 To Len(Caracteres)
                Caracter = Mid(Caracteres, Ic, 1)
                Pos = 1
                Inicio = 1
                If InStr(Inicio, Texto, Caracter) > 0 Then
                    For It = 1 To Len(Texto)
                        Pos = InStr(Inicio, Texto, Caracter)
                        CarRemove = Mid(Texto, 1, Pos - 1) & Mid(Texto, Pos + 1)
                        Inicio = Pos
                    Next It
                    Texto = CarRemove
                    Ic = Ic - 1
                Else
                    CarRemove = Texto
                End If
            Next Ic
            Return Texto
        Catch

        End Try

        Return False
    End Function


    Public Shared Function VERIFICAR_RZ(loja As String, pdv As String, data As String, conexao As String)
        Dim contador As Boolean = 0
        Try
            Dim sql As String
            TABELA = "MAPA_FISCAL_ECF"
            sql = "select count(*) from mapa_fiscal_ecf mfc
               where mfc.mapa_data =  '" & data & "'
               and mfc.empresa_codigo ='" & loja & "'
               and mfc.mapa_numero_caixa = '" & pdv & "'"
            If conexao = "N" Then
                CONEXAO_FIREBIRD(sql)
                contador = (fs.Tables(TABELA).Rows(0)("COUNT").ToString())
            ElseIf conexao = "O" Then
                CONEXAO_ODBC(sql)
                contador = (Ods.Tables(TABELA).Rows(0)("COUNT").ToString())
            End If

            Return contador
        Catch
            'MsgBox(Err.Description)
        End Try
        Return contador
    End Function

    Public Shared Function ATUALIZAR_RZ(loja As String, pdv As String, data As String, valor As String, conexao As String)
        Try
            Dim sql As String
            TABELA = "MAPA_FISCAL_ECF"

            sql = "update mapa_fiscal_ecf ms Set ms.mapa_total_isentos = ms.mapa_total_isentos + " & valor & ",
            MS.mapa_total_movimento = MS.mapa_total_movimento + " & valor & "
            where MS.empresa_codigo = '" & loja & "'
            And MS.mapa_numero_caixa = '" & pdv & "'
            And MS.mapa_data ='" & data & "'"

            If conexao = "N" Then
                CONEXAO_FIREBIRD(sql)

            ElseIf conexao = "O" Then
                CONEXAO_ODBC(sql)

            End If

            Return True
        Catch
            'MsgBox(Err.Description)
        End Try
        Return False

    End Function
    Public Shared Function LER_RECARGA(xml As String)
        Dim xmlRecarga As New XmlDocument
        Dim retorno As String = False
        Try
            With xmlRecarga
                .Load(xml)
                Dim data As String = String.Empty
                data = .GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(0, 4)
                data += "." & .GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(4, 2)
                data += "." & .GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(6, 2)

                retorno = .GetElementsByTagName("STORE")(0).InnerText.PadLeft(4, "0")
                retorno += "|" & .GetElementsByTagName("POS")(0).InnerText.PadLeft(4, "0")
                retorno += "|" & data
                retorno += "|" & .GetElementsByTagName("AMOUNT_DUE")(0).InnerText

            End With
            Return retorno
        Catch ex As Exception

        End Try

        Return False

    End Function

    Public Shared Function VALIDAR_NO_XML(xml As String, tag As String)
        Dim doc As XmlDocument = New XmlDocument
        doc.Load(xml)
        Dim No As XmlNodeList = doc.GetElementsByTagName(tag)
        If (No.Count > 0) Then
            Return True
        End If

        Return False
    End Function

    Public Shared Function VALIDAR_OPERACAO(xml As String, Codigo As String)
        Dim doc As XmlDocument = New XmlDocument
        Try
            doc.Load(xml)
            Dim No As XmlNodeList = doc.GetElementsByTagName("HEAD")
            If (No.Count > 0) Then
                If No.Item(0).ChildNodes(0).InnerText = Codigo Then
                    Return True
                End If
            End If
        Catch
            Return False
        End Try
    End Function

    Public Shared Function VALIDAR_QTDE_MODO_XML(xml As String)
        Dim xmlDoc As New XmlDocument
        xmlDoc.Load(xml)

        Dim No As XmlNodeList = xmlDoc.GetElementsByTagName("ITEM")
        If (No.Count > 0) Then
            Dim alteracao As String = "N"
            For Each nos As XmlElement In No
                If nos.SelectSingleNode("QTY").InnerText = 0 Then
                    nos.SelectSingleNode("QTY").InnerText = "0.174"
                    alteracao = "S"
                End If
            Next
            If alteracao = "S" Then
                Dim xmlWriter As XmlTextWriter = New XmlTextWriter(xml, Nothing)
                xmlDoc.PreserveWhitespace = True
                xmlDoc.Save(xmlWriter)
                xmlWriter = Nothing
                No = Nothing
                xmlDoc = Nothing
            End If
        End If

    End Function

    Public Shared Function VALIDAR_QTDE_MODO_TEXTO(xml As String)
        Dim fluxoTexto As IO.StreamReader
        Dim linhaTexto As String
        Dim xml_modificado As String = String.Empty
        Try
            fluxoTexto = New IO.StreamReader(xml)
            linhaTexto = fluxoTexto.ReadLine
            Dim alterado As String = "N"

            While linhaTexto <> Nothing
                If linhaTexto Like "*<QTY>0</QTY>*" Then
                    xml_modificado += linhaTexto.Replace("<QTY>0</QTY>", "<QTY>1</QTY>") & vbCrLf
                    alterado = "S"
                Else
                    xml_modificado += linhaTexto & vbCrLf
                End If
                linhaTexto = fluxoTexto.ReadLine()
            End While
            fluxoTexto.Close()

            If alterado = "S" Then
                If Gravar_XML(xml_modificado, xml) = True Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    Public Shared Function VALIDAR_SUBTOTAL_DUPLICADO(xml As String)
        Dim fluxoTexto As IO.StreamReader
        Dim linhaTexto As String
        Dim xml_modificado As String = String.Empty
        Try
            'Verificar Quantidade de Subtotal
            Dim oXML As New XmlDocument
            Dim oNoLista As XmlNodeList
            Dim contador_subtotal As Integer = 0
            oXML.Load(xml)

            oNoLista = oXML.SelectNodes("/SALE/SUBTOTAL")
            contador_subtotal = oNoLista.Count

            oNoLista = Nothing
            oXML = Nothing

            'Retirar Subtotal
            fluxoTexto = New IO.StreamReader(xml)
            linhaTexto = fluxoTexto.ReadLine
            Dim alterado As String = "N"
            Dim i As Integer = 1

            While linhaTexto <> Nothing
                If linhaTexto Like "*<SUBTOTAL>*</SUBTOTAL><SUBTOTAL_SEC>*</SUBTOTAL_SEC><INTEREST>*</INTEREST><DUE>*</DUE>*" Then
                    If i < contador_subtotal Then
                        linhaTexto = fluxoTexto.ReadLine()
                        alterado = "S"
                        i += 1
                        Continue While
                    Else
                        xml_modificado += linhaTexto & vbCrLf
                    End If
                Else
                    xml_modificado += linhaTexto & vbCrLf
                End If
                linhaTexto = fluxoTexto.ReadLine()
            End While
            fluxoTexto.Close()

            If alterado = "S" Then
                If Gravar_XML(xml_modificado, xml) = True Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
        Return False

    End Function



End Class
