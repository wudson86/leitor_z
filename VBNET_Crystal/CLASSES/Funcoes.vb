﻿Imports System.Xml
Imports System.Net

Public Class Funcoes

    Public Shared Function lerXml(ByVal xml As String, ByVal lst As ListBox, ByVal banco As String)

        Try
            Dim Ds As New DataSet
            Dim FLAG, LOJA, PDV, DATA, OPERADOR, VALOR, TICKET, CANCELADO As String

            Ds.ReadXml(xml)
            FLAG = String.Empty

            FLAG = Ds.Tables(0).Rows(0)("DELIVERY_CMD")
            LOJA = Ds.Tables(0).Rows(0)("STORE")
            PDV = Ds.Tables(0).Rows(0)("POS")

            'Montar Data e Hora
            DATA = Ds.Tables(0).Rows(0)("FISCAL_TIME").ToString.Substring(0, 4) & "/"
            DATA += Ds.Tables(0).Rows(0)("FISCAL_TIME").ToString.Substring(4, 2) & "/"
            DATA += Ds.Tables(0).Rows(0)("FISCAL_TIME").ToString.Substring(6, 2) & " "
            DATA += Ds.Tables(0).Rows(0)("FISCAL_TIME").ToString.Substring(8, 2) & ":"
            DATA += Ds.Tables(0).Rows(0)("FISCAL_TIME").ToString.Substring(10, 2) & ":"
            DATA += Ds.Tables(0).Rows(0)("FISCAL_TIME").ToString.Substring(12, 2)

            OPERADOR = Ds.Tables(0).Rows(0)("CASHIER_NAME")
            VALOR = Ds.Tables(0).Rows(0)("AMOUNT_DUE")
            TICKET = Ds.Tables(0).Rows(0)("TICKET")
            Try
                CANCELADO = Ds.Tables(0).Rows(0)("VOIDED")
            Catch
                CANCELADO = 0
            End Try

            If FLAG <> "" Then
                Dim SQL As String

                TABELA = "ENTREGA"

                SQL = "INSERT INTO ENTREGA" _
                      & "(ENTREGA_LOJA" _
                      & ",ENTREGA_PDV" _
                      & ",ENTREGA_DATA" _
                      & ",ENTREGA_VALOR" _
                      & ",ENTREGA_TICKET" _
                      & ",ENTREGA_OPERADOR" _
                      & ",ENTREGA_CANCELADO)" _
                                    & "VALUES (" _
                      & "'" & LOJA & "'" _
                      & ",'" & PDV & "'" _
                      & ",'" & Replace(DATA, "/", ".") & "'" _
                      & ",'" & Replace(VALOR, ",", ".") & "'" _
                      & ",'" & TICKET & "'" _
                      & ",'" & OPERADOR & "'" _
                      & ",'" & CANCELADO & "')"

                CONEXAO_FIREBIRD_ENTREGA(SQL)
                Ds.Dispose()
            End If

            Return True

        Catch ex As Exception
            'lst.Items.Add("Erro" & ex.ToString)
            Return False
        End Try



    End Function

    Public Shared Function ENVIAR_XML_POST(url_post As String, xml_origem As String)
        Dim xml As New System.Xml.XmlDocument()
        xml.Load(xml_origem)
        Dim url = url_post
        Dim client As New WebClient
        Dim sentXml As Byte() = System.Text.Encoding.ASCII.GetBytes("")

        Try
            client.Headers.Add("Content-Type", "application/xml")
            client.Headers.Add("Custom: API_Method")
            sentXml = System.Text.Encoding.ASCII.GetBytes(xml.OuterXml)
        Catch

        End Try

        Return System.Text.Encoding.ASCII.GetString(client.UploadData(url, "POST", sentXml))

    End Function

    Public Shared Function GRAVAR_LOG(caminho As String, texto As String)

        Try
            Dim file As System.IO.StreamWriter
            file = My.Computer.FileSystem.OpenTextFileWriter(caminho, True)
            file.WriteLine(texto & vbCrLf)
            file.Close()
            Return True
        Catch

        End Try

        Return False

    End Function

    Public Shared Function Localizar_Cancelado(cupom As String)
        Dim dados As String
        Try
            Dim xml As New XmlDocument
            Dim oNoLista As XmlNodeList
            Dim oNo As XmlNode

            xml.Load(cupom)

            'Pegar Loja
            oNoLista = xml.SelectNodes("/SALE_VOID/STORE")

            For Each oNo In oNoLista
                dados = (oNo.ChildNodes.Item(0).InnerText)
                Exit For
            Next

            'Pegar PDV
            oNoLista = xml.SelectNodes("/SALE_VOID/POS")

            For Each oNo In oNoLista
                dados += "|" & (oNo.ChildNodes.Item(0).InnerText)
                Exit For
            Next

            'PEGAR DATA
            oNoLista = xml.SelectNodes("/SALE_VOID/FISCAL_DAY")

            For Each oNo In oNoLista
                dados += "|" & (oNo.ChildNodes.Item(0).InnerText)
                Exit For
            Next

            'PEGAR TIPO DE NOTA
            oNoLista = xml.SelectNodes("/SALE_VOID/SALE_TYPE")

            For Each oNo In oNoLista
                dados += "|" & (oNo.ChildNodes.Item(0).InnerText)
                Exit For
            Next

            'PEGAR NOTA CANCELADA
            oNoLista = xml.SelectNodes("/SALE_VOID/VOIDED_TICKET")

            For Each oNo In oNoLista
                dados += "|" & (oNo.ChildNodes.Item(0).InnerText)
                Exit For
            Next

            Return dados
        Catch ex As Exception

        End Try

        Return False

    End Function

    Public Shared Function RENOMEAR_XML(xml As String)
        Dim DATA As String = String.Empty
        Dim HORA As String = String.Empty
        Dim NUMERO As String = String.Empty


        Dim oxml As New XmlDocument
        oxml.Load(xml)
        DATA = oxml.GetElementsByTagName("STORE")(0).InnerText.PadLeft(4, "0") & "-"
        DATA += oxml.GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(6, 2) & "-"
        DATA += oxml.GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(4, 2) & "-"
        DATA += oxml.GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(0, 4)

        HORA = oxml.GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(8, 2) & ";"
        HORA += oxml.GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(10, 2) & ";"
        HORA += oxml.GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(12, 2)

        NUMERO = oxml.GetElementsByTagName("TICKET")(0).InnerText.PadLeft(8, "0")

        oxml = Nothing

        Return DATA & "_" & HORA & "_" & NUMERO & ".xml"



    End Function

    Public Shared Function RZ_EMPORIUM_CABECALHO_XML(arquivos As String)
        Try
            Dim dadosReducao As String = String.Empty
            Dim oXml As New XmlDocument
            oXml.Load(arquivos)
            dadosReducao = oXml.GetElementsByTagName("STORE")(0).InnerText & "|"
            dadosReducao += oXml.GetElementsByTagName("POS")(0).InnerText & "|"

            dadosReducao += oXml.GetElementsByTagName("FISCAL_DAY")(0).InnerText.Substring(0, 4) & "-"
            dadosReducao += oXml.GetElementsByTagName("FISCAL_DAY")(0).InnerText.Substring(4, 2) & "-"
            dadosReducao += oXml.GetElementsByTagName("FISCAL_DAY")(0).InnerText.Substring(6, 2) & "|"

            dadosReducao += oXml.GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(8, 2) & ":"
            dadosReducao += oXml.GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(10, 2) & ":"
            dadosReducao += oXml.GetElementsByTagName("FISCAL_TIME")(0).InnerText.Substring(12, 2) & "|"

            dadosReducao += oXml.GetElementsByTagName("INITIAL_TICKET")(0).InnerText & "|"
            dadosReducao += oXml.GetElementsByTagName("TICKET")(0).InnerText & "|"
            Return dadosReducao
        Catch
            'MsgBox(Err.Description)
            Return False
        End Try

    End Function

    Public Shared Function pegarValorRz(loja As String, pdv As String, dia As String)
        Dim SQL_Z As String = String.Empty

        Try
            TABELA = "fiscal_accum_item"

            SQL_Z = "SELECT SUM(amount) as 'Valor'
                FROM fiscal_accum_item fi
                WHERE fi.store_key =" & loja & "
                and fi.fiscal_date >= '" & dia & "'
                and fi.fiscal_date <= '" & dia & "'
                and fi.ecf_number = " & pdv & "
                GROUP BY fi.store_key, fi.ecf_number
                ORDER by fi.store_key, fi.ecf_number"

            CONEXAO_MySQL(SQL_Z)

            Return myds.Tables(0).Rows(0)("valor")
        Catch
            Return False
        End Try

    End Function

    Public Shared Function VALIDAR_DATA_HORA_CUPOM(dtini As DateTime, dtfim As DateTime)
        'TimeSpan
        Dim dif As TimeSpan = dtfim.Subtract(dtini)
        'TextBox1.Text = dif.TotalSeconds.ToString + "  Segundos" + vbCrLf
        'TextBox1.Text += dif.TotalMinutes.ToString + "  Minutos " + vbCrLf
        If dif.TotalMinutes.ToString >= 0 Then
            Return True
        End If
        'TextBox1.Text += dif.TotalHours.ToString + "  Horas " + vbCrLf
        'TextBox1.Text += dif.TotalDays.ToString + "  Dias " + vbCrLf

        'DateDiff
        'TextBox1.Text = DateDiff(DateInterval.Second, dtini, dtfim).ToString + "  Segundos" + vbCrLf
        'If DateDiff(DateInterval.Minute, dtini, dtfim).ToString >= 0 Then
        'Return True
        'End If
        'TextBox1.Text += DateDiff(DateInterval.Hour, dtini, dtfim).ToString + " Horas" + vbCrLf
        'TextBox1.Text += DateDiff(DateInterval.Day, dtini, dtfim).ToString + "  Dias" + vbCrLf
        'TextBox1.Text += DateDiff(DateInterval.Quarter, dtini, dtfim).ToString + "  Trimestres" + vbCrLf
        'TextBox1.Text += DateDiff(DateInterval.Month, dtini, dtfim).ToString + "  Meses" + vbCrLf
        Return False
    End Function

    Public Shared Function CLASSIFICAR_ARQUIVO(Xml As String)
        Try
            Dim texto As String = String.Empty
            'Tabela de Retornos
            '1-SALE - Pricipal
            '2-SALE_VOID - Principal
            '3-CUPOM CANCELADO - Secundario SALE
            '4-REDUCAO Z - Principal
            '5-DEVOLUCAO - Principal
            '6-NOTA FISCAL - Secundadio SALE
            '7-NFCe - Principal - Principal
            '8-Evento NFCe - Principal

            texto = CLASSE_TXT.ABRIR_TXT(Xml)

            If texto Like "*<SALE>*" Then
                If texto Like "*<SALE>*" Then

                ElseIf texto Like "*<SALE>*" Then

                Else
                    Return 1
                End If
            ElseIf texto Like "*<SALE_VOID>*" Then
                    Return 2
                ElseIf texto Like "*<Z_REPORT>*" Then
                    Return 4
                ElseIf texto Like "*<RETURN>*" Then
                    Return 5
                ElseIf texto Like "*<infNFe*" Then
                    Return 7
                ElseIf texto Like "*<infEvento*" Then
                    Return 8
            End If

        Catch ex As Exception
            MsgBox(Err.Description)
        End Try
        Return 0
    End Function


End Class
