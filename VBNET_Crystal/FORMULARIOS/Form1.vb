﻿Imports System.Xml
Imports System.IO
Imports Microsoft.VisualBasic.FileIO
Imports FirebirdSql.Data.FirebirdClient
Imports System.Net
Imports System.Data.Odbc
Imports System.Data.OleDb
Imports System.Net.Mail
Imports System.Security.Cryptography
Imports System.Web
Imports System.Globalization
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Security.Cryptography.X509Certificates
Imports System.Windows
Imports System.Runtime.InteropServices
Imports System.Xml.XPath
Imports System.Threading

Public Class Form1

    Inherits Form

    Private appVendas As Process
    Private appVendasInfos As ProcessStartInfo
    Shared timeOut As Boolean = False
    Private initThread As Boolean = True
    Shared timer As System.Timers.Timer
    Public threadTimer As Thread
    Private countTrys As Integer = 0
    Private maxTrys As Integer = 1
    Shared timeOutTimer As Integer = 5000
    'Private path As String = "C://teorema//bin//vendas.exe"
    Private cliente As String = "Cheiro da terra"
    Private controler As Controller
    Private EMAIL_ENVIADO As Boolean = False



    Dim CUPOM As String = ""
    Dim ARQUIVO As String = ""
    Dim TOTALIZADOR As Double = 0
    Dim TOTAL_IMPOSTO As Double = 0
    Dim Resetar_Tag_Protocolo As String = ""
    Public CaminhoArquivo As String = ""
    Dim MES As String = ""
    Dim EMPRESA As String = ""
    Dim Contador_pasta As Long = 0
    'Verificar Cupons Cancelados no SEFAZ
    Function Ler(arquivo As String)
        Dim CHAVE As String = ""
        Dim TIPO_CANCELAMENTO As String = ""
        'Dividir o texto em linhas
        Dim Linha() As String = Strings.Split(My.Computer.FileSystem.ReadAllText(arquivo, System.Text.Encoding.UTF8), ControlChars.Lf)

        If Linha.Length - 1 = 1 Then
            Dim texto As String
            texto = Replace(Linha(0), "><", ">" & Chr(13) & "<")
            Linha = Strings.Split(texto, ControlChars.CrLf)
        End If

        'Por cada linha
        For i As Integer = 0 To Linha.Length - 1

            'Se não estiver vazia...
            If Linha(i) <> "" Then

                If Linha(i) Like "*INVOICE_NAME*" Then
                    CHAVE = Linha(i).Substring(Linha(i).IndexOf(">") + 1, 44)
                ElseIf Linha(i) Like "*VOIDED*" Then
                    TIPO_CANCELAMENTO = 1
                End If

            End If

        Next
        'Libertar Memória, visto que já não precisamos destas variáveis
        Linha = Nothing

        Return TIPO_CANCELAMENTO + "|" + CHAVE

    End Function

    Function VENDA_CANCELADA_OBRIGATORIA(xml As String)

        Dim oXml As New XmlDocument

        oXml.Load(xml)

        If oXml.GetElementsByTagName("INVOICE_NAME").Count = 0 Then
            oXml = Nothing
            Return False
        End If
        oXml = Nothing
        Return True

    End Function

    'Funcao desativada
    Function DESTIVADA_CANCELAR(arquivo As String)
        Dim chave() As String
        Dim status() As String

        If chbNFCeCancelada.Checked = True Then
            chave = (Ler(arquivo).ToString.Split("|"))

            If chave(0) <> "" Then
                Select Case cbAmbiente.Text.Substring(0, 1)
                    Case 1
                        status = (FUNCOES_XML.ValidarCancelamento(WEBSERVICES_400.Producao.NFCeConsultaChave(Replace(chave(1), vbLf, ""), cbCertificado)).ToString.Split("|"))
                    Case 2
                        status = (FUNCOES_XML.ValidarCancelamento(WEBSERVICES_400.Homologacao.NFCeConsultaChave(Replace(chave(1), vbLf, ""), cbCertificado)).ToString.Split("|"))
                End Select
            Else
                Return False
            End If
        End If

        'Envio do cancelamento
        Dim UF As String = "41"
        Dim VERSAO As String = "2.9.1"
        Dim ChaveReferenciada As String = ""
        Dim verificacao() As String


        Select Case status(0)
            Case 100
                'Cancelamento Normal
                FUNCOES_XML.XML_CANCELAMENTO(chave(1), UF, cbAmbiente.Text.Substring(0, 1), 1, status(1), "SEM CONEXAO COM O SEFAZ")

                verificacao = TERMINAR_NFCE.ToString.Split("|")

                Select Case verificacao(0)
                    Case "501"
                        'Cancelamento Por Substitiuicao
                        ChaveReferenciada = FUNCOES_XML.GerarChaveSubstituta(chave(1))
                        FUNCOES_XML.XML_CANCELAMENTO_SUBSTITUICAO(chave(1), UF, cbAmbiente.Text.Substring(0, 1), 1, status(1), "NAO OBTEVE RETORNO NO ATO DO ENVIO", ChaveReferenciada, VERSAO)
                        verificacao = TERMINAR_NFCE.ToString.Split("|")
                        lstArquivos.Items.Add(verificacao(1))
                End Select

            Case 150
                'Cancelamento Normal
                FUNCOES_XML.XML_CANCELAMENTO(chave(1), UF, cbAmbiente.Text.Substring(0, 1), 1, status(1), "SEM CONEXAO COM O SEFAZ")

                verificacao = TERMINAR_NFCE.ToString.Split("|")

                Select Case verificacao(0)
                    Case 501
                        'Cancelamento Por Substitiuicao
                        ChaveReferenciada = FUNCOES_XML.GerarChaveSubstituta(chave(1))
                        FUNCOES_XML.XML_CANCELAMENTO_SUBSTITUICAO(chave(1), UF, cbAmbiente.Text.Substring(0, 1), 1, status(1), "NAO OBTEVE RETORNO NO ATO DO ENVIO", ChaveReferenciada, VERSAO)
                        verificacao = TERMINAR_NFCE.ToString.Split("|")
                        lstArquivos.Items.Add(verificacao(1))
                End Select
            Case 135
                lstArquivos.Items.Add("NFC-e " & chave(1).Substring(25, 9) & " Cancelada")
                Return True
            Case Else
                Return True
        End Select

        Return False
    End Function

    Function TERMINAR_NFCE()
        Dim envioCancelamento As String = ""
        'Assinar Xml Cancelamento 
        FUNCOES_XML.VALIDA_NFE("./ENVIO\NFCeEvent.xml", "infEvento", "<infEvento", cbCertificado)

        'Corrige TAG Assinatura
        FUNCOES_XML.ASSINAR_XML_CANCELAMENTO()

        'Chamar WebService
        Select Case cbAmbiente.Text.Substring(0, 1)
            Case 1
                'PRODUÇÃO
                envioCancelamento = (FUNCOES_XML.ValidarCancelamento(WEBSERVICES_400.Producao.EventoNfce("./ENVIO\NFCeEvent.xml", cbCertificado)))
            Case 2
                'HOMOLOGAÇÃO
                envioCancelamento = (FUNCOES_XML.ValidarCancelamento(WEBSERVICES_400.Homologacao.EventoNfce("./ENVIO\NFCeEvent.xml", cbCertificado)))
        End Select

        Return envioCancelamento
    End Function

    Function CONVERTER_CONTABILIDADE(DADOS As String, VERSAO As String)
        'DADOS = "D:\AREA DE TRABALHO\LZ_NOVO\CONT_NORMAL\AUTORIZADA\HOMOLOGACAO\NFCe41180868843234000134650040000000201000000201_000000020.xml"
        'DADOS = "D:\AREA DE TRABALHO\ERROS\OK\CADORI\CONT\000004U022454.xml"
        Dim protocolo2 As String = ""

        Dim xmlProcessado As String
        Dim xml As New XmlDocument
        xml.Load(DADOS)
        'MsgBox(PROTOCOLO_NFCE(DADOS))
        'Try
        'protocolo2 = xml.GetElementsByTagName("protNFe")(0).OuterXml

        'xmlProcessado = "<?xml version=""1.0"" encoding=""utf-8""?><nfeProc xmlns=""http://www.portalfiscal.inf.br/nfe"" versao=""" & VERSAO & """>"
        'xmlProcessado += xml.GetElementsByTagName("NFe")(0).OuterXml & protocolo2
        'xmlProcessado += "</nfeProc>"
        'Return xmlProcessado
        'Catch
        xmlProcessado = "<?xml version=""1.0"" encoding=""utf-8""?><nfeProc xmlns=""http://www.portalfiscal.inf.br/nfe"" versao=""" & VERSAO & """>"
        xmlProcessado += xml.GetElementsByTagName("NFe")(0).OuterXml & PROTOCOLO_NFCE(DADOS)
        xmlProcessado += "</nfeProc>"
        Return xmlProcessado

        'End Try

        'Return False

    End Function

    Function PROTOCOLO_NFCE(CUPOM As String)
        Try

            Dim TEXTO_PROTOCOLO As String = ""
            Dim xmlDoc As New XmlDocument

            Dim Ds As New DataSet
            Ds.ReadXml(CUPOM)

            Dim bdsAux As New BindingSource
            bdsAux.DataSource = Ds

            bdsAux.DataMember = "infNFe" 'aqui é o pulo do gato!
0:          TEXTO_PROTOCOLO = (Replace((bdsAux.Current("Id").ToString()), "NFe", "")) & ";"
1:          TEXTO_PROTOCOLO += bdsAux.Current("versao").ToString() & ";"

            bdsAux.DataMember = "ide"
2:          TEXTO_PROTOCOLO += bdsAux.Current("dhEmi").ToString() & ";"
3:          TEXTO_PROTOCOLO += bdsAux.Current("tpAmb").ToString() & ";"

            bdsAux.DataMember = "infNFeSupl"
4:          TEXTO_PROTOCOLO += bdsAux.Current("qrCode").ToString() & ";"

            bdsAux.DataMember = "Reference"
5:          TEXTO_PROTOCOLO += bdsAux.Current("DigestValue").ToString()

            bdsAux.Dispose()
            Ds.Dispose()

            Dim valores() As String
            valores = TEXTO_PROTOCOLO.Split(";")

            Dim protocolo As String = ""
            Dim dataProtocolo As String = ""
            Dim retornoProtocolo As String = ""

            If chbOnline.Checked = True Then
                Dim webClient As New WebClient
                webClient.Encoding = Encoding.UTF8
                Dim response = webClient.DownloadString(New Uri(valores(4)))

                'Pegar Protocolo

                retornoProtocolo = (response)

                Dim Linhas = retornoProtocolo.Split(System.Environment.NewLine)
                Dim var1 As String
                retornoProtocolo = String.Empty

                '=======Recuperar Protocolo normal ============
                For Each Linha As String In Linhas
                    Dim Inicio As Integer = Linha.IndexOf("Protocolo")
                    'MessageBox.Show(Linha.IndexOf(TextBox2.Text))

                    If (Inicio <> -1) Then
                        Linha = Linha.Replace(Linha.Substring(Inicio + 76), "")
                        retornoProtocolo += Linha + System.Environment.NewLine
                        var1 = Microsoft.VisualBasic.Right(retornoProtocolo, 78)
                        'MsgBox(var1)
                        'MsgBox(Replace(Replace(var1, "Protocolo de Autorização:", ""), " ", ""))
                        protocolo = (Replace(Replace(var1, "Protocolo de Autorização:", ""), " ", "")).Substring(9, 15)
                        dataProtocolo = Format(CDate((Replace(Replace(var1, "Protocolo de Autorização:", ""), " ", "")).Substring(24, 10)), "yyyy-MM-dd") _
                                & "T" & ((Replace(Replace(var1, "Protocolo de Autorização:", ""), " ", "")).Substring(34, 8)) & "-03:00"

                    Else
                        retornoProtocolo += Linha + System.Environment.NewLine
                        protocolo = "999999999999999"
                        'dataProtocolo = Format(CDate(Now), "dd/MM/yyyy")
                        dataProtocolo = valores(1)
                    End If
                Next
            Else
                protocolo = "999999999999999"
                'dataProtocolo = Format(CDate(Now), "dd/MM/yyyy")
                dataProtocolo = valores(2)
            End If
            '=======FIM Recuperar Protocolo normal ============
            Dim protNFe As String = "<protNFe versao=""" & valores(1) & """>" _
                                     & "<infProt Id=""ID" & protocolo & """>" _
                                     & "<tpAmb>" & valores(3) & "</tpAmb>" _
                                     & "<verAplic>PR-v4_1_7</verAplic>" _
                                     & "<chNFe>" & valores(0) & "</chNFe>" _
                                     & "<dhRecbto>" & dataProtocolo & "</dhRecbto>" _
                                     & "<nProt>" & protocolo & "</nProt>" _
                                     & "<digVal>" & valores(5) & "</digVal>" _
                                     & "<cStat>100</cStat>" _
                                     & "<xMotivo>Autorizado o uso da NF-e</xMotivo></infProt>" _
                                     & "</protNFe>"

            Return protNFe

        Catch ex As Exception

        End Try

        Return False
    End Function

    Function VALIDAR_DATA(xml As String)

        Try

            Dim data_hora_arquivo As String = ""
            Dim data_hora_importador_inicial As String = ""
            Dim data_hora_importador_final As String = ""

            data_hora_arquivo = Path.GetFileName(xml).Substring(5 + 6, 4)
            data_hora_arquivo += Path.GetFileName(xml).Substring(5 + 3, 2)
            data_hora_arquivo += Path.GetFileName(xml).Substring(5 + 0, 2)
            data_hora_arquivo += Path.GetFileName(xml).Substring(5 + 11, 2)
            data_hora_arquivo += Path.GetFileName(xml).Substring(5 + 14, 2)
            data_hora_arquivo += Path.GetFileName(xml).Substring(5 + 17, 2)

            '0001-13-07-2020_12;15;33_00005018

            data_hora_importador_inicial = Format(CDate(dtInicial.Value), "yyyyMMdd") & Replace(txCupomHoraInicial.Text, ":", "")
            data_hora_importador_final = Format(CDate(dtFinal.Value), "yyyyMMdd") & Replace(txCupomHoraFinal.Text, ":", "")

            If data_hora_arquivo >= data_hora_importador_inicial Then
                If data_hora_arquivo <= data_hora_importador_final Then
                    Return True
                Else
                    Return False
                End If
            End If

            'MsgBox(Path.GetFileName(xml))

        Catch
            Return False
        End Try
        Return False
    End Function

    'Verifificar Pastas
    Function VERIFICAR_PASTA()
        Try
            'Validar Diretórios
            If Not IO.Directory.Exists(txDestinoVendas.Text) Then
                'MsgBox("Diretório " & txDestinoVendas.Text & " Não Existe")
                'chbHabilitaImportador.Checked = False
                ToolStripStatusLabel1.Text = "Status: Erro -" & ("Diretório " & txDestinoVendas.Text & " Não Existe")
                StatusStrip1.BackColor = Color.Red
                Return False
            ElseIf Not IO.Directory.Exists(txDestinoZ.Text) Then
                'MsgBox("Diretório " & txDestinoZ.Text & " Não Existe")
                'chbHabilitaImportador.Checked = False
                ToolStripStatusLabel1.Text = "Status: Erro - " & ("Diretório " & txDestinoZ.Text & " Não Existe")
                StatusStrip1.BackColor = Color.Red
                Return False
            ElseIf Not IO.Directory.Exists(txOrigemArquivos.Text) Then
                'MsgBox("Diretório " & txOrigemArquivos.Text & " Não Existe")
                'chbHabilitaImportador.Checked = False
                ToolStripStatusLabel1.Text = "Status: Erro - " & ("Diretório " & txOrigemArquivos.Text & " Não Existe")
                StatusStrip1.BackColor = Color.Red
                Return False
            ElseIf Not IO.Directory.Exists(txDestinoNFCe.Text) Then
                'MsgBox("Diretório " & txDestinoNFCe.Text & " Não Existe")
                'chbHabilitaImportador.Checked = False
                ToolStripStatusLabel1.Text = "Status: Erro - " & ("Diretório " & txDestinoNFCe.Text & " Não Existe")
                StatusStrip1.BackColor = Color.Red
                Return False
            ElseIf Not IO.Directory.Exists(txDestinoScannTech.Text) Then
                'MsgBox("Diretório " & txDestinoNFCe.Text & " Não Existe")
                'chbHabilitaImportador.Checked = False
                ToolStripStatusLabel1.Text = "Status: Erro - " & ("Diretório " & txDestinoScannTech.Text & " Não Existe")
                StatusStrip1.BackColor = Color.Red
                Return False
            Else
                ToolStripStatusLabel1.Text = "Status - ON-LINE"
                StatusStrip1.BackColor = Color.LimeGreen
            End If
            If txExtensao.Text = "" Then
                MsgBox("Extensão Não informada!")
                Return False
            ElseIf txTempo.Text = "" Then
                MsgBox("Tempo não informado!")
                Return False
            End If

            Return True
        Catch
            ToolStripStatusLabel1.Text = "Status - OFF-LINE"
            StatusStrip1.BackColor = Color.Red
            Return False
        End Try
    End Function

    'Abrir importador de vendas
    Function ABRE_VENDAS(hora_inicial As Date, hora_final As Date, Status As String)
        Try

            Dim hora_atual As Date = Now

            If (hora_atual >= hora_inicial And hora_atual <= hora_final) Then
                Select Case Status
                    Case "ABERTO"
                        'MsgBox("ABERTO")
                        'Return True
                    Case "FECHADO"
                        'MsgBox("FECHADO")
                        Call Shell("c:\teorema\bin\Vendas.exe", vbMaximizedFocus)
                        lbStatusVendas.Text = "Status:ABERTO"
                End Select
            Else
                Select Case Status
                    Case "ABERTO"
                        'MsgBox("ABERTO")
                        Call Shell("taskkill /f /im vendas.exe")
                        lbStatusVendas.Text = "Status:FECHADO"
                    Case "FECHADO"
                        'MsgBox("FECHADO")
                End Select
                'Return False
            End If

            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    'Validade dia para Manter Aberto o Vendas
    Function VALIDAR_DATA_VENDAS_TEOREMA()
        Try

            Select Case Weekday(Now)
                Case 1
                    'dia = "Domingo"
                    Return chbDomingo.Checked
                Case 2
                    'dia = "Segunda-Feira"
                    Return chbSegunda.Checked
                Case 3
                    'dia = "Terça-Feira"
                    Return chbTerca.Checked
                Case 4
                    'dia = "Quarta-Feira"
                    Return chbQuarta.Checked
                Case 5
                    'dia = "Quinta-Feira"
                    Return chbQuinta.Checked
                Case 6
                    'dia = "Sexta-Feira"
                    Return chbSexta.Checked
                Case 7
                    'dia = "Sábado"
                    Return chbSabado.Checked
            End Select

        Catch ex As Exception

        End Try

        Return False
    End Function

    'Preencher Checkbox dos Dias
    Function PREENCHER_DIAS(dia As Integer)

        Select Case dia
            Case 1
                'dia = "Domingo"
                chbDomingo.Checked = True
            Case 2
                'dia = "Segunda-Feira"
                chbSegunda.Checked = True
            Case 3
                'dia = "Terça-Feira"
                chbTerca.Checked = True
            Case 4
                'dia = "Quarta-Feira"
                chbQuarta.Checked = True
            Case 5
                'dia = "Quinta-Feira"
                chbQuinta.Checked = True
            Case 6
                'dia = "Sexta-Feira"
                chbSexta.Checked = True
            Case 7
                'dia = "Sábado"
                chbSabado.Checked = True
        End Select

        Return True
    End Function

    'Função Obter dados Da Web
    Public Function RequestDadosWeb(ByVal pstrURL As String) As String
        Dim oWebRequest As WebRequest
        Dim oWebResponse As WebResponse = Nothing
        Dim strBuffer As String = ""
        Dim objSR As StreamReader = Nothing
        'conecta com o website
        Try
            oWebRequest = HttpWebRequest.Create(pstrURL)
            oWebRequest.Timeout = 10000
            oWebResponse = oWebRequest.GetResponse()
            'Le a resposta do web site e armazena em uma stream
            objSR = New StreamReader(oWebResponse.GetResponseStream)
            strBuffer = objSR.ReadToEnd
        Catch ex As Exception
            'Throw ex
            'Finally
            'objSR.Close()
            'oWebResponse.Close()
        End Try
        Return strBuffer
    End Function

    Sub HABILITAR()
        TabControl1.Controls.Add(TabPage2)
        TabControl1.Controls.Add(TabPage3)
        TabControl1.Controls.Add(TabPage4)
        TabControl1.Size = New System.Drawing.Size(725, 636)
        'TabControl1.Location = New Point(lstArquivos.Location.X, 25)
        txOrigemArquivos.Visible = True
        txDestinoVendas.Visible = True
        txDestinoZ.Visible = True
        txExtensao.Visible = True
        txTempo.Visible = True
        dtInicial.Visible = True
        dtFinal.Visible = True
        chbHabilitaImportador.Visible = True
        chbHabilitaData.Visible = True
        Label1.Visible = True
        Label2.Visible = True
        Label3.Visible = True
        Label4.Visible = True
        Label60.Visible = True
        Label112.Visible = True
        Label63.Visible = True
        Label59.Visible = True
        Label65.Visible = True
        btOrigemArquivos.Visible = True
        btDestinoVendas.Visible = True
        btDestinoZ.Visible = True
        btSalvar.Visible = True
        Label63.Location = New Point(Label63.Location.X, 385)
        lstArquivos.Location = New Point(lstArquivos.Location.X, 400)
        lstArquivos.Size = New Size(685, 108)
        Me.MinimumSize = New System.Drawing.Size(765, 730)
        Me.MaximumSize = New System.Drawing.Size(765, 730)
        Me.Size = New System.Drawing.Size(765, 704)
        btSairConfiguracao.Visible = True
        txSenhaConfiguracao.Visible = True
        Label66.Visible = True
        grpFinanceiro.Visible = True
        txDestinoNFCe.Visible = True
        Label74.Visible = True
        btDestinoNFCe.Visible = True
    End Sub

    Sub DESABILITAR()
        TabControl1.Controls.Remove(TabPage2)
        TabControl1.Controls.Remove(TabPage3)
        TabControl1.Controls.Remove(TabPage4)
        TabControl1.Size = New System.Drawing.Size(460, 185)
        txOrigemArquivos.Visible = False
        txDestinoVendas.Visible = False
        txDestinoZ.Visible = False
        txExtensao.Visible = False
        txTempo.Visible = False
        dtInicial.Visible = False
        dtFinal.Visible = False
        chbHabilitaImportador.Visible = False
        chbHabilitaData.Visible = False
        Label1.Visible = False
        Label2.Visible = False
        Label3.Visible = False
        Label4.Visible = False
        Label59.Visible = False
        Label60.Visible = False
        Label112.Visible = False
        Label65.Visible = False
        Label66.Visible = False
        Label74.Visible = False
        Label63.Location = New Point(Label63.Location.X, 16)
        btOrigemArquivos.Visible = False
        btDestinoVendas.Visible = False
        btDestinoZ.Visible = False
        btSalvar.Visible = False
        lstArquivos.Location = New Point(lstArquivos.Location.X, 30)
        lstArquivos.Size = New Size(412, 108)
        Me.MinimumSize = New System.Drawing.Size(500, 280)
        Me.MaximumSize = New System.Drawing.Size(500, 280)
        Me.Size = New System.Drawing.Size(500, 280)
        btSairConfiguracao.Visible = False
        txSenhaConfiguracao.Visible = False
        grpFinanceiro.Visible = False
        txDestinoNFCe.Visible = False
        btDestinoNFCe.Visible = False

    End Sub
    'Função para Ler Diretorio de Arquivos
    Sub TOTAL_BASE()
        TOTALIZADOR = CDbl(B1.Text) + CDbl(B2.Text) + CDbl(B3.Text) + CDbl(B4.Text) + CDbl(B5.Text) + CDbl(B6.Text) + CDbl(B7.Text) + CDbl(B8.Text)
        TOTAL_IMPOSTO = CDbl(I1.Text) + CDbl(I2.Text) + CDbl(I3.Text) + CDbl(I4.Text) + CDbl(I5.Text) + CDbl(I6.Text) + CDbl(I7.Text) + CDbl(I8.Text)
    End Sub

    Sub LIMPAR_DADOS()
        'Zerar Aliquotas, bases, e impostos
        A1.Text = "0,00"
        B1.Text = "0,00"
        I1.Text = "0,00"
        A2.Text = "0,00"
        B2.Text = "0,00"
        I2.Text = "0,00"
        A3.Text = "0,00"
        B3.Text = "0,00"
        I3.Text = "0,00"
        A4.Text = "0,00"
        B4.Text = "0,00"
        I4.Text = "0,00"
        A5.Text = "0,00"
        B5.Text = "0,00"
        I5.Text = "0,00"
        A6.Text = "0,00"
        B6.Text = "0,00"
        I6.Text = "0,00"
        A7.Text = "0,00"
        B7.Text = "0,00"
        I7.Text = "0,00"
        A8.Text = "0,00"
        B8.Text = "0,00"
        I8.Text = "0,00"
        Acrescimo.Text = "0,00"
        Movimento.Text = "0,00"
        cancelamento.Text = "0,00"
        desconto.Text = "0,00"
        sangria.Text = "0,00"
        isento.Text = "0,00"
        substituicao.Text = "0,00"
        naotributado.Text = "0,00"
        loja.Text = ""
        data_fiscal.Text = ""
        hora.Text = ""
        fabricante.Text = ""
        impressora.Text = ""
        ticket_inicial.Text = ""
        ticket_final.Text = ""
        caixa.Text = ""
        gt_inicial.Text = ""
        gt_final.Text = ""
        cro.Text = ""
        crz.Text = ""
        nfce_Chave.Text = ""
        nfce_Data.Text = ""
        nfce_Loja.Text = ""
        nfce_Nota.Text = ""
        nfce_Serie.Text = ""
        nfce_Xml.Text = ""

    End Sub

    Sub LER_ARQUIVOS3()
        Try
            'Classifica a lista em ordem alfabética
            Dim di As New IO.DirectoryInfo(txOrigemArquivos.Text)
            Dim diar1 As IO.FileInfo() = di.GetFiles()
            Dim dra As IO.FileInfo

            tmInicioImportacao.Stop()

            If chbmonitorpasta.Checked = True Then
                Dim counter = My.Computer.FileSystem.GetFiles(txDestinoVendas.Text)
                ToolStripStatusLabel4.Text = "Cupons: " & counter.Count

                Dim ver_cupons() = ToolStripStatusLabel4.Text.Split(":")

                If IsNumeric(ver_cupons(1).Replace(" ", "")) = True Then
                    If ver_cupons(1).Replace(" ", "") = 0 Then

                    Else
                        tmInicioImportacao.Start()
                        Exit Sub
                    End If
                End If
            End If

            'Ler lista de arquivos da Pasta
            For Each dra In diar1

                If chbHabilitaData.Checked = True Then

                Else

                End If
            Next
        Catch

        End Try
    End Sub

    Sub LER_ARQUIVOS2()
        Try
            'Classifica a lista em ordem alfabética
            Dim di As New IO.DirectoryInfo(txOrigemArquivos.Text)
            Dim diar1 As IO.FileInfo() = di.GetFiles()
            Dim dra As IO.FileInfo

            tmInicioImportacao.Stop()

            If chbmonitorpasta.Checked = True Then
                Dim counter = My.Computer.FileSystem.GetFiles(txDestinoVendas.Text)
                ToolStripStatusLabel4.Text = "Cupons: " & counter.Count

                Dim ver_cupons() = ToolStripStatusLabel4.Text.Split(":")

                If IsNumeric(ver_cupons(1).Replace(" ", "")) = True Then
                    If ver_cupons(1).Replace(" ", "") = 0 Then

                    Else
                        tmInicioImportacao.Start()
                        Exit Sub
                    End If
                End If
            End If

            For Each dra In diar1

                'Filtrar por *.xml
                If System.IO.Path.GetExtension(dra.Name).ToLower() = ".xml" Then
                    CUPOM = txOrigemArquivos.Text & dra.ToString
                    Label63.Text = "Arquivos: Lendo - " & System.IO.Path.GetFileName(CUPOM)

                    'Verificar se Arquivo Ja Foi renomeado
                    Try
                        Dim data_arquivo As Date = CDate(System.IO.Path.GetFileName(CUPOM).Substring(5, 10) & " 00:00:00")
                        Dim valida As Boolean = False
                        'MsgBox(data_arquivo)

                        valida = (Funcoes.VALIDAR_DATA_HORA_CUPOM(dtInicial.Value & " " & txCupomHoraInicial.Text, data_arquivo & " 00:00:00"))
                        If chbHabilitaData.Checked = True Then

                            If valida = True Then
                                valida = (Funcoes.VALIDAR_DATA_HORA_CUPOM(data_arquivo & " 00:00:00", dtFinal.Value & " " & txCupomHoraFinal.Text))
                            Else
                                If IsDate(CDate(System.IO.Path.GetFileName(CUPOM).Substring(5, 10))) = True Then
                                    Continue For
                                End If
                            End If
                            End If

                        If IsDate(CDate(System.IO.Path.GetFileName(CUPOM).Substring(5, 10))) = True Then
                            If valida = True Then
                                'Monitoramento Pasta
                                If chbmonitorpasta.Checked = True Then

                                    Dim counter = My.Computer.FileSystem.GetFiles(txDestinoVendas.Text)
                                    ToolStripStatusLabel4.Text = "Cupons: " & counter.Count

                                    If counter.Count = 0 Then
                                        Contador_pasta = 1
                                        File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                        lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                    Else
                                        If Contador_pasta < txNumeroArquivos.Text Then
                                            File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                            Contador_pasta += 1
                                            lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                        Else
                                            Continue For
                                        End If
                                    End If
                                Else
                                    'File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                    'lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))

                                    If File.Exists(txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM)) = True Then
                                        File.Delete(CUPOM)
                                        lstArquivos.Items.Add("Arquivo Duplicado - " & System.IO.Path.GetFileName(CUPOM))
                                    Else
                                        CLASSE_TXT.VALIDAR_QTDE_MODO_TEXTO(CUPOM)
                                        File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                        lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                    End If
                                End If



                            End If
                            Application.DoEvents()
                            Continue For

                        End If
                    Catch
                    End Try

                    '========================Verificar XML =======================
                    Dim fluxoTexto As IO.StreamReader
                    Dim linhaTexto As String

                    If IO.File.Exists(CUPOM) Then
                        fluxoTexto = New IO.StreamReader(CUPOM)
                        linhaTexto = fluxoTexto.ReadToEnd

NFCE:                   If linhaTexto Like "*<infNFe*" Then
                            'Verificar Pasta Destino NFCE
                            fluxoTexto.Close()

                            'Ler Xml
                            Dim CHAVE As String = ""
                            Dim VERSAO As String = ""
                            Dim DATA_EMISSAO As String = ""
                            Try


                                Dim xmlRet As New XmlDocument
                                xmlRet.Load(CUPOM)

                                Dim xmlModificado As String
                                xmlModificado = xmlRet.GetElementsByTagName("NFe")(0).OuterXml

                                Dim xEle As XElement = XElement.Parse(xmlModificado)
                                Dim nsr As XNamespace = "http://www.portalfiscal.inf.br/nfe"


                                Dim ns As System.Xml.XmlNamespaceManager = New System.Xml.XmlNamespaceManager(xmlRet.NameTable)
                                ns.AddNamespace("nfe", "http://www.portalfiscal.inf.br/nfe")

                                Dim xpathNav As XPathNavigator = xmlRet.CreateNavigator()

                                ' 1 - Pegar Chave da nota
                                Dim node As XPathNavigator = xpathNav.SelectSingleNode("//nfe:infNFe", ns)
                                CHAVE = node.GetAttribute("Id", "").Substring(3, 44)
                                VERSAO = node.GetAttribute("versao", "")

                                Dim ide = xEle.Elements(nsr + "infNFe").Elements(nsr + "ide").ToList()

                                For Each cDet As XElement In ide
                                    DATA_EMISSAO = ide.Elements(nsr + "dhEmi").Value
                                Next

                                xEle = Nothing
                                xmlRet = Nothing
                                'Dim data_hora As String = ""
                                'data_hora += DATA_EMISSAO.Substring(8, 2) & "-"
                                'data_hora += DATA_EMISSAO.Substring(5, 2) & "-"
                                'data_hora += DATA_EMISSAO.Substring(0, 4)
                            Catch

                                lstArquivos.Items.Add(" Erro NFC-e Distribruição - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Move(CUPOM, txDestinoErro.Text & System.IO.Path.GetFileName(CUPOM))
                                Catch
                                    File.Delete(txDestinoErro.Text & System.IO.Path.GetFileName(CUPOM))
                                    File.Move(CUPOM, txDestinoErro.Text & System.IO.Path.GetFileName(CUPOM))
                                End Try

                                Continue For
                            End Try


                            nfce_Chave.Text = CHAVE
                            txVesaoNFCe.Text = VERSAO

                            nfce_Data.Text = Format(CDate(DATA_EMISSAO.Substring(0, 10)), "General Date")

                            MES = nfce_Chave.Text.Substring(2, 4)
                            EMPRESA = nfce_Chave.Text.Substring(6, 14)

                            If Directory.Exists(txDestinoNFCe.Text & MES & " \") = False Then
                                Directory.CreateDirectory(txDestinoNFCe.Text & MES)
                            End If
                            If Directory.Exists(txDestinoNFCe.Text & MES & " \" & EMPRESA & " \") = False Then
                                Directory.CreateDirectory(txDestinoNFCe.Text & MES & " \" & EMPRESA)
                            End If

                            'Try
                            'converter xml para a contabilidade
                            Dim Nome_NFCe As String = txDestinoNFCe.Text & MES & " \" & EMPRESA & " \" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), " -", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & "_" & Replace(txVesaoNFCe.Text, ".", "") & ".xml"
                            Dim xml_distribuicao = New IO.StreamWriter(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & "_" & Replace(txVesaoNFCe.Text, ".", "") & ".xml", False)

                            xml_distribuicao.Write(CONVERTER_CONTABILIDADE(CUPOM, txVesaoNFCe.Text))
                            xml_distribuicao.Close()

                            'Criar Copia de NFC-e
                            Try
                                If chbScannTech.Checked = True Then
                                    File.Copy(Nome_NFCe, txDestinoScannTech.Text & Path.GetFileName(Nome_NFCe))
                                End If
                            Catch
                                lstArquivos.Items.Add("Erro ao copia NFC-e - " & Nome_NFCe)
                            End Try
                            If chbContabilidade.Checked = True Then

                                Try
                                    If Funcoes.ENVIAR_XML_POST(txLinkContabiliade.Text, txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & "_" & Replace(txVesaoNFCe.Text, ".", "") & ".xml") Like "*Importado com sucesso*" Then
                                        lstArquivos.Items.Add("Arquivo " & CUPOM & " Enviado para Contabilidade")
                                    Else
                                        Funcoes.GRAVAR_LOG(Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & ".txt", txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & "_" & Replace(txVesaoNFCe.Text, ".", "") & ".xml")
                                    End If
                                Catch
                                End Try
                                Try
                                    File.Copy(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & "_" & Replace(txVesaoNFCe.Text, ".", "") & ".xml", txLinkContabiliade.Text & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & "_" & Replace(txVesaoNFCe.Text, ".", "") & ".xml")
                                Catch
                                End Try

                            End If

                            Try
                                File.Delete(CUPOM)
                            Catch
                                ' MsgBox(Err.Description)
                            End Try


                            lstArquivos.Items.Add("Movido NFC-e Distribruição - " & System.IO.Path.GetFileName(CUPOM))

                            If lstArquivos.Items.Count >= 10 Then
                                With lstArquivos
                                    .ForeColor = Color.DarkGreen
                                    .Items.Clear()
                                End With
                            End If

                            Application.DoEvents()
                            MemoryManagement.FlushMemory()
                            Continue For

                        End If
LZ_SALE:                If linhaTexto Like "*<SALE>*" Then           'Ler Aqruivo de Vendas
                            'Chamar Função Mover vendas
                            fluxoTexto.Close()

                            CLASSE_TXT.Verificar_CARACTER_ESPECIAL(CUPOM)
                            CLASSE_TXT.Gravar_XML(CLASSE_TXT.LIMPAR(CLASSE_TXT.ABRIR_TXT("c:\teste\SALE_TMP.xml")), "c:\teste\SALE_TMP.xml")
                            'Verificar se existe item com quantidae zero
                            CLASSE_TXT.VALIDAR_QTDE_MODO_TEXTO("c:\teste\SALE_TMP.xml")
                            CLASSE_TXT.VALIDAR_SUBTOTAL_DUPLICADO("c:\teste\SALE_TMP.xml")
                            'Verifica se Cupom esta na receita e se foi cancelado.
                            'CANCELAR(CUPOM)


                            'Importar Recarga
                            If chbRzRecarga.Checked = True Then
                                Dim TEM_RECARGA As String = "N"
                                If linhaTexto Like "*<SALE_TYPE>47</SALE_TYPE>*" Then
                                    TEM_RECARGA = "S"
                                End If

                                If TEM_RECARGA = "S" Then
                                    Dim RZ_IMPORTADA As Boolean
                                    Dim RET_RECARGA() As String
                                    RET_RECARGA = CLASSE_TXT.LER_RECARGA(CUPOM).ToString.Split("|")

                                    If RET_RECARGA(0) <> False Then
                                        'MsgBox(RET_RECARGA(3))
                                    Else
                                        Continue For
                                    End If

                                    RZ_IMPORTADA = CLASSE_TXT.VERIFICAR_RZ(RET_RECARGA(0), RET_RECARGA(1), RET_RECARGA(2), TIPO_CONEXAO)

                                    If RZ_IMPORTADA = True Then
                                        CLASSE_TXT.ATUALIZAR_RZ(RET_RECARGA(0), RET_RECARGA(1), RET_RECARGA(2), RET_RECARGA(3), TIPO_CONEXAO)
                                    Else
                                        Continue For
                                    End If
                                End If
                            End If

                            'Renomear e Classificar venda
                            If CLASSE_TXT.LER_SALE("c:\teste\SALE_TMP.xml", CUPOM, txDestinoErro.Text) = True Then
                                If CONTINUAR = True Then


                                    Try
                                        File.Delete(CUPOM)
                                    Catch
                                    End Try


                                    If CLASSE_TXT.Gravar_XML(CLASSE_TXT.Verificar_XML("c:\teste\SALE_TMP.xml"), "c:\teste\SALE_TMP.xml") = True Then
                                    Else
                                        Continue For
                                    End If

                                    Try
                                        'File.Move("c:\teste\SALE_TMP.xml", CUPOM)
                                        Dim local As String = String.Empty
                                        local = Path.GetDirectoryName(CUPOM) & "\" & Funcoes.RENOMEAR_XML("c:\teste\SALE_TMP.xml")
                                        File.Move("c:\teste\SALE_TMP.xml", local)
                                    Catch

                                    End Try

                                Else
                                    lstArquivos.Items.Add("Erro:" & System.IO.Path.GetFileName(CUPOM))
                                    MemoryManagement.FlushMemory()
                                    Application.DoEvents()
                                    Continue For
                                End If
                            Else
                                If CONTINUAR = True Then
                                    Try
                                        File.Delete(CUPOM)
                                    Catch
                                    End Try

                                    Try
                                        If VENDA_CANCELADA_OBRIGATORIA("c:\teste\SALE_TMP.xml") = False Then
                                            File.Move("c:\teste\SALE_TMP.xml", txDestinoErro.Text & "ERR_" & Path.GetFileName(CUPOM))
                                            lstArquivos.Items.Add("Movido Erros - " & System.IO.Path.GetFileName(CUPOM))
                                            Continue For
                                        End If
                                    Catch

                                        File.Move("c:\teste\SALE_TMP.xml", txDestinoErro.Text & "ERR_" & Path.GetFileName(CUPOM))
                                        lstArquivos.Items.Add("Movido Erros - " & System.IO.Path.GetFileName(CUPOM))
                                        Continue For
                                    End Try

                                    'Verificar se Cupom de Venda(65) ou Nota Fiscal(100)
                                    If (CLASSE_TXT.VALIDAR_OPERACAO("c:\teste\SALE_TMP.xml", 65)) = False Then
                                        File.Delete(CUPOM)
                                        lstArquivos.Items.Add("Venda Nota Fiscal:" & System.IO.Path.GetFileName(CUPOM))
                                        Continue For
                                    End If

                                    Try

                                        'File.Move("c:\teste\SALE_TMP.xml", CUPOM)
                                        Dim local As String = String.Empty
                                        'local = txDestinoVendas.Text & "\" & Funcoes.RENOMEAR_XML("c:\teste\SALE_TMP.xml")
                                        local = txOrigemArquivos.Text & "\" & Funcoes.RENOMEAR_XML("c:\teste\SALE_TMP.xml")
                                        File.Move("c:\teste\SALE_TMP.xml", local)
                                        CUPOM = local
                                    Catch

                                    End Try

                                Else
                                    lstArquivos.Items.Add("Erro:" & System.IO.Path.GetFileName(CUPOM))
                                    MemoryManagement.FlushMemory()
                                    Application.DoEvents()
                                    Continue For
                                End If
                            End If

                            'Importar Entregas
                            Try
                                If chbEntrega.Checked = True Then
                                    Try
                                        Funcoes.lerXml(CUPOM, lstArquivos, txBancoEntregaFdb.Text)
                                    Catch ex As Exception
                                        lstArquivos.Items.Add("Erro:" & ex.ToString)
                                    End Try
                                End If

                            Catch ex As Exception
                                lstArquivos.Items.Add("Erro:" & ex.ToString)
                            End Try


                            With lstArquivos
                                .ForeColor = Color.DarkGreen
                            End With

                            If File.Exists(txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM)) = True Then
                                lstArquivos.Items.Add("Ticket Duplicado Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Delete(CUPOM)
                                Catch

                                End Try
                            Else
                                'lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))

                                'If chbScannTech.Checked = True Then
                                '    Try
                                '        File.Copy(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                '    Catch
                                '    End Try
                                '    Try
                                '        File.Move(CUPOM, txDestinoScannTech.Text & System.IO.Path.GetFileName(CUPOM))
                                '    Catch

                                '    End Try

                                'ElseIf chbScannTech.Checked = False Then
                                Try

                                    'Validar Data
                                    If chbHabilitaData.Checked = True Then
                                        If VALIDAR_DATA(CUPOM) = True Then

                                            If chbmonitorpasta.Checked = True Then
                                                Dim counter = My.Computer.FileSystem.GetFiles(txDestinoVendas.Text)
                                                ToolStripStatusLabel4.Text = "Cupons: " & counter.Count

                                                If counter.Count = 0 Then
                                                    Contador_pasta = 1
                                                    File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                                    lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                                Else
                                                    If Contador_pasta <= counter.Count Then
                                                        Contador_pasta = counter.Count
                                                    End If
                                                    If Contador_pasta < txNumeroArquivos.Text Then
                                                        File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                                        Contador_pasta += 1
                                                        lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                                    Else
                                                        Continue For
                                                    End If
                                                End If
                                            Else
                                                File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                                lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                            End If
                                        Else
                                            Continue For
                                        End If

                                    Else
                                        'Monitoramento Pasta
                                        If chbmonitorpasta.Checked = True Then

                                            Dim counter = My.Computer.FileSystem.GetFiles(txDestinoVendas.Text)
                                            ToolStripStatusLabel4.Text = "Cupons: " & counter.Count

                                            If counter.Count = 0 Then
                                                Contador_pasta = 1
                                                File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                                lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                            Else
                                                If Contador_pasta < txNumeroArquivos.Text Then
                                                    File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                                    Contador_pasta += 1
                                                    lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                                Else
                                                    Continue For
                                                End If
                                            End If
                                        Else
                                            File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                            lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                        End If
                                    End If

                                Catch
                                End Try
                                'End If

                            End If

LZ_RED_Z:               ElseIf linhaTexto Like "*<Z_REPORT>*" Then   'Ler Redução Z
                            'Chamar Função importar Z
                            If chbImportaRz.Checked = True Then
                                LER_XML_RZ()
                            End If
                            fluxoTexto.Close()
                            Try
                                lstArquivos.Items.Add("Importado Redução Z - " & System.IO.Path.GetFileName(CUPOM))
                            Catch ex As Exception

                            End Try
                            With lstArquivos
                                .ForeColor = Color.DarkGreen
                            End With
                            'Selecioner tipo de conexão para importação
                            If rbConexaoNormal.Checked = True Then
                                IMPORTAR_RZ_FiREBIRD()
                            ElseIf rbConexaoODBC.Checked = True Then
                                IMPORTAR_RZ_ODBC()
                            End If
                            If File.Exists(txDestinoZ.Text & System.IO.Path.GetFileName(CUPOM)) = True Then
                                lstArquivos.Items.Add("Ticket Duplicado Redução Z - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Delete(CUPOM)
                                Catch

                                End Try
                            Else
                                lstArquivos.Items.Add("Importado Redução Z - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Move(CUPOM, txDestinoZ.Text & System.IO.Path.GetFileName(CUPOM))
                                Catch
                                End Try
                            End If

DEVOLUCAO:              ElseIf linhaTexto Like "*<RETURN>*" Then   'Ler Devolução
                            'Chamar Função Mover Devolução de Vendas
                            fluxoTexto.Close()
                            CLASSE_TXT.Verificar_CARACTER_ESPECIAL(CUPOM)

                            If CLASSE_TXT.VALIDAR_NO_XML("c:\teste\SALE_TMP.xml", "RETURN_NUMBER") = False Then
                                File.Delete(CUPOM)
                                Continue For
                            End If

                            Try
                                lstArquivos.Items.Add("Importado Devolução - " & System.IO.Path.GetFileName(CUPOM))
                            Catch ex As Exception

                            End Try


                            With lstArquivos
                                .ForeColor = Color.DarkGreen
                            End With

                            If File.Exists(txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM)) = True Then
                                lstArquivos.Items.Add("Ticket Duplicado Devolução Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Delete(CUPOM)
                                Catch
                                End Try
                            Else
                                'Importar Devolução

                                '################### Inicio Importação Vendas Canceladas #######################

                                'Decodificar XML
                                Dim encoding__1 = Encoding.GetEncoding("ISO-8859-1")
                                Dim sr = New StreamReader("c:\teste\SALE_TMP.xml", encoding__1)
                                Dim bytes = encoding__1.GetBytes(System.Web.HttpUtility.HtmlDecode(sr.ReadToEnd()))
                                Dim ms = New MemoryStream(bytes)
                                Dim ds = New DataSet()

                                'Carregar XML
                                ds.ReadXml(ms)

                                Dim bdsAux As New BindingSource
                                bdsAux.DataSource = ds
                                bdsAux.DataMember = "RETURN"
                                Dim I, G, v As Integer
                                DataGridView1.Rows.Clear()
                                'Try
                                With DataGridView1
                                    .Rows.Clear()
                                    .Rows.Add()
                                    .Rows(.Rows.Count - 1).Cells("LOJA_VENDAS").Value = bdsAux.Current("STORE").ToString
                                    .Rows(.Rows.Count - 1).Cells("PDV").Value = bdsAux.Current("POS").ToString
                                    .Rows(.Rows.Count - 1).Cells("TICKET_DEVOLUCAO").Value = bdsAux.Current("TICKET").ToString
                                    .Rows(.Rows.Count - 1).Cells("DATA").Value = Format(CInt(bdsAux.Current("FISCAL_DAY").ToString), "0000-00-00")
                                    .Rows(.Rows.Count - 1).Cells("NUMERO_DEVOLUCAO").Value = bdsAux.Current("RETURN_NUMBER").ToString
                                    .Rows(.Rows.Count - 1).Cells("TRANSACAO_VENDAS").Value = "50"
                                    .Rows(.Rows.Count - 1).Cells("REASON").Value = .Rows(.Rows.Count - 1).Cells("NUMERO_DEVOLUCAO").Value
                                    .Rows(.Rows.Count - 1).Cells("MAKER").Value = "0"

                                    bdsAux.DataSource = ds
                                    bdsAux.DataMember = "ITEM"

                                    .Rows(.Rows.Count - 1).Cells("SEQUENCIA").Value = bdsAux.Current("SEQ").ToString
                                    .Rows(.Rows.Count - 1).Cells("PLU").Value = bdsAux.Current("ID").ToString
                                    .Rows(.Rows.Count - 1).Cells("DESCRICAO").Value = bdsAux.Current("DESCRIPTION").ToString
                                    .Rows(.Rows.Count - 1).Cells("VALOR").Value = Replace(Format(CDbl(bdsAux.Current("AMOUNT").ToString.Replace(".", ",")), "0.000"), ",", ".")
                                    .Rows(.Rows.Count - 1).Cells("QUANTIDADE").Value = bdsAux.Current("QTY").ToString
                                    Try
                                        .Rows(.Rows.Count - 1).Cells("DEPARTAMENTO").Value = bdsAux.Current("DEPT_ID").ToString * 1
                                    Catch
                                        .Rows(.Rows.Count - 1).Cells("DEPARTAMENTO").Value = 1
                                    End Try
                                    For I = 1 To bdsAux.List.Count - 1
                                        bdsAux.MoveNext()

                                        'For v = 0 To .Rows.Count - 1
                                        .Rows.Add()
                                        .Rows(.Rows.Count - 1).Cells("LOJA_VENDAS").Value = .Rows(.Rows.Count - 2).Cells("LOJA_VENDAS").Value
                                        .Rows(.Rows.Count - 1).Cells("PDV").Value = .Rows(.Rows.Count - 2).Cells("PDV").Value
                                        .Rows(.Rows.Count - 1).Cells("TICKET_DEVOLUCAO").Value = .Rows(.Rows.Count - 2).Cells("TICKET_DEVOLUCAO").Value
                                        .Rows(.Rows.Count - 1).Cells("DATA").Value = .Rows(.Rows.Count - 2).Cells("DATA").Value
                                        .Rows(.Rows.Count - 1).Cells("NUMERO_DEVOLUCAO").Value = .Rows(.Rows.Count - 2).Cells("NUMERO_DEVOLUCAO").Value
                                        .Rows(.Rows.Count - 1).Cells("TRANSACAO_VENDAS").Value = "50"
                                        .Rows(.Rows.Count - 1).Cells("REASON").Value = .Rows(.Rows.Count - 2).Cells("NUMERO_DEVOLUCAO").Value
                                        .Rows(.Rows.Count - 1).Cells("MAKER").Value = "0"

                                        .Rows(.Rows.Count - 1).Cells("SEQUENCIA").Value = bdsAux.Current("SEQ").ToString
                                        .Rows(.Rows.Count - 1).Cells("PLU").Value = bdsAux.Current("ID").ToString
                                        .Rows(.Rows.Count - 1).Cells("DESCRICAO").Value = bdsAux.Current("DESCRIPTION").ToString
                                        .Rows(.Rows.Count - 1).Cells("VALOR").Value = Replace(Format(CDbl(bdsAux.Current("AMOUNT").ToString.Replace(".", ",")), "0.000"), ",", ".")
                                        .Rows(.Rows.Count - 1).Cells("QUANTIDADE").Value = bdsAux.Current("QTY").ToString
                                        Try
                                            .Rows(.Rows.Count - 1).Cells("DEPARTAMENTO").Value = bdsAux.Current("DEPT_ID").ToString * 1
                                        Catch
                                            .Rows(.Rows.Count - 1).Cells("DEPARTAMENTO").Value = 1
                                        End Try
                                        'Next
                                    Next

                                    Dim valor As Double = 0

                                    TABELA = "sale"
                                    Dim SqlEmporium As String = ""

                                    For G = 0 To .Rows.Count - 1


                                        'Iniciar Script

                                        SqlEmporium = "INSERT INTO accum_returned_item_reason " _
                                        & "(store_key,pos_number,fiscal_date,plu_id,desc_plu,quantity,amount," _
                                        & "department_key,maker_key,reason,transaction) " _
                                        & "VALUES ('" _
                                        & .Rows(.Rows.Count - 1).Cells("LOJA_VENDAS").Value _
                                        & "','" & .Rows(G).Cells("PDV").Value _
                                        & "','" & .Rows(G).Cells("DATA").Value _
                                        & "','" & .Rows(G).Cells("PLU").Value _
                                        & "','" & .Rows(G).Cells("DESCRICAO").Value _
                                        & "','" & .Rows(G).Cells("QUANTIDADE").Value _
                                        & "','" & .Rows(G).Cells("VALOR").Value _
                                        & "','" & .Rows(G).Cells("DEPARTAMENTO").Value _
                                        & "','" & .Rows(G).Cells("MAKER").Value _
                                        & "',(SELECT coalesce (max(reason)+1,null,1) from accum_returned_item_reason as reason)" _
                                        & ",'" & .Rows(G).Cells("TRANSACAO_VENDAS").Value _
                                        & "');"

                                        CONEXAO_MySQL(SqlEmporium)

                                        valor = valor + (.Rows(G).Cells("VALOR").Value.Replace(".", ",")) * 1
                                    Next

                                    SqlEmporium = ""
                                    SqlEmporium = "update sale set amount_due = '" & Format(valor, "0.000").Replace(",", ".") _
                                    & "' where store_key = " & .Rows(0).Cells("LOJA_VENDAS").Value _
                                    & " and ticket_number=" & .Rows(0).Cells("TICKET_DEVOLUCAO").Value _
                                    & " and start_time between '" & .Rows(0).Cells("DATA").Value & " 00:00:00'" _
                                    & " and '" & .Rows(0).Cells("DATA").Value & " 23:59:59'" _
                                    & " and sale_type = 50"

                                    CONEXAO_MySQL(SqlEmporium)

                                End With

                                sr.Dispose()
                                ds.Dispose()
                                ms.Dispose()

                                '################### Fim Importação Vendas Canceladas #######################

                                lstArquivos.Items.Add("Movido Devolução de Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                Catch

                                End Try

                            End If
SALE_VOID:              ElseIf linhaTexto Like "*<SALE_VOID>*" Then   'Ler Redução Z
                            'Chamar Função Cancelamento
                            fluxoTexto.Close()
                            CLASSE_TXT.Gravar_XML(CLASSE_TXT.LIMPAR(CLASSE_TXT.ABRIR_TXT(CUPOM)), CUPOM)
                            'MsgBox(Funcoes.Localizar_Cancelado("Z:\0C046753.xml"))
                            IMPORTAR_CANCELADA_FiREBIRD(CUPOM)
                            'MsgBox(RETORNO_CANCELADA)


                            Try
                                If RETORNO_CANCELADA = True Then

                                    If File.Exists(txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM)) = True Then
                                        lstArquivos.Items.Add("Ticket Duplicado Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                        Try
                                            File.Delete(CUPOM)
                                        Catch

                                        End Try
                                    Else
                                        lstArquivos.Items.Add("Cancelado - " & System.IO.Path.GetFileName(CUPOM))

                                        Try
                                            File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                        Catch

                                        End Try

                                    End If
                                Else
                                    Continue For
                                End If


                            Catch ex As Exception

                            End Try

                            With lstArquivos
                                .ForeColor = Color.DarkGreen
                            End With

                        End If
                    End If
                    Application.DoEvents()
                    fluxoTexto.Close()
                    File.Delete(CUPOM)
                Else
                    'MessageBox.Show("Arquivo não existe")
                End If
                MemoryManagement.FlushMemory()
                '==================Fim Verificação ===========================

                If lstArquivos.Items.Count >= 10 Then
                    lstArquivos.Items.Clear()
                End If
            Next
            MemoryManagement.FlushMemory()
            Label63.Text = "Último arquivo lido até ás " & Now & " : " & System.IO.Path.GetFileName(CUPOM)
            Application.DoEvents()
            tmInicioImportacao.Start()
        Catch
            lstArquivos.Items.Add(Err.Description)
            'Clipboard.SetText(Err.Description)
            tmInicioImportacao.Start()
        End Try
    End Sub

    Sub LER_ARQUIVOS()

        'Classifica a lista em ordem alfabética
        Dim di As New IO.DirectoryInfo(txOrigemArquivos.Text)
        Dim diar1 As IO.FileInfo() = di.GetFiles()
        Dim dra As IO.FileInfo

        tmInicioImportacao.Stop()

        For Each dra In diar1

            'Filtrar por *.xml
            If System.IO.Path.GetExtension(dra.Name).ToLower() = ".xml" Then
                CUPOM = txOrigemArquivos.Text & dra.ToString
                Label63.Text = "Arquivos: Lendo - " & System.IO.Path.GetFileName(CUPOM)
                'If chbXmlContabilidade.Checked = False Then
                Try
                    txVesaoNFCe.Text = ""
                    'Define o caminho do arquivo XML 
                    CAMINHO_NFCE = CUPOM
                    Dim xmlDoc As New XmlDocument

                    xmlDoc.Load(CUPOM)

                    Dim Ds As New DataSet
                    Ds.ReadXml(CUPOM)

                    Dim bdsAux As New BindingSource

                    bdsAux.DataSource = Ds

                    bdsAux.DataMember = "infNFe" 'aqui é o pulo do gato!
                    nfce_Chave.Text = bdsAux.Current("Id").ToString.Substring(3, 44)
                    txVesaoNFCe.Text = bdsAux.Current("versao").ToString
                    nfce_Nota.Text = nfce_Chave.Text.Substring(25, 9)
                    nfce_Serie.Text = nfce_Chave.Text.Substring(22, 3)

                    bdsAux.DataMember = "ide"
                    nfce_Data.Text = Format(CDate(bdsAux.Current("dhEmi").ToString.Substring(0, 10)), "General Date")
                    nfce_Loja.Text = nfce_Chave.Text.Substring(6, 14)

                    bdsAux.DataMember = "ICMSTot"
                    nfce_valor.Text = bdsAux.Current("vNF").ToString
                Catch

                End Try
                'End If

                '========================Verificar XML =======================
                Dim fluxoTexto As IO.StreamReader
                Dim linhaTexto As String

                If IO.File.Exists(CUPOM) Then
                    fluxoTexto = New IO.StreamReader(CUPOM)
                    linhaTexto = fluxoTexto.ReadLine
                    If linhaTexto Like "*<nfeProc*" Then
                        'Verificar Pasta Destino NFCE
                        fluxoTexto.Close()

                        'Decodificar XML
                        Dim encoding__1 = Encoding.GetEncoding("ISO-8859-1")
                        Dim sr = New StreamReader(CUPOM, encoding__1)
                        Dim bytes = encoding__1.GetBytes(System.Web.HttpUtility.HtmlDecode(sr.ReadToEnd()))
                        Dim ms = New MemoryStream(bytes)
                        Dim ds = New DataSet()
                        'Ler Xml
                        ds.ReadXml(ms)

                        'Coletar dados
                        Dim bdsAux As New BindingSource
                        bdsAux.DataSource = ds

                        'Coletar Chave
                        bdsAux.DataMember = "infNFe"
                        nfce_Chave.Text = bdsAux.Current("Id").ToString.Substring(3, 44)
                        txVesaoNFCe.Text = bdsAux.Current("versao").ToString

                        'Coletar Data
                        bdsAux.DataMember = "ide"
                        nfce_Data.Text = Format(CDate(bdsAux.Current("dhEmi").ToString.Substring(0, 10)), "General Date")


                        ds.Dispose()
                        ms.Dispose()
                        bdsAux.Dispose()
                        sr.Close()

                        MES = nfce_Chave.Text.Substring(2, 4)
                        EMPRESA = nfce_Chave.Text.Substring(6, 14)

                        If Directory.Exists(txDestinoNFCe.Text & MES & "\") = False Then
                            Directory.CreateDirectory(txDestinoNFCe.Text & MES)
                        End If
                        If Directory.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\") = False Then
                            Directory.CreateDirectory(txDestinoNFCe.Text & MES & "\" & EMPRESA)
                        End If
                        If Directory.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\PROC\") = False Then
                            Directory.CreateDirectory(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\PROC\")
                        End If

                        Try
                            If File.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & "\PROC\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml") = True Then
                                File.Delete(CUPOM)
                            Else
                                fluxoTexto.Close()

                                If chbContabilidade.Checked = True Then

                                    If Funcoes.ENVIAR_XML_POST(txLinkContabiliade.Text, CUPOM) Like "*Importado com sucesso*" Then
                                        lstArquivos.Items.Add("Arquivo " & CUPOM & " Enviado para Contabilidade")
                                    Else
                                        Funcoes.GRAVAR_LOG(Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & ".txt", txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & "\PROC\" & "PROC_" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml")
                                    End If

                                End If

                                File.Move(CUPOM, txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & "\PROC\" & "PROC_" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml")
                                lstArquivos.Items.Add("Movido NFC-e Distribruição - " & System.IO.Path.GetFileName(CUPOM))
                                MemoryManagement.FlushMemory()
                                Application.DoEvents()
                                Continue For
                            End If
                        Catch

                        Finally

                        End Try

                    ElseIf linhaTexto Like "*soap12:Envelope*" Then      'Ler Arquivo Normal
                        If linhaTexto Like "*<enviNFe*" Then
                            If linhaTexto Like "*<infNFe*" Then
                                fluxoTexto.Close()
                                If chbXmlContabilidade.Checked = True Then
                                    CONVERTER_XML_NORMAL()
                                End If
                                'Importar NFC-e
                                If chbImportarNFCe.Checked = True Then
                                    IMPORTAR_NFCE()
                                End If
                                Try
                                    lstArquivos.Items.Add("NFC-e Importado - " & System.IO.Path.GetFileName(CUPOM))
                                Catch

                                End Try
                                With lstArquivos
                                    .ForeColor = Color.DarkGreen
                                End With

                            End If

                            fluxoTexto.Close()

                            If chbXmlContabilidade.Checked = False Then

                                MES = nfce_Chave.Text.Substring(2, 4)
                                EMPRESA = nfce_Chave.Text.Substring(6, 14)

                                'Verificar Pasta Destino NFCE

                                If Directory.Exists(txDestinoNFCe.Text & MES & "\") = False Then
                                    Directory.CreateDirectory(txDestinoNFCe.Text & MES)
                                End If
                                If Directory.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\") = False Then
                                    Directory.CreateDirectory(txDestinoNFCe.Text & MES & "\" & EMPRESA)
                                End If

                                Try
                                    If File.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml") = True Then
                                        File.Delete(CUPOM)
                                    Else

                                        If chbContabilidade.Checked = True Then

                                            If Funcoes.ENVIAR_XML_POST(txLinkContabiliade.Text, CUPOM) Like "*Importado com sucesso*" Then
                                                lstArquivos.Items.Add("Arquivo " & CUPOM & " Enviado para Contabilidade")
                                            Else
                                                Funcoes.GRAVAR_LOG(Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & ".txt", txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml")
                                            End If

                                        End If

                                        File.Move(CUPOM, txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml")
                                        MemoryManagement.FlushMemory()
                                        Application.DoEvents()
                                        Continue For
                                    End If
                                Catch

                                Finally

                                End Try

                            Else
                                Try
                                    File.Delete(CUPOM)
                                    MemoryManagement.FlushMemory()
                                    Application.DoEvents()
                                    Continue For
                                Catch

                                End Try
                            End If
                        End If

                    ElseIf linhaTexto Like "*<infNFe*" Then          'Ler Contigencia 1 
                        fluxoTexto.Close()
                        If chbXmlContabilidade.Checked = True Then
                            CONVERTER_XML_CONTIGENCIA()
                        End If

                        'Importar NFC-e
                        If chbImportarNFCe.Checked = True Then
                            IMPORTAR_NFCE()
                        End If

                        Try
                            lstArquivos.Items.Add("NFC-e Importado - " & System.IO.Path.GetFileName(CUPOM))
                        Catch
                        End Try
                        With lstArquivos
                            .ForeColor = Color.DarkGreen
                        End With

                        If chbXmlContabilidade.Checked = False Then
                            MES = nfce_Chave.Text.Substring(2, 4)
                            EMPRESA = nfce_Chave.Text.Substring(6, 14)

                            'Verificar Pasta Destino NFCE

                            If Directory.Exists(txDestinoNFCe.Text & MES & "\") = False Then
                                Directory.CreateDirectory(txDestinoNFCe.Text & MES)
                            End If
                            If Directory.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\") = False Then
                                Directory.CreateDirectory(txDestinoNFCe.Text & MES & "\" & EMPRESA)
                            End If

                            Try
                                If File.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml") = True Then
                                    File.Delete(CUPOM)
                                Else

                                    If chbContabilidade.Checked = True Then

                                        If Funcoes.ENVIAR_XML_POST(txLinkContabiliade.Text, CUPOM) Like "*Importado com sucesso*" Then
                                            lstArquivos.Items.Add("Arquivo " & CUPOM & " Enviado para Contabilidade")
                                        Else
                                            Funcoes.GRAVAR_LOG(Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & ".txt", txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml")
                                        End If

                                    End If

                                    File.Move(CUPOM, txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml")
                                    MemoryManagement.FlushMemory()
                                    Application.DoEvents()
                                    Continue For
                                End If
                            Catch

                            End Try
                        Else
                            Try
                                File.Delete(CUPOM)
                                MemoryManagement.FlushMemory()
                                Application.DoEvents()
                                Continue For
                            Catch

                            End Try
                        End If

                    Else
                        linhaTexto = fluxoTexto.ReadLine
                        If linhaTexto Like "*<infNFe*" Then          'Ler Contigencia 2 
                            fluxoTexto.Close()
                            If chbXmlContabilidade.Checked = True Then
                                CONVERTER_XML_CONTIGENCIA()
                            End If
                            'Importar NFC-e
                            If chbImportarNFCe.Checked = True Then
                                IMPORTAR_NFCE()
                            End If

                            Try
                                lstArquivos.Items.Add("NFC-e Importado - " & System.IO.Path.GetFileName(CUPOM))
                            Catch
                            End Try

                            With lstArquivos
                                .ForeColor = Color.DarkGreen
                            End With

                            If chbXmlContabilidade.Checked = False Then
                                MES = nfce_Chave.Text.Substring(2, 4)
                                EMPRESA = nfce_Chave.Text.Substring(6, 14)

                                'Verificar Pasta Destino NFCE

                                If Directory.Exists(txDestinoNFCe.Text & MES & "\") = False Then
                                    Directory.CreateDirectory(txDestinoNFCe.Text & MES)
                                End If
                                If Directory.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\") = False Then
                                    Directory.CreateDirectory(txDestinoNFCe.Text & MES & "\" & EMPRESA)
                                End If

                                Try
                                    If File.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml") = True Then
                                        File.Delete(CUPOM)
                                    Else

                                        If chbContabilidade.Checked = True Then

                                            If Funcoes.ENVIAR_XML_POST(txLinkContabiliade.Text, CUPOM) Like "*Importado com sucesso*" Then
                                                lstArquivos.Items.Add("Arquivo " & CUPOM & " Enviado para Contabilidade")
                                            Else
                                                Funcoes.GRAVAR_LOG(Now.Day.ToString.PadLeft(2, "0") & Now.Month.ToString.PadLeft(2, "0") & Now.Year & ".txt", txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml")
                                            End If

                                        End If

                                        File.Move(CUPOM, txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(nfce_Data.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & nfce_Chave.Text & "-" & nfce_Chave.Text.Substring(28, 6) & ".xml")
                                        MemoryManagement.FlushMemory()
                                        Application.DoEvents()
                                        Continue For
                                    End If
                                Catch

                                End Try

                            Else
                                Try
                                    File.Delete(CUPOM)
                                    MemoryManagement.FlushMemory()
                                    Application.DoEvents()
                                    Continue For
                                Catch
                                End Try
                            End If

                        End If
LZ_SALE:                If linhaTexto Like "*<SALE>*" Then           'Ler Aqruivo de Vendas

                            'Chamar Função Mover vendas
                            fluxoTexto.Close()

                            'Retirar Tag 191 repetida
                            'Dim xml_retorno As New XmlDocument
                            'xml_retorno.LoadXml(CLASSE_TXT.Verificar_CARACTER_ESPECIAL(CUPOM))
                            'xml_retorno.Save("c:\teste\SALE_TMP.xml")

                            'CLASSE_TXT.Gravar_XML(CLASSE_TXT.Verificar_CARACTER_ESPECIAL(CUPOM), "c:\teste\SALE_TMP.xml")

                            CLASSE_TXT.Verificar_CARACTER_ESPECIAL(CUPOM)

                            'System.IO.Path.GetFileName(CUPOM)
                            If CLASSE_TXT.LER_SALE("c:\teste\SALE_TMP.xml", CUPOM, txDestinoErro.Text) = True Then
                                If CONTINUAR = True Then
                                    'File.Move(CUPOM, "c:\teste\SALE_TMP.xml")
                                    File.Delete(CUPOM)

                                    'Dim XML_VERIFICADO As New XmlDocument
                                    'XML_VERIFICADO.LoadXml(CLASSE_TXT.Verificar_XML("c:\teste\SALE_TMP.xml"))
                                    'XML_VERIFICADO.Save("c:\teste\SALE_TMP.xml")

                                    'Dim a As String
                                    'a = CLASSE_TXT.Verificar_XML("c:\teste\SALE_TMP.xml")
                                    'Retira os ENTERS
                                    'a = Regex.Replace(a, "(\r\n)+", "")
                                    'Retira os Espacos
                                    'a = Regex.Replace(a, " +", " ").Replace("> <", "><")

                                    'CLASSE_TXT.Gravar_XML(a, "c:\teste\SALE_TMP.xml")

                                    CLASSE_TXT.Gravar_XML(CLASSE_TXT.Verificar_XML("c:\teste\SALE_TMP.xml"), "c:\teste\SALE_TMP.xml")

                                    File.Move("c:\teste\SALE_TMP.xml", CUPOM)
                                Else
                                    lstArquivos.Items.Add("Erro:" & System.IO.Path.GetFileName(CUPOM))
                                    MemoryManagement.FlushMemory()
                                    Application.DoEvents()
                                    Continue For
                                End If
                            Else
                                If CONTINUAR = True Then
                                    File.Delete(CUPOM)
                                    File.Move("c:\teste\SALE_TMP.xml", CUPOM)
                                Else
                                    lstArquivos.Items.Add("Erro:" & System.IO.Path.GetFileName(CUPOM))
                                    MemoryManagement.FlushMemory()
                                    Application.DoEvents()
                                    Continue For
                                End If
                            End If


                            Try
                                If chbEntrega.Checked = True Then
                                    'Importar Entregas

                                    Try
                                        Funcoes.lerXml(CUPOM, lstArquivos, txBancoEntregaFdb.Text)
                                    Catch ex As Exception
                                        lstArquivos.Items.Add("Erro:" & ex.ToString)
                                    End Try
                                End If

                                'lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))
                            Catch ex As Exception
                                lstArquivos.Items.Add("Erro:" & ex.ToString)
                            End Try


                            With lstArquivos
                                .ForeColor = Color.DarkGreen
                            End With

                            If File.Exists(txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM)) = True Then
                                lstArquivos.Items.Add("Ticket Duplicado Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Delete(CUPOM)
                                Catch

                                End Try
                            Else
                                lstArquivos.Items.Add("Movido Vendas - " & System.IO.Path.GetFileName(CUPOM))

                                If chbScannTech.Checked = True Then
                                    Try
                                        File.Copy(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                        File.Move(CUPOM, txDestinoScannTech.Text & System.IO.Path.GetFileName(CUPOM))
                                    Catch

                                    End Try

                                ElseIf chbScannTech.Checked = False Then
                                    Try
                                        File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                    Catch
                                    End Try
                                End If

                            End If

                        ElseIf linhaTexto Like "*<Z_REPORT>*" Then   'Ler Redução Z
                            'Chamar Função importar Z
                            If chbImportaRz.Checked = True Then
                                LER_XML_RZ()
                            End If
                            fluxoTexto.Close()
                            Try
                                lstArquivos.Items.Add("Importado Redução Z - " & System.IO.Path.GetFileName(CUPOM))
                            Catch ex As Exception

                            End Try
                            With lstArquivos
                                .ForeColor = Color.DarkGreen
                            End With
                            'Selecioner tipo de conexão para importação
                            If Movimento.Text > 0 Then
                                If rbConexaoNormal.Checked = True Then
                                    IMPORTAR_RZ_FiREBIRD()
                                ElseIf rbConexaoODBC.Checked = True Then
                                    IMPORTAR_RZ_ODBC()
                                End If
                            End If
                            If File.Exists(txDestinoZ.Text & System.IO.Path.GetFileName(CUPOM)) = True Then
                                lstArquivos.Items.Add("Ticket Duplicado Redução Z - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Delete(CUPOM)
                                Catch

                                End Try
                            Else
                                lstArquivos.Items.Add("Importado Redução Z - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Move(CUPOM, txDestinoZ.Text & System.IO.Path.GetFileName(CUPOM))
                                Catch
                                End Try
                            End If

                        ElseIf linhaTexto Like "*<RETURN>*" Then   'Ler Devolução
                            'Chamar Função Mover Devolução de Vendas
                            fluxoTexto.Close()
                            Try
                                lstArquivos.Items.Add("Importado Devolução - " & System.IO.Path.GetFileName(CUPOM))
                            Catch ex As Exception

                            End Try


                            With lstArquivos
                                .ForeColor = Color.DarkGreen
                            End With

                            If File.Exists(txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM)) = True Then
                                lstArquivos.Items.Add("Ticket Duplicado Devolução Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Delete(CUPOM)
                                Catch
                                End Try
                            Else
                                'Importar Devolução

                                '################### Inicio Importação Vendas Canceladas #######################

                                'Decodificar XML
                                Dim encoding__1 = Encoding.GetEncoding("ISO-8859-1")
                                Dim sr = New StreamReader(CUPOM, encoding__1)
                                Dim bytes = encoding__1.GetBytes(System.Web.HttpUtility.HtmlDecode(sr.ReadToEnd()))
                                Dim ms = New MemoryStream(bytes)
                                Dim ds = New DataSet()

                                'Carregar XML
                                ds.ReadXml(ms)

                                'Dim xmlDoc As New XmlDocument
                                'xmlDoc.Load(CUPOM)
                                'Dim Ds As New DataSet
                                'Ds.ReadXml(CUPOM)

                                Dim bdsAux As New BindingSource
                                bdsAux.DataSource = ds
                                bdsAux.DataMember = "RETURN"
                                Dim I, G, v As Integer
                                DataGridView1.Rows.Clear()
                                'Try
                                With DataGridView1
                                    .Rows.Clear()
                                    .Rows.Add()
                                    .Rows(.Rows.Count - 1).Cells("LOJA_VENDAS").Value = bdsAux.Current("STORE").ToString
                                    .Rows(.Rows.Count - 1).Cells("PDV").Value = bdsAux.Current("POS").ToString
                                    .Rows(.Rows.Count - 1).Cells("TICKET_DEVOLUCAO").Value = bdsAux.Current("TICKET").ToString
                                    .Rows(.Rows.Count - 1).Cells("DATA").Value = Format(CInt(bdsAux.Current("FISCAL_DAY").ToString), "0000-00-00")
                                    .Rows(.Rows.Count - 1).Cells("NUMERO_DEVOLUCAO").Value = bdsAux.Current("RETURN_NUMBER").ToString
                                    .Rows(.Rows.Count - 1).Cells("TRANSACAO_VENDAS").Value = "50"
                                    .Rows(.Rows.Count - 1).Cells("REASON").Value = .Rows(.Rows.Count - 1).Cells("NUMERO_DEVOLUCAO").Value
                                    .Rows(.Rows.Count - 1).Cells("MAKER").Value = "0"

                                    bdsAux.DataSource = ds
                                    bdsAux.DataMember = "ITEM"

                                    .Rows(.Rows.Count - 1).Cells("SEQUENCIA").Value = bdsAux.Current("SEQ").ToString
                                    .Rows(.Rows.Count - 1).Cells("PLU").Value = bdsAux.Current("ID").ToString
                                    .Rows(.Rows.Count - 1).Cells("DESCRICAO").Value = bdsAux.Current("DESCRIPTION").ToString
                                    .Rows(.Rows.Count - 1).Cells("VALOR").Value = Replace(Format(CDbl(bdsAux.Current("AMOUNT").ToString.Replace(".", ",")), "0.000"), ",", ".")
                                    .Rows(.Rows.Count - 1).Cells("QUANTIDADE").Value = bdsAux.Current("QTY").ToString
                                    .Rows(.Rows.Count - 1).Cells("DEPARTAMENTO").Value = bdsAux.Current("DEPT_ID").ToString * 1

                                    For I = 1 To bdsAux.List.Count - 1
                                        bdsAux.MoveNext()

                                        'For v = 0 To .Rows.Count - 1
                                        .Rows.Add()
                                        .Rows(.Rows.Count - 1).Cells("LOJA_VENDAS").Value = .Rows(.Rows.Count - 2).Cells("LOJA_VENDAS").Value
                                        .Rows(.Rows.Count - 1).Cells("PDV").Value = .Rows(.Rows.Count - 2).Cells("PDV").Value
                                        .Rows(.Rows.Count - 1).Cells("TICKET_DEVOLUCAO").Value = .Rows(.Rows.Count - 2).Cells("TICKET_DEVOLUCAO").Value
                                        .Rows(.Rows.Count - 1).Cells("DATA").Value = .Rows(.Rows.Count - 2).Cells("DATA").Value
                                        .Rows(.Rows.Count - 1).Cells("NUMERO_DEVOLUCAO").Value = .Rows(.Rows.Count - 2).Cells("NUMERO_DEVOLUCAO").Value
                                        .Rows(.Rows.Count - 1).Cells("TRANSACAO_VENDAS").Value = "50"
                                        .Rows(.Rows.Count - 1).Cells("REASON").Value = .Rows(.Rows.Count - 2).Cells("NUMERO_DEVOLUCAO").Value
                                        .Rows(.Rows.Count - 1).Cells("MAKER").Value = "0"

                                        .Rows(.Rows.Count - 1).Cells("SEQUENCIA").Value = bdsAux.Current("SEQ").ToString
                                        .Rows(.Rows.Count - 1).Cells("PLU").Value = bdsAux.Current("ID").ToString
                                        .Rows(.Rows.Count - 1).Cells("DESCRICAO").Value = bdsAux.Current("DESCRIPTION").ToString
                                        .Rows(.Rows.Count - 1).Cells("VALOR").Value = Replace(Format(CDbl(bdsAux.Current("AMOUNT").ToString.Replace(".", ",")), "0.000"), ",", ".")
                                        .Rows(.Rows.Count - 1).Cells("QUANTIDADE").Value = bdsAux.Current("QTY").ToString
                                        .Rows(.Rows.Count - 1).Cells("DEPARTAMENTO").Value = bdsAux.Current("DEPT_ID").ToString * 1
                                        'Next
                                    Next

                                    Dim valor As Double = 0

                                    TABELA = "sale"
                                    Dim SqlEmporium As String = ""

                                    For G = 0 To .Rows.Count - 1


                                        'Iniciar Script

                                        SqlEmporium = "INSERT INTO accum_returned_item_reason " _
                                        & "(store_key,pos_number,fiscal_date,plu_id,desc_plu,quantity,amount," _
                                        & "department_key,maker_key,reason,transaction) " _
                                        & "VALUES ('" _
                                        & .Rows(.Rows.Count - 1).Cells("LOJA_VENDAS").Value _
                                        & "','" & .Rows(G).Cells("PDV").Value _
                                        & "','" & .Rows(G).Cells("DATA").Value _
                                        & "','" & .Rows(G).Cells("PLU").Value _
                                        & "','" & .Rows(G).Cells("DESCRICAO").Value _
                                        & "','" & .Rows(G).Cells("QUANTIDADE").Value _
                                        & "','" & .Rows(G).Cells("VALOR").Value _
                                        & "','" & .Rows(G).Cells("DEPARTAMENTO").Value _
                                        & "','" & .Rows(G).Cells("MAKER").Value _
                                        & "',(SELECT coalesce (max(reason)+1,null,1) from accum_returned_item_reason as reason)" _
                                        & ",'" & .Rows(G).Cells("TRANSACAO_VENDAS").Value _
                                        & "');"

                                        CONEXAO_MySQL(SqlEmporium)

                                        valor = valor + (.Rows(G).Cells("VALOR").Value.Replace(".", ",")) * 1
                                    Next

                                    SqlEmporium = ""
                                    SqlEmporium = "update sale set amount_due = '" & Format(valor, "0.000").Replace(",", ".") _
                                    & "' where store_key = " & .Rows(0).Cells("LOJA_VENDAS").Value _
                                    & " and ticket_number=" & .Rows(0).Cells("TICKET_DEVOLUCAO").Value _
                                    & " and start_time between '" & .Rows(0).Cells("DATA").Value & " 00:00:00'" _
                                    & " and '" & .Rows(0).Cells("DATA").Value & " 23:59:59'" _
                                    & " and sale_type = 50"

                                    CONEXAO_MySQL(SqlEmporium)

                                End With

                                sr.Dispose()
                                ds.Dispose()
                                ms.Dispose()

                                '################### Fim Importação Vendas Canceladas #######################

                                lstArquivos.Items.Add("Movido Devolução de Vendas - " & System.IO.Path.GetFileName(CUPOM))
                                Try
                                    File.Move(CUPOM, txDestinoVendas.Text & System.IO.Path.GetFileName(CUPOM))
                                Catch

                                End Try

                                'Catch
                                'lstArquivos.Items.Add(Err.Description)
                                'End Try
                            End If

                        End If
                    End If
                    Application.DoEvents()
                    fluxoTexto.Close()
                    File.Delete(CUPOM)
                Else
                    'MessageBox.Show("Arquivo não existe")
                End If
                MemoryManagement.FlushMemory()
                '==================Fim Verificação ===========================

            End If
            If lstArquivos.Items.Count >= 10 Then
                lstArquivos.Items.Clear()
            End If
        Next
        MemoryManagement.FlushMemory()
        Label63.Text = "Último arquivo lido: " & System.IO.Path.GetFileName(CUPOM)
        Application.DoEvents()
        tmInicioImportacao.Start()
    End Sub

    Sub LER_XML_RZ()
        Try
            'Cria uma instância de um documento XML
            Dim oXML As New XmlDocument
            Dim oNoLista As XmlNodeList
            Dim oNo As XmlNode

            'Define o caminho do arquivo XML 
            Dim ArquivoXML As String = CUPOM
            Dim SQL As String

            'Consulta qual tipo de conexão com o banco (NORMAL ou ODBC)
            If rbConexaoNormal.Checked = True Then
                'Selecionar numero transação
                TABELA = "teo_contador"
                SQL = "select * from teo_contador tc" _
                       & " where tc.contador_tabela in('TRANSACAO')" _
                       & " and tc.empresa_codigo='0'"
                CONEXAO_FIREBIRD(SQL)
                transacao.Text = (fs.Tables(TABELA).Rows(0)("CONTADOR_CODIGO").ToString())

                'Atualizar numero transação
                TABELA = "teo_contador"
                SQL = "UPDATE TEO_CONTADOR tc SET tc.CONTADOR_CODIGO=tc.contador_codigo +1" _
                       & " where tc.contador_tabela in('TRANSACAO')" _
                       & " and tc.empresa_codigo='0'"
                CONEXAO_FIREBIRD(SQL)

                'Selecionar numero mapa
                TABELA = "teo_contador"
                SQL = "select * from teo_contador tc" _
                       & " where tc.contador_tabela in('MAPA_FISCAL_ECF')" _
                       & " and tc.empresa_codigo='0'"
                CONEXAO_FIREBIRD(SQL)
                mapa_numero.Text = (fs.Tables(TABELA).Rows(0)("CONTADOR_CODIGO").ToString())

                'Atualizar Contador MAPA FISCAL
                TABELA = "teo_contador"
                SQL = "update teo_contador tc set tc.contador_codigo=tc.contador_codigo + 1" _
                & " where tc.contador_tabela in('MAPA_FISCAL_ECF')" _
                & " and tc.empresa_codigo='0'"
                CONEXAO_FIREBIRD(SQL)

            ElseIf rbConexaoODBC.Checked = True Then

                'Selecionar numero transação
                TABELA = "teo_contador"
                SQL = "select * from teo_contador tc" _
                       & " where tc.contador_tabela in('TRANSACAO')" _
                       & " and tc.empresa_codigo='0'"
                CONEXAO_ODBC(SQL)
                transacao.Text = (Ods.Tables(TABELA).Rows(0)("CONTADOR_CODIGO").ToString())

                'Atualizar numero transação
                TABELA = "teo_contador"
                SQL = "UPDATE TEO_CONTADOR tc SET tc.CONTADOR_CODIGO=tc.contador_codigo +1" _
                       & " where tc.contador_tabela in('TRANSACAO')" _
                       & " and tc.empresa_codigo='0'"
                CONEXAO_ODBC(SQL)

                'Selecionar numero mapa
                TABELA = "teo_contador"
                SQL = "select * from teo_contador tc" _
                       & " where tc.contador_tabela in('MAPA_FISCAL_ECF')" _
                       & " and tc.empresa_codigo='0'"
                CONEXAO_ODBC(SQL)
                mapa_numero.Text = (Ods.Tables(TABELA).Rows(0)("CONTADOR_CODIGO").ToString())

                'Atualizar Contador MAPA FISCAL
                TABELA = "teo_contador"
                SQL = "update teo_contador tc set tc.contador_codigo=tc.contador_codigo + 1" _
                & " where tc.contador_tabela in('MAPA_FISCAL_ECF')" _
                & " and tc.empresa_codigo='0'"
                CONEXAO_ODBC(SQL)

            End If

            '*******Verificar se no Xml existe caracter expecial-------------

            Dim fluxoTexto As IO.StreamReader
            Dim linhaTexto As String
            Using leitor As New TextFieldParser(CUPOM)
                'Dim linhaAtual As String()
                'Dim i As Integer = 0
                'Dim r As Integer = 1
                'Dim texto As String
                Dim fluxoTextogravado As IO.StreamWriter
                'Dim linhaTextogravado As String

                'Informamos que será importado com Delimitação
                leitor.TextFieldType = FileIO.FieldType.Delimited
                fluxoTextogravado = New IO.StreamWriter("c:\TESTE\temp.xml", False)

                If IO.File.Exists(CUPOM) Then
                    fluxoTexto = New IO.StreamReader(CUPOM)
                    linhaTexto = fluxoTexto.ReadLine
                    While linhaTexto <> Nothing
                        'linhaAtual = leitor.ReadFields()
                        If linhaTexto Like "*<MFD_ADITIONAL>*" Then
                            linhaTexto = fluxoTexto.ReadLine
                        Else
                            fluxoTextogravado.WriteLine(Replace(linhaTexto, "&", "E"))
                            linhaTexto = fluxoTexto.ReadLine
                        End If

                    End While
                    fluxoTextogravado.Close()
                    fluxoTexto.Close()
                Else
                    'MessageBox.Show("Arquivo não existe")
                    fluxoTextogravado.Close()
                    fluxoTexto.Close()
                End If
            End Using

            '*******Fim virificar Xml-------------

            LIMPAR_DADOS()

            If chbRzEmporium.Checked = True Then
                Dim dados_z() As String
                Dim valor As String = ""
                dados_z = Funcoes.RZ_EMPORIUM_CABECALHO_XML("c:\teste\temp.xml").ToString.Split("|")
                valor = Replace(Funcoes.pegarValorRz(dados_z(0), dados_z(1), dados_z(2)), ",", ".")

                If valor = False Then
                    valor = 0
                End If

                loja.Text = dados_z(0)
                data_fiscal.Text = dados_z(2)
                caixa.Text = dados_z(1)
                hora.Text = dados_z(3)
                ticket_inicial.Text = dados_z(4)
                ticket_final.Text = dados_z(5)

                Movimento.Text = valor
                isento.Text = valor

                Exit Sub
            End If


            'oXML.Load(ArquivoXML)
            oXML.Load("c:\teste\temp.xml")

            'RECUPERA ALIQUOTA
            oNoLista = oXML.SelectNodes("/Z_REPORT/TAXES/PERCENT")
            For Each oNo In oNoLista
                lstDados.Items.Add(oNo.ChildNodes.Item(0).InnerText)

            Next
            Dim i As Integer = 0
            For Each oNo In oNoLista
                For i = 0 To 7
                    If i <= lstDados.Items.Count - 1 Then
                        If i = 0 Then
                            A1.Text = Replace(oNo.ChildNodes.Item(0).InnerText, ".", ",")
                        End If
                        If i = 1 Then
                            A2.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 2 Then
                            A3.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 3 Then
                            A4.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 4 Then
                            A5.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 5 Then
                            A6.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 6 Then
                            A7.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 7 Then
                            A8.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                    End If
                Next
            Next
            lstDados.Items.Clear()


            'RECUPERA BASE
            oNoLista = oXML.SelectNodes("/Z_REPORT/TAXES/AMOUNT")
            For Each oNo In oNoLista
                ' lstDados.Items.Add(oNo.Attributes.GetNamedItem("ID").Value)
                lstDados.Items.Add(oNo.ChildNodes.Item(0).InnerText)
            Next
            i = 0
            For Each oNo In oNoLista
                For i = 0 To 7
                    If i <= lstDados.Items.Count - 1 Then
                        If i = 0 Then
                            B1.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 1 Then
                            B2.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 2 Then
                            B3.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 3 Then
                            B4.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 4 Then
                            B5.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 5 Then
                            B6.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 6 Then
                            B7.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 7 Then
                            B8.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                    End If
                Next
            Next
            lstDados.Items.Clear()

            'RECUPERA IMPOSTO
            oNoLista = oXML.SelectNodes("/Z_REPORT/TAXES/TAX_AMOUNT")
            For Each oNo In oNoLista
                ' lstDados.Items.Add(oNo.Attributes.GetNamedItem("ID").Value)
                lstDados.Items.Add(oNo.ChildNodes.Item(0).InnerText)
            Next
            i = 0
            For Each oNo In oNoLista
                For i = 0 To 7
                    If i <= lstDados.Items.Count - 1 Then
                        If i = 0 Then
                            I1.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 1 Then
                            I2.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 2 Then
                            I3.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 3 Then
                            I4.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 4 Then
                            I5.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 5 Then
                            I6.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 6 Then
                            I7.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                        If i = 7 Then
                            I8.Text = Replace(lstDados.Items(i).ToString(), ".", ",")
                        End If
                    End If
                Next
            Next
            lstDados.Items.Clear()


            'RECUPERA NUMERO DA LOJA
            oNoLista = oXML.SelectNodes("/Z_REPORT/STORE")
            For Each oNo In oNoLista
                loja.Text = Format(CInt(oNo.ChildNodes.Item(0).InnerText), "0000")
            Next

            'RECUPERA NUMERO DE IMPRESSORA
            oNoLista = oXML.SelectNodes("/Z_REPORT/FISCAL_POS")
            For Each oNo In oNoLista
                caixa.Text = Format(CInt(oNo.ChildNodes.Item(0).InnerText), "0000")
            Next

            'RECUPERA FABRICANTE DA IMPRESSORA
            oNoLista = oXML.SelectNodes("/Z_REPORT/MFD_ECF_BRAND")
            For Each oNo In oNoLista
                fabricante.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA NUMERO DE SERIE DA IMPRESSORA
            oNoLista = oXML.SelectNodes("/Z_REPORT/ECF_SERIAL_NUMBER")
            For Each oNo In oNoLista
                impressora.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA DATA FISCAL
            Try
                oNoLista = oXML.SelectNodes("/Z_REPORT/FISCAL_DAY")
                For Each oNo In oNoLista
                    data_fiscal.Text = Format(CDate(Format(CInt(oNo.ChildNodes.Item(0).InnerText), "####/##/##")), "dd/MM/yyyy")
                Next
            Catch
                oNoLista = oXML.SelectNodes("/Z_REPORT/FISCAL_TIME")
                For Each oNo In oNoLista
                    data_fiscal.Text = Format(CDate(Format(CInt(Microsoft.VisualBasic.Left(oNo.ChildNodes.Item(0).InnerText, 8)), "####/##/##")), "dd/MM/yyyy")
                Next
            End Try

            'RECUPERA HORA
            oNoLista = oXML.SelectNodes("/Z_REPORT/FISCAL_TIME")
            For Each oNo In oNoLista
                '   hora.Text = Format(CInt(Replace(oNo.ChildNodes.Item(0).InnerText, data_fiscal.Text, "")), "##:##:##")
                hora.Text = Format(CInt(Microsoft.VisualBasic.Right(oNo.ChildNodes.Item(0).InnerText, 6)), "##:##:##")
            Next

            'RECUPERA TICKET INICIAL
            oNoLista = oXML.SelectNodes("/Z_REPORT/INITIAL_TICKET")
            For Each oNo In oNoLista
                ticket_inicial.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA TICKET FINAL
            oNoLista = oXML.SelectNodes("/Z_REPORT/TICKET")
            For Each oNo In oNoLista
                ticket_final.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA GT INICIAL
            oNoLista = oXML.SelectNodes("/Z_REPORT/INITIAL_GT")
            For Each oNo In oNoLista
                gt_inicial.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA GT FINAL
            oNoLista = oXML.SelectNodes("/Z_REPORT/FINAL_GT")
            For Each oNo In oNoLista
                gt_final.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA CRZ
            oNoLista = oXML.SelectNodes("/Z_REPORT/Z_REPORT_NUMBER")
            For Each oNo In oNoLista
                crz.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA CRO
            oNoLista = oXML.SelectNodes("/Z_REPORT/CRO")
            For Each oNo In oNoLista
                cro.Text = Format(CInt(oNo.ChildNodes.Item(0).InnerText), "0000")
            Next

            'RECUPERA CRO
            oNoLista = oXML.SelectNodes("/Z_REPORT/FISCAL_REPAIR_NUMBER")
            For Each oNo In oNoLista
                cro.Text = Format(CInt(oNo.ChildNodes.Item(0).InnerText), "0000")
            Next

            'RECUPERA CANCELAMENTO
            oNoLista = oXML.SelectNodes("/Z_REPORT/AMOUNT_VOID")
            For Each oNo In oNoLista
                cancelamento.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA DESCONTO
            oNoLista = oXML.SelectNodes("/Z_REPORT/AMOUNT_DISCOUNT")
            For Each oNo In oNoLista
                desconto.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA SANGRIA
            oNoLista = oXML.SelectNodes("/Z_REPORT/AMOUNT_PICKUP")
            For Each oNo In oNoLista
                sangria.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA ACRESCIMO
            oNoLista = oXML.SelectNodes("/Z_REPORT/AMOUNT_INCREASE")
            For Each oNo In oNoLista
                Acrescimo.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA ISENTO
            oNoLista = oXML.SelectNodes("/Z_REPORT/ISENTO")
            For Each oNo In oNoLista
                isento.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA NAO TRIBUTADO
            oNoLista = oXML.SelectNodes("/Z_REPORT/NAO_TRIBUTADO")
            For Each oNo In oNoLista
                naotributado.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            'RECUPERA SUBSTITUICAO
            oNoLista = oXML.SelectNodes("/Z_REPORT/SUBSTITUICAO")
            For Each oNo In oNoLista
                substituicao.Text = oNo.ChildNodes.Item(0).InnerText
            Next

            oNoLista = Nothing
            oXML = Nothing

            Movimento.Text = Format(((Replace(gt_final.Text, ".", ",") - Replace(gt_inicial.Text, ".", ",")) - Replace(cancelamento.Text, ".", ",")) - Replace(desconto.Text, ".", ","), "0.00")
            MemoryManagement.FlushMemory()
        Catch
            'MsgBox(Err.Description)
            lstArquivos.Items.Add(Err.Description)
        Finally

        End Try
    End Sub

    Sub IMPORTAR_CANCELADA_FiREBIRD(ticket As String)
        Dim SQL_CANCELADO As String
        SQL_CANCELADO = ""
        Dim RETORNO() As String
        Dim TRANSACAO As String
        RETORNO_CANCELADA = False

        Try
            'Localizar Cancelado
            RETORNO = (Funcoes.Localizar_Cancelado(ticket)).ToString.Split("|")

            Dim data As String = ""

            data = RETORNO(2).ToString.Substring(0, 4)
            data += "." & RETORNO(2).ToString.Substring(4, 2)
            data += "." & RETORNO(2).ToString.Substring(6, 2)

            SQL_CANCELADO = "select distinct m.transacao from movimento_itens_ms m
                            join movimento_itens_dt d on m.transacao=d.transacao
                            where d.movimento_numero_pedido='" & RETORNO(4).PadLeft(10, "0") & "'
                            and m.movimento_data_emissao='" & data & "'
                            and m.movimento_serie='" & RETORNO(1).PadLeft(3, "0") & "'
                            and m.empresa_movto='" & RETORNO(0).PadLeft(4, "0") & "'
                            and m.movimento_modelo='" & RETORNO(3) & "'
                            and m.movimento_origem_lancamento='V'"

            TABELA = "movimento_itens_ms"
            CONEXAO_FIREBIRD(SQL_CANCELADO)


            If fs.Tables(0).Rows.Count > 0 Then
                TRANSACAO = fs.Tables(0).Rows(0)(0).ToString
            Else
                RETORNO_CANCELADA = False
                Exit Sub
            End If


            'Atualizar Contador MAPA FISCAL
            TABELA = "movimento_itens_ms"
            SQL = "delete from movimento_itens_ms m where transacao=" & TRANSACAO & " " _
                    & "And m.movimento_data_emissao='" & data & "'
                    And m.movimento_serie='" & RETORNO(1).PadLeft(3, "0") & "'
                    And m.empresa_movto='" & RETORNO(0).PadLeft(4, "0") & "'
                    And m.movimento_modelo='" & RETORNO(3) & "'
                    And m.movimento_origem_lancamento='V'"

            CONEXAO_FIREBIRD(SQL)
            Label65.Text = "Cancelado:" & ticket
            RETORNO_CANCELADA = True
        Catch
            RETORNO_CANCELADA = False
            Label65.Text = "Erro a Excluir Nota:" & CUPOM
        End Try
        MemoryManagement.FlushMemory()
    End Sub

    Sub IMPORTAR_RZ_FiREBIRD()
        Dim SQL, SQL_TMP As String
        SQL = ""
        SQL_TMP = ""
        TOTAL_BASE()

        'Verificar tipo de importação Redução Z
        If chbRzConsul.Checked = False Then

            If chbRzRepetida.Checked = True Then
                'Importar Redução Por ECF
                SQL_TMP = "UPDATE OR INSERT INTO MAPA_FISCAL_ECF" _
        & "(EMPRESA_CODIGO,MAPA_NUMERO_CAIXA,MAPA_DATA,MAPA_NUMERO,MAPA_NUMERO_CUPOM_INICIAL,MAPA_NUMERO_CUPOM_FINAL," _
        & "MAPA_GRANDE_TOTAL,MAPA_NUMERO_REDUCOES,MAPA_CONTADOR_REINICIO_OPERACAO,MAPA_TOTAL_CANCELAMENTOS,MAPA_TOTAL_DESCONTOS," _
        & "MAPA_ALIQUOTA_01,MAPA_TOTALIZADOR_01,MAPA_IMPOSTO_01," _
        & "MAPA_ALIQUOTA_02,MAPA_TOTALIZADOR_02,MAPA_IMPOSTO_02," _
        & "MAPA_ALIQUOTA_03,MAPA_TOTALIZADOR_03,MAPA_IMPOSTO_03," _
        & "MAPA_ALIQUOTA_04,MAPA_TOTALIZADOR_04,MAPA_IMPOSTO_04," _
        & "MAPA_TOTAL_ISENTOS," _
        & "MAPA_TOTAL_SUBSTITUICAO," _
        & "MAPA_TOTAL_TOTALIZADOR," _
        & "MAPA_TOTAL_IMPOSTO," _
        & "MAPA_TOTAL_MOVIMENTO," _
        & "MAPA_REDUCAOZ_MAPA," _
        & "MAPA_DATA_LANCAMENTO," _
        & "MAPA_LIBERADO," _
        & "MAPA_TOTAL_SUPRIMENTO," _
        & "MAPA_TOTAL_NAO_INCIDENCIA," _
        & "TRANSACAO)" _
        & "VALUES" _
        & "('" & loja.Text & "','" & caixa.Text & "','" & Replace(data_fiscal.Text, "/", ".") & "','" & mapa_numero.Text & "','" & ticket_inicial.Text & "','" & ticket_final.Text & "','" _
        & gt_final.Text & "','" & crz.Text & "','" & cro.Text & "','" & cancelamento.Text & "','" & desconto.Text & "','" _
        & Replace(A1.Text, ",", ".") & "','" & Replace(B1.Text, ",", ".") & "','" & Replace(I1.Text, ",", ".") & "','" _
        & Replace(A2.Text, ",", ".") & "','" & Replace(B2.Text, ",", ".") & "','" & Replace(I2.Text, ",", ".") & "','" _
        & Replace(A3.Text, ",", ".") & "','" & Replace(B3.Text, ",", ".") & "','" & Replace(I3.Text, ",", ".") & "','" _
        & Replace(A4.Text, ",", ".") & "','" & Replace(B4.Text, ",", ".") & "','" & Replace(I4.Text, ",", ".") & "','" _
        & isento.Text & "','" _
        & substituicao.Text & "','" _
        & Replace(TOTALIZADOR, ",", ".") & "','" _
        & Replace(TOTAL_IMPOSTO, ",", ".") & "','" _
        & Replace(Movimento.Text, ",", ".") & "'," _
        & "'R','" _
        & Replace(data_fiscal.Text, "/", ".") & "'," _
        & "'N','" _
        & sangria.Text & "','" _
        & naotributado.Text & "','" _
        & transacao.Text & "')"
            Else
                'Importar Redução Por ECF
                SQL_TMP = "INSERT INTO MAPA_FISCAL_ECF" _
        & "(EMPRESA_CODIGO,MAPA_NUMERO_CAIXA,MAPA_DATA,MAPA_NUMERO,MAPA_NUMERO_CUPOM_INICIAL,MAPA_NUMERO_CUPOM_FINAL," _
        & "MAPA_GRANDE_TOTAL,MAPA_NUMERO_REDUCOES,MAPA_CONTADOR_REINICIO_OPERACAO,MAPA_TOTAL_CANCELAMENTOS,MAPA_TOTAL_DESCONTOS," _
        & "MAPA_ALIQUOTA_01,MAPA_TOTALIZADOR_01,MAPA_IMPOSTO_01," _
        & "MAPA_ALIQUOTA_02,MAPA_TOTALIZADOR_02,MAPA_IMPOSTO_02," _
        & "MAPA_ALIQUOTA_03,MAPA_TOTALIZADOR_03,MAPA_IMPOSTO_03," _
        & "MAPA_ALIQUOTA_04,MAPA_TOTALIZADOR_04,MAPA_IMPOSTO_04," _
        & "MAPA_TOTAL_ISENTOS," _
        & "MAPA_TOTAL_SUBSTITUICAO," _
        & "MAPA_TOTAL_TOTALIZADOR," _
        & "MAPA_TOTAL_IMPOSTO," _
        & "MAPA_TOTAL_MOVIMENTO," _
        & "MAPA_REDUCAOZ_MAPA," _
        & "MAPA_DATA_LANCAMENTO," _
        & "MAPA_LIBERADO," _
        & "MAPA_TOTAL_SUPRIMENTO," _
        & "MAPA_TOTAL_NAO_INCIDENCIA," _
        & "TRANSACAO)" _
        & "VALUES" _
        & "('" & loja.Text.PadLeft(4, "0") & "','" & caixa.Text.PadLeft(4, "0") & "','" & Replace(data_fiscal.Text, "-", ".") & "','" & mapa_numero.Text & "','" & ticket_inicial.Text & "','" & ticket_final.Text & "','" _
        & iif(gt_final.Text = "", 0, gt_final.Text) & "','" & crz.Text & "','" & cro.Text & "','" & replace(cancelamento.Text, ",", ".") & "','" & replace(desconto.Text, ",", ".") & "','" _
        & Replace(A1.Text, ",", ".") & "','" & Replace(B1.Text, ",", ".") & "','" & Replace(I1.Text, ",", ".") & "','" _
        & Replace(A2.Text, ",", ".") & "','" & Replace(B2.Text, ",", ".") & "','" & Replace(I2.Text, ",", ".") & "','" _
        & Replace(A3.Text, ",", ".") & "','" & Replace(B3.Text, ",", ".") & "','" & Replace(I3.Text, ",", ".") & "','" _
        & Replace(A4.Text, ",", ".") & "','" & Replace(B4.Text, ",", ".") & "','" & Replace(I4.Text, ",", ".") & "','" _
        & Replace(isento.Text, ",", ".") & "','" _
        & Replace(substituicao.Text, ",", ".") & "','" _
        & Replace(TOTALIZADOR, ",", ".") & "','" _
        & Replace(TOTAL_IMPOSTO, ",", ".") & "','" _
        & Replace(Movimento.Text, ",", ".") & "'," _
        & "'R','" _
        & Replace(data_fiscal.Text, "/", ".") & "'," _
        & "'N','" _
        & Replace(sangria.Text, ",", ".") & "','" _
        & Replace(naotributado.Text, ",", ".") & "','" _
        & transacao.Text & "')"
            End If

        ElseIf chbRzConsul.Checked = True Then
            'Importar Redução Consolidada
            'Verificar se existe registro no banco no periodo solicitado.

            Dim SQL_FINANCEIRO As String
            TABELA = "MAPA_FISCAL_ECF"
            SQL_FINANCEIRO = "select mf.empresa_codigo,mf.mapa_data,mf.mapa_numero_caixa " _
                             & "from mapa_fiscal_ecf mf where mf.mapa_data ='" _
                             & Format(CDate(Replace(data_fiscal.Text, "/", ".")), "yyyy.MM.dd") & "'" _
                             & " and mf.empresa_codigo='" & loja.Text & "'" _
                             & " and mf.mapa_numero_caixa ='" & txtEcfPadrao.Text & "'"
            'TextBox3.Text = SQL_FINANCEIRO
            'Exit Sub
            CONEXAO_FIREBIRD(SQL_FINANCEIRO)

            If fs.Tables(TABELA).Rows.Count = 0 Then
                SQL_TMP = "INSERT INTO MAPA_FISCAL_ECF" _
        & "(EMPRESA_CODIGO,MAPA_NUMERO_CAIXA,MAPA_DATA,MAPA_NUMERO,MAPA_NUMERO_CUPOM_INICIAL,MAPA_NUMERO_CUPOM_FINAL," _
        & "MAPA_GRANDE_TOTAL,MAPA_NUMERO_REDUCOES,MAPA_CONTADOR_REINICIO_OPERACAO,MAPA_TOTAL_CANCELAMENTOS,MAPA_TOTAL_DESCONTOS," _
        & "MAPA_ALIQUOTA_01,MAPA_TOTALIZADOR_01,MAPA_IMPOSTO_01," _
        & "MAPA_ALIQUOTA_02,MAPA_TOTALIZADOR_02,MAPA_IMPOSTO_02," _
        & "MAPA_ALIQUOTA_03,MAPA_TOTALIZADOR_03,MAPA_IMPOSTO_03," _
        & "MAPA_ALIQUOTA_04,MAPA_TOTALIZADOR_04,MAPA_IMPOSTO_04," _
        & "MAPA_TOTAL_ISENTOS," _
        & "MAPA_TOTAL_SUBSTITUICAO," _
        & "MAPA_TOTAL_TOTALIZADOR," _
        & "MAPA_TOTAL_IMPOSTO," _
        & "MAPA_TOTAL_MOVIMENTO," _
        & "MAPA_REDUCAOZ_MAPA," _
        & "MAPA_DATA_LANCAMENTO," _
        & "MAPA_LIBERADO," _
        & "MAPA_TOTAL_SUPRIMENTO," _
        & "MAPA_TOTAL_NAO_INCIDENCIA," _
        & "TRANSACAO)" _
        & "VALUES" _
        & "('" & loja.Text & "','" & txtEcfPadrao.Text & "','" & Replace(data_fiscal.Text, "/", ".") & "','" & mapa_numero.Text & "','" & "0" & "','" & "0" & "','" _
        & "0" & "','" & "0" & "','" & "0" & "','" & "0.00" & "','" & "0.00" & "','" _
        & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" _
        & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" _
        & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" _
        & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" _
        & Replace(Movimento.Text, ",", ".") & "','" _
        & "0.00" & "','" _
        & Replace("0.00", ",", ".") & "','" _
        & Replace("0.00", ",", ".") & "','" _
        & Replace(Movimento.Text, ",", ".") & "'," _
        & "'R','" _
        & Replace(data_fiscal.Text, "/", ".") & "'," _
        & "'N','" _
        & "0.00" & "','" _
        & "0.00" & "','" _
        & transacao.Text & "')"
            ElseIf fs.Tables(TABELA).Rows.Count <> 0 Then
                SQL_TMP = "update mapa_fiscal_ecf mf " _
                                 & "set mf.mapa_total_movimento = mapa_total_movimento + " & Replace(Movimento.Text, ",", ".") & ",mf.MAPA_TOTAL_ISENTOS = mapa_total_movimento + " & Replace(Movimento.Text, ",", ".") _
                                 & " where mf.mapa_data ='" & Replace(data_fiscal.Text, "/", ".") & "'" _
                                 & " and mf.empresa_codigo='" & loja.Text & "'" _
                                 & " and mf.mapa_numero_caixa ='" & txtEcfPadrao.Text & "'"

            End If

        End If

        'Importar Dados para o Banco
        Try
            'importar dados ECF
            TABELA = "teo_contador"
            SQL = SQL_TMP
            CONEXAO_FIREBIRD(SQL)

            'Atualizar Contador MAPA FISCAL
            TABELA = "teo_contador"
            SQL = "update teo_contador tc set tc.contador_codigo=tc.contador_codigo + 1" _
            & " where tc.contador_tabela in('MAPA_FISCAL_ECF')" _
            & " and tc.empresa_codigo='0'"

            CONEXAO_FIREBIRD(SQL)
            Label65.Text = "Ticket:" & ticket_final.Text
        Catch
            'MsgBox(Err.Description)
            Label65.Text = "Redução Z Ticket:" & ticket_final.Text & " Já importada"
        End Try
        MemoryManagement.FlushMemory()
    End Sub

    Sub IMPORTAR_RZ_ODBC()
        Dim SQL, SQL_TMP As String
        SQL = ""
        SQL_TMP = ""

        TOTAL_BASE()
        'Verificar tipo de importação Redução Z
        If chbRzConsul.Checked = False Then
            If chbRzRepetida.Checked = True Then
                'Importar Redução Por ECF
                SQL_TMP = "UPDATE OR INSERT INTO MAPA_FISCAL_ECF" _
        & "(EMPRESA_CODIGO,MAPA_NUMERO_CAIXA,MAPA_DATA,MAPA_NUMERO,MAPA_NUMERO_CUPOM_INICIAL,MAPA_NUMERO_CUPOM_FINAL," _
        & "MAPA_GRANDE_TOTAL,MAPA_NUMERO_REDUCOES,MAPA_CONTADOR_REINICIO_OPERACAO,MAPA_TOTAL_CANCELAMENTOS,MAPA_TOTAL_DESCONTOS," _
        & "MAPA_ALIQUOTA_01,MAPA_TOTALIZADOR_01,MAPA_IMPOSTO_01," _
        & "MAPA_ALIQUOTA_02,MAPA_TOTALIZADOR_02,MAPA_IMPOSTO_02," _
        & "MAPA_ALIQUOTA_03,MAPA_TOTALIZADOR_03,MAPA_IMPOSTO_03," _
        & "MAPA_ALIQUOTA_04,MAPA_TOTALIZADOR_04,MAPA_IMPOSTO_04," _
        & "MAPA_TOTAL_ISENTOS," _
        & "MAPA_TOTAL_SUBSTITUICAO," _
        & "MAPA_TOTAL_TOTALIZADOR," _
        & "MAPA_TOTAL_IMPOSTO," _
        & "MAPA_TOTAL_MOVIMENTO," _
        & "MAPA_REDUCAOZ_MAPA," _
        & "MAPA_DATA_LANCAMENTO," _
        & "MAPA_LIBERADO," _
        & "MAPA_TOTAL_SUPRIMENTO," _
        & "MAPA_TOTAL_NAO_INCIDENCIA," _
        & "TRANSACAO)" _
        & "VALUES" _
        & "('" & loja.Text & "','" & caixa.Text & "','" & Replace(data_fiscal.Text, "/", ".") & "','" & mapa_numero.Text & "','" & ticket_inicial.Text & "','" & ticket_final.Text & "','" _
        & gt_final.Text & "','" & crz.Text & "','" & cro.Text & "','" & cancelamento.Text & "','" & desconto.Text & "','" _
        & Replace(A1.Text, ",", ".") & "','" & Replace(B1.Text, ",", ".") & "','" & Replace(I1.Text, ",", ".") & "','" _
        & Replace(A2.Text, ",", ".") & "','" & Replace(B2.Text, ",", ".") & "','" & Replace(I2.Text, ",", ".") & "','" _
        & Replace(A3.Text, ",", ".") & "','" & Replace(B3.Text, ",", ".") & "','" & Replace(I3.Text, ",", ".") & "','" _
        & Replace(A4.Text, ",", ".") & "','" & Replace(B4.Text, ",", ".") & "','" & Replace(I4.Text, ",", ".") & "','" _
        & isento.Text & "','" _
        & substituicao.Text & "','" _
        & Replace(TOTALIZADOR, ",", ".") & "','" _
        & Replace(TOTAL_IMPOSTO, ",", ".") & "','" _
        & Replace(Movimento.Text, ",", ".") & "'," _
        & "'R','" _
        & Replace(data_fiscal.Text, "/", ".") & "'," _
        & "'N','" _
        & sangria.Text & "','" _
        & naotributado.Text & "','" _
        & transacao.Text & "')"
            Else
                'Importar Redução Por ECF
                SQL_TMP = "INSERT INTO MAPA_FISCAL_ECF" _
        & "(EMPRESA_CODIGO,MAPA_NUMERO_CAIXA,MAPA_DATA,MAPA_NUMERO,MAPA_NUMERO_CUPOM_INICIAL,MAPA_NUMERO_CUPOM_FINAL," _
        & "MAPA_GRANDE_TOTAL,MAPA_NUMERO_REDUCOES,MAPA_CONTADOR_REINICIO_OPERACAO,MAPA_TOTAL_CANCELAMENTOS,MAPA_TOTAL_DESCONTOS," _
        & "MAPA_ALIQUOTA_01,MAPA_TOTALIZADOR_01,MAPA_IMPOSTO_01," _
        & "MAPA_ALIQUOTA_02,MAPA_TOTALIZADOR_02,MAPA_IMPOSTO_02," _
        & "MAPA_ALIQUOTA_03,MAPA_TOTALIZADOR_03,MAPA_IMPOSTO_03," _
        & "MAPA_ALIQUOTA_04,MAPA_TOTALIZADOR_04,MAPA_IMPOSTO_04," _
        & "MAPA_TOTAL_ISENTOS," _
        & "MAPA_TOTAL_SUBSTITUICAO," _
        & "MAPA_TOTAL_TOTALIZADOR," _
        & "MAPA_TOTAL_IMPOSTO," _
        & "MAPA_TOTAL_MOVIMENTO," _
        & "MAPA_REDUCAOZ_MAPA," _
        & "MAPA_DATA_LANCAMENTO," _
        & "MAPA_LIBERADO," _
        & "MAPA_TOTAL_SUPRIMENTO," _
        & "MAPA_TOTAL_NAO_INCIDENCIA," _
        & "TRANSACAO)" _
        & "VALUES" _
        & "('" & loja.Text & "','" & caixa.Text & "','" & Replace(data_fiscal.Text, "/", ".") & "','" & mapa_numero.Text & "','" & ticket_inicial.Text & "','" & ticket_final.Text & "','" _
        & gt_final.Text & "','" & crz.Text & "','" & cro.Text & "','" & cancelamento.Text & "','" & desconto.Text & "','" _
        & Replace(A1.Text, ",", ".") & "','" & Replace(B1.Text, ",", ".") & "','" & Replace(I1.Text, ",", ".") & "','" _
        & Replace(A2.Text, ",", ".") & "','" & Replace(B2.Text, ",", ".") & "','" & Replace(I2.Text, ",", ".") & "','" _
        & Replace(A3.Text, ",", ".") & "','" & Replace(B3.Text, ",", ".") & "','" & Replace(I3.Text, ",", ".") & "','" _
        & Replace(A4.Text, ",", ".") & "','" & Replace(B4.Text, ",", ".") & "','" & Replace(I4.Text, ",", ".") & "','" _
        & isento.Text & "','" _
        & substituicao.Text & "','" _
        & Replace(TOTALIZADOR, ",", ".") & "','" _
        & Replace(TOTAL_IMPOSTO, ",", ".") & "','" _
        & Replace(Movimento.Text, ",", ".") & "'," _
        & "'R','" _
        & Replace(data_fiscal.Text, "/", ".") & "'," _
        & "'N','" _
        & sangria.Text & "','" _
        & naotributado.Text & "','" _
        & transacao.Text & "')"
            End If
        ElseIf chbRzConsul.Checked = True Then
            'Importar Redução Consolidada
            'Verificar se existe registro no banco no periodo solicitado.

            Dim SQL_FINANCEIRO As String
            TABELA = "MAPA_FISCAL_ECF"
            SQL_FINANCEIRO = "select mf.empresa_codigo,mf.mapa_data,mf.mapa_numero_caixa " _
                             & "from mapa_fiscal_ecf mf where mf.mapa_data ='" _
                             & Format(CDate(Replace(data_fiscal.Text, "/", ".")), "yyyy.MM.dd") & "'" _
                             & " and mf.empresa_codigo='" & loja.Text & "'" _
                             & " and mf.mapa_numero_caixa ='" & txtEcfPadrao.Text & "'"
            'TextBox3.Text = SQL_FINANCEIRO
            'Exit Sub
            CONEXAO_ODBC(SQL_FINANCEIRO)

            If Ods.Tables(TABELA).Rows.Count = 0 Then
                SQL_TMP = "INSERT INTO MAPA_FISCAL_ECF" _
        & "(EMPRESA_CODIGO,MAPA_NUMERO_CAIXA,MAPA_DATA,MAPA_NUMERO,MAPA_NUMERO_CUPOM_INICIAL,MAPA_NUMERO_CUPOM_FINAL," _
        & "MAPA_GRANDE_TOTAL,MAPA_NUMERO_REDUCOES,MAPA_CONTADOR_REINICIO_OPERACAO,MAPA_TOTAL_CANCELAMENTOS,MAPA_TOTAL_DESCONTOS," _
        & "MAPA_ALIQUOTA_01,MAPA_TOTALIZADOR_01,MAPA_IMPOSTO_01," _
        & "MAPA_ALIQUOTA_02,MAPA_TOTALIZADOR_02,MAPA_IMPOSTO_02," _
        & "MAPA_ALIQUOTA_03,MAPA_TOTALIZADOR_03,MAPA_IMPOSTO_03," _
        & "MAPA_ALIQUOTA_04,MAPA_TOTALIZADOR_04,MAPA_IMPOSTO_04," _
        & "MAPA_TOTAL_ISENTOS," _
        & "MAPA_TOTAL_SUBSTITUICAO," _
        & "MAPA_TOTAL_TOTALIZADOR," _
        & "MAPA_TOTAL_IMPOSTO," _
        & "MAPA_TOTAL_MOVIMENTO," _
        & "MAPA_REDUCAOZ_MAPA," _
        & "MAPA_DATA_LANCAMENTO," _
        & "MAPA_LIBERADO," _
        & "MAPA_TOTAL_SUPRIMENTO," _
        & "MAPA_TOTAL_NAO_INCIDENCIA," _
        & "TRANSACAO)" _
        & "VALUES" _
        & "('" & loja.Text & "','" & txtEcfPadrao.Text & "','" & Replace(data_fiscal.Text, "/", ".") & "','" & mapa_numero.Text & "','" & "0" & "','" & "0" & "','" _
        & "0" & "','" & "0" & "','" & "0" & "','" & "0.00" & "','" & "0.00" & "','" _
        & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" _
        & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" _
        & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" _
        & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" & Replace("0.00", ",", ".") & "','" _
        & Replace(Movimento.Text, ",", ".") & "','" _
        & "0.00" & "','" _
        & Replace("0.00", ",", ".") & "','" _
        & Replace("0.00", ",", ".") & "','" _
        & Replace(Movimento.Text, ",", ".") & "'," _
        & "'R','" _
        & Replace(data_fiscal.Text, "/", ".") & "'," _
        & "'N','" _
        & "0.00" & "','" _
        & "0.00" & "','" _
        & transacao.Text & "')"
            ElseIf Ods.Tables(TABELA).Rows.Count <> 0 Then
                SQL_TMP = "update mapa_fiscal_ecf mf " _
                                 & "set mf.mapa_total_movimento = mapa_total_movimento + " & Replace(Movimento.Text, ",", ".") & ",mf.MAPA_TOTAL_ISENTOS = mapa_total_movimento + " & Replace(Movimento.Text, ",", ".") _
                                 & " where mf.mapa_data ='" & Replace(data_fiscal.Text, "/", ".") & "'" _
                                 & " and mf.empresa_codigo='" & loja.Text & "'" _
                                 & " and mf.mapa_numero_caixa ='" & txtEcfPadrao.Text & "'"
                'TextBox3.Text = TextBox1.Text

            End If

        End If

        'Importar Dados para o Banco
        Try
            'importar dados ECF
            TABELA = "teo_contador"
            SQL = SQL_TMP
            CONEXAO_ODBC(SQL)

            ''Atualizar Contador MAPA FISCAL
            'TABELA = "teo_contador"
            'SQL = "update teo_contador tc set tc.contador_codigo=tc.contador_codigo + 1" _
            '& " where tc.contador_tabela in('MAPA_FISCAL_ECF')" _
            '& " and tc.empresa_codigo='0'"

            'CONEXAO_ODBC(SQL)
            Label65.Text = "Ticket:" & ticket_final.Text
        Catch
            Label65.Text = "Redução Z Ticket:" & ticket_final.Text & " Já importada"
        End Try
        MemoryManagement.FlushMemory()
    End Sub

    'Colocar Informaçõs do Protocolo Pegos na Web
    Sub GERAR_PROTOCOLO()
        Resetar_Tag_Protocolo = tx_tag_protocolo.Text
        If txVesaoNFCe.Text = "3.10" Then
            Resetar_Tag_Protocolo = Replace(Replace(Replace(Replace(tx_tag_protocolo.Text, "@chave", txChaveNFCe.Text), "@data", txDataProtocolo.Text), "@assinatura", txDigestValue.Text), "@protocolo", txProtocolo.Text)
        ElseIf txVesaoNFCe.Text = "4.00" Then
            Resetar_Tag_Protocolo = Replace(Replace(Replace(Replace(Replace(tx_tag_protocolo.Text, "@chave", txChaveNFCe.Text), "@data", txDataProtocolo.Text), "@assinatura", txDigestValue.Text), "@protocolo", txProtocolo.Text), "versao=""3.10""", "versao=""4.00""")
        End If
    End Sub

    'Enviar Email
    Protected Sub send()
        Dim objNovoEmail As New MailMessage
        Dim objSmtp As New SmtpClient

        'Adicionando o e-mail do remetente
        objNovoEmail.From = New MailAddress(txRemetente.Text)

        'Adicionando o e-mail do destinatário
        objNovoEmail.To.Add(New MailAddress(txDestinatario.Text))

        'Adiciona a prioridade do e-mail
        objNovoEmail.Priority = MailPriority.High

        'Adicionando o assunto ao atributo assunto
        objNovoEmail.Subject = txAssunto.Text

        'Formato de e-mail em Html?
        objNovoEmail.IsBodyHtml = True

        'Inserir o corpo da mensagem no atributo Body
        objNovoEmail.Body = txMensagem.Text

        'Adiciona o anexo a enviar
        'objNovoEmail.Attachments.Add(New Attachment(txAnexo.Text))

        'Configuração de tipagem da linguagem, para não aparecer caracteres estranhos na mensagem
        objNovoEmail.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1")
        objNovoEmail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1")

        'Adicionando os campos dos atributos da configuração do e-mail
        'parametros necessários para envio de e-mail.


        'Configuração do IP do servidor SMTP
        'Por exemplo: smtp.mail.yahoo.com.br (EXEMPLO)
        objSmtp.Host = txServidorSMTP.Text
        objSmtp.Port = txPorta.Text

        'Caso queira definir um tempo do timeout 
        'objSmtp.Timeout = 10


        'Define se deve usar autenticação ou não
        If txUsuario.Text <> String.Empty And txSenha.Text <> String.Empty Then
            objSmtp.Credentials = New System.Net.NetworkCredential(txUsuario.Text, txSenha.Text)
        End If

        If txUsuario.Text = String.Empty Then
            objSmtp.UseDefaultCredentials = False
        End If

        'Habilitar SSL ou não? 
        objSmtp.EnableSsl = chbSSL.CheckState

        'Enviando a mensagem por e-mail
        Try
            objSmtp.Send(objNovoEmail)
        Catch ex As Exception
            Throw ex
        Finally
            objNovoEmail.Dispose()
        End Try
        objNovoEmail.Dispose()
    End Sub

    'Importar dados do Protocolo
    Sub IMPORTA_DADOS_PROTOCOLO()
        txProtocolo.Text = ""
        txDataProtocolo.Text = ""
        If chbOnline.Checked = True Then
            Dim retorno As String
            retorno = RequestDadosWeb(txLinkQRCode.Text.Trim)
            'WebBrowser1.Navigate(TextBox1.Text)

            Dim Linhas = retorno.Split(System.Environment.NewLine)
            Dim var1 As String
            retorno = String.Empty

            'Try


            For Each Linha As String In Linhas
                Dim Inicio As Integer = Linha.IndexOf("Protocolo")
                'MessageBox.Show(Linha.IndexOf(TextBox2.Text))

                If (Inicio <> -1) Then
                    Linha = Linha.Replace(Linha.Substring(Inicio + 76), "")
                    retorno += Linha + System.Environment.NewLine
                    var1 = Microsoft.VisualBasic.Right(retorno, 78)
                    'MsgBox(var1)
                    'MsgBox(Replace(Replace(var1, "Protocolo de Autorização:", ""), " ", ""))
                    txProtocolo.Text = (Replace(Replace(var1, "Protocolo de Autorização:", ""), " ", "")).Substring(9, 15)
                    txDataProtocolo.Text = Format(CDate((Replace(Replace(var1, "Protocolo de Autorização:", ""), " ", "")).Substring(24, 10)), "yyyy-MM-dd") _
                                        & "T" & ((Replace(Replace(var1, "Protocolo de Autorização:", ""), " ", "")).Substring(34, 8)) & "-03:00"

                Else
                    retorno += Linha + System.Environment.NewLine
                    txProtocolo.Text = "999999999999999"
                    txDataProtocolo.Text = txEmissao.Text
                End If
            Next
        ElseIf chbOnline.Checked = False Then
            'retorno += Linha + System.Environment.NewLine
            txProtocolo.Text = "999999999999999"
            txDataProtocolo.Text = txEmissao.Text
        End If
        'Catch
        'txProtocolo.Text = "999999999999999"
        'txDataProtocolo.Text = txEmissao.Text
        'End Try
        'Exit Sub
    End Sub

    'Converter com Tag SOAP
    Sub CONVERTER_XML_NORMAL()
        '*******Verificar se no Xml existe caracter expecial-------------

        'Quebrar XML

        txTagSitProtocolo.Text = "N"

        CAMINHO_NFCE = ""

        Dim fluxoTexto As IO.StreamReader
        Dim linhaTexto As String
        Using leitor As New TextFieldParser(CUPOM)
            Dim linhaAtual As String()
            'Dim i As Integer = 0
            'Dim r As Integer = 1
            Dim texto As String
            Dim fluxoTextogravado, fluxoTextogravado2 As IO.StreamWriter
            Dim linhaTextogravado As String

            'Informamos que será importado com Delimitação
            leitor.TextFieldType = FileIO.FieldType.Delimited
            'Informamos o Delimitador
            'leitor.SetDelimiters(vbCr)
            fluxoTextogravado = New IO.StreamWriter("c:\TESTE\temp.xml", False)

            If IO.File.Exists(CUPOM) Then
                fluxoTexto = New IO.StreamReader(CUPOM)
                linhaTexto = fluxoTexto.ReadLine
                While linhaTexto <> Nothing
                    'linhaAtual = leitor.ReadFields()
                    If linhaTexto Like "*<protNFe*" Then
                        txTagSitProtocolo.Text = "S"
                    End If
                    If linhaTexto Like "*><*" Then
                        fluxoTextogravado.WriteLine(Replace(linhaTexto, "><", ">" & Chr(13) & "<"))
                        linhaTexto = fluxoTexto.ReadLine
                    Else
                        fluxoTextogravado.WriteLine(linhaTexto)
                        linhaTexto = fluxoTexto.ReadLine
                    End If

                End While
                fluxoTextogravado.Close()
                fluxoTexto.Close()
            Else
                'MessageBox.Show("Arquivo não existe")
            End If


            Dim i As Integer = 0
            Dim r As Integer = 0

            'tx_Data.Text.Substring(8, 2) & "-" & tx_Data.Text.Substring(5, 2) & "-" & tx_Data.Text.Substring(0, 4)
            'Dim data As Date = (FileDateTime((tx_cupom.Text)))
            'Dim data As Date = (txEmissao.Text.Substring(8, 2) & "-" & txEmissao.Text.Substring(5, 2) & "-" & txEmissao.Text.Substring(0, 4))

            'fluxoTextogravado2 = New IO.StreamWriter(txDestinoAssinados.Text & Format(data, "dd-MM-yyyy") & "_" & txChaveNFCe.Text & "-" & Microsoft.VisualBasic.Right(Cupom, 10), False)
            'fluxoTextogravado2 = New IO.StreamWriter(txDestinoAssinados.Text & "-" & Microsoft.VisualBasic.Right(Cupom, 10), False)

            'Retirar Tags

            fluxoTextogravado2 = New IO.StreamWriter("c:\TESTE\" & System.IO.Path.GetFileName(CUPOM), False)
            ' CUPOM_DELETAR = System.IO.Path.GetFileName(Cupom)
            If IO.File.Exists(CUPOM) Then
                fluxoTexto = New IO.StreamReader("c:\TESTE\temp.xml")
                linhaTexto = fluxoTexto.ReadLine
                While linhaTexto <> Nothing
                    Try
                        If txTagSitProtocolo.Text = "N" Then
                            If i = 5 Then
                                linhaTexto = fluxoTexto.ReadLine
                                i = i + 1
                            Else
                                If linhaTexto Like "*soap12:Envelope*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*<infNFe Id*" Then
                                    txChaveNFCe.Text = Microsoft.VisualBasic.Right(Microsoft.VisualBasic.Left(linhaTexto, 59), 44)
                                    i = i + 1
                                    fluxoTextogravado2.Write(linhaTexto)
                                    linhaTexto = fluxoTexto.ReadLine
                                ElseIf linhaTexto Like "*<infNFe versao*" Then
                                    txChaveNFCe.Text = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.Right(linhaTexto, 46), 44)
                                    i = i + 1
                                    fluxoTextogravado2.Write(linhaTexto)
                                    linhaTexto = fluxoTexto.ReadLine
                                ElseIf linhaTexto Like "*soap12:Header*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*soap12:Body*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*autXML*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    linhaTexto = fluxoTexto.ReadLine
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*<nfeCabecMsg*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    linhaTexto = fluxoTexto.ReadLine
                                    linhaTexto = fluxoTexto.ReadLine
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*nfeDadosMsg*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*idLote*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*indSinc*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*versaoDados*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*enviNFe*" Then
                                    fluxoTextogravado2.Write(Replace(linhaTexto, "enviNFe", "nfeProc"))
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*<dhEmi>*" Then
                                    fluxoTextogravado2.Write(linhaTexto)
                                    txEmissao.Text = Replace(Replace(linhaTexto, "<dhEmi>", ""), "</dhEmi>", "")
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*CDATA*" Then
                                    fluxoTextogravado2.Write(linhaTexto)
                                    txLinkQRCode.Text = Replace(Replace(linhaTexto, "<![CDATA[", ""), "]]>", "")
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*DigestValue*" Then
                                    fluxoTextogravado2.Write(linhaTexto)
                                    txDigestValue.Text = Replace(Replace(linhaTexto, "<DigestValue>", ""), "</DigestValue>", "")
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*<protNFe*" Then
                                    fluxoTextogravado2.Write(linhaTexto)
                                    txTagSitProtocolo.Text = "S"
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*<vNF>*" Then
                                    fluxoTextogravado2.Write(linhaTexto)
                                    nfce_valor.Text = Replace(Replace(linhaTexto, "<vNF>", ""), "</vNF>", "")
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                Else
                                    fluxoTextogravado2.Write(linhaTexto)
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                End If
                            End If
                        Else
                            If i = 5 Then
                                linhaTexto = fluxoTexto.ReadLine
                                i = i + 1
                            Else
                                If linhaTexto Like "*soap12:Envelope*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*<infNFe Id*" Then
                                    txChaveNFCe.Text = Microsoft.VisualBasic.Right(Microsoft.VisualBasic.Left(linhaTexto, 59), 44)
                                    i = i + 1
                                    fluxoTextogravado2.Write(linhaTexto)
                                    linhaTexto = fluxoTexto.ReadLine
                                ElseIf linhaTexto Like "*<infNFe versao*" Then
                                    txChaveNFCe.Text = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.Right(linhaTexto, 46), 44)
                                    i = i + 1
                                    fluxoTextogravado2.Write(linhaTexto)
                                    linhaTexto = fluxoTexto.ReadLine
                                ElseIf linhaTexto Like "*soap12:Header*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*soap12:Body*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*<nfeCabecMsg*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    linhaTexto = fluxoTexto.ReadLine
                                    linhaTexto = fluxoTexto.ReadLine
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*nfeDadosMsg*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*idLote*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*indSinc*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*versaoDados*" Then
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*enviNFe*" Then
                                    fluxoTextogravado2.Write(Replace(linhaTexto, "enviNFe", "nfeProc"))
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*<dhEmi>*" Then
                                    fluxoTextogravado2.Write(linhaTexto)
                                    txEmissao.Text = Replace(Replace(linhaTexto, "<dhEmi>", ""), "</dhEmi>", "")
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*CDATA*" Then
                                    fluxoTextogravado2.Write(linhaTexto)
                                    txLinkQRCode.Text = Replace(Replace(linhaTexto, "<![CDATA[", ""), "]]>", "")
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*DigestValue*" Then
                                    fluxoTextogravado2.Write(linhaTexto)
                                    txDigestValue.Text = Replace(Replace(linhaTexto, "<DigestValue>", ""), "</DigestValue>", "")
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*<protNFe*" Then
                                    fluxoTextogravado2.Write(linhaTexto)
                                    txTagSitProtocolo.Text = "S"
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                ElseIf linhaTexto Like "*<vNF>*" Then
                                    fluxoTextogravado2.Write(linhaTexto)
                                    nfce_valor.Text = Replace(Replace(linhaTexto, "<vNF>", ""), "</vNF>", "")
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                Else
                                    fluxoTextogravado2.Write(linhaTexto)
                                    linhaTexto = fluxoTexto.ReadLine
                                    i = i + 1
                                End If
                            End If
                        End If
                    Catch
                        'MsgBox(Err.Description)
                        fluxoTextogravado2.Close()
                        fluxoTexto.Close()
                        Exit Sub
                    End Try

                End While

                fluxoTextogravado2.Close()
                fluxoTexto.Close()

            Else
                'MessageBox.Show("Arquivo não existe")
            End If

        End Using
        '========================Verificar XML =======================

        IMPORTA_DADOS_PROTOCOLO()
        GERAR_PROTOCOLO()

        '=========================INICIO INCLUIR PROTOCOLO NO XML ===================
        CaminhoArquivo = ""
        CaminhoArquivo = "c:\TESTE\" & System.IO.Path.GetFileName(CUPOM)
        'Dim fluxoTexto As IO.StreamReader
        'Dim linhaTexto As String
        Using leitor As New TextFieldParser(CaminhoArquivo)
            Dim linhaAtual As String()
            'Dim i As Integer = 0
            'Dim r As Integer = 1
            Dim texto As String
            Dim fluxoTextogravado, fluxoTextogravado2 As IO.StreamWriter
            Dim linhaTextogravado As String

            'Informamos que será importado com Delimitação
            leitor.TextFieldType = FileIO.FieldType.Delimited
            'Informamos o Delimitador
            'leitor.SetDelimiters(vbCr)
            'If txProtocolo.Text = "999999999999999" Then
            'fluxoTextogravado = New IO.StreamWriter(txDestinoNfce.Text & "INVALIDO-" & System.IO.Path.GetFileName(CaminhoArquivo), False)
            'Else
            'fluxoTextogravado = New IO.StreamWriter(txDestinoNfce.Text & System.IO.Path.GetFileName(CaminhoArquivo), False)
            MES = txChaveNFCe.Text.Substring(2, 4)
            EMPRESA = txChaveNFCe.Text.Substring(6, 14)

            'Verificar Pasta Destino NFCE

            If Directory.Exists(txDestinoNFCe.Text & MES & "\") = False Then
                Directory.CreateDirectory(txDestinoNFCe.Text & MES)
            End If
            If Directory.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\") = False Then
                Directory.CreateDirectory(txDestinoNFCe.Text & MES & "\" & EMPRESA)
            End If

            fluxoTextogravado = New IO.StreamWriter(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(txEmissao.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & txChaveNFCe.Text & "-" & Microsoft.VisualBasic.Right(CUPOM, 10), False)
            CAMINHO_NFCE = txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(txEmissao.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & txChaveNFCe.Text & "-" & Microsoft.VisualBasic.Right(CUPOM, 10)
            'End If


            If IO.File.Exists(CaminhoArquivo) Then
                fluxoTexto = New IO.StreamReader(CaminhoArquivo)
                linhaTexto = fluxoTexto.ReadLine
                While linhaTexto <> Nothing
                    'linhaAtual = leitor.ReadFields()
                    If linhaTexto Like "*</CRT></emit><det*" Then
                        If txTagSitProtocolo.Text = "N" Then
                            fluxoTextogravado.Write(Replace(Replace(linhaTexto, "</CRT></emit>", "</CRT></emit><autXML><CNPJ>" & txChaveNFCe.Text.Substring(6, 14) & "</CNPJ></autXML>"), "</Signature></NFe>", "</Signature></NFe>" & Resetar_Tag_Protocolo))
                        Else
                            fluxoTextogravado.Write(linhaTexto)
                        End If
                        linhaTexto = fluxoTexto.ReadLine
                    ElseIf linhaTexto Like "*</CRT></emit><dest>*" Then
                        If txTagSitProtocolo.Text = "N" Then
                            fluxoTextogravado.Write(Replace(Replace(linhaTexto, "</indIEDest></dest>", "</indIEDest></dest><autXML><CNPJ>" & txChaveNFCe.Text.Substring(6, 14) & "</CNPJ></autXML>"), "</Signature></NFe>", "</Signature></NFe>" & Resetar_Tag_Protocolo))
                        Else
                            fluxoTextogravado.Write(linhaTexto)
                        End If
                        linhaTexto = fluxoTexto.ReadLine
                        'ElseIf linhaTexto Like "*</Signature></NFe>*" Then
                        '   fluxoTextogravado.Write(Replace(linhaTexto, "</Signature></NFe>", "</Signature></NFe>" & Resetar_Tag_Protocolo))
                        '  linhaTexto = fluxoTexto.ReadLine
                    Else
                        fluxoTextogravado.Write(linhaTexto)
                        linhaTexto = fluxoTexto.ReadLine
                    End If

                End While
                fluxoTextogravado.Close()
                fluxoTexto.Close()
            Else
                ' MessageBox.Show("Arquivo não existe")
            End If
        End Using

        File.Delete(CaminhoArquivo)
        '=========================FIM INCLUIR PROTOCOLO NO XML ===================
    End Sub

    'Converter Xml Contigência
    Sub CONVERTER_XML_CONTIGENCIA()
        '*******Verificar se no Xml existe caracter expecial-------------
        CAMINHO_NFCE = ""
        txTagSitProtocolo.Text = "N"

        Dim fluxoTexto As IO.StreamReader
        Dim linhaTexto As String
        Using leitor As New TextFieldParser(CUPOM)
            Dim linhaAtual As String()
            'Dim i As Integer = 0
            'Dim r As Integer = 1
            Dim texto As String
            Dim fluxoTextogravado, fluxoTextogravado2 As IO.StreamWriter
            Dim linhaTextogravado As String

            'Informamos que será importado com Delimitação
            leitor.TextFieldType = FileIO.FieldType.Delimited
            'Informamos o Delimitador
            'leitor.SetDelimiters(vbCr)
            fluxoTextogravado = New IO.StreamWriter("c:\TESTE\temp.xml", False)

            If IO.File.Exists(CUPOM) Then
                fluxoTexto = New IO.StreamReader(CUPOM)
                linhaTexto = fluxoTexto.ReadLine
                While linhaTexto <> Nothing
                    'linhaAtual = leitor.ReadFields()
                    If linhaTexto Like "*<protNFe*" Then
                        txTagSitProtocolo.Text = "S"
                    End If

                    If linhaTexto Like "*><*" Then
                        fluxoTextogravado.WriteLine(Replace(linhaTexto, "><", ">" & Chr(13) & "<"))
                        linhaTexto = fluxoTexto.ReadLine
                    Else
                        fluxoTextogravado.WriteLine(linhaTexto)
                        linhaTexto = fluxoTexto.ReadLine
                    End If

                End While
                fluxoTextogravado.Close()
                fluxoTexto.Close()
            Else
                'MessageBox.Show("Arquivo não existe")
            End If


            Dim i As Integer = 0
            Dim r As Integer = 0



            'tx_Data.Text.Substring(8, 2) & "-" & tx_Data.Text.Substring(5, 2) & "-" & tx_Data.Text.Substring(0, 4)
            'Dim data As Date = (FileDateTime((tx_cupom.Text)))
            'Dim data As Date = (txEmissao.Text.Substring(8, 2) & "-" & txEmissao.Text.Substring(5, 2) & "-" & txEmissao.Text.Substring(0, 4))

            'fluxoTextogravado2 = New IO.StreamWriter(txDestinoAssinados.Text & Format(data, "dd-MM-yyyy") & "_" & txChaveNFCe.Text & "-" & Microsoft.VisualBasic.Right(Cupom, 10), False)
            'fluxoTextogravado2 = New IO.StreamWriter(txDestinoAssinados.Text & "-" & Microsoft.VisualBasic.Right(Cupom, 10), False)
            fluxoTextogravado2 = New IO.StreamWriter("c:\TESTE\" & System.IO.Path.GetFileName(CUPOM), False)
            'CUPOM_DELETAR = System.IO.Path.GetFileName(Cupom)

            If IO.File.Exists(CUPOM) Then
                fluxoTexto = New IO.StreamReader("c:\TESTE\temp.xml")
                linhaTexto = fluxoTexto.ReadLine
                While linhaTexto <> Nothing
                    Try
                        'linhaAtual = leitor.ReadFields()
                        If linhaTexto Like "*<NFe xmlns=*" Then
                            fluxoTextogravado2.Write("<nfeProc xmlns=" & Chr(34) & "http://www.portalfiscal.inf.br/nfe" & Chr(34) & " versao=" & Chr(34) & txVesaoNFCe.Text & Chr(34) & ">")
                            fluxoTextogravado2.Write(linhaTexto)
                            linhaTexto = fluxoTexto.ReadLine
                            i = i + 1
                        ElseIf linhaTexto Like "*<infNFe Id*" Then
                            txChaveNFCe.Text = Microsoft.VisualBasic.Right(Microsoft.VisualBasic.Left(linhaTexto, 59), 44)
                            i = i + 1

                            fluxoTextogravado2.Write(linhaTexto)
                            linhaTexto = fluxoTexto.ReadLine
                        ElseIf linhaTexto Like "*<infNFe versao*" Then
                            txChaveNFCe.Text = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.Right(linhaTexto, 46), 44)
                            i = i + 1

                            fluxoTextogravado2.Write(linhaTexto)
                            linhaTexto = fluxoTexto.ReadLine
                        ElseIf linhaTexto Like "*indSinc*" Then
                            linhaTexto = fluxoTexto.ReadLine
                            i = i + 1
                        ElseIf linhaTexto Like "*<dhEmi>*" Then
                            fluxoTextogravado2.Write(linhaTexto)
                            txEmissao.Text = Replace(Replace(linhaTexto, "<dhEmi>", ""), "</dhEmi>", "")
                            linhaTexto = fluxoTexto.ReadLine
                            i = i + 1
                        ElseIf linhaTexto Like "*CDATA*" Then
                            fluxoTextogravado2.Write(linhaTexto)
                            txLinkQRCode.Text = Replace(Replace(linhaTexto, "<![CDATA[", ""), "]]>", "")
                            linhaTexto = fluxoTexto.ReadLine
                            i = i + 1
                        ElseIf linhaTexto Like "*DigestValue*" Then
                            fluxoTextogravado2.Write(linhaTexto)
                            txDigestValue.Text = Replace(Replace(linhaTexto, "<DigestValue>", ""), "</DigestValue>", "")
                            linhaTexto = fluxoTexto.ReadLine
                            i = i + 1
                        ElseIf linhaTexto Like "*<protNFe*" Then
                            fluxoTextogravado2.Write(linhaTexto)
                            txTagSitProtocolo.Text = "S"
                            linhaTexto = fluxoTexto.ReadLine
                            i = i + 1
                        ElseIf linhaTexto Like "*<vNF>*" Then
                            fluxoTextogravado2.Write(linhaTexto)
                            nfce_valor.Text = Replace(Replace(linhaTexto, "<vNF>", ""), "</vNF>", "")
                            linhaTexto = fluxoTexto.ReadLine
                            i = i + 1
                        Else
                            fluxoTextogravado2.Write(linhaTexto)
                            linhaTexto = fluxoTexto.ReadLine
                            i = i + 1
                        End If

                    Catch
                        'MsgBox(Err.Description)
                        fluxoTextogravado2.Close()
                        fluxoTexto.Close()
                        Exit Sub
                    End Try

                End While

                fluxoTextogravado2.Close()
                fluxoTexto.Close()

            Else
                'MessageBox.Show("Arquivo não existe")
            End If

        End Using
        '========================Verificar XML =======================

        IMPORTA_DADOS_PROTOCOLO()
        GERAR_PROTOCOLO()

        '=========================INICIO INCLUIR PROTOCOLO NO XML ===================
        CaminhoArquivo = ""
        CaminhoArquivo = "c:\TESTE\" & System.IO.Path.GetFileName(CUPOM)
        'Dim fluxoTexto As IO.StreamReader
        'Dim linhaTexto As String
        Using leitor As New TextFieldParser(CaminhoArquivo)
            Dim linhaAtual As String()
            'Dim i As Integer = 0
            'Dim r As Integer = 1
            Dim texto As String
            Dim fluxoTextogravado, fluxoTextogravado2 As IO.StreamWriter
            Dim linhaTextogravado As String

            'Informamos que será importado com Delimitação
            leitor.TextFieldType = FileIO.FieldType.Delimited
            'Informamos o Delimitador
            'leitor.SetDelimiters(vbCr)
            'If txProtocolo.Text = "999999999999999" Then
            'fluxoTextogravado = New IO.StreamWriter(txDestinoNfce.Text & "INVALIDO-" & System.IO.Path.GetFileName(CaminhoArquivo), False)
            'Else
            'fluxoTextogravado = New IO.StreamWriter(txDestinoNfce.Text & System.IO.Path.GetFileName(CaminhoArquivo), False)

            MES = txChaveNFCe.Text.Substring(2, 4)
            EMPRESA = txChaveNFCe.Text.Substring(6, 14)

            'Verificar Pasta Destino NFCE

            If Directory.Exists(txDestinoNFCe.Text & MES & "\") = False Then
                Directory.CreateDirectory(txDestinoNFCe.Text & MES)
            End If
            If Directory.Exists(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\") = False Then
                Directory.CreateDirectory(txDestinoNFCe.Text & MES & "\" & EMPRESA)
            End If

            fluxoTextogravado = New IO.StreamWriter(txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(txEmissao.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & txChaveNFCe.Text & "-" & Microsoft.VisualBasic.Right(CUPOM, 10), False)
            CAMINHO_NFCE = txDestinoNFCe.Text & MES & "\" & EMPRESA & "\" & Format(CDate(Replace(txEmissao.Text.Substring(0, 10), "-", "/")), "dd-MM-yyyy") & "_" & txChaveNFCe.Text & "-" & Microsoft.VisualBasic.Right(CUPOM, 10)
            'End If


            If IO.File.Exists(CaminhoArquivo) Then
                fluxoTexto = New IO.StreamReader(CaminhoArquivo)
                linhaTexto = fluxoTexto.ReadLine

                While linhaTexto <> Nothing
                    If txTagSitProtocolo.Text = "N" Then
                        fluxoTextogravado.Write(Replace(linhaTexto, "</Signature></NFe>", "</Signature></NFe>" & Resetar_Tag_Protocolo & "</nfeProc>"))
                        linhaTexto = fluxoTexto.ReadLine
                    Else
                        fluxoTextogravado.Write(linhaTexto)
                        linhaTexto = fluxoTexto.ReadLine
                    End If

                End While

                fluxoTextogravado.Close()
                fluxoTexto.Close()
            Else
                'MessageBox.Show("Arquivo não existe")
            End If
        End Using

        File.Delete(CaminhoArquivo)
        '=========================FIM INCLUIR PROTOCOLO NO XML ===================

    End Sub

    'Importar NFC-e
    Sub IMPORTAR_NFCE()
        Dim SQL As String

        If chbXmlContabilidade.Checked = True Then
            'Preencher TextBox de importação NFC-e
            nfce_Loja.Text = txChaveNFCe.Text.Substring(6, 14)
            nfce_Data.Text = CDate(Replace(txEmissao.Text.Substring(0, 10), "-", "/"))
            nfce_Serie.Text = txChaveNFCe.Text.Substring(22, 3)
            nfce_Nota.Text = txChaveNFCe.Text.Substring(25, 9)
            nfce_Chave.Text = txChaveNFCe.Text
        End If


        'Consultar Chave
        TABELA = "WB_XML_NFCE"
        SQL = "select count(WB_CHAVE) from WB_XML_NFCE where WB_CHAVE = '" & nfce_Chave.Text & "'"

        If rbConexaoNormal.Checked = True Then
            CONEXAO_WB_XML_NFCE(SQL)

            If xs.Tables(TABELA).Rows(0)("COUNT").ToString() > 0 Then
                Exit Sub
            Else
                'Inserir Informações no Banco

                CARREGAR_INI_TEOREMA()
                WB_XML_NFCE_SERVIDOR = WB_ODBC_BANCO
                Dim myConnectionString As String = "User=SYSDBA;Password=masterkey;Database=" & WB_XML_NFCE_SERVIDOR & ";DataSource=localhost;Port=3050;Dialect=3;"
                Using connection As New FbConnection(myConnectionString)

                    connection.Open()

                    Dim strNFCE As String
                    strNFCE = "?"
                    Dim cmd As New FbCommand
                    cmd.Connection = connection
                    cmd.CommandText = "INSERT INTO WB_XML_NFCE" _
                      & "(WB_CHAVE,WB_DATA,WB_NM_NFCE,WB_EMPRESA_CNPJ,WB_SERIE,WB_XML_NFCE,WB_VALOR_NFCE,WB_PROTOCOLO,WB_DATA_PROTOCOLO)" _
                      & " VALUES " _
                      & "('" & nfce_Chave.Text & "','" & Format(CDate(nfce_Data.Text), "dd.MM.yyyy") & "','" & nfce_Nota.Text & "','" & nfce_Loja.Text & "','" & nfce_Serie.Text & "'," & strNFCE & ",'" & nfce_valor.Text & "','" & txProtocolo.Text & "','" & txDataProtocolo.Text & "')"

                    If strNFCE = "?" Then
                        cmd.Parameters.Add(strNFCE, FbDbType.Binary).Value = File.ReadAllBytes(CAMINHO_NFCE)
                    End If
                    cmd.ExecuteNonQuery()

                End Using
            End If
        ElseIf rbConexaoODBC.Checked = True Then
            TABELA = "WB_XML_NFCE"
            CONEXAO_WBPORT_ODBC(SQL)

            If Ods.Tables(TABELA).Rows(0)("COUNT").ToString() > 0 Then
                Exit Sub
            Else
                Dim myConnectionString As String = "DRIVER=Firebird/InterBase(r) driver;UID=" & WB_ODBC_USUARIO & ";PWD=" & WB_ODBC_SENHA & ";DBNAME=" & WB_ODBC_SERVIDOR & "/" & WB_ODBC_PORTA & ":" & WB_ODBC_BANCO & ""
                Using connection As New OdbcConnection(myConnectionString)

                    connection.Open()

                    Dim strNFCE As String
                    strNFCE = "?"
                    Dim cmd = New OdbcCommand
                    cmd.Connection = connection
                    cmd.CommandText = "INSERT INTO WB_XML_NFCE" _
                        & "(WB_CHAVE,WB_DATA,WB_NM_NFCE,WB_EMPRESA_CNPJ,WB_SERIE,WB_XML_NFCE,WB_VALOR_NFCE,WB_PROTOCOLO,WB_DATA_PROTOCOLO)" _
                        & " VALUES " _
                        & "('" & nfce_Chave.Text & "','" & Format(CDate(nfce_Data.Text), "dd.MM.yyyy") & "','" & nfce_Nota.Text & "','" & nfce_Loja.Text & "','" & nfce_Serie.Text & "'," & strNFCE & ",'" & nfce_valor.Text & "','" & txProtocolo.Text & "','" & txDataProtocolo.Text & "')"
                    'Clipboard.SetText(cmd.CommandText)
                    If strNFCE = "?" Then
                        cmd.Parameters.Add(strNFCE, FbDbType.Binary).Value = File.ReadAllBytes(CAMINHO_NFCE)
                    End If
                    cmd.ExecuteNonQuery()
                End Using
            End If
        End If
    End Sub

    Private Sub btOrigemArquivos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btOrigemArquivos.Click
        'Selecionar Origem dos Arquivos
        FolderBrowserDialog1.ShowDialog()
        txOrigemArquivos.Text = Replace(FolderBrowserDialog1.SelectedPath & "\", "\\", "\")
    End Sub

    Private Sub btDestinoVendas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDestinoVendas.Click
        'Selecionar destino dos Arquivos de vendas
        FolderBrowserDialog1.ShowDialog()
        txDestinoVendas.Text = Replace(FolderBrowserDialog1.SelectedPath & "\", "\\", "\")
    End Sub

    Private Sub btDestinoZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDestinoZ.Click
        'Selecionar destino dos Arquivos RZ
        FolderBrowserDialog1.ShowDialog()
        txDestinoZ.Text = Replace(FolderBrowserDialog1.SelectedPath & "\", "\\", "\")
    End Sub

    Private Sub btDestinoNFCe_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDestinoNFCe.Click
        'Selecionar destino dos Arquivos NFC-e
        FolderBrowserDialog1.ShowDialog()
        txDestinoNFCe.Text = Replace(FolderBrowserDialog1.SelectedPath & "\", "\\", "\")
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        FUNCOES_XML.LISTAR_CERTIFICADOS(cbCertificado)

        If Directory.Exists("c:\TESTE\") = False Then
            Directory.CreateDirectory("C:\TESTE\")
        End If

        txtVersao.Text = (String.Format("{0} " & " | " & " Última Atualização: {1} ",
        System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
        System.IO.File.GetLastWriteTime(System.Reflection.Assembly.GetExecutingAssembly().Location).ToShortDateString()))
        ToolStripStatusLabel4.Text = "Cupons: Não Monitorado"


        CARREGAR_INI_IMPORTADOR()
        ToolStripStatusLabel1.Text = "STATUS - OFF-LINE"
        ToolStripStatusLabel2.Text = "Versão - " & txtVersao.Text
        Try
            txOrigemArquivos.Text = ORIGEM_VENDAS
            txDestinoVendas.Text = DESTINO_VENDAS
            txDestinoScannTech.Text = DESTINO_SCANNTECH
            txDestinoZ.Text = DESTINO_RZ
            txExtensao.Text = EXTENSAO
            txTempo.Text = TEMPO_IMPORTADOR
            dtInicial.Value = DATA_INICIAL
            dtFinal.Value = DATA_FINAL
            chbHabilitaImportador.Checked = HABILITADO_IMPORTADOR
            chbHabilitaData.Checked = HABILITADO_DATA
            chbXmlContabilidade.Checked = HABILITADO_CONVERTER_XML
            txSenhaConfiguracao.Text = SENHA
            txtEcfPadrao.Text = ECF_PADRAO
            chbRzConsul.Checked = RZ_CONSOLIDADO
            txDestinoNFCe.Text = DESTINO_NFCE
            txDestinoErro.Text = DESTINO_ERROS
            chbXmlContabilidade.Checked = HABILITADO_CONVERTER_XML
            chbOnline.Checked = CONVERTER_ONLINE
            chbImportaRz.Checked = HABILITAR_Z
            chbRzRepetida.Checked = HABILITAR_Z_REPETIDA
            chbRzEmporium.Checked = HABILITAR_Z_EMPORIUM
            chbRzRecarga.Checked = HABILITAR_Z_RECARGA
            chbEmail.Checked = HABILITAR_EMAIL
            chbmonitorpasta.Checked = HABILITAR_MONITORAMENTO
            chbImportarNFCe.Checked = HABILITAR_IMPORTAR_NFCE
            chbScannTech.Checked = HABILITAR_IMPORTAR_SCANNTECH
            chbEntrega.Checked = HABILITAR_IMPORTAR_ENTREGA
            txLinkContabiliade.Text = DIRETORIO_COTABILIDADE
            chbContabilidade.Checked = HABILITAR_DIRETORIO_COTABILIDADE
            chbNFCeCancelada.Checked = HABILITAR_VENDAS_CANCELADAS
            cbCertificado.Text = CERTIFICADO_DIGITAL
            cbAmbiente.Text = AMBIENTE
            txCupomHoraInicial.Text = HORA_INICIAL
            txCupomHoraFinal.Text = HORA_FINAL
            txNumeroArquivos.Text = NMR_ARQUIVOS

            'btOcultar_Click(Nothing, Nothing)

            txConexaoBanco.Text = ODBC_BANCO
            txConexaoPorta.Text = ODBC_PORTA
            txConexaoSenha.Text = ODBC_SENHA
            txConexaoServidor.Text = ODBC_SERVIDOR
            txConexaoUsuario.Text = ODBC_USUARIO


            txConexaoBancoNFce.Text = WB_ODBC_BANCO
            txConexaoPortaNfce.Text = WB_ODBC_PORTA
            txConexaoSenhaNfce.Text = WB_ODBC_SENHA
            txConexaoServidorNFce.Text = WB_ODBC_SERVIDOR
            txConexaoUsuarioNfce.Text = WB_ODBC_USUARIO

            txBancoMysql.Text = WB_MYSQL_BANCO
            txServidorMySql.Text = WB_MYSQL_SERVIDOR
            txPortaMySql.Text = WB_MYSQL_PORTA
            txUsuarioMySql.Text = WB_MYSQL_USUARIO
            txSenhaMySql.Text = WB_MYSQL_SENHA

            txBancoEntregaFdb.Text = FIREBIRD_SERVIDOR_ENTREGA

            'Carregar dados de Configuração de E-mail
            txServidorSMTP.Text = EMAIL_SMTP
            txPorta.Text = EMAIL_PORTA
            chbSSL.Checked = EMAIL_SEGURANCA
            txMonitorTempo.Text = EMAIL_TEMPO_MONITOR
            txUsuario.Text = EMAIL_USUARIO
            txRemetente.Text = EMAIL_REMETENTE
            txAssunto.Text = EMAIL_ASSUNTO
            txDestinatario.Text = EMAIL_DESTINATARIO


            'Carregar Informações Vendas
            chbVendasTeorema.Checked = HABILITAR_VENDAS_TEOREMA
            txHoraInicial1.Text = VENDAS_HORA_INICIAL
            txHoraFinal1.Text = VENDAS_HORA_FINAL
            txHoraInicial2.Text = VENDAS_HORA_INICIAL_2
            txHoraFinal2.Text = VENDAS_HORA_FINAL_2

            Dim dia() As String
            dia = VENDAS_DIA_SEMANA.Split(",")

            For i = 0 To dia.Length - 1
                PREENCHER_DIAS(dia(i))
            Next

            Dim cripto As New cls_crypto
            txSenha.Text = cripto.Decrypt(EMAIL_SENHA)
            If TIPO_CONEXAO = "N" Then
                rbConexaoNormal.Checked = True
            ElseIf TIPO_CONEXAO = "O" Then
                rbConexaoODBC.Checked = True
            End If

            Me.Hide()
        Catch

        End Try
        If Not File.Exists("c:\WBPORT.ini") Then
            DESABILITAR()
            HABILITAR()
            Me.Show()
            Me.WindowState = FormWindowState.Normal
        Else
            DESABILITAR()
        End If

        If Not IO.Directory.Exists(txDestinoVendas.Text) Then
            Exit Sub
        End If
        Dim counter = My.Computer.FileSystem.GetFiles(txDestinoVendas.Text)
        Label79.Text = "Total de Arquivos ás :" & Now
        txContador.Text = counter.Count


        tmMonitorPasta.Interval = (100 * 600) * IIf(IIf(txMonitorTempo.Text = "", 0, txMonitorTempo.Text) = 0, 1, IIf(txMonitorTempo.Text = "", 0, txMonitorTempo.Text))

        ''Habilitar Envio de Email
        'If chbEmail.Checked = True Then
        '    tmMonitorPasta.Enabled = True
        '    chbmonitorpasta.Checked = True
        'ElseIf chbEmail.Checked = False Then
        '    tmMonitorPasta.Enabled = False
        '    chbmonitorpasta.Checked = False
        'End If

        ''Habilitar monitoramento de pasta
        'If chbmonitorpasta.Checked = True Then
        '    Timer2.Enabled = True
        'ElseIf chbmonitorpasta.Checked = False Then
        '    Timer2.Enabled = False
        'End If



    End Sub

    Private Sub cmdConexaoNormalSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Salvar configurações
        btSalvar_Click(Nothing, Nothing)
    End Sub

    Private Sub btSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSalvar.Click
        'Salvar Ini
        ORIGEM_VENDAS = txOrigemArquivos.Text
        DESTINO_VENDAS = txDestinoVendas.Text
        DESTINO_SCANNTECH = txDestinoScannTech.Text
        DESTINO_RZ = txDestinoZ.Text
        DESTINO_NFCE = txDestinoNFCe.Text
        DESTINO_ERROS = txDestinoErro.Text
        EXTENSAO = txExtensao.Text
        TEMPO_IMPORTADOR = txTempo.Text
        DATA_INICIAL = dtInicial.Value
        DATA_FINAL = dtFinal.Value
        HABILITADO_IMPORTADOR = CStr(chbHabilitaImportador.Checked)
        HABILITADO_DATA = CStr(chbHabilitaData.Checked)
        HABILITADO_CONVERTER_XML = CStr(chbXmlContabilidade.Checked)
        CONVERTER_ONLINE = CStr(chbOnline.Checked)
        SENHA = txSenhaConfiguracao.Text
        RZ_CONSOLIDADO = CStr(chbRzConsul.Checked)
        HABILITAR_Z = CStr(chbImportaRz.Checked)
        HABILITAR_Z_REPETIDA = CStr(chbRzRepetida.Checked)
        HABILITAR_Z_EMPORIUM = CStr(chbRzEmporium.Checked)
        HABILITAR_Z_RECARGA = CStr(chbRzRecarga.Checked)
        HABILITAR_MONITORAMENTO = chbmonitorpasta.Checked
        HABILITAR_IMPORTAR_NFCE = chbImportarNFCe.Checked
        HABILITAR_IMPORTAR_SCANNTECH = chbScannTech.Checked
        HABILITAR_IMPORTAR_ENTREGA = chbEntrega.Checked
        HABILITAR_EMAIL = chbEmail.Checked
        ECF_PADRAO = txtEcfPadrao.Text
        DIRETORIO_COTABILIDADE = txLinkContabiliade.Text
        HABILITAR_DIRETORIO_COTABILIDADE = chbContabilidade.Checked
        HABILITAR_VENDAS_CANCELADAS = chbNFCeCancelada.Checked
        CERTIFICADO_DIGITAL = cbCertificado.Text
        AMBIENTE = cbAmbiente.Text
        HORA_INICIAL = txCupomHoraInicial.Text
        HORA_FINAL = txCupomHoraFinal.Text
        NMR_ARQUIVOS = txNumeroArquivos.Text

        HABILITAR_VENDAS_TEOREMA = chbVendasTeorema.Checked
        VENDAS_HORA_INICIAL = txHoraInicial1.Text
        VENDAS_HORA_FINAL = txHoraFinal1.Text
        VENDAS_HORA_INICIAL_2 = txHoraInicial2.Text
        VENDAS_HORA_FINAL_2 = txHoraFinal2.Text

        VENDAS_DIA_SEMANA = ""

        If chbDomingo.Checked = True Then VENDAS_DIA_SEMANA += "1,"
        If chbSegunda.Checked = True Then VENDAS_DIA_SEMANA += "2,"
        If chbTerca.Checked = True Then VENDAS_DIA_SEMANA += "3,"
        If chbQuarta.Checked = True Then VENDAS_DIA_SEMANA += "4,"
        If chbQuinta.Checked = True Then VENDAS_DIA_SEMANA += "5,"
        If chbSexta.Checked = True Then VENDAS_DIA_SEMANA += "6,"
        If chbSabado.Checked = True Then VENDAS_DIA_SEMANA += "7"

        'Banco NFC-e
        WB_XML_NFCE_BANCO = txConexaoBancoNFce.Text

        If rbConexaoNormal.Checked = True Then
            TIPO_CONEXAO = "N"
        ElseIf rbConexaoODBC.Checked = True Then
            TIPO_CONEXAO = "O"
        End If

        'Dados Teorema
        ODBC_BANCO = txConexaoBanco.Text
        ODBC_PORTA = txConexaoPorta.Text
        ODBC_SENHA = txConexaoSenha.Text
        ODBC_SERVIDOR = txConexaoServidor.Text
        ODBC_USUARIO = txConexaoUsuario.Text

        'Dados WB_PORT
        WB_ODBC_BANCO = txConexaoBancoNFce.Text
        WB_ODBC_PORTA = txConexaoPortaNfce.Text
        WB_ODBC_SENHA = txConexaoSenhaNfce.Text
        WB_ODBC_SERVIDOR = txConexaoServidorNFce.Text
        WB_ODBC_USUARIO = txConexaoUsuarioNfce.Text

        'Dados MySql
        WB_MYSQL_BANCO = txBancoMysql.Text
        WB_MYSQL_SERVIDOR = txServidorMySql.Text
        WB_MYSQL_PORTA = txPortaMySql.Text
        WB_MYSQL_USUARIO = txUsuarioMySql.Text
        WB_MYSQL_SENHA = txSenhaMySql.Text

        'Dados Access
        FIREBIRD_SERVIDOR_ENTREGA = txBancoEntregaFdb.Text

        'Salvar dados de Configuração de E-mail
        EMAIL_SMTP = txServidorSMTP.Text
        EMAIL_PORTA = txPorta.Text
        EMAIL_SEGURANCA = chbSSL.Checked
        EMAIL_TEMPO_MONITOR = txMonitorTempo.Text
        EMAIL_USUARIO = txUsuario.Text
        EMAIL_REMETENTE = txRemetente.Text
        EMAIL_ASSUNTO = txAssunto.Text
        EMAIL_DESTINATARIO = txDestinatario.Text
        Dim cripto As New cls_crypto
        EMAIL_SENHA = cripto.Encrypt(txSenha.Text)

        ESCREVER_INI_IMPORTADOR()
    End Sub

    'Habilita/Desabilita Importação
    Private Sub chbHabilitaImportador_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chbHabilitaImportador.CheckedChanged
        'Inicia Importação 
        If chbHabilitaImportador.Checked = False Then
            tmInicioImportacao.Enabled = False
        ElseIf chbHabilitaImportador.Checked = True Then
            tmInicioImportacao.Enabled = True
        End If
    End Sub

    'Inicia Importação
    Private Sub tmInicioImportacao_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmInicioImportacao.Tick

        If txTempo.Text = "" Then
            txTempo.Text = 1
        ElseIf txTempo.Text < 1 Then
            txTempo.Text = 1
        End If
        tmInicioImportacao.Interval = CInt(txTempo.Text * 1000)

        If chbHabilitaImportador.Checked = True Then
            If VERIFICAR_PASTA() = True Then
                'Limpar Variaveis
                LIMPAR_DADOS()

                'Ler arquivos do diretório
                LER_ARQUIVOS2()
            End If

        End If
    End Sub

    'Exportar XML banco de dados
    Private Sub btExportarXml_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btExportarXml.Click
        Dim SQL, XML, DATA, NOTA, CHAVE As String
        Dim i As Integer
        Dim fluxoTextogravado As IO.StreamWriter
        If txNfceCaminhoExportacao.Text = "" Then
            MsgBox("Selecione o Diretorio para Exportação dos Arquivos")
            txNfceCaminhoExportacao.Select()
            Exit Sub
        End If

        'Ativar/Desativar importação NFC-e
        If chbImportarNFCe.Checked = False Then
            Exit Sub
        End If

        'Consulta Informações no Banco
        SQL = ""
        XML = ""
        TABELA = "WB_XML_NFCE"
        SQL = "select * from wb_xml_nfce w " _
            & "where w.wb_data between '" & Format(CDate(dtNfceDataIni.Value), "yyyy.MM.dd") _
            & "' and '" & Format(CDate(dtNfceFinal.Value), "yyyy.MM.dd") & "'" _
            & " and WB_CHAVE like'%" & txNfceChave.Text & "%'" _
            & " and WB_EMPRESA_CNPJ like '%" & txNfceLoja.Text & "%'" _
            & " and WB_NM_NFCE like '%" & txNfceNota.Text & "%'"
        Try
            CONEXAO_WB_XML_NFCE(SQL)
            For i = 0 To xs.Tables(TABELA).Rows.Count - 1
                XML = xs.Tables(TABELA).Rows(i)("WB_XML_NFCE").ToString()
                DATA = xs.Tables(TABELA).Rows(i)("WB_DATA").ToString()
                CHAVE = xs.Tables(TABELA).Rows(i)("WB_CHAVE").ToString()
                NOTA = xs.Tables(TABELA).Rows(i)("WB_NM_NFCE").ToString()

                fluxoTextogravado = New IO.StreamWriter(txNfceCaminhoExportacao.Text & Replace(Format(CDate(DATA), "dd.MM.yyyy"), "/", "-") & "-" & CHAVE & "-" & NOTA & ".xml", False)
                fluxoTextogravado.Write(XML)
                fluxoTextogravado.Close()
            Next

            MsgBox("Total de Itens Exportados " & i)
        Catch
            MsgBox("Não existe arquivos com o período selecionado", vbCritical, "Erro")
        End Try

    End Sub

    Private Sub btLocalExportacao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btLocalExportacao.Click
        'Exportar XML
        FolderBrowserDialog1.ShowDialog()
        txNfceCaminhoExportacao.Text = Replace(FolderBrowserDialog1.SelectedPath & "\", "\\", "\")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        frmRelNFCe.Show()
    End Sub

    Private Sub ntfMinimizar_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ntfMinimizar.MouseDoubleClick
        Me.Show()
        Me.WindowState = FormWindowState.Normal
    End Sub

    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        If (Me.WindowState = FormWindowState.Minimized) Then
            Me.Hide()
        End If
    End Sub

    Private Sub chbXmlContabilidade_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chbXmlContabilidade.CheckedChanged
        If chbXmlContabilidade.Checked = True Then
            '    chbOnline.Visible = True
        ElseIf chbXmlContabilidade.Checked = False Then
            '   chbOnline.Visible = False
        End If
    End Sub

    Private Sub btSairConfiguracao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSairConfiguracao.Click
        DESABILITAR()
    End Sub

    Private Sub SairToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SairToolStripMenuItem.Click
        End
    End Sub

    Private Sub ConfiguraçõesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguraçõesToolStripMenuItem.Click

        If Application.OpenForms.OfType(Of frmSenha)().Count() > 0 Then
            MessageBox.Show("O Formulário já está aberto!")
        Else
            'Colocar em Modal o Form
            frmSenha.ShowDialog(Me)
        End If
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        Me.Show()
        Me.WindowState = FormWindowState.Normal
    End Sub

    Private Sub btVisualizarXml_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btVisualizarXml.Click
        MsgBox("Em Desenvolvimento")
    End Sub

    Private Sub btEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btEnviar.Click
        If chbEmail.Checked = True Then
            Try
                send()
                ToolStripStatusLabel1.Text = "E-mail enviado com sucesso! " & Now
                ToolStripStatusLabel1.ForeColor = Color.Green
            Catch ex As Exception
                ToolStripStatusLabel1.Text = "Falha!" & ex.ToString & " " & Now
                ToolStripStatusLabel1.ForeColor = Color.Red
            End Try
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim abrirArquivo As OpenFileDialog = New OpenFileDialog()
        Dim caminho As DialogResult
        Dim arquivo As String
        abrirArquivo.Filter = "Arquivo de Banco De Dados|*."
        caminho = abrirArquivo.ShowDialog
        arquivo = abrirArquivo.FileName
        txAnexo.Text = arquivo
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmMonitorPasta.Tick
        'If chbmonitorpasta.Checked = True Then
        '    Try
        '        Dim counter = My.Computer.FileSystem.GetFiles(txDestinoVendas.Text)

        '        If txContador.Text = counter.Count Then
        '            If counter.Count > 10 Then
        '                txMensagem.Text = "Numero de Arquivos Restantes Para Importar - " & CStr(counter.Count)
        '                btEnviar_Click(Nothing, Nothing)
        '            End If
        '        Else
        '            Label79.Text = "Total de Arquivos ás :" & Now
        '            txContador.Text = counter.Count
        '        End If
        '    Catch
        '        Label79.Text = Err.Description
        '    End Try
        'End If
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        '    If chbmonitorpasta.Checked = True Then
        '        If Not IO.Directory.Exists(txDestinoVendas.Text) Then
        '            ToolStripStatusLabel3.Text = "ERRO - Diretório " & txDestinoVendas.Text & " Não encontrado"
        '            StatusStrip1.BackColor = Color.Red
        '            Exit Sub
        '        Else

        '            Me.Text = "WBPort - Enviar E-mail - " & Now
        '            Dim counter = My.Computer.FileSystem.GetFiles(txDestinoVendas.Text)
        '            ToolStripStatusLabel3.Text = "Total de Arquivos em " & txDestinoVendas.Text & " - " & counter.Count

        '            If chbHabilitaImportador.Checked = True Then
        '                StatusStrip1.BackColor = Color.LimeGreen
        '            Else
        '                StatusStrip1.BackColor = Me.BackColor
        '            End If

        '        End If
        '    End If


    End Sub

    Private Sub btEmailSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btEmailSalvar.Click
        btSalvar_Click(Nothing, Nothing)
        MsgBox("Dados Salvos com sucesso!")
    End Sub

    Private Sub chbmonitorpasta_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chbmonitorpasta.CheckedChanged

        If chbmonitorpasta.Checked = True Then
            Timer2.Enabled = True
        ElseIf chbmonitorpasta.Checked = False Then
            Timer2.Enabled = False
        End If

    End Sub

    Private Sub chbEmail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chbEmail.CheckedChanged
        If chbEmail.Checked = True Then
            tmMonitorPasta.Enabled = True
            chbmonitorpasta.Checked = True
        ElseIf chbEmail.Checked = False Then
            tmMonitorPasta.Enabled = False
            chbmonitorpasta.Checked = False
        End If
    End Sub

    Private Sub btLocalizarBancoTeorema_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btLocalizarBancoTeorema.Click
        Dim FDB As New OpenFileDialog
        'Selecionar Origem dos Arquivos
        FDB.Filter = "Firebird (*.FDB)|*.FDB|Todos Arquivos (*.*)|*.*"
        FDB.ShowDialog()
        txConexaoBanco.Text = FDB.FileName
    End Sub

    Private Sub btLocalizarBancoNfce_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btLocalizarBancoNfce.Click
        Dim FDB As New OpenFileDialog
        'Selecionar Origem dos Arquivos
        FDB.Filter = "Firebird (*.FDB)|*.FDB|Todos Arquivos (*.*)|*.*"
        FDB.ShowDialog()
        txConexaoBancoNFce.Text = FDB.FileName
    End Sub

    Private Sub cmdConexaoNormalSalvar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConexaoNormalSalvar.Click
        btSalvar_Click(Nothing, Nothing)
    End Sub

    Private Sub btDestinoScannTech_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDestinoScannTech.Click
        'Selecionar destino dos Arquivos ScannTech
        FolderBrowserDialog1.ShowDialog()
        txDestinoScannTech.Text = Replace(FolderBrowserDialog1.SelectedPath & "\", "\\", "\")
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim FDB As New OpenFileDialog
        'Selecionar Origem dos Arquivos
        FDB.Filter = "Firebird (*.FDB)|*.FDB|Todos Arquivos (*.*)|*.*"
        FDB.ShowDialog()
        txBancoEntregaFdb.Text = FDB.FileName
    End Sub

    Private Sub btDestinoErro_Click(sender As Object, e As EventArgs) Handles btDestinoErro.Click
        'Selecionar destino dos Arquivos XML com erro.
        FolderBrowserDialog1.ShowDialog()
        txDestinoErro.Text = Replace(FolderBrowserDialog1.SelectedPath & "\", "\\", "\")
    End Sub

    Private Sub cbCertificado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCertificado.SelectedIndexChanged
        Dim ListaCertificados As New List(Of X509Certificate2)()

        Dim Store As X509Store = New X509Store(StoreName.My, StoreLocation.CurrentUser)
        Store.Open(OpenFlags.ReadOnly Or OpenFlags.OpenExistingOnly)

        Dim _certs As X509Certificate2Collection = Store.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, True).Find(X509FindType.FindByKeyUsage, X509KeyUsageFlags.DigitalSignature, True)

        For Each Certificado In _certs

            If Certificado.Subject = cbCertificado.Text Then
                lbValidade.Text = "Data de Validade: " & Certificado.GetExpirationDateString
                Exit For
            End If

        Next
    End Sub

    Private Sub txCupomHoraFinal_KeyDown(sender As Object, e As KeyEventArgs) Handles txCupomHoraFinal.KeyDown
        If e.KeyCode <> 8 Then
            With txCupomHoraFinal
                Select Case Len(.Text)
                    Case 2
                        .Text += ":"
                        .SelectionStart = 3
                    Case 5
                        .Text += ":"
                        .SelectionStart = 6
                End Select
            End With
        End If
    End Sub

    Private Sub txCupomHoraInicial_KeyDown(sender As Object, e As KeyEventArgs) Handles txCupomHoraInicial.KeyDown
        If e.KeyCode <> 8 Then
            With txCupomHoraInicial
                Select Case Len(.Text)
                    Case 2
                        .Text += ":"
                        .SelectionStart = 3
                    Case 5
                        .Text += ":"
                        .SelectionStart = 6
                End Select
            End With
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles btAbrir.Click
        'StartPosition = CenterScreen
        Call Shell("C:\teorema\bin\Vendas.exe", vbMaximizedFocus) 'vbNormal  ' Coloque isto no seu botão e execute.
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles btFechar.Click
        Call Shell("taskkill /f /im vendas.exe")
    End Sub

    Private Sub txHoraInicial1_KeyDown(sender As Object, e As KeyEventArgs) Handles txHoraInicial1.KeyDown
        If Len(txHoraInicial1.Text) = 2 And e.KeyCode <> 8 Then
            txHoraInicial1.Text = txHoraInicial1.Text & ":"
            txHoraInicial1.SelectionStart = 3

        End If

        If Len(txHoraInicial1.Text) = 5 And e.KeyCode <> 8 Then
            txHoraInicial1.Text = txHoraInicial1.Text & ":"
            txHoraInicial1.SelectionStart = 6
        End If

        txHoraInicial1.MaxLength = 8
    End Sub

    Private Sub txHoraFinal1_KeyDown(sender As Object, e As KeyEventArgs) Handles txHoraFinal1.KeyDown
        If Len(txHoraFinal1.Text) = 2 And e.KeyCode <> 8 Then
            txHoraFinal1.Text = txHoraFinal1.Text & ":"
            txHoraFinal1.SelectionStart = 3

        End If

        If Len(txHoraFinal1.Text) = 5 And e.KeyCode <> 8 Then
            txHoraFinal1.Text = txHoraFinal1.Text & ":"
            txHoraFinal1.SelectionStart = 6
        End If

        txHoraFinal1.MaxLength = 8
    End Sub


    Private Sub txHoraInicial2_KeyDown(sender As Object, e As KeyEventArgs) Handles txHoraInicial2.KeyDown
        If Len(txHoraInicial2.Text) = 2 And e.KeyCode <> 8 Then
            txHoraInicial2.Text = txHoraInicial2.Text & ":"
            txHoraInicial2.SelectionStart = 3

        End If

        If Len(txHoraInicial2.Text) = 5 And e.KeyCode <> 8 Then
            txHoraInicial2.Text = txHoraInicial2.Text & ":"
            txHoraInicial2.SelectionStart = 6
        End If

        txHoraInicial2.MaxLength = 8
    End Sub

    Private Sub txHoraFinal2_KeyDown(sender As Object, e As KeyEventArgs) Handles txHoraFinal2.KeyDown
        If Len(txHoraFinal2.Text) = 2 And e.KeyCode <> 8 Then
            txHoraFinal2.Text = txHoraFinal2.Text & ":"
            txHoraFinal2.SelectionStart = 3

        End If

        If Len(txHoraFinal2.Text) = 5 And e.KeyCode <> 8 Then
            txHoraFinal2.Text = txHoraFinal2.Text & ":"
            txHoraFinal2.SelectionStart = 6
        End If

        txHoraFinal2.MaxLength = 8
    End Sub


    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim path As String = "c:\teorema\bin\vendas.exe"
        Dim appVendasInfos = New ProcessStartInfo(path)
        'appVendasInfos.WindowStyle = ProcessWindowStyle.Maximized
        Dim appvendas = Process.Start(appVendasInfos)
        'Dim initThread As Boolean = True
        'Dim threadTimer As System.Threading.Thread
        'Dim Timeout As Boolean = True
        'Dim countTrys As Integer = 0
        'Dim maxTrys As Integer = 1

        Using (appvendas)
            Do

                If Not appvendas.HasExited Then
                    appvendas.Refresh()

                    If appvendas.Responding Then
                        Application.DoEvents()
                        lbVendas.Text = "Status:Respondendo"
                    Else
                        lbVendas.Text = "Status:Não Respondendo"
                        Application.DoEvents()

                        If initThread Then
                            initThread = False
                            threadTimer = New Thread(AddressOf initTimer)
                            threadTimer.Start()
                        End If

                        If Timeout Then
                            timeOut = False
                            threadTimer.Abort()
                            appvendas.Kill()
                        End If

                    End If
                End If


            Loop While Not appvendas.WaitForExit(2000)

            appvendas.CloseMainWindow()
            appvendas.Close()
            initThread = True

            If countTrys < maxTrys Then
                countTrys += 1
                Button4.PerformClick()
            Else
                Dim controler = New Controller()
                Dim cliente As String = "TESTE VENDAS"
                'controler.enviarEmail(cliente)
            End If
        End Using


    End Sub

    Private Shared Sub initTimer()
        'Dim timeOutTimer As Integer = 5000
        timer = New System.Timers.Timer(timeOutTimer)
        timer.Enabled = True
        AddHandler Timer.Elapsed, AddressOf reiniciarAplicacao
    End Sub
    Private Shared Sub reiniciarAplicacao(ByVal sender As Object, ByVal e As EventArgs)
        'Dim timer As System.Timers.Timer
        timeOut = True
        timer.Enabled = False
    End Sub

    Private Sub tmVendas_Tick(sender As Object, e As EventArgs) Handles tmVendas.Tick

        'Dim path As String = "c:\teorema\bin\vendas.exe"
        'Dim appVendasInfos = New ProcessStartInfo(path)
        'appVendasInfos.WindowStyle = ProcessWindowStyle.Maximized
        'Dim appvendas = Process.Start(appVendasInfos)
        ''Dim initThread As Boolean = True
        ''Dim threadTimer As System.Threading.Thread
        ''Dim Timeout As Boolean = True
        ''Dim countTrys As Integer = 0
        ''Dim maxTrys As Integer = 1

        'If countTrys = 1 Then Exit Sub

        'Using (appvendas)
        '    Do

        '        If Not appvendas.HasExited Then
        '            appvendas.Refresh()

        '            If appvendas.Responding Then
        '            Else

        '                If initThread Then
        '                    initThread = False
        '                    Control.CheckForIllegalCrossThreadCalls = False
        '                    threadTimer = New Thread(AddressOf initTimer)
        '                    threadTimer.Start()
        '                End If

        '                If Timeout Then
        '                    Timeout = False
        '                    threadTimer.Abort()
        '                    appvendas.Kill()
        '                End If
        '            End If
        '        End If
        '        Application.DoEvents()
        '        lbVendas.Text = initThread
        '    Loop While Not appvendas.WaitForExit(2000)

        '    appvendas.CloseMainWindow()
        '    appvendas.Close()
        '    initThread = True

        '    If countTrys < maxTrys Then
        '        countTrys += 1
        '        'Button1.PerformClick()
        '    Else
        '        Dim controler = New Controller()
        '        Dim cliente As String = "TESTE VENDAS"
        '        'controler.enviarEmail(cliente)

        '    End If
        'End Using


        'Exit Sub

        If chbVendasTeorema.Checked = True Then
            'Verificar se Aplicativo Esta Aberto
            Dim ativo As Boolean
            Dim myprocesses As Process()
            myprocesses = Process.GetProcessesByName("vendas") 'obter processos com o nome X
            If myprocesses.Length > 0 Then 'se for maior que 0 então existem processos com o nome X
                ativo = True 'está a correr
                lbStatusVendas.Text = "Status:ABERTO"
            Else
                ativo = False 'não está a correr
                lbStatusVendas.Text = "Status:FECHADO"
            End If

            If VALIDAR_DATA_VENDAS_TEOREMA() = True Then
                If ativo = False Then
                    Button5_Click(Nothing, Nothing)
                End If
                Exit Sub
            End If

            'Preenche valores dos horarios
            Dim status() As String = lbStatusVendas.Text.Split(":")
            'Hora 1
            Dim hora_inicial As Date = Format(Now, "dd/MM/yyyy") & " " & txHoraInicial1.Text
            Dim hora_final As Date = Format(Now, "dd/MM/yyyy") & " " & txHoraFinal1.Text
            'Hora 2
            Dim hora_inicial2 As Date = Format(Now, "dd/MM/yyyy") & " " & txHoraInicial2.Text
            Dim hora_final2 As Date = Format(Now, "dd/MM/yyyy") & " " & txHoraFinal2.Text

            Dim hora_atual As Date = Now

            If hora_final < hora_inicial Then
                hora_final = hora_final.AddDays(1)
            End If

            If hora_final2 < hora_inicial2 Then
                hora_final2 = hora_final2.AddDays(1)
            End If

            'Verificar Data
            If (hora_atual >= hora_inicial And hora_atual <= hora_final) Then
                ABRE_VENDAS(hora_inicial, hora_final, status(1))
            ElseIf (hora_atual >= hora_inicial2) And (hora_atual <= hora_final2) Then
                ABRE_VENDAS(hora_inicial2, hora_final2, status(1))
            Else
                Select Case status(1)
                    Case "ABERTO"
                        'MsgBox("ABERTO")
                        Call Shell("taskkill /f /im vendas.exe")
                        lbStatusVendas.Text = "Status:FECHADO"
                    Case "FECHADO"
                        'MsgBox("FECHADO")
                End Select
            End If

        End If
    End Sub

    Private Sub Button5_Click_1(sender As Object, e As EventArgs) Handles Button5.Click
        'CLASSE_TXT.VALIDAR_SUBTOTAL_DUPLICADO("c:\teste\teste.xml")
    End Sub
End Class
