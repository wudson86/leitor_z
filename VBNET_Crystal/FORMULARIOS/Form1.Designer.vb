﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.lbVendas = New System.Windows.Forms.Label()
        Me.chbHabilitarVendas = New System.Windows.Forms.CheckBox()
        Me.Label122 = New System.Windows.Forms.Label()
        Me.txNumeroArquivos = New System.Windows.Forms.TextBox()
        Me.Label121 = New System.Windows.Forms.Label()
        Me.Label118 = New System.Windows.Forms.Label()
        Me.txCupomHoraFinal = New System.Windows.Forms.TextBox()
        Me.txCupomHoraInicial = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.btDestinoErro = New System.Windows.Forms.Button()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.txDestinoErro = New System.Windows.Forms.TextBox()
        Me.chbEntrega = New System.Windows.Forms.CheckBox()
        Me.btDestinoScannTech = New System.Windows.Forms.Button()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.txDestinoScannTech = New System.Windows.Forms.TextBox()
        Me.chbScannTech = New System.Windows.Forms.CheckBox()
        Me.chbImportarNFCe = New System.Windows.Forms.CheckBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.LOJA_VENDAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NUMERO_DEVOLUCAO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PDV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TICKET_DEVOLUCAO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SEQUENCIA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PLU = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRICAO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTIDADE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VALOR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DEPARTAMENTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MAKER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.REASON = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRANSACAO_VENDAS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chbEmail = New System.Windows.Forms.CheckBox()
        Me.chbmonitorpasta = New System.Windows.Forms.CheckBox()
        Me.chbOnline = New System.Windows.Forms.CheckBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.txSenhaConfiguracao = New System.Windows.Forms.TextBox()
        Me.btSairConfiguracao = New System.Windows.Forms.Button()
        Me.chbHabilitaImportador = New System.Windows.Forms.CheckBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.chbHabilitaData = New System.Windows.Forms.CheckBox()
        Me.dtFinal = New System.Windows.Forms.DateTimePicker()
        Me.dtInicial = New System.Windows.Forms.DateTimePicker()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.btSalvar = New System.Windows.Forms.Button()
        Me.lstArquivos = New System.Windows.Forms.ListBox()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.txtVersao = New System.Windows.Forms.TextBox()
        Me.chbXmlContabilidade = New System.Windows.Forms.CheckBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.txExtensao = New System.Windows.Forms.TextBox()
        Me.txTempo = New System.Windows.Forms.TextBox()
        Me.btDestinoNFCe = New System.Windows.Forms.Button()
        Me.btDestinoZ = New System.Windows.Forms.Button()
        Me.btDestinoVendas = New System.Windows.Forms.Button()
        Me.btOrigemArquivos = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txDestinoNFCe = New System.Windows.Forms.TextBox()
        Me.txDestinoZ = New System.Windows.Forms.TextBox()
        Me.txDestinoVendas = New System.Windows.Forms.TextBox()
        Me.txOrigemArquivos = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.lstDados = New System.Windows.Forms.ListBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.A2 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.A1 = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.B1 = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.I1 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.I8 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.B8 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.A8 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.B2 = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.I2 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.I7 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.B7 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.A7 = New System.Windows.Forms.TextBox()
        Me.A3 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.B3 = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.I3 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.I6 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.B6 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.A6 = New System.Windows.Forms.TextBox()
        Me.A4 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.B4 = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.I4 = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.I5 = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.B5 = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.A5 = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.transacao = New System.Windows.Forms.TextBox()
        Me.mapa_numero = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.hora = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.naotributado = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.substituicao = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.isento = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Acrescimo = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.sangria = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.desconto = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.cancelamento = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Movimento = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.fabricante = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.impressora = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.loja = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.crz = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.cro = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.gt_final = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.gt_inicial = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.ticket_final = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.ticket_inicial = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.data_fiscal = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.caixa = New System.Windows.Forms.TextBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.nfce_valor = New System.Windows.Forms.TextBox()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.txTagSitProtocolo = New System.Windows.Forms.TextBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.txDataProtocolo = New System.Windows.Forms.TextBox()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.txProtocolo = New System.Windows.Forms.TextBox()
        Me.txLinkQRCode = New System.Windows.Forms.TextBox()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.txDigestValue = New System.Windows.Forms.TextBox()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.txEmissao = New System.Windows.Forms.TextBox()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.txChaveNFCe = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label115 = New System.Windows.Forms.Label()
        Me.txVesaoNFCe = New System.Windows.Forms.TextBox()
        Me.btVisualizarXml = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.tx_tag_protocolo = New System.Windows.Forms.TextBox()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.btLocalExportacao = New System.Windows.Forms.Button()
        Me.txNfceCaminhoExportacao = New System.Windows.Forms.TextBox()
        Me.btExportarXml = New System.Windows.Forms.Button()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.dtNfceFinal = New System.Windows.Forms.DateTimePicker()
        Me.dtNfceDataIni = New System.Windows.Forms.DateTimePicker()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.txNfceChave = New System.Windows.Forms.TextBox()
        Me.txNfceNota = New System.Windows.Forms.TextBox()
        Me.txNfceLoja = New System.Windows.Forms.TextBox()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.nfce_Data = New System.Windows.Forms.TextBox()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.nfce_Xml = New System.Windows.Forms.TextBox()
        Me.nfce_Chave = New System.Windows.Forms.TextBox()
        Me.nfce_Nota = New System.Windows.Forms.TextBox()
        Me.nfce_Serie = New System.Windows.Forms.TextBox()
        Me.nfce_Loja = New System.Windows.Forms.TextBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label113 = New System.Windows.Forms.Label()
        Me.txBancoEntregaFdb = New System.Windows.Forms.TextBox()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.txSenhaMySql = New System.Windows.Forms.TextBox()
        Me.txPortaMySql = New System.Windows.Forms.TextBox()
        Me.txUsuarioMySql = New System.Windows.Forms.TextBox()
        Me.txServidorMySql = New System.Windows.Forms.TextBox()
        Me.txBancoMysql = New System.Windows.Forms.TextBox()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.btLocalizarBancoNfce = New System.Windows.Forms.Button()
        Me.txConexaoSenhaNfce = New System.Windows.Forms.TextBox()
        Me.txConexaoPortaNfce = New System.Windows.Forms.TextBox()
        Me.txConexaoUsuarioNfce = New System.Windows.Forms.TextBox()
        Me.txConexaoServidorNFce = New System.Windows.Forms.TextBox()
        Me.txConexaoBancoNFce = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.rbConexaoODBC = New System.Windows.Forms.RadioButton()
        Me.rbConexaoNormal = New System.Windows.Forms.RadioButton()
        Me.cmdConexaoNormalSalvar = New System.Windows.Forms.Button()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.btLocalizarBancoTeorema = New System.Windows.Forms.Button()
        Me.txConexaoSenha = New System.Windows.Forms.TextBox()
        Me.txConexaoPorta = New System.Windows.Forms.TextBox()
        Me.txConexaoUsuario = New System.Windows.Forms.TextBox()
        Me.txConexaoServidor = New System.Windows.Forms.TextBox()
        Me.txConexaoBanco = New System.Windows.Forms.TextBox()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.grpFinanceiro = New System.Windows.Forms.GroupBox()
        Me.chbRzEmporium = New System.Windows.Forms.CheckBox()
        Me.chbRzRecarga = New System.Windows.Forms.CheckBox()
        Me.chbRzRepetida = New System.Windows.Forms.CheckBox()
        Me.chbImportaRz = New System.Windows.Forms.CheckBox()
        Me.chbRzMenorZero = New System.Windows.Forms.CheckBox()
        Me.chbRzValor = New System.Windows.Forms.CheckBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.txtEcfPadrao = New System.Windows.Forms.TextBox()
        Me.chbRzConsul = New System.Windows.Forms.CheckBox()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.btEmailSalvar = New System.Windows.Forms.Button()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.txMonitorTempo = New System.Windows.Forms.TextBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.txContador = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.txAnexo = New System.Windows.Forms.TextBox()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.txAssunto = New System.Windows.Forms.TextBox()
        Me.btEnviar = New System.Windows.Forms.Button()
        Me.chbSSL = New System.Windows.Forms.CheckBox()
        Me.txMensagem = New System.Windows.Forms.TextBox()
        Me.txDestinatario = New System.Windows.Forms.TextBox()
        Me.txRemetente = New System.Windows.Forms.TextBox()
        Me.txSenha = New System.Windows.Forms.TextBox()
        Me.txUsuario = New System.Windows.Forms.TextBox()
        Me.txPorta = New System.Windows.Forms.TextBox()
        Me.txServidorSMTP = New System.Windows.Forms.TextBox()
        Me.chbCopiaContabilidade = New System.Windows.Forms.TabPage()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.chbDomingo = New System.Windows.Forms.CheckBox()
        Me.chbSabado = New System.Windows.Forms.CheckBox()
        Me.chbSexta = New System.Windows.Forms.CheckBox()
        Me.chbQuinta = New System.Windows.Forms.CheckBox()
        Me.chbQuarta = New System.Windows.Forms.CheckBox()
        Me.chbTerca = New System.Windows.Forms.CheckBox()
        Me.chbSegunda = New System.Windows.Forms.CheckBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.Label125 = New System.Windows.Forms.Label()
        Me.Label126 = New System.Windows.Forms.Label()
        Me.txHoraFinal2 = New System.Windows.Forms.TextBox()
        Me.txHoraInicial2 = New System.Windows.Forms.TextBox()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.Label124 = New System.Windows.Forms.Label()
        Me.Label123 = New System.Windows.Forms.Label()
        Me.txHoraFinal1 = New System.Windows.Forms.TextBox()
        Me.txHoraInicial1 = New System.Windows.Forms.TextBox()
        Me.lbStatusVendas = New System.Windows.Forms.Label()
        Me.btFechar = New System.Windows.Forms.Button()
        Me.btAbrir = New System.Windows.Forms.Button()
        Me.chbVendasTeorema = New System.Windows.Forms.CheckBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.Label120 = New System.Windows.Forms.Label()
        Me.Label119 = New System.Windows.Forms.Label()
        Me.txIdToken = New System.Windows.Forms.TextBox()
        Me.txCSC = New System.Windows.Forms.TextBox()
        Me.lbValidade = New System.Windows.Forms.Label()
        Me.Label117 = New System.Windows.Forms.Label()
        Me.Label116 = New System.Windows.Forms.Label()
        Me.cbAmbiente = New System.Windows.Forms.ComboBox()
        Me.cbCertificado = New System.Windows.Forms.ComboBox()
        Me.chbNFCeCancelada = New System.Windows.Forms.CheckBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.chbContabilidade = New System.Windows.Forms.CheckBox()
        Me.txLinkContabiliade = New System.Windows.Forms.TextBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.tmInicioImportacao = New System.Windows.Forms.Timer(Me.components)
        Me.ntfMinimizar = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ConfiguraçõesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SairToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.tmMonitorPasta = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.tmVendas = New System.Windows.Forms.Timer(Me.components)
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.grpFinanceiro.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.chbCopiaContabilidade.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(12, 27)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(725, 636)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Button5)
        Me.TabPage1.Controls.Add(Me.lbVendas)
        Me.TabPage1.Controls.Add(Me.chbHabilitarVendas)
        Me.TabPage1.Controls.Add(Me.Label122)
        Me.TabPage1.Controls.Add(Me.txNumeroArquivos)
        Me.TabPage1.Controls.Add(Me.Label121)
        Me.TabPage1.Controls.Add(Me.Label118)
        Me.TabPage1.Controls.Add(Me.txCupomHoraFinal)
        Me.TabPage1.Controls.Add(Me.txCupomHoraInicial)
        Me.TabPage1.Controls.Add(Me.Button4)
        Me.TabPage1.Controls.Add(Me.btDestinoErro)
        Me.TabPage1.Controls.Add(Me.Label114)
        Me.TabPage1.Controls.Add(Me.txDestinoErro)
        Me.TabPage1.Controls.Add(Me.chbEntrega)
        Me.TabPage1.Controls.Add(Me.btDestinoScannTech)
        Me.TabPage1.Controls.Add(Me.Label112)
        Me.TabPage1.Controls.Add(Me.txDestinoScannTech)
        Me.TabPage1.Controls.Add(Me.chbScannTech)
        Me.TabPage1.Controls.Add(Me.chbImportarNFCe)
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Controls.Add(Me.chbEmail)
        Me.TabPage1.Controls.Add(Me.chbmonitorpasta)
        Me.TabPage1.Controls.Add(Me.chbOnline)
        Me.TabPage1.Controls.Add(Me.Label64)
        Me.TabPage1.Controls.Add(Me.txSenhaConfiguracao)
        Me.TabPage1.Controls.Add(Me.btSairConfiguracao)
        Me.TabPage1.Controls.Add(Me.chbHabilitaImportador)
        Me.TabPage1.Controls.Add(Me.Label61)
        Me.TabPage1.Controls.Add(Me.Label62)
        Me.TabPage1.Controls.Add(Me.chbHabilitaData)
        Me.TabPage1.Controls.Add(Me.dtFinal)
        Me.TabPage1.Controls.Add(Me.dtInicial)
        Me.TabPage1.Controls.Add(Me.Label63)
        Me.TabPage1.Controls.Add(Me.btSalvar)
        Me.TabPage1.Controls.Add(Me.lstArquivos)
        Me.TabPage1.Controls.Add(Me.Label84)
        Me.TabPage1.Controls.Add(Me.txtVersao)
        Me.TabPage1.Controls.Add(Me.chbXmlContabilidade)
        Me.TabPage1.Controls.Add(Me.Label59)
        Me.TabPage1.Controls.Add(Me.Label60)
        Me.TabPage1.Controls.Add(Me.txExtensao)
        Me.TabPage1.Controls.Add(Me.txTempo)
        Me.TabPage1.Controls.Add(Me.btDestinoNFCe)
        Me.TabPage1.Controls.Add(Me.btDestinoZ)
        Me.TabPage1.Controls.Add(Me.btDestinoVendas)
        Me.TabPage1.Controls.Add(Me.btOrigemArquivos)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.txDestinoNFCe)
        Me.TabPage1.Controls.Add(Me.txDestinoZ)
        Me.TabPage1.Controls.Add(Me.txDestinoVendas)
        Me.TabPage1.Controls.Add(Me.txOrigemArquivos)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(717, 610)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Importador"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(567, 114)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(103, 27)
        Me.Button5.TabIndex = 99
        Me.Button5.Text = "Button5"
        Me.Button5.UseVisualStyleBackColor = True
        Me.Button5.Visible = False
        '
        'lbVendas
        '
        Me.lbVendas.AutoSize = True
        Me.lbVendas.Location = New System.Drawing.Point(442, 370)
        Me.lbVendas.Name = "lbVendas"
        Me.lbVendas.Size = New System.Drawing.Size(40, 13)
        Me.lbVendas.TabIndex = 98
        Me.lbVendas.Text = "Status:"
        '
        'chbHabilitarVendas
        '
        Me.chbHabilitarVendas.AutoSize = True
        Me.chbHabilitarVendas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbHabilitarVendas.Location = New System.Drawing.Point(440, 344)
        Me.chbHabilitarVendas.Name = "chbHabilitarVendas"
        Me.chbHabilitarVendas.Size = New System.Drawing.Size(230, 17)
        Me.chbHabilitarVendas.TabIndex = 97
        Me.chbHabilitarVendas.Text = "Habilitar Monitoramento Vendas.exe"
        Me.chbHabilitarVendas.UseVisualStyleBackColor = True
        '
        'Label122
        '
        Me.Label122.AutoSize = True
        Me.Label122.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label122.Location = New System.Drawing.Point(214, 343)
        Me.Label122.Name = "Label122"
        Me.Label122.Size = New System.Drawing.Size(161, 13)
        Me.Label122.TabIndex = 96
        Me.Label122.Text = "Total de Arquivos na Pasta"
        '
        'txNumeroArquivos
        '
        Me.txNumeroArquivos.Location = New System.Drawing.Point(216, 359)
        Me.txNumeroArquivos.MaxLength = 8
        Me.txNumeroArquivos.Name = "txNumeroArquivos"
        Me.txNumeroArquivos.Size = New System.Drawing.Size(135, 20)
        Me.txNumeroArquivos.TabIndex = 95
        Me.txNumeroArquivos.Text = "0"
        '
        'Label121
        '
        Me.Label121.AutoSize = True
        Me.Label121.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label121.Location = New System.Drawing.Point(113, 343)
        Me.Label121.Name = "Label121"
        Me.Label121.Size = New System.Drawing.Size(69, 13)
        Me.Label121.TabIndex = 94
        Me.Label121.Text = "Hora Final:"
        '
        'Label118
        '
        Me.Label118.AutoSize = True
        Me.Label118.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label118.Location = New System.Drawing.Point(112, 304)
        Me.Label118.Name = "Label118"
        Me.Label118.Size = New System.Drawing.Size(76, 13)
        Me.Label118.TabIndex = 93
        Me.Label118.Text = "Hora Inicial:"
        '
        'txCupomHoraFinal
        '
        Me.txCupomHoraFinal.Location = New System.Drawing.Point(115, 359)
        Me.txCupomHoraFinal.MaxLength = 8
        Me.txCupomHoraFinal.Name = "txCupomHoraFinal"
        Me.txCupomHoraFinal.Size = New System.Drawing.Size(80, 20)
        Me.txCupomHoraFinal.TabIndex = 92
        Me.txCupomHoraFinal.Text = "23:59:59"
        '
        'txCupomHoraInicial
        '
        Me.txCupomHoraInicial.Location = New System.Drawing.Point(115, 320)
        Me.txCupomHoraInicial.MaxLength = 8
        Me.txCupomHoraInicial.Name = "txCupomHoraInicial"
        Me.txCupomHoraInicial.Size = New System.Drawing.Size(80, 20)
        Me.txCupomHoraInicial.TabIndex = 91
        Me.txCupomHoraInicial.Text = "00:00:00"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(504, 164)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(103, 27)
        Me.Button4.TabIndex = 90
        Me.Button4.Text = "Button4"
        Me.Button4.UseVisualStyleBackColor = True
        Me.Button4.Visible = False
        '
        'btDestinoErro
        '
        Me.btDestinoErro.Location = New System.Drawing.Point(357, 242)
        Me.btDestinoErro.Name = "btDestinoErro"
        Me.btDestinoErro.Size = New System.Drawing.Size(33, 22)
        Me.btDestinoErro.TabIndex = 89
        Me.btDestinoErro.Text = "..."
        Me.btDestinoErro.UseVisualStyleBackColor = True
        '
        'Label114
        '
        Me.Label114.AutoSize = True
        Me.Label114.Location = New System.Drawing.Point(19, 228)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(112, 13)
        Me.Label114.TabIndex = 88
        Me.Label114.Text = "Destino Xml Com Erro:"
        '
        'txDestinoErro
        '
        Me.txDestinoErro.Location = New System.Drawing.Point(22, 244)
        Me.txDestinoErro.Name = "txDestinoErro"
        Me.txDestinoErro.Size = New System.Drawing.Size(329, 20)
        Me.txDestinoErro.TabIndex = 87
        '
        'chbEntrega
        '
        Me.chbEntrega.AutoSize = True
        Me.chbEntrega.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbEntrega.Location = New System.Drawing.Point(440, 321)
        Me.chbEntrega.Name = "chbEntrega"
        Me.chbEntrega.Size = New System.Drawing.Size(126, 17)
        Me.chbEntrega.TabIndex = 86
        Me.chbEntrega.Text = "Importar Entregas"
        Me.chbEntrega.UseVisualStyleBackColor = True
        '
        'btDestinoScannTech
        '
        Me.btDestinoScannTech.Location = New System.Drawing.Point(357, 162)
        Me.btDestinoScannTech.Name = "btDestinoScannTech"
        Me.btDestinoScannTech.Size = New System.Drawing.Size(33, 22)
        Me.btDestinoScannTech.TabIndex = 85
        Me.btDestinoScannTech.Text = "..."
        Me.btDestinoScannTech.UseVisualStyleBackColor = True
        '
        'Label112
        '
        Me.Label112.AutoSize = True
        Me.Label112.Location = New System.Drawing.Point(19, 148)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(102, 13)
        Me.Label112.TabIndex = 84
        Me.Label112.Text = "Destino ScannTech"
        '
        'txDestinoScannTech
        '
        Me.txDestinoScannTech.Location = New System.Drawing.Point(22, 164)
        Me.txDestinoScannTech.Name = "txDestinoScannTech"
        Me.txDestinoScannTech.Size = New System.Drawing.Size(329, 20)
        Me.txDestinoScannTech.TabIndex = 83
        '
        'chbScannTech
        '
        Me.chbScannTech.AutoSize = True
        Me.chbScannTech.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbScannTech.Location = New System.Drawing.Point(440, 208)
        Me.chbScannTech.Name = "chbScannTech"
        Me.chbScannTech.Size = New System.Drawing.Size(141, 17)
        Me.chbScannTech.TabIndex = 82
        Me.chbScannTech.Text = "Importar ScannTech"
        Me.chbScannTech.UseVisualStyleBackColor = True
        '
        'chbImportarNFCe
        '
        Me.chbImportarNFCe.AutoSize = True
        Me.chbImportarNFCe.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbImportarNFCe.Location = New System.Drawing.Point(440, 231)
        Me.chbImportarNFCe.Name = "chbImportarNFCe"
        Me.chbImportarNFCe.Size = New System.Drawing.Size(111, 17)
        Me.chbImportarNFCe.TabIndex = 81
        Me.chbImportarNFCe.Text = "Importar NFC-e"
        Me.chbImportarNFCe.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LOJA_VENDAS, Me.NUMERO_DEVOLUCAO, Me.PDV, Me.DATA, Me.TICKET_DEVOLUCAO, Me.SEQUENCIA, Me.PLU, Me.DESCRICAO, Me.QUANTIDADE, Me.VALOR, Me.DEPARTAMENTO, Me.MAKER, Me.REASON, Me.TRANSACAO_VENDAS})
        Me.DataGridView1.Location = New System.Drawing.Point(20, 541)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(685, 63)
        Me.DataGridView1.TabIndex = 80
        '
        'LOJA_VENDAS
        '
        Me.LOJA_VENDAS.HeaderText = "LOJA"
        Me.LOJA_VENDAS.Name = "LOJA_VENDAS"
        '
        'NUMERO_DEVOLUCAO
        '
        Me.NUMERO_DEVOLUCAO.HeaderText = "NUMERO_DEVOLUCAO"
        Me.NUMERO_DEVOLUCAO.Name = "NUMERO_DEVOLUCAO"
        '
        'PDV
        '
        Me.PDV.HeaderText = "PDV"
        Me.PDV.Name = "PDV"
        '
        'DATA
        '
        Me.DATA.HeaderText = "DATA"
        Me.DATA.Name = "DATA"
        '
        'TICKET_DEVOLUCAO
        '
        Me.TICKET_DEVOLUCAO.HeaderText = "TICKET_DEVOLUCAO"
        Me.TICKET_DEVOLUCAO.Name = "TICKET_DEVOLUCAO"
        '
        'SEQUENCIA
        '
        Me.SEQUENCIA.HeaderText = "SEQUENCIA"
        Me.SEQUENCIA.Name = "SEQUENCIA"
        '
        'PLU
        '
        Me.PLU.HeaderText = "PLU"
        Me.PLU.Name = "PLU"
        '
        'DESCRICAO
        '
        Me.DESCRICAO.HeaderText = "DESCRICAO"
        Me.DESCRICAO.Name = "DESCRICAO"
        '
        'QUANTIDADE
        '
        Me.QUANTIDADE.HeaderText = "QUANTIDADE"
        Me.QUANTIDADE.Name = "QUANTIDADE"
        '
        'VALOR
        '
        Me.VALOR.HeaderText = "VALOR"
        Me.VALOR.Name = "VALOR"
        '
        'DEPARTAMENTO
        '
        Me.DEPARTAMENTO.HeaderText = "DEPARTAMENTO"
        Me.DEPARTAMENTO.Name = "DEPARTAMENTO"
        '
        'MAKER
        '
        Me.MAKER.HeaderText = "MAKER"
        Me.MAKER.Name = "MAKER"
        '
        'REASON
        '
        Me.REASON.HeaderText = "REASON"
        Me.REASON.Name = "REASON"
        '
        'TRANSACAO_VENDAS
        '
        Me.TRANSACAO_VENDAS.HeaderText = "TRANSACAO"
        Me.TRANSACAO_VENDAS.Name = "TRANSACAO_VENDAS"
        '
        'chbEmail
        '
        Me.chbEmail.AutoSize = True
        Me.chbEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbEmail.Location = New System.Drawing.Point(440, 252)
        Me.chbEmail.Name = "chbEmail"
        Me.chbEmail.Size = New System.Drawing.Size(187, 17)
        Me.chbEmail.TabIndex = 78
        Me.chbEmail.Text = "Enviar E-mail de Notificação"
        Me.chbEmail.UseVisualStyleBackColor = True
        '
        'chbmonitorpasta
        '
        Me.chbmonitorpasta.AutoSize = True
        Me.chbmonitorpasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbmonitorpasta.Location = New System.Drawing.Point(440, 275)
        Me.chbmonitorpasta.Name = "chbmonitorpasta"
        Me.chbmonitorpasta.Size = New System.Drawing.Size(251, 17)
        Me.chbmonitorpasta.TabIndex = 77
        Me.chbmonitorpasta.Text = "Monitorar Número de Arquivos na Pasta"
        Me.chbmonitorpasta.UseVisualStyleBackColor = True
        '
        'chbOnline
        '
        Me.chbOnline.AutoSize = True
        Me.chbOnline.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbOnline.Location = New System.Drawing.Point(440, 298)
        Me.chbOnline.Name = "chbOnline"
        Me.chbOnline.Size = New System.Drawing.Size(128, 17)
        Me.chbOnline.TabIndex = 76
        Me.chbOnline.Text = "Converter Online?"
        Me.chbOnline.UseVisualStyleBackColor = True
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(19, 264)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(119, 13)
        Me.Label64.TabIndex = 75
        Me.Label64.Text = "Senha de Configuração"
        '
        'txSenhaConfiguracao
        '
        Me.txSenhaConfiguracao.Location = New System.Drawing.Point(22, 280)
        Me.txSenhaConfiguracao.Name = "txSenhaConfiguracao"
        Me.txSenhaConfiguracao.Size = New System.Drawing.Size(329, 20)
        Me.txSenhaConfiguracao.TabIndex = 74
        '
        'btSairConfiguracao
        '
        Me.btSairConfiguracao.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSairConfiguracao.Location = New System.Drawing.Point(227, 511)
        Me.btSairConfiguracao.Name = "btSairConfiguracao"
        Me.btSairConfiguracao.Size = New System.Drawing.Size(195, 26)
        Me.btSairConfiguracao.TabIndex = 73
        Me.btSairConfiguracao.Text = "Sair Configurações"
        Me.btSairConfiguracao.UseVisualStyleBackColor = True
        '
        'chbHabilitaImportador
        '
        Me.chbHabilitaImportador.AutoSize = True
        Me.chbHabilitaImportador.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbHabilitaImportador.Location = New System.Drawing.Point(217, 323)
        Me.chbHabilitaImportador.Name = "chbHabilitaImportador"
        Me.chbHabilitaImportador.Size = New System.Drawing.Size(160, 17)
        Me.chbHabilitaImportador.TabIndex = 72
        Me.chbHabilitaImportador.Text = "Habilitar Monitoramento"
        Me.chbHabilitaImportador.UseVisualStyleBackColor = True
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.Location = New System.Drawing.Point(17, 343)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(69, 13)
        Me.Label61.TabIndex = 71
        Me.Label61.Text = "Data Final:"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.Location = New System.Drawing.Point(17, 304)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(76, 13)
        Me.Label62.TabIndex = 70
        Me.Label62.Text = "Data Inicial:"
        '
        'chbHabilitaData
        '
        Me.chbHabilitaData.AutoSize = True
        Me.chbHabilitaData.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbHabilitaData.Location = New System.Drawing.Point(217, 302)
        Me.chbHabilitaData.Name = "chbHabilitaData"
        Me.chbHabilitaData.Size = New System.Drawing.Size(104, 17)
        Me.chbHabilitaData.TabIndex = 69
        Me.chbHabilitaData.Text = "Habilitar Data"
        Me.chbHabilitaData.UseVisualStyleBackColor = True
        '
        'dtFinal
        '
        Me.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFinal.Location = New System.Drawing.Point(20, 359)
        Me.dtFinal.Name = "dtFinal"
        Me.dtFinal.Size = New System.Drawing.Size(87, 20)
        Me.dtFinal.TabIndex = 68
        Me.dtFinal.Value = New Date(2014, 5, 1, 0, 0, 0, 0)
        '
        'dtInicial
        '
        Me.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtInicial.Location = New System.Drawing.Point(20, 320)
        Me.dtInicial.Name = "dtInicial"
        Me.dtInicial.Size = New System.Drawing.Size(87, 20)
        Me.dtInicial.TabIndex = 67
        Me.dtInicial.Value = New Date(2014, 5, 1, 0, 0, 0, 0)
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.ForeColor = System.Drawing.Color.Blue
        Me.Label63.Location = New System.Drawing.Point(17, 382)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(60, 13)
        Me.Label63.TabIndex = 66
        Me.Label63.Text = "Arquivos:"
        '
        'btSalvar
        '
        Me.btSalvar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSalvar.Location = New System.Drawing.Point(20, 511)
        Me.btSalvar.Name = "btSalvar"
        Me.btSalvar.Size = New System.Drawing.Size(186, 26)
        Me.btSalvar.TabIndex = 65
        Me.btSalvar.Text = "Salvar Dados"
        Me.btSalvar.UseVisualStyleBackColor = True
        '
        'lstArquivos
        '
        Me.lstArquivos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstArquivos.FormattingEnabled = True
        Me.lstArquivos.Location = New System.Drawing.Point(20, 398)
        Me.lstArquivos.Name = "lstArquivos"
        Me.lstArquivos.Size = New System.Drawing.Size(685, 108)
        Me.lstArquivos.TabIndex = 64
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label84.Location = New System.Drawing.Point(449, 401)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(119, 13)
        Me.Label84.TabIndex = 63
        Me.Label84.Text = "Controle de Versão:"
        Me.Label84.Visible = False
        '
        'txtVersao
        '
        Me.txtVersao.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVersao.Location = New System.Drawing.Point(440, 417)
        Me.txtVersao.Multiline = True
        Me.txtVersao.Name = "txtVersao"
        Me.txtVersao.Size = New System.Drawing.Size(251, 54)
        Me.txtVersao.TabIndex = 62
        Me.txtVersao.Text = "1.0.05a05.29a"
        Me.txtVersao.Visible = False
        '
        'chbXmlContabilidade
        '
        Me.chbXmlContabilidade.AutoSize = True
        Me.chbXmlContabilidade.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbXmlContabilidade.Location = New System.Drawing.Point(440, 298)
        Me.chbXmlContabilidade.Name = "chbXmlContabilidade"
        Me.chbXmlContabilidade.Size = New System.Drawing.Size(216, 17)
        Me.chbXmlContabilidade.TabIndex = 61
        Me.chbXmlContabilidade.Text = "Converter Xml Para Contabilidade"
        Me.chbXmlContabilidade.UseVisualStyleBackColor = True
        Me.chbXmlContabilidade.Visible = False
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(444, 61)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(63, 13)
        Me.Label59.TabIndex = 59
        Me.Label59.Text = "Extensão:"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(444, 25)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(49, 13)
        Me.Label60.TabIndex = 58
        Me.Label60.Text = "Tempo:"
        '
        'txExtensao
        '
        Me.txExtensao.Location = New System.Drawing.Point(447, 77)
        Me.txExtensao.Name = "txExtensao"
        Me.txExtensao.Size = New System.Drawing.Size(70, 20)
        Me.txExtensao.TabIndex = 57
        '
        'txTempo
        '
        Me.txTempo.Location = New System.Drawing.Point(447, 41)
        Me.txTempo.Name = "txTempo"
        Me.txTempo.Size = New System.Drawing.Size(70, 20)
        Me.txTempo.TabIndex = 56
        Me.txTempo.Text = "30"
        '
        'btDestinoNFCe
        '
        Me.btDestinoNFCe.Location = New System.Drawing.Point(357, 203)
        Me.btDestinoNFCe.Name = "btDestinoNFCe"
        Me.btDestinoNFCe.Size = New System.Drawing.Size(33, 22)
        Me.btDestinoNFCe.TabIndex = 11
        Me.btDestinoNFCe.Text = "..."
        Me.btDestinoNFCe.UseVisualStyleBackColor = True
        '
        'btDestinoZ
        '
        Me.btDestinoZ.Location = New System.Drawing.Point(357, 119)
        Me.btDestinoZ.Name = "btDestinoZ"
        Me.btDestinoZ.Size = New System.Drawing.Size(33, 22)
        Me.btDestinoZ.TabIndex = 10
        Me.btDestinoZ.Text = "..."
        Me.btDestinoZ.UseVisualStyleBackColor = True
        '
        'btDestinoVendas
        '
        Me.btDestinoVendas.Location = New System.Drawing.Point(357, 78)
        Me.btDestinoVendas.Name = "btDestinoVendas"
        Me.btDestinoVendas.Size = New System.Drawing.Size(33, 22)
        Me.btDestinoVendas.TabIndex = 9
        Me.btDestinoVendas.Text = "..."
        Me.btDestinoVendas.UseVisualStyleBackColor = True
        '
        'btOrigemArquivos
        '
        Me.btOrigemArquivos.Location = New System.Drawing.Point(357, 38)
        Me.btOrigemArquivos.Name = "btOrigemArquivos"
        Me.btOrigemArquivos.Size = New System.Drawing.Size(33, 22)
        Me.btOrigemArquivos.TabIndex = 8
        Me.btOrigemArquivos.Text = "..."
        Me.btOrigemArquivos.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(19, 189)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Destino NFC-e"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 105)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Destino Redução Z:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Destino Vendas:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Origem Arquivos:"
        '
        'txDestinoNFCe
        '
        Me.txDestinoNFCe.Location = New System.Drawing.Point(22, 205)
        Me.txDestinoNFCe.Name = "txDestinoNFCe"
        Me.txDestinoNFCe.Size = New System.Drawing.Size(329, 20)
        Me.txDestinoNFCe.TabIndex = 3
        '
        'txDestinoZ
        '
        Me.txDestinoZ.Location = New System.Drawing.Point(22, 121)
        Me.txDestinoZ.Name = "txDestinoZ"
        Me.txDestinoZ.Size = New System.Drawing.Size(329, 20)
        Me.txDestinoZ.TabIndex = 2
        '
        'txDestinoVendas
        '
        Me.txDestinoVendas.Location = New System.Drawing.Point(22, 80)
        Me.txDestinoVendas.Name = "txDestinoVendas"
        Me.txDestinoVendas.Size = New System.Drawing.Size(329, 20)
        Me.txDestinoVendas.TabIndex = 1
        '
        'txOrigemArquivos
        '
        Me.txOrigemArquivos.Location = New System.Drawing.Point(22, 41)
        Me.txOrigemArquivos.Name = "txOrigemArquivos"
        Me.txOrigemArquivos.Size = New System.Drawing.Size(329, 20)
        Me.txOrigemArquivos.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label65)
        Me.TabPage2.Controls.Add(Me.lstDados)
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Controls.Add(Me.Label54)
        Me.TabPage2.Controls.Add(Me.Label53)
        Me.TabPage2.Controls.Add(Me.transacao)
        Me.TabPage2.Controls.Add(Me.mapa_numero)
        Me.TabPage2.Controls.Add(Me.Label52)
        Me.TabPage2.Controls.Add(Me.hora)
        Me.TabPage2.Controls.Add(Me.Label49)
        Me.TabPage2.Controls.Add(Me.naotributado)
        Me.TabPage2.Controls.Add(Me.Label50)
        Me.TabPage2.Controls.Add(Me.substituicao)
        Me.TabPage2.Controls.Add(Me.Label51)
        Me.TabPage2.Controls.Add(Me.isento)
        Me.TabPage2.Controls.Add(Me.Label48)
        Me.TabPage2.Controls.Add(Me.Acrescimo)
        Me.TabPage2.Controls.Add(Me.Label47)
        Me.TabPage2.Controls.Add(Me.sangria)
        Me.TabPage2.Controls.Add(Me.Label46)
        Me.TabPage2.Controls.Add(Me.desconto)
        Me.TabPage2.Controls.Add(Me.Label44)
        Me.TabPage2.Controls.Add(Me.cancelamento)
        Me.TabPage2.Controls.Add(Me.Label45)
        Me.TabPage2.Controls.Add(Me.Movimento)
        Me.TabPage2.Controls.Add(Me.Label43)
        Me.TabPage2.Controls.Add(Me.fabricante)
        Me.TabPage2.Controls.Add(Me.Label42)
        Me.TabPage2.Controls.Add(Me.impressora)
        Me.TabPage2.Controls.Add(Me.Label41)
        Me.TabPage2.Controls.Add(Me.loja)
        Me.TabPage2.Controls.Add(Me.Label39)
        Me.TabPage2.Controls.Add(Me.crz)
        Me.TabPage2.Controls.Add(Me.Label40)
        Me.TabPage2.Controls.Add(Me.cro)
        Me.TabPage2.Controls.Add(Me.Label37)
        Me.TabPage2.Controls.Add(Me.gt_final)
        Me.TabPage2.Controls.Add(Me.Label38)
        Me.TabPage2.Controls.Add(Me.gt_inicial)
        Me.TabPage2.Controls.Add(Me.Label35)
        Me.TabPage2.Controls.Add(Me.ticket_final)
        Me.TabPage2.Controls.Add(Me.Label36)
        Me.TabPage2.Controls.Add(Me.ticket_inicial)
        Me.TabPage2.Controls.Add(Me.Label34)
        Me.TabPage2.Controls.Add(Me.data_fiscal)
        Me.TabPage2.Controls.Add(Me.Label33)
        Me.TabPage2.Controls.Add(Me.caixa)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(717, 610)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Informações Redução Z"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(25, 407)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(40, 13)
        Me.Label65.TabIndex = 209
        Me.Label65.Text = "Ticket:"
        '
        'lstDados
        '
        Me.lstDados.FormattingEnabled = True
        Me.lstDados.Location = New System.Drawing.Point(626, 422)
        Me.lstDados.Name = "lstDados"
        Me.lstDados.Size = New System.Drawing.Size(46, 56)
        Me.lstDados.TabIndex = 208
        Me.lstDados.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.A2)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.A1)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.B1)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.I1)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.I8)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.B8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.A8)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.B2)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.I2)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.I7)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.B7)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.A7)
        Me.GroupBox1.Controls.Add(Me.A3)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.B3)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.I3)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.I6)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.B6)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.A6)
        Me.GroupBox1.Controls.Add(Me.A4)
        Me.GroupBox1.Controls.Add(Me.Label29)
        Me.GroupBox1.Controls.Add(Me.B4)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.I4)
        Me.GroupBox1.Controls.Add(Me.Label31)
        Me.GroupBox1.Controls.Add(Me.Label32)
        Me.GroupBox1.Controls.Add(Me.Label55)
        Me.GroupBox1.Controls.Add(Me.Label56)
        Me.GroupBox1.Controls.Add(Me.I5)
        Me.GroupBox1.Controls.Add(Me.Label57)
        Me.GroupBox1.Controls.Add(Me.B5)
        Me.GroupBox1.Controls.Add(Me.Label58)
        Me.GroupBox1.Controls.Add(Me.A5)
        Me.GroupBox1.Location = New System.Drawing.Point(371, 64)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(301, 341)
        Me.GroupBox1.TabIndex = 207
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Impostos:"
        '
        'A2
        '
        Me.A2.Location = New System.Drawing.Point(43, 78)
        Me.A2.Name = "A2"
        Me.A2.Size = New System.Drawing.Size(62, 20)
        Me.A2.TabIndex = 10
        Me.A2.Text = "0,00"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(17, 304)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(22, 13)
        Me.Label17.TabIndex = 58
        Me.Label17.Text = "08."
        '
        'A1
        '
        Me.A1.Location = New System.Drawing.Point(43, 40)
        Me.A1.Name = "A1"
        Me.A1.Size = New System.Drawing.Size(62, 20)
        Me.A1.TabIndex = 3
        Me.A1.Text = "0,00"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(198, 287)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(44, 13)
        Me.Label18.TabIndex = 57
        Me.Label18.Text = "Imposto"
        '
        'B1
        '
        Me.B1.Location = New System.Drawing.Point(120, 40)
        Me.B1.Name = "B1"
        Me.B1.Size = New System.Drawing.Size(65, 20)
        Me.B1.TabIndex = 4
        Me.B1.Text = "0,00"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(117, 288)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(31, 13)
        Me.Label19.TabIndex = 56
        Me.Label19.Text = "Base"
        '
        'I1
        '
        Me.I1.Location = New System.Drawing.Point(201, 40)
        Me.I1.Name = "I1"
        Me.I1.Size = New System.Drawing.Size(64, 20)
        Me.I1.TabIndex = 5
        Me.I1.Text = "0,00"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(41, 288)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(45, 13)
        Me.Label20.TabIndex = 55
        Me.Label20.Text = "Aliquota"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(41, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Aliquota"
        '
        'I8
        '
        Me.I8.Location = New System.Drawing.Point(201, 302)
        Me.I8.Name = "I8"
        Me.I8.Size = New System.Drawing.Size(64, 20)
        Me.I8.TabIndex = 54
        Me.I8.Text = "0,00"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(117, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Base"
        '
        'B8
        '
        Me.B8.Location = New System.Drawing.Point(120, 302)
        Me.B8.Name = "B8"
        Me.B8.Size = New System.Drawing.Size(65, 20)
        Me.B8.TabIndex = 53
        Me.B8.Text = "0,00"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(198, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(44, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Imposto"
        '
        'A8
        '
        Me.A8.Location = New System.Drawing.Point(43, 302)
        Me.A8.Name = "A8"
        Me.A8.Size = New System.Drawing.Size(62, 20)
        Me.A8.TabIndex = 52
        Me.A8.Text = "0,00"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(17, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(22, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "01."
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(17, 266)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(22, 13)
        Me.Label21.TabIndex = 51
        Me.Label21.Text = "06."
        '
        'B2
        '
        Me.B2.Location = New System.Drawing.Point(120, 78)
        Me.B2.Name = "B2"
        Me.B2.Size = New System.Drawing.Size(65, 20)
        Me.B2.TabIndex = 11
        Me.B2.Text = "0,00"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(198, 249)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(44, 13)
        Me.Label22.TabIndex = 50
        Me.Label22.Text = "Imposto"
        '
        'I2
        '
        Me.I2.Location = New System.Drawing.Point(201, 78)
        Me.I2.Name = "I2"
        Me.I2.Size = New System.Drawing.Size(64, 20)
        Me.I2.TabIndex = 12
        Me.I2.Text = "0,00"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(117, 250)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(31, 13)
        Me.Label23.TabIndex = 49
        Me.Label23.Text = "Base"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(41, 64)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Aliquota"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(41, 250)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(45, 13)
        Me.Label24.TabIndex = 48
        Me.Label24.Text = "Aliquota"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(117, 64)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(31, 13)
        Me.Label10.TabIndex = 14
        Me.Label10.Text = "Base"
        '
        'I7
        '
        Me.I7.Location = New System.Drawing.Point(201, 264)
        Me.I7.Name = "I7"
        Me.I7.Size = New System.Drawing.Size(64, 20)
        Me.I7.TabIndex = 47
        Me.I7.Text = "0,00"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(198, 63)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(44, 13)
        Me.Label11.TabIndex = 15
        Me.Label11.Text = "Imposto"
        '
        'B7
        '
        Me.B7.Location = New System.Drawing.Point(120, 264)
        Me.B7.Name = "B7"
        Me.B7.Size = New System.Drawing.Size(65, 20)
        Me.B7.TabIndex = 46
        Me.B7.Text = "0,00"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(17, 80)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(22, 13)
        Me.Label12.TabIndex = 16
        Me.Label12.Text = "02."
        '
        'A7
        '
        Me.A7.Location = New System.Drawing.Point(43, 264)
        Me.A7.Name = "A7"
        Me.A7.Size = New System.Drawing.Size(62, 20)
        Me.A7.TabIndex = 45
        Me.A7.Text = "0,00"
        '
        'A3
        '
        Me.A3.Location = New System.Drawing.Point(43, 115)
        Me.A3.Name = "A3"
        Me.A3.Size = New System.Drawing.Size(62, 20)
        Me.A3.TabIndex = 17
        Me.A3.Text = "0,00"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(17, 229)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(22, 13)
        Me.Label25.TabIndex = 44
        Me.Label25.Text = "06."
        '
        'B3
        '
        Me.B3.Location = New System.Drawing.Point(120, 115)
        Me.B3.Name = "B3"
        Me.B3.Size = New System.Drawing.Size(65, 20)
        Me.B3.TabIndex = 18
        Me.B3.Text = "0,00"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(198, 212)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(44, 13)
        Me.Label26.TabIndex = 43
        Me.Label26.Text = "Imposto"
        '
        'I3
        '
        Me.I3.Location = New System.Drawing.Point(201, 115)
        Me.I3.Name = "I3"
        Me.I3.Size = New System.Drawing.Size(64, 20)
        Me.I3.TabIndex = 19
        Me.I3.Text = "0,00"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(117, 213)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(31, 13)
        Me.Label27.TabIndex = 42
        Me.Label27.Text = "Base"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(41, 101)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(45, 13)
        Me.Label16.TabIndex = 20
        Me.Label16.Text = "Aliquota"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(41, 213)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(45, 13)
        Me.Label28.TabIndex = 41
        Me.Label28.Text = "Aliquota"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(117, 101)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(31, 13)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Base"
        '
        'I6
        '
        Me.I6.Location = New System.Drawing.Point(201, 227)
        Me.I6.Name = "I6"
        Me.I6.Size = New System.Drawing.Size(64, 20)
        Me.I6.TabIndex = 40
        Me.I6.Text = "0,00"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(198, 100)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(44, 13)
        Me.Label14.TabIndex = 22
        Me.Label14.Text = "Imposto"
        '
        'B6
        '
        Me.B6.Location = New System.Drawing.Point(120, 227)
        Me.B6.Name = "B6"
        Me.B6.Size = New System.Drawing.Size(65, 20)
        Me.B6.TabIndex = 39
        Me.B6.Text = "0,00"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(17, 117)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(22, 13)
        Me.Label13.TabIndex = 23
        Me.Label13.Text = "03."
        '
        'A6
        '
        Me.A6.Location = New System.Drawing.Point(43, 227)
        Me.A6.Name = "A6"
        Me.A6.Size = New System.Drawing.Size(62, 20)
        Me.A6.TabIndex = 38
        Me.A6.Text = "0,00"
        '
        'A4
        '
        Me.A4.Location = New System.Drawing.Point(43, 153)
        Me.A4.Name = "A4"
        Me.A4.Size = New System.Drawing.Size(62, 20)
        Me.A4.TabIndex = 24
        Me.A4.Text = "0,00"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(17, 191)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(22, 13)
        Me.Label29.TabIndex = 37
        Me.Label29.Text = "05."
        '
        'B4
        '
        Me.B4.Location = New System.Drawing.Point(120, 153)
        Me.B4.Name = "B4"
        Me.B4.Size = New System.Drawing.Size(65, 20)
        Me.B4.TabIndex = 25
        Me.B4.Text = "0,00"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(198, 174)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(44, 13)
        Me.Label30.TabIndex = 36
        Me.Label30.Text = "Imposto"
        '
        'I4
        '
        Me.I4.Location = New System.Drawing.Point(201, 153)
        Me.I4.Name = "I4"
        Me.I4.Size = New System.Drawing.Size(64, 20)
        Me.I4.TabIndex = 26
        Me.I4.Text = "0,00"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(117, 175)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(31, 13)
        Me.Label31.TabIndex = 35
        Me.Label31.Text = "Base"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(41, 139)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(45, 13)
        Me.Label32.TabIndex = 27
        Me.Label32.Text = "Aliquota"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(41, 175)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(45, 13)
        Me.Label55.TabIndex = 34
        Me.Label55.Text = "Aliquota"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(117, 139)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(31, 13)
        Me.Label56.TabIndex = 28
        Me.Label56.Text = "Base"
        '
        'I5
        '
        Me.I5.Location = New System.Drawing.Point(201, 189)
        Me.I5.Name = "I5"
        Me.I5.Size = New System.Drawing.Size(64, 20)
        Me.I5.TabIndex = 33
        Me.I5.Text = "0,00"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(198, 138)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(44, 13)
        Me.Label57.TabIndex = 29
        Me.Label57.Text = "Imposto"
        '
        'B5
        '
        Me.B5.Location = New System.Drawing.Point(120, 189)
        Me.B5.Name = "B5"
        Me.B5.Size = New System.Drawing.Size(65, 20)
        Me.B5.TabIndex = 32
        Me.B5.Text = "0,00"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(17, 155)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(22, 13)
        Me.Label58.TabIndex = 30
        Me.Label58.Text = "04."
        '
        'A5
        '
        Me.A5.Location = New System.Drawing.Point(43, 189)
        Me.A5.Name = "A5"
        Me.A5.Size = New System.Drawing.Size(62, 20)
        Me.A5.TabIndex = 31
        Me.A5.Text = "0,00"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(234, 344)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(58, 13)
        Me.Label54.TabIndex = 206
        Me.Label54.Text = "Transacao"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(133, 344)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(74, 13)
        Me.Label53.TabIndex = 205
        Me.Label53.Text = "Mapa Numero"
        '
        'transacao
        '
        Me.transacao.Location = New System.Drawing.Point(237, 360)
        Me.transacao.Name = "transacao"
        Me.transacao.Size = New System.Drawing.Size(91, 20)
        Me.transacao.TabIndex = 204
        '
        'mapa_numero
        '
        Me.mapa_numero.Location = New System.Drawing.Point(136, 360)
        Me.mapa_numero.Name = "mapa_numero"
        Me.mapa_numero.Size = New System.Drawing.Size(91, 20)
        Me.mapa_numero.TabIndex = 203
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(231, 64)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(30, 13)
        Me.Label52.TabIndex = 202
        Me.Label52.Text = "Hora"
        '
        'hora
        '
        Me.hora.Location = New System.Drawing.Point(234, 80)
        Me.hora.Name = "hora"
        Me.hora.Size = New System.Drawing.Size(91, 20)
        Me.hora.TabIndex = 201
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(22, 344)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(75, 13)
        Me.Label49.TabIndex = 200
        Me.Label49.Text = "Nao Tributado"
        '
        'naotributado
        '
        Me.naotributado.Location = New System.Drawing.Point(25, 360)
        Me.naotributado.Name = "naotributado"
        Me.naotributado.Size = New System.Drawing.Size(91, 20)
        Me.naotributado.TabIndex = 199
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(234, 305)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(65, 13)
        Me.Label50.TabIndex = 198
        Me.Label50.Text = "Substituicao"
        '
        'substituicao
        '
        Me.substituicao.Location = New System.Drawing.Point(236, 321)
        Me.substituicao.Name = "substituicao"
        Me.substituicao.Size = New System.Drawing.Size(91, 20)
        Me.substituicao.TabIndex = 197
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(133, 305)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(36, 13)
        Me.Label51.TabIndex = 196
        Me.Label51.Text = "Isento"
        '
        'isento
        '
        Me.isento.Location = New System.Drawing.Point(136, 321)
        Me.isento.Name = "isento"
        Me.isento.Size = New System.Drawing.Size(91, 20)
        Me.isento.TabIndex = 195
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(231, 227)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(56, 13)
        Me.Label48.TabIndex = 194
        Me.Label48.Text = "Acrescimo"
        '
        'Acrescimo
        '
        Me.Acrescimo.Location = New System.Drawing.Point(234, 243)
        Me.Acrescimo.Name = "Acrescimo"
        Me.Acrescimo.Size = New System.Drawing.Size(91, 20)
        Me.Acrescimo.TabIndex = 193
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(22, 305)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(43, 13)
        Me.Label47.TabIndex = 192
        Me.Label47.Text = "Sangria"
        '
        'sangria
        '
        Me.sangria.Location = New System.Drawing.Point(25, 321)
        Me.sangria.Name = "sangria"
        Me.sangria.Size = New System.Drawing.Size(91, 20)
        Me.sangria.TabIndex = 191
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(231, 266)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(53, 13)
        Me.Label46.TabIndex = 190
        Me.Label46.Text = "Desconto"
        '
        'desconto
        '
        Me.desconto.Location = New System.Drawing.Point(234, 282)
        Me.desconto.Name = "desconto"
        Me.desconto.Size = New System.Drawing.Size(91, 20)
        Me.desconto.TabIndex = 189
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(131, 266)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(80, 13)
        Me.Label44.TabIndex = 188
        Me.Label44.Text = "Cancelamentos"
        '
        'cancelamento
        '
        Me.cancelamento.Location = New System.Drawing.Point(134, 282)
        Me.cancelamento.Name = "cancelamento"
        Me.cancelamento.Size = New System.Drawing.Size(91, 20)
        Me.cancelamento.TabIndex = 187
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(22, 266)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(68, 13)
        Me.Label45.TabIndex = 186
        Me.Label45.Text = "Mvto Liquido"
        '
        'Movimento
        '
        Me.Movimento.Location = New System.Drawing.Point(25, 282)
        Me.Movimento.Name = "Movimento"
        Me.Movimento.Size = New System.Drawing.Size(91, 20)
        Me.Movimento.TabIndex = 185
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(22, 103)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(57, 13)
        Me.Label43.TabIndex = 184
        Me.Label43.Text = "Fabricante"
        '
        'fabricante
        '
        Me.fabricante.Location = New System.Drawing.Point(25, 119)
        Me.fabricante.Name = "fabricante"
        Me.fabricante.Size = New System.Drawing.Size(91, 20)
        Me.fabricante.TabIndex = 183
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(131, 103)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(69, 13)
        Me.Label42.TabIndex = 182
        Me.Label42.Text = "N° Serie ECF"
        '
        'impressora
        '
        Me.impressora.Location = New System.Drawing.Point(134, 119)
        Me.impressora.Name = "impressora"
        Me.impressora.Size = New System.Drawing.Size(193, 20)
        Me.impressora.TabIndex = 181
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(22, 64)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(30, 13)
        Me.Label41.TabIndex = 180
        Me.Label41.Text = "Loja:"
        '
        'loja
        '
        Me.loja.Location = New System.Drawing.Point(25, 80)
        Me.loja.Name = "loja"
        Me.loja.Size = New System.Drawing.Size(91, 20)
        Me.loja.TabIndex = 179
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(132, 227)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(29, 13)
        Me.Label39.TabIndex = 178
        Me.Label39.Text = "CRZ"
        '
        'crz
        '
        Me.crz.Location = New System.Drawing.Point(135, 243)
        Me.crz.Name = "crz"
        Me.crz.Size = New System.Drawing.Size(91, 20)
        Me.crz.TabIndex = 177
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(23, 227)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(30, 13)
        Me.Label40.TabIndex = 176
        Me.Label40.Text = "CRO"
        '
        'cro
        '
        Me.cro.Location = New System.Drawing.Point(26, 243)
        Me.cro.Name = "cro"
        Me.cro.Size = New System.Drawing.Size(91, 20)
        Me.cro.TabIndex = 175
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(131, 188)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(47, 13)
        Me.Label37.TabIndex = 174
        Me.Label37.Text = "GT Final"
        '
        'gt_final
        '
        Me.gt_final.Location = New System.Drawing.Point(134, 204)
        Me.gt_final.Name = "gt_final"
        Me.gt_final.Size = New System.Drawing.Size(91, 20)
        Me.gt_final.TabIndex = 173
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(22, 188)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(52, 13)
        Me.Label38.TabIndex = 172
        Me.Label38.Text = "GT Inicial"
        '
        'gt_inicial
        '
        Me.gt_inicial.Location = New System.Drawing.Point(25, 204)
        Me.gt_inicial.Name = "gt_inicial"
        Me.gt_inicial.Size = New System.Drawing.Size(91, 20)
        Me.gt_inicial.TabIndex = 171
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(132, 149)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(62, 13)
        Me.Label35.TabIndex = 170
        Me.Label35.Text = "Ticket Final"
        '
        'ticket_final
        '
        Me.ticket_final.Location = New System.Drawing.Point(135, 165)
        Me.ticket_final.Name = "ticket_final"
        Me.ticket_final.Size = New System.Drawing.Size(91, 20)
        Me.ticket_final.TabIndex = 169
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(23, 149)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(67, 13)
        Me.Label36.TabIndex = 168
        Me.Label36.Text = "Ticket Inicial"
        '
        'ticket_inicial
        '
        Me.ticket_inicial.Location = New System.Drawing.Point(26, 165)
        Me.ticket_inicial.Name = "ticket_inicial"
        Me.ticket_inicial.Size = New System.Drawing.Size(90, 20)
        Me.ticket_inicial.TabIndex = 167
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(128, 64)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(33, 13)
        Me.Label34.TabIndex = 166
        Me.Label34.Text = "Data:"
        '
        'data_fiscal
        '
        Me.data_fiscal.Location = New System.Drawing.Point(131, 80)
        Me.data_fiscal.Name = "data_fiscal"
        Me.data_fiscal.Size = New System.Drawing.Size(91, 20)
        Me.data_fiscal.TabIndex = 165
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(231, 149)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(48, 13)
        Me.Label33.TabIndex = 164
        Me.Label33.Text = "N° Caixa"
        '
        'caixa
        '
        Me.caixa.Location = New System.Drawing.Point(234, 165)
        Me.caixa.Name = "caixa"
        Me.caixa.Size = New System.Drawing.Size(91, 20)
        Me.caixa.TabIndex = 163
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox4)
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Controls.Add(Me.Label72)
        Me.TabPage3.Controls.Add(Me.nfce_Data)
        Me.TabPage3.Controls.Add(Me.Label73)
        Me.TabPage3.Controls.Add(Me.Label71)
        Me.TabPage3.Controls.Add(Me.Label70)
        Me.TabPage3.Controls.Add(Me.Label69)
        Me.TabPage3.Controls.Add(Me.Label68)
        Me.TabPage3.Controls.Add(Me.nfce_Xml)
        Me.TabPage3.Controls.Add(Me.nfce_Chave)
        Me.TabPage3.Controls.Add(Me.nfce_Nota)
        Me.TabPage3.Controls.Add(Me.nfce_Serie)
        Me.TabPage3.Controls.Add(Me.nfce_Loja)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(717, 610)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "NFC-e"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label66)
        Me.GroupBox4.Controls.Add(Me.nfce_valor)
        Me.GroupBox4.Controls.Add(Me.Label86)
        Me.GroupBox4.Controls.Add(Me.txTagSitProtocolo)
        Me.GroupBox4.Controls.Add(Me.Label82)
        Me.GroupBox4.Controls.Add(Me.txDataProtocolo)
        Me.GroupBox4.Controls.Add(Me.Label83)
        Me.GroupBox4.Controls.Add(Me.txProtocolo)
        Me.GroupBox4.Controls.Add(Me.txLinkQRCode)
        Me.GroupBox4.Controls.Add(Me.Label93)
        Me.GroupBox4.Controls.Add(Me.txDigestValue)
        Me.GroupBox4.Controls.Add(Me.Label96)
        Me.GroupBox4.Controls.Add(Me.txEmissao)
        Me.GroupBox4.Controls.Add(Me.Label100)
        Me.GroupBox4.Controls.Add(Me.txChaveNFCe)
        Me.GroupBox4.Location = New System.Drawing.Point(26, 197)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(643, 146)
        Me.GroupBox4.TabIndex = 39
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Dados NFC-e"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(550, 101)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(34, 13)
        Me.Label66.TabIndex = 39
        Me.Label66.Text = "Valor:"
        '
        'nfce_valor
        '
        Me.nfce_valor.AccessibleName = ""
        Me.nfce_valor.Location = New System.Drawing.Point(553, 117)
        Me.nfce_valor.Name = "nfce_valor"
        Me.nfce_valor.Size = New System.Drawing.Size(77, 20)
        Me.nfce_valor.TabIndex = 38
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(16, 120)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(114, 13)
        Me.Label86.TabIndex = 37
        Me.Label86.Text = "Possui Tag Protocolo?"
        '
        'txTagSitProtocolo
        '
        Me.txTagSitProtocolo.Location = New System.Drawing.Point(136, 117)
        Me.txTagSitProtocolo.Name = "txTagSitProtocolo"
        Me.txTagSitProtocolo.Size = New System.Drawing.Size(41, 20)
        Me.txTagSitProtocolo.TabIndex = 36
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(331, 101)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(81, 13)
        Me.Label82.TabIndex = 35
        Me.Label82.Text = "Data Protocolo:"
        '
        'txDataProtocolo
        '
        Me.txDataProtocolo.AccessibleName = ""
        Me.txDataProtocolo.Location = New System.Drawing.Point(334, 117)
        Me.txDataProtocolo.Name = "txDataProtocolo"
        Me.txDataProtocolo.Size = New System.Drawing.Size(200, 20)
        Me.txDataProtocolo.TabIndex = 34
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(194, 101)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(55, 13)
        Me.Label83.TabIndex = 33
        Me.Label83.Text = "Protocolo:"
        '
        'txProtocolo
        '
        Me.txProtocolo.AccessibleName = ""
        Me.txProtocolo.Location = New System.Drawing.Point(197, 117)
        Me.txProtocolo.Name = "txProtocolo"
        Me.txProtocolo.Size = New System.Drawing.Size(122, 20)
        Me.txProtocolo.TabIndex = 32
        '
        'txLinkQRCode
        '
        Me.txLinkQRCode.Location = New System.Drawing.Point(16, 56)
        Me.txLinkQRCode.Multiline = True
        Me.txLinkQRCode.Name = "txLinkQRCode"
        Me.txLinkQRCode.Size = New System.Drawing.Size(616, 42)
        Me.txLinkQRCode.TabIndex = 27
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Location = New System.Drawing.Point(422, 14)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(70, 13)
        Me.Label93.TabIndex = 20
        Me.Label93.Text = "Digest Value:"
        '
        'txDigestValue
        '
        Me.txDigestValue.AccessibleName = ""
        Me.txDigestValue.Location = New System.Drawing.Point(425, 30)
        Me.txDigestValue.Name = "txDigestValue"
        Me.txDigestValue.Size = New System.Drawing.Size(207, 20)
        Me.txDigestValue.TabIndex = 19
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.Location = New System.Drawing.Point(301, 14)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(49, 13)
        Me.Label96.TabIndex = 14
        Me.Label96.Text = "Emissão:"
        '
        'txEmissao
        '
        Me.txEmissao.AccessibleName = ""
        Me.txEmissao.Location = New System.Drawing.Point(304, 30)
        Me.txEmissao.Name = "txEmissao"
        Me.txEmissao.Size = New System.Drawing.Size(114, 20)
        Me.txEmissao.TabIndex = 13
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.Location = New System.Drawing.Point(13, 14)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(71, 13)
        Me.Label100.TabIndex = 6
        Me.Label100.Text = "Chave NFCe:"
        '
        'txChaveNFCe
        '
        Me.txChaveNFCe.AccessibleName = ""
        Me.txChaveNFCe.Location = New System.Drawing.Point(16, 30)
        Me.txChaveNFCe.MaxLength = 44
        Me.txChaveNFCe.Name = "txChaveNFCe"
        Me.txChaveNFCe.Size = New System.Drawing.Size(280, 20)
        Me.txChaveNFCe.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label115)
        Me.GroupBox2.Controls.Add(Me.txVesaoNFCe)
        Me.GroupBox2.Controls.Add(Me.btVisualizarXml)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.tx_tag_protocolo)
        Me.GroupBox2.Controls.Add(Me.Label81)
        Me.GroupBox2.Controls.Add(Me.Label80)
        Me.GroupBox2.Controls.Add(Me.btLocalExportacao)
        Me.GroupBox2.Controls.Add(Me.txNfceCaminhoExportacao)
        Me.GroupBox2.Controls.Add(Me.btExportarXml)
        Me.GroupBox2.Controls.Add(Me.Label78)
        Me.GroupBox2.Controls.Add(Me.Label77)
        Me.GroupBox2.Controls.Add(Me.dtNfceFinal)
        Me.GroupBox2.Controls.Add(Me.dtNfceDataIni)
        Me.GroupBox2.Controls.Add(Me.Label76)
        Me.GroupBox2.Controls.Add(Me.Label75)
        Me.GroupBox2.Controls.Add(Me.txNfceChave)
        Me.GroupBox2.Controls.Add(Me.txNfceNota)
        Me.GroupBox2.Controls.Add(Me.txNfceLoja)
        Me.GroupBox2.Location = New System.Drawing.Point(26, 349)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(643, 148)
        Me.GroupBox2.TabIndex = 37
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Exportar XML"
        '
        'Label115
        '
        Me.Label115.AutoSize = True
        Me.Label115.Location = New System.Drawing.Point(372, 64)
        Me.Label115.Name = "Label115"
        Me.Label115.Size = New System.Drawing.Size(40, 13)
        Me.Label115.TabIndex = 42
        Me.Label115.Text = "Versão"
        '
        'txVesaoNFCe
        '
        Me.txVesaoNFCe.Location = New System.Drawing.Point(375, 80)
        Me.txVesaoNFCe.Name = "txVesaoNFCe"
        Me.txVesaoNFCe.Size = New System.Drawing.Size(66, 20)
        Me.txVesaoNFCe.TabIndex = 41
        '
        'btVisualizarXml
        '
        Me.btVisualizarXml.Location = New System.Drawing.Point(501, 46)
        Me.btVisualizarXml.Name = "btVisualizarXml"
        Me.btVisualizarXml.Size = New System.Drawing.Size(65, 45)
        Me.btVisualizarXml.TabIndex = 40
        Me.btVisualizarXml.Text = "Visualizar XML"
        Me.btVisualizarXml.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(572, 46)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(65, 45)
        Me.Button2.TabIndex = 39
        Me.Button2.Text = "Gerar Relatório"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'tx_tag_protocolo
        '
        Me.tx_tag_protocolo.Enabled = False
        Me.tx_tag_protocolo.Location = New System.Drawing.Point(447, 19)
        Me.tx_tag_protocolo.Multiline = True
        Me.tx_tag_protocolo.Name = "tx_tag_protocolo"
        Me.tx_tag_protocolo.Size = New System.Drawing.Size(185, 72)
        Me.tx_tag_protocolo.TabIndex = 38
        Me.tx_tag_protocolo.Text = resources.GetString("tx_tag_protocolo.Text")
        Me.tx_tag_protocolo.Visible = False
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Location = New System.Drawing.Point(10, 64)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(41, 13)
        Me.Label81.TabIndex = 22
        Me.Label81.Text = "Chave:"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Location = New System.Drawing.Point(10, 105)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(201, 13)
        Me.Label80.TabIndex = 21
        Me.Label80.Text = "Local para Exportação do Arquivos XML:"
        '
        'btLocalExportacao
        '
        Me.btLocalExportacao.Location = New System.Drawing.Point(516, 118)
        Me.btLocalExportacao.Name = "btLocalExportacao"
        Me.btLocalExportacao.Size = New System.Drawing.Size(41, 24)
        Me.btLocalExportacao.TabIndex = 20
        Me.btLocalExportacao.Text = "..."
        Me.btLocalExportacao.UseVisualStyleBackColor = True
        '
        'txNfceCaminhoExportacao
        '
        Me.txNfceCaminhoExportacao.Location = New System.Drawing.Point(13, 122)
        Me.txNfceCaminhoExportacao.Name = "txNfceCaminhoExportacao"
        Me.txNfceCaminhoExportacao.Size = New System.Drawing.Size(500, 20)
        Me.txNfceCaminhoExportacao.TabIndex = 19
        '
        'btExportarXml
        '
        Me.btExportarXml.Location = New System.Drawing.Point(572, 97)
        Me.btExportarXml.Name = "btExportarXml"
        Me.btExportarXml.Size = New System.Drawing.Size(65, 45)
        Me.btExportarXml.TabIndex = 18
        Me.btExportarXml.Text = "Exportar"
        Me.btExportarXml.UseVisualStyleBackColor = True
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(331, 22)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(26, 13)
        Me.Label78.TabIndex = 10
        Me.Label78.Text = "Até:"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(231, 22)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(24, 13)
        Me.Label77.TabIndex = 9
        Me.Label77.Text = "De:"
        '
        'dtNfceFinal
        '
        Me.dtNfceFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtNfceFinal.Location = New System.Drawing.Point(334, 38)
        Me.dtNfceFinal.Name = "dtNfceFinal"
        Me.dtNfceFinal.Size = New System.Drawing.Size(85, 20)
        Me.dtNfceFinal.TabIndex = 8
        '
        'dtNfceDataIni
        '
        Me.dtNfceDataIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtNfceDataIni.Location = New System.Drawing.Point(234, 38)
        Me.dtNfceDataIni.Name = "dtNfceDataIni"
        Me.dtNfceDataIni.Size = New System.Drawing.Size(85, 20)
        Me.dtNfceDataIni.TabIndex = 7
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(133, 22)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(33, 13)
        Me.Label76.TabIndex = 6
        Me.Label76.Text = "Nota:"
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(10, 22)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(66, 13)
        Me.Label75.TabIndex = 5
        Me.Label75.Text = "Loja (CNPJ):"
        '
        'txNfceChave
        '
        Me.txNfceChave.Location = New System.Drawing.Point(13, 80)
        Me.txNfceChave.Name = "txNfceChave"
        Me.txNfceChave.Size = New System.Drawing.Size(353, 20)
        Me.txNfceChave.TabIndex = 4
        '
        'txNfceNota
        '
        Me.txNfceNota.Location = New System.Drawing.Point(136, 38)
        Me.txNfceNota.Name = "txNfceNota"
        Me.txNfceNota.Size = New System.Drawing.Size(82, 20)
        Me.txNfceNota.TabIndex = 1
        '
        'txNfceLoja
        '
        Me.txNfceLoja.Location = New System.Drawing.Point(13, 38)
        Me.txNfceLoja.MaxLength = 14
        Me.txNfceLoja.Name = "txNfceLoja"
        Me.txNfceLoja.Size = New System.Drawing.Size(117, 20)
        Me.txNfceLoja.TabIndex = 0
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label72.Location = New System.Drawing.Point(166, 10)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(38, 13)
        Me.Label72.TabIndex = 33
        Me.Label72.Text = "Data:"
        '
        'nfce_Data
        '
        Me.nfce_Data.Location = New System.Drawing.Point(169, 26)
        Me.nfce_Data.Name = "nfce_Data"
        Me.nfce_Data.Size = New System.Drawing.Size(83, 20)
        Me.nfce_Data.TabIndex = 32
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.Location = New System.Drawing.Point(23, 49)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(31, 13)
        Me.Label73.TabIndex = 30
        Me.Label73.Text = "Xml:"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label71.Location = New System.Drawing.Point(382, 10)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(47, 13)
        Me.Label71.TabIndex = 29
        Me.Label71.Text = "Chave:"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label70.Location = New System.Drawing.Point(339, 10)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(40, 13)
        Me.Label70.TabIndex = 28
        Me.Label70.Text = "Série:"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.Location = New System.Drawing.Point(255, 10)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(38, 13)
        Me.Label69.TabIndex = 27
        Me.Label69.Text = "Nota:"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.Location = New System.Drawing.Point(23, 10)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(35, 13)
        Me.Label68.TabIndex = 26
        Me.Label68.Text = "Loja:"
        '
        'nfce_Xml
        '
        Me.nfce_Xml.Location = New System.Drawing.Point(26, 65)
        Me.nfce_Xml.MaxLength = 1000000
        Me.nfce_Xml.Multiline = True
        Me.nfce_Xml.Name = "nfce_Xml"
        Me.nfce_Xml.Size = New System.Drawing.Size(643, 126)
        Me.nfce_Xml.TabIndex = 25
        '
        'nfce_Chave
        '
        Me.nfce_Chave.Location = New System.Drawing.Point(385, 26)
        Me.nfce_Chave.Name = "nfce_Chave"
        Me.nfce_Chave.Size = New System.Drawing.Size(284, 20)
        Me.nfce_Chave.TabIndex = 24
        '
        'nfce_Nota
        '
        Me.nfce_Nota.Location = New System.Drawing.Point(258, 26)
        Me.nfce_Nota.Name = "nfce_Nota"
        Me.nfce_Nota.Size = New System.Drawing.Size(83, 20)
        Me.nfce_Nota.TabIndex = 23
        '
        'nfce_Serie
        '
        Me.nfce_Serie.Location = New System.Drawing.Point(347, 26)
        Me.nfce_Serie.Name = "nfce_Serie"
        Me.nfce_Serie.Size = New System.Drawing.Size(32, 20)
        Me.nfce_Serie.TabIndex = 22
        '
        'nfce_Loja
        '
        Me.nfce_Loja.Location = New System.Drawing.Point(26, 26)
        Me.nfce_Loja.Name = "nfce_Loja"
        Me.nfce_Loja.Size = New System.Drawing.Size(123, 20)
        Me.nfce_Loja.TabIndex = 21
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.TabControl2)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(717, 610)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Configurações"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage5)
        Me.TabControl2.Controls.Add(Me.TabPage6)
        Me.TabControl2.Controls.Add(Me.TabPage7)
        Me.TabControl2.Controls.Add(Me.chbCopiaContabilidade)
        Me.TabControl2.Location = New System.Drawing.Point(4, 4)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(688, 582)
        Me.TabControl2.TabIndex = 62
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.GroupBox3)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(680, 556)
        Me.TabPage5.TabIndex = 0
        Me.TabPage5.Text = "Conf. Conexão"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button3)
        Me.GroupBox3.Controls.Add(Me.Label113)
        Me.GroupBox3.Controls.Add(Me.txBancoEntregaFdb)
        Me.GroupBox3.Controls.Add(Me.Label107)
        Me.GroupBox3.Controls.Add(Me.Label108)
        Me.GroupBox3.Controls.Add(Me.Label109)
        Me.GroupBox3.Controls.Add(Me.Label110)
        Me.GroupBox3.Controls.Add(Me.Label111)
        Me.GroupBox3.Controls.Add(Me.txSenhaMySql)
        Me.GroupBox3.Controls.Add(Me.txPortaMySql)
        Me.GroupBox3.Controls.Add(Me.txUsuarioMySql)
        Me.GroupBox3.Controls.Add(Me.txServidorMySql)
        Me.GroupBox3.Controls.Add(Me.txBancoMysql)
        Me.GroupBox3.Controls.Add(Me.Label74)
        Me.GroupBox3.Controls.Add(Me.Label85)
        Me.GroupBox3.Controls.Add(Me.Label92)
        Me.GroupBox3.Controls.Add(Me.Label94)
        Me.GroupBox3.Controls.Add(Me.Label95)
        Me.GroupBox3.Controls.Add(Me.btLocalizarBancoNfce)
        Me.GroupBox3.Controls.Add(Me.txConexaoSenhaNfce)
        Me.GroupBox3.Controls.Add(Me.txConexaoPortaNfce)
        Me.GroupBox3.Controls.Add(Me.txConexaoUsuarioNfce)
        Me.GroupBox3.Controls.Add(Me.txConexaoServidorNFce)
        Me.GroupBox3.Controls.Add(Me.txConexaoBancoNFce)
        Me.GroupBox3.Controls.Add(Me.GroupBox5)
        Me.GroupBox3.Controls.Add(Me.cmdConexaoNormalSalvar)
        Me.GroupBox3.Controls.Add(Me.Label87)
        Me.GroupBox3.Controls.Add(Me.Label88)
        Me.GroupBox3.Controls.Add(Me.Label89)
        Me.GroupBox3.Controls.Add(Me.Label90)
        Me.GroupBox3.Controls.Add(Me.Label91)
        Me.GroupBox3.Controls.Add(Me.btLocalizarBancoTeorema)
        Me.GroupBox3.Controls.Add(Me.txConexaoSenha)
        Me.GroupBox3.Controls.Add(Me.txConexaoPorta)
        Me.GroupBox3.Controls.Add(Me.txConexaoUsuario)
        Me.GroupBox3.Controls.Add(Me.txConexaoServidor)
        Me.GroupBox3.Controls.Add(Me.txConexaoBanco)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(22, 19)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(635, 464)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Conexão:"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(524, 431)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(28, 19)
        Me.Button3.TabIndex = 47
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label113
        '
        Me.Label113.AutoSize = True
        Me.Label113.Location = New System.Drawing.Point(24, 415)
        Me.Label113.Name = "Label113"
        Me.Label113.Size = New System.Drawing.Size(317, 13)
        Me.Label113.TabIndex = 46
        Me.Label113.Text = "Caminho do Banco de Dados Entrega (Firebird(.FDB)): "
        '
        'txBancoEntregaFdb
        '
        Me.txBancoEntregaFdb.Location = New System.Drawing.Point(24, 431)
        Me.txBancoEntregaFdb.Name = "txBancoEntregaFdb"
        Me.txBancoEntregaFdb.Size = New System.Drawing.Size(494, 20)
        Me.txBancoEntregaFdb.TabIndex = 45
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.Location = New System.Drawing.Point(414, 355)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(47, 13)
        Me.Label107.TabIndex = 44
        Me.Label107.Text = "Senha:"
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Location = New System.Drawing.Point(270, 355)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(54, 13)
        Me.Label108.TabIndex = 43
        Me.Label108.Text = "Usuario:"
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Location = New System.Drawing.Point(170, 355)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(41, 13)
        Me.Label109.TabIndex = 42
        Me.Label109.Text = "Porta:"
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Location = New System.Drawing.Point(24, 355)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(58, 13)
        Me.Label110.TabIndex = 41
        Me.Label110.Text = "Servidor:"
        '
        'Label111
        '
        Me.Label111.AutoSize = True
        Me.Label111.Location = New System.Drawing.Point(24, 311)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(283, 13)
        Me.Label111.TabIndex = 40
        Me.Label111.Text = "Caminho do Banco de Dados Emporium (MySql): "
        '
        'txSenhaMySql
        '
        Me.txSenhaMySql.Location = New System.Drawing.Point(417, 371)
        Me.txSenhaMySql.Name = "txSenhaMySql"
        Me.txSenhaMySql.Size = New System.Drawing.Size(143, 20)
        Me.txSenhaMySql.TabIndex = 39
        '
        'txPortaMySql
        '
        Me.txPortaMySql.Location = New System.Drawing.Point(173, 371)
        Me.txPortaMySql.Name = "txPortaMySql"
        Me.txPortaMySql.Size = New System.Drawing.Size(94, 20)
        Me.txPortaMySql.TabIndex = 38
        '
        'txUsuarioMySql
        '
        Me.txUsuarioMySql.Location = New System.Drawing.Point(273, 371)
        Me.txUsuarioMySql.Name = "txUsuarioMySql"
        Me.txUsuarioMySql.Size = New System.Drawing.Size(135, 20)
        Me.txUsuarioMySql.TabIndex = 37
        '
        'txServidorMySql
        '
        Me.txServidorMySql.Location = New System.Drawing.Point(24, 371)
        Me.txServidorMySql.Name = "txServidorMySql"
        Me.txServidorMySql.Size = New System.Drawing.Size(143, 20)
        Me.txServidorMySql.TabIndex = 36
        '
        'txBancoMysql
        '
        Me.txBancoMysql.Location = New System.Drawing.Point(24, 327)
        Me.txBancoMysql.Name = "txBancoMysql"
        Me.txBancoMysql.Size = New System.Drawing.Size(526, 20)
        Me.txBancoMysql.TabIndex = 35
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(414, 173)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(47, 13)
        Me.Label74.TabIndex = 34
        Me.Label74.Text = "Senha:"
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(270, 173)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(54, 13)
        Me.Label85.TabIndex = 33
        Me.Label85.Text = "Usuario:"
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(170, 173)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(41, 13)
        Me.Label92.TabIndex = 32
        Me.Label92.Text = "Porta:"
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.Location = New System.Drawing.Point(24, 173)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(58, 13)
        Me.Label94.TabIndex = 31
        Me.Label94.Text = "Servidor:"
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.Location = New System.Drawing.Point(24, 129)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(382, 13)
        Me.Label95.TabIndex = 30
        Me.Label95.Text = "Caminho do Banco de Dados Importador NFC-e (NORMAL/ODBC):"
        '
        'btLocalizarBancoNfce
        '
        Me.btLocalizarBancoNfce.Location = New System.Drawing.Point(556, 145)
        Me.btLocalizarBancoNfce.Name = "btLocalizarBancoNfce"
        Me.btLocalizarBancoNfce.Size = New System.Drawing.Size(28, 19)
        Me.btLocalizarBancoNfce.TabIndex = 29
        Me.btLocalizarBancoNfce.Text = "..."
        Me.btLocalizarBancoNfce.UseVisualStyleBackColor = True
        '
        'txConexaoSenhaNfce
        '
        Me.txConexaoSenhaNfce.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txConexaoSenhaNfce.Location = New System.Drawing.Point(417, 189)
        Me.txConexaoSenhaNfce.Name = "txConexaoSenhaNfce"
        Me.txConexaoSenhaNfce.Size = New System.Drawing.Size(143, 20)
        Me.txConexaoSenhaNfce.TabIndex = 28
        '
        'txConexaoPortaNfce
        '
        Me.txConexaoPortaNfce.Location = New System.Drawing.Point(173, 189)
        Me.txConexaoPortaNfce.Name = "txConexaoPortaNfce"
        Me.txConexaoPortaNfce.Size = New System.Drawing.Size(94, 20)
        Me.txConexaoPortaNfce.TabIndex = 27
        '
        'txConexaoUsuarioNfce
        '
        Me.txConexaoUsuarioNfce.Location = New System.Drawing.Point(273, 189)
        Me.txConexaoUsuarioNfce.Name = "txConexaoUsuarioNfce"
        Me.txConexaoUsuarioNfce.Size = New System.Drawing.Size(135, 20)
        Me.txConexaoUsuarioNfce.TabIndex = 26
        '
        'txConexaoServidorNFce
        '
        Me.txConexaoServidorNFce.Location = New System.Drawing.Point(24, 189)
        Me.txConexaoServidorNFce.Name = "txConexaoServidorNFce"
        Me.txConexaoServidorNFce.Size = New System.Drawing.Size(143, 20)
        Me.txConexaoServidorNFce.TabIndex = 25
        '
        'txConexaoBancoNFce
        '
        Me.txConexaoBancoNFce.Location = New System.Drawing.Point(24, 145)
        Me.txConexaoBancoNFce.Name = "txConexaoBancoNFce"
        Me.txConexaoBancoNFce.Size = New System.Drawing.Size(526, 20)
        Me.txConexaoBancoNFce.TabIndex = 24
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rbConexaoODBC)
        Me.GroupBox5.Controls.Add(Me.rbConexaoNormal)
        Me.GroupBox5.Location = New System.Drawing.Point(24, 215)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(280, 61)
        Me.GroupBox5.TabIndex = 23
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Selecione o Tipo de Conexão:"
        '
        'rbConexaoODBC
        '
        Me.rbConexaoODBC.AutoSize = True
        Me.rbConexaoODBC.Location = New System.Drawing.Point(155, 23)
        Me.rbConexaoODBC.Name = "rbConexaoODBC"
        Me.rbConexaoODBC.Size = New System.Drawing.Size(80, 17)
        Me.rbConexaoODBC.TabIndex = 1
        Me.rbConexaoODBC.Text = "ODBC (O)"
        Me.rbConexaoODBC.UseVisualStyleBackColor = True
        '
        'rbConexaoNormal
        '
        Me.rbConexaoNormal.AutoSize = True
        Me.rbConexaoNormal.Checked = True
        Me.rbConexaoNormal.Location = New System.Drawing.Point(15, 23)
        Me.rbConexaoNormal.Name = "rbConexaoNormal"
        Me.rbConexaoNormal.Size = New System.Drawing.Size(85, 17)
        Me.rbConexaoNormal.TabIndex = 0
        Me.rbConexaoNormal.TabStop = True
        Me.rbConexaoNormal.Text = "Normal (N)"
        Me.rbConexaoNormal.UseVisualStyleBackColor = True
        '
        'cmdConexaoNormalSalvar
        '
        Me.cmdConexaoNormalSalvar.Location = New System.Drawing.Point(566, 402)
        Me.cmdConexaoNormalSalvar.Name = "cmdConexaoNormalSalvar"
        Me.cmdConexaoNormalSalvar.Size = New System.Drawing.Size(63, 49)
        Me.cmdConexaoNormalSalvar.TabIndex = 22
        Me.cmdConexaoNormalSalvar.Text = "Salvar"
        Me.cmdConexaoNormalSalvar.UseVisualStyleBackColor = True
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Location = New System.Drawing.Point(414, 74)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(47, 13)
        Me.Label87.TabIndex = 21
        Me.Label87.Text = "Senha:"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Location = New System.Drawing.Point(270, 74)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(54, 13)
        Me.Label88.TabIndex = 20
        Me.Label88.Text = "Usuario:"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Location = New System.Drawing.Point(170, 74)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(41, 13)
        Me.Label89.TabIndex = 19
        Me.Label89.Text = "Porta:"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Location = New System.Drawing.Point(24, 74)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(58, 13)
        Me.Label90.TabIndex = 18
        Me.Label90.Text = "Servidor:"
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.Location = New System.Drawing.Point(24, 30)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(342, 13)
        Me.Label91.TabIndex = 17
        Me.Label91.Text = "Caminho do Banco de Dados Teroema (SOMENTE ODBC): "
        '
        'btLocalizarBancoTeorema
        '
        Me.btLocalizarBancoTeorema.Location = New System.Drawing.Point(556, 46)
        Me.btLocalizarBancoTeorema.Name = "btLocalizarBancoTeorema"
        Me.btLocalizarBancoTeorema.Size = New System.Drawing.Size(28, 19)
        Me.btLocalizarBancoTeorema.TabIndex = 16
        Me.btLocalizarBancoTeorema.Text = "..."
        Me.btLocalizarBancoTeorema.UseVisualStyleBackColor = True
        '
        'txConexaoSenha
        '
        Me.txConexaoSenha.Location = New System.Drawing.Point(417, 90)
        Me.txConexaoSenha.Name = "txConexaoSenha"
        Me.txConexaoSenha.Size = New System.Drawing.Size(143, 20)
        Me.txConexaoSenha.TabIndex = 15
        '
        'txConexaoPorta
        '
        Me.txConexaoPorta.Location = New System.Drawing.Point(173, 90)
        Me.txConexaoPorta.Name = "txConexaoPorta"
        Me.txConexaoPorta.Size = New System.Drawing.Size(94, 20)
        Me.txConexaoPorta.TabIndex = 14
        '
        'txConexaoUsuario
        '
        Me.txConexaoUsuario.Location = New System.Drawing.Point(273, 90)
        Me.txConexaoUsuario.Name = "txConexaoUsuario"
        Me.txConexaoUsuario.Size = New System.Drawing.Size(135, 20)
        Me.txConexaoUsuario.TabIndex = 13
        '
        'txConexaoServidor
        '
        Me.txConexaoServidor.Location = New System.Drawing.Point(24, 90)
        Me.txConexaoServidor.Name = "txConexaoServidor"
        Me.txConexaoServidor.Size = New System.Drawing.Size(143, 20)
        Me.txConexaoServidor.TabIndex = 12
        '
        'txConexaoBanco
        '
        Me.txConexaoBanco.Location = New System.Drawing.Point(24, 46)
        Me.txConexaoBanco.Name = "txConexaoBanco"
        Me.txConexaoBanco.Size = New System.Drawing.Size(526, 20)
        Me.txConexaoBanco.TabIndex = 11
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.grpFinanceiro)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(680, 556)
        Me.TabPage6.TabIndex = 1
        Me.TabPage6.Text = "Conf. Redução Z"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'grpFinanceiro
        '
        Me.grpFinanceiro.Controls.Add(Me.chbRzEmporium)
        Me.grpFinanceiro.Controls.Add(Me.chbRzRecarga)
        Me.grpFinanceiro.Controls.Add(Me.chbRzRepetida)
        Me.grpFinanceiro.Controls.Add(Me.chbImportaRz)
        Me.grpFinanceiro.Controls.Add(Me.chbRzMenorZero)
        Me.grpFinanceiro.Controls.Add(Me.chbRzValor)
        Me.grpFinanceiro.Controls.Add(Me.Label67)
        Me.grpFinanceiro.Controls.Add(Me.txtEcfPadrao)
        Me.grpFinanceiro.Controls.Add(Me.chbRzConsul)
        Me.grpFinanceiro.Location = New System.Drawing.Point(18, 6)
        Me.grpFinanceiro.Name = "grpFinanceiro"
        Me.grpFinanceiro.Size = New System.Drawing.Size(636, 219)
        Me.grpFinanceiro.TabIndex = 62
        Me.grpFinanceiro.TabStop = False
        Me.grpFinanceiro.Text = "Opcões Financeiro:"
        '
        'chbRzEmporium
        '
        Me.chbRzEmporium.AutoSize = True
        Me.chbRzEmporium.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbRzEmporium.Location = New System.Drawing.Point(17, 174)
        Me.chbRzEmporium.Name = "chbRzEmporium"
        Me.chbRzEmporium.Size = New System.Drawing.Size(210, 17)
        Me.chbRzEmporium.TabIndex = 43
        Me.chbRzEmporium.Text = "Importa Redução Pelo Emporium"
        Me.chbRzEmporium.UseVisualStyleBackColor = True
        '
        'chbRzRecarga
        '
        Me.chbRzRecarga.AutoSize = True
        Me.chbRzRecarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbRzRecarga.Location = New System.Drawing.Point(17, 151)
        Me.chbRzRecarga.Name = "chbRzRecarga"
        Me.chbRzRecarga.Size = New System.Drawing.Size(126, 17)
        Me.chbRzRecarga.TabIndex = 42
        Me.chbRzRecarga.Text = "Importa Recargas"
        Me.chbRzRecarga.UseVisualStyleBackColor = True
        '
        'chbRzRepetida
        '
        Me.chbRzRepetida.AutoSize = True
        Me.chbRzRepetida.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbRzRepetida.Location = New System.Drawing.Point(17, 128)
        Me.chbRzRepetida.Name = "chbRzRepetida"
        Me.chbRzRepetida.Size = New System.Drawing.Size(185, 17)
        Me.chbRzRepetida.TabIndex = 41
        Me.chbRzRepetida.Text = "Importa Redução Z repetida"
        Me.chbRzRepetida.UseVisualStyleBackColor = True
        '
        'chbImportaRz
        '
        Me.chbImportaRz.AutoSize = True
        Me.chbImportaRz.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbImportaRz.Location = New System.Drawing.Point(17, 36)
        Me.chbImportaRz.Name = "chbImportaRz"
        Me.chbImportaRz.Size = New System.Drawing.Size(146, 17)
        Me.chbImportaRz.TabIndex = 40
        Me.chbImportaRz.Text = "Importar Redução Z?"
        Me.chbImportaRz.UseVisualStyleBackColor = True
        '
        'chbRzMenorZero
        '
        Me.chbRzMenorZero.AutoSize = True
        Me.chbRzMenorZero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbRzMenorZero.Location = New System.Drawing.Point(17, 105)
        Me.chbRzMenorZero.Name = "chbRzMenorZero"
        Me.chbRzMenorZero.Size = New System.Drawing.Size(259, 17)
        Me.chbRzMenorZero.TabIndex = 39
        Me.chbRzMenorZero.Text = "Não Importar Redução Z menor que Zero"
        Me.chbRzMenorZero.UseVisualStyleBackColor = True
        '
        'chbRzValor
        '
        Me.chbRzValor.AutoSize = True
        Me.chbRzValor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbRzValor.Location = New System.Drawing.Point(17, 82)
        Me.chbRzValor.Name = "chbRzValor"
        Me.chbRzValor.Size = New System.Drawing.Size(158, 17)
        Me.chbRzValor.TabIndex = 38
        Me.chbRzValor.Text = "Importar Somente Valor"
        Me.chbRzValor.UseVisualStyleBackColor = True
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.Location = New System.Drawing.Point(227, 60)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(78, 13)
        Me.Label67.TabIndex = 37
        Me.Label67.Text = "ECF Padrão:"
        '
        'txtEcfPadrao
        '
        Me.txtEcfPadrao.Location = New System.Drawing.Point(311, 53)
        Me.txtEcfPadrao.Name = "txtEcfPadrao"
        Me.txtEcfPadrao.Size = New System.Drawing.Size(125, 20)
        Me.txtEcfPadrao.TabIndex = 36
        '
        'chbRzConsul
        '
        Me.chbRzConsul.AutoSize = True
        Me.chbRzConsul.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbRzConsul.Location = New System.Drawing.Point(17, 59)
        Me.chbRzConsul.Name = "chbRzConsul"
        Me.chbRzConsul.Size = New System.Drawing.Size(200, 17)
        Me.chbRzConsul.TabIndex = 35
        Me.chbRzConsul.Text = "Importar Redução Consolidada"
        Me.chbRzConsul.UseVisualStyleBackColor = True
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.btEmailSalvar)
        Me.TabPage7.Controls.Add(Me.Label106)
        Me.TabPage7.Controls.Add(Me.txMonitorTempo)
        Me.TabPage7.Controls.Add(Me.Label79)
        Me.TabPage7.Controls.Add(Me.txContador)
        Me.TabPage7.Controls.Add(Me.Button1)
        Me.TabPage7.Controls.Add(Me.Label97)
        Me.TabPage7.Controls.Add(Me.txAnexo)
        Me.TabPage7.Controls.Add(Me.Label98)
        Me.TabPage7.Controls.Add(Me.Label99)
        Me.TabPage7.Controls.Add(Me.Label101)
        Me.TabPage7.Controls.Add(Me.Label102)
        Me.TabPage7.Controls.Add(Me.Label103)
        Me.TabPage7.Controls.Add(Me.Label104)
        Me.TabPage7.Controls.Add(Me.Label105)
        Me.TabPage7.Controls.Add(Me.txAssunto)
        Me.TabPage7.Controls.Add(Me.btEnviar)
        Me.TabPage7.Controls.Add(Me.chbSSL)
        Me.TabPage7.Controls.Add(Me.txMensagem)
        Me.TabPage7.Controls.Add(Me.txDestinatario)
        Me.TabPage7.Controls.Add(Me.txRemetente)
        Me.TabPage7.Controls.Add(Me.txSenha)
        Me.TabPage7.Controls.Add(Me.txUsuario)
        Me.TabPage7.Controls.Add(Me.txPorta)
        Me.TabPage7.Controls.Add(Me.txServidorSMTP)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(680, 556)
        Me.TabPage7.TabIndex = 2
        Me.TabPage7.Text = "Conf. E-mail"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'btEmailSalvar
        '
        Me.btEmailSalvar.Location = New System.Drawing.Point(524, 381)
        Me.btEmailSalvar.Name = "btEmailSalvar"
        Me.btEmailSalvar.Size = New System.Drawing.Size(72, 53)
        Me.btEmailSalvar.TabIndex = 52
        Me.btEmailSalvar.Text = "Salvar"
        Me.btEmailSalvar.UseVisualStyleBackColor = True
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.Location = New System.Drawing.Point(387, 12)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(89, 13)
        Me.Label106.TabIndex = 48
        Me.Label106.Text = "Tempo - Minutos:"
        '
        'txMonitorTempo
        '
        Me.txMonitorTempo.Location = New System.Drawing.Point(390, 28)
        Me.txMonitorTempo.Name = "txMonitorTempo"
        Me.txMonitorTempo.Size = New System.Drawing.Size(59, 20)
        Me.txMonitorTempo.TabIndex = 47
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.Location = New System.Drawing.Point(145, 443)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(111, 13)
        Me.Label79.TabIndex = 46
        Me.Label79.Text = "Total de Arquivos:"
        '
        'txContador
        '
        Me.txContador.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txContador.Location = New System.Drawing.Point(399, 440)
        Me.txContador.Name = "txContador"
        Me.txContador.Size = New System.Drawing.Size(74, 20)
        Me.txContador.TabIndex = 45
        Me.txContador.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(355, 237)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(33, 22)
        Me.Button1.TabIndex = 44
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.Location = New System.Drawing.Point(19, 221)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(40, 13)
        Me.Label97.TabIndex = 43
        Me.Label97.Text = "Anexo:"
        '
        'txAnexo
        '
        Me.txAnexo.Location = New System.Drawing.Point(22, 238)
        Me.txAnexo.Name = "txAnexo"
        Me.txAnexo.Size = New System.Drawing.Size(332, 20)
        Me.txAnexo.TabIndex = 42
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.Location = New System.Drawing.Point(19, 181)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(48, 13)
        Me.Label98.TabIndex = 41
        Me.Label98.Text = "Assunto:"
        '
        'Label99
        '
        Me.Label99.AutoSize = True
        Me.Label99.Location = New System.Drawing.Point(19, 142)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(66, 13)
        Me.Label99.TabIndex = 40
        Me.Label99.Text = "Destinatário:"
        '
        'Label101
        '
        Me.Label101.AutoSize = True
        Me.Label101.Location = New System.Drawing.Point(19, 103)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(62, 13)
        Me.Label101.TabIndex = 39
        Me.Label101.Text = "Remetente:"
        '
        'Label102
        '
        Me.Label102.AutoSize = True
        Me.Label102.Location = New System.Drawing.Point(305, 64)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(41, 13)
        Me.Label102.TabIndex = 38
        Me.Label102.Text = "Senha:"
        '
        'Label103
        '
        Me.Label103.AutoSize = True
        Me.Label103.Location = New System.Drawing.Point(19, 64)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(46, 13)
        Me.Label103.TabIndex = 37
        Me.Label103.Text = "Usuário:"
        '
        'Label104
        '
        Me.Label104.AutoSize = True
        Me.Label104.Location = New System.Drawing.Point(209, 15)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(35, 13)
        Me.Label104.TabIndex = 36
        Me.Label104.Text = "Porta:"
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Location = New System.Drawing.Point(19, 15)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(82, 13)
        Me.Label105.TabIndex = 35
        Me.Label105.Text = "Servidor SMTP:"
        '
        'txAssunto
        '
        Me.txAssunto.Location = New System.Drawing.Point(22, 198)
        Me.txAssunto.Name = "txAssunto"
        Me.txAssunto.Size = New System.Drawing.Size(360, 20)
        Me.txAssunto.TabIndex = 34
        '
        'btEnviar
        '
        Me.btEnviar.Location = New System.Drawing.Point(399, 205)
        Me.btEnviar.Name = "btEnviar"
        Me.btEnviar.Size = New System.Drawing.Size(72, 53)
        Me.btEnviar.TabIndex = 33
        Me.btEnviar.Text = "Enviar"
        Me.btEnviar.UseVisualStyleBackColor = True
        '
        'chbSSL
        '
        Me.chbSSL.AutoSize = True
        Me.chbSSL.Location = New System.Drawing.Point(308, 30)
        Me.chbSSL.Name = "chbSSL"
        Me.chbSSL.Size = New System.Drawing.Size(46, 17)
        Me.chbSSL.TabIndex = 32
        Me.chbSSL.Text = "SSL"
        Me.chbSSL.UseVisualStyleBackColor = True
        '
        'txMensagem
        '
        Me.txMensagem.Location = New System.Drawing.Point(22, 288)
        Me.txMensagem.Multiline = True
        Me.txMensagem.Name = "txMensagem"
        Me.txMensagem.Size = New System.Drawing.Size(449, 146)
        Me.txMensagem.TabIndex = 31
        '
        'txDestinatario
        '
        Me.txDestinatario.Location = New System.Drawing.Point(22, 158)
        Me.txDestinatario.Name = "txDestinatario"
        Me.txDestinatario.Size = New System.Drawing.Size(360, 20)
        Me.txDestinatario.TabIndex = 30
        '
        'txRemetente
        '
        Me.txRemetente.Location = New System.Drawing.Point(22, 119)
        Me.txRemetente.Name = "txRemetente"
        Me.txRemetente.Size = New System.Drawing.Size(360, 20)
        Me.txRemetente.TabIndex = 29
        '
        'txSenha
        '
        Me.txSenha.Location = New System.Drawing.Point(308, 80)
        Me.txSenha.Name = "txSenha"
        Me.txSenha.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txSenha.Size = New System.Drawing.Size(163, 20)
        Me.txSenha.TabIndex = 28
        '
        'txUsuario
        '
        Me.txUsuario.Location = New System.Drawing.Point(22, 80)
        Me.txUsuario.Name = "txUsuario"
        Me.txUsuario.Size = New System.Drawing.Size(234, 20)
        Me.txUsuario.TabIndex = 27
        '
        'txPorta
        '
        Me.txPorta.Location = New System.Drawing.Point(210, 28)
        Me.txPorta.Name = "txPorta"
        Me.txPorta.Size = New System.Drawing.Size(81, 20)
        Me.txPorta.TabIndex = 26
        '
        'txServidorSMTP
        '
        Me.txServidorSMTP.Location = New System.Drawing.Point(22, 31)
        Me.txServidorSMTP.Name = "txServidorSMTP"
        Me.txServidorSMTP.Size = New System.Drawing.Size(163, 20)
        Me.txServidorSMTP.TabIndex = 25
        '
        'chbCopiaContabilidade
        '
        Me.chbCopiaContabilidade.Controls.Add(Me.GroupBox8)
        Me.chbCopiaContabilidade.Controls.Add(Me.GroupBox7)
        Me.chbCopiaContabilidade.Controls.Add(Me.GroupBox6)
        Me.chbCopiaContabilidade.Location = New System.Drawing.Point(4, 22)
        Me.chbCopiaContabilidade.Name = "chbCopiaContabilidade"
        Me.chbCopiaContabilidade.Padding = New System.Windows.Forms.Padding(3)
        Me.chbCopiaContabilidade.Size = New System.Drawing.Size(680, 556)
        Me.chbCopiaContabilidade.TabIndex = 3
        Me.chbCopiaContabilidade.Text = "Conf. Adicionais"
        Me.chbCopiaContabilidade.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.GroupBox11)
        Me.GroupBox8.Controls.Add(Me.GroupBox10)
        Me.GroupBox8.Controls.Add(Me.GroupBox9)
        Me.GroupBox8.Controls.Add(Me.lbStatusVendas)
        Me.GroupBox8.Controls.Add(Me.btFechar)
        Me.GroupBox8.Controls.Add(Me.btAbrir)
        Me.GroupBox8.Controls.Add(Me.chbVendasTeorema)
        Me.GroupBox8.Location = New System.Drawing.Point(28, 298)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(616, 227)
        Me.GroupBox8.TabIndex = 44
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Configuração Vendas Teorema:"
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.chbDomingo)
        Me.GroupBox11.Controls.Add(Me.chbSabado)
        Me.GroupBox11.Controls.Add(Me.chbSexta)
        Me.GroupBox11.Controls.Add(Me.chbQuinta)
        Me.GroupBox11.Controls.Add(Me.chbQuarta)
        Me.GroupBox11.Controls.Add(Me.chbTerca)
        Me.GroupBox11.Controls.Add(Me.chbSegunda)
        Me.GroupBox11.Location = New System.Drawing.Point(506, 19)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(104, 196)
        Me.GroupBox11.TabIndex = 57
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Manter Aberto:"
        '
        'chbDomingo
        '
        Me.chbDomingo.AutoSize = True
        Me.chbDomingo.Location = New System.Drawing.Point(16, 163)
        Me.chbDomingo.Name = "chbDomingo"
        Me.chbDomingo.Size = New System.Drawing.Size(68, 17)
        Me.chbDomingo.TabIndex = 70
        Me.chbDomingo.Text = "Domingo"
        Me.chbDomingo.UseVisualStyleBackColor = True
        '
        'chbSabado
        '
        Me.chbSabado.AutoSize = True
        Me.chbSabado.Location = New System.Drawing.Point(16, 140)
        Me.chbSabado.Name = "chbSabado"
        Me.chbSabado.Size = New System.Drawing.Size(63, 17)
        Me.chbSabado.TabIndex = 69
        Me.chbSabado.Text = "Sábado"
        Me.chbSabado.UseVisualStyleBackColor = True
        '
        'chbSexta
        '
        Me.chbSexta.AutoSize = True
        Me.chbSexta.Location = New System.Drawing.Point(16, 117)
        Me.chbSexta.Name = "chbSexta"
        Me.chbSexta.Size = New System.Drawing.Size(53, 17)
        Me.chbSexta.TabIndex = 68
        Me.chbSexta.Text = "Sexta"
        Me.chbSexta.UseVisualStyleBackColor = True
        '
        'chbQuinta
        '
        Me.chbQuinta.AutoSize = True
        Me.chbQuinta.Location = New System.Drawing.Point(16, 94)
        Me.chbQuinta.Name = "chbQuinta"
        Me.chbQuinta.Size = New System.Drawing.Size(57, 17)
        Me.chbQuinta.TabIndex = 67
        Me.chbQuinta.Text = "Quinta"
        Me.chbQuinta.UseVisualStyleBackColor = True
        '
        'chbQuarta
        '
        Me.chbQuarta.AutoSize = True
        Me.chbQuarta.Location = New System.Drawing.Point(16, 71)
        Me.chbQuarta.Name = "chbQuarta"
        Me.chbQuarta.Size = New System.Drawing.Size(58, 17)
        Me.chbQuarta.TabIndex = 66
        Me.chbQuarta.Text = "Quarta"
        Me.chbQuarta.UseVisualStyleBackColor = True
        '
        'chbTerca
        '
        Me.chbTerca.AutoSize = True
        Me.chbTerca.Location = New System.Drawing.Point(16, 48)
        Me.chbTerca.Name = "chbTerca"
        Me.chbTerca.Size = New System.Drawing.Size(54, 17)
        Me.chbTerca.TabIndex = 65
        Me.chbTerca.Text = "Terça"
        Me.chbTerca.UseVisualStyleBackColor = True
        '
        'chbSegunda
        '
        Me.chbSegunda.AutoSize = True
        Me.chbSegunda.Location = New System.Drawing.Point(16, 25)
        Me.chbSegunda.Name = "chbSegunda"
        Me.chbSegunda.Size = New System.Drawing.Size(69, 17)
        Me.chbSegunda.TabIndex = 64
        Me.chbSegunda.Text = "Segunda"
        Me.chbSegunda.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.Label125)
        Me.GroupBox10.Controls.Add(Me.Label126)
        Me.GroupBox10.Controls.Add(Me.txHoraFinal2)
        Me.GroupBox10.Controls.Add(Me.txHoraInicial2)
        Me.GroupBox10.Location = New System.Drawing.Point(236, 42)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(206, 65)
        Me.GroupBox10.TabIndex = 56
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "2º Horário:"
        '
        'Label125
        '
        Me.Label125.AutoSize = True
        Me.Label125.Location = New System.Drawing.Point(106, 16)
        Me.Label125.Name = "Label125"
        Me.Label125.Size = New System.Drawing.Size(58, 13)
        Me.Label125.TabIndex = 54
        Me.Label125.Text = "Hora Final:"
        '
        'Label126
        '
        Me.Label126.AutoSize = True
        Me.Label126.Location = New System.Drawing.Point(9, 16)
        Me.Label126.Name = "Label126"
        Me.Label126.Size = New System.Drawing.Size(63, 13)
        Me.Label126.TabIndex = 53
        Me.Label126.Text = "Hora Inicial:"
        '
        'txHoraFinal2
        '
        Me.txHoraFinal2.Location = New System.Drawing.Point(109, 32)
        Me.txHoraFinal2.Name = "txHoraFinal2"
        Me.txHoraFinal2.Size = New System.Drawing.Size(86, 20)
        Me.txHoraFinal2.TabIndex = 52
        '
        'txHoraInicial2
        '
        Me.txHoraInicial2.Location = New System.Drawing.Point(10, 32)
        Me.txHoraInicial2.Name = "txHoraInicial2"
        Me.txHoraInicial2.Size = New System.Drawing.Size(86, 20)
        Me.txHoraInicial2.TabIndex = 51
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.Label124)
        Me.GroupBox9.Controls.Add(Me.Label123)
        Me.GroupBox9.Controls.Add(Me.txHoraFinal1)
        Me.GroupBox9.Controls.Add(Me.txHoraInicial1)
        Me.GroupBox9.Location = New System.Drawing.Point(14, 42)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(206, 65)
        Me.GroupBox9.TabIndex = 55
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "1º Horário:"
        '
        'Label124
        '
        Me.Label124.AutoSize = True
        Me.Label124.Location = New System.Drawing.Point(106, 16)
        Me.Label124.Name = "Label124"
        Me.Label124.Size = New System.Drawing.Size(58, 13)
        Me.Label124.TabIndex = 54
        Me.Label124.Text = "Hora Final:"
        '
        'Label123
        '
        Me.Label123.AutoSize = True
        Me.Label123.Location = New System.Drawing.Point(9, 16)
        Me.Label123.Name = "Label123"
        Me.Label123.Size = New System.Drawing.Size(63, 13)
        Me.Label123.TabIndex = 53
        Me.Label123.Text = "Hora Inicial:"
        '
        'txHoraFinal1
        '
        Me.txHoraFinal1.Location = New System.Drawing.Point(109, 32)
        Me.txHoraFinal1.Name = "txHoraFinal1"
        Me.txHoraFinal1.Size = New System.Drawing.Size(86, 20)
        Me.txHoraFinal1.TabIndex = 52
        '
        'txHoraInicial1
        '
        Me.txHoraInicial1.Location = New System.Drawing.Point(10, 32)
        Me.txHoraInicial1.Name = "txHoraInicial1"
        Me.txHoraInicial1.Size = New System.Drawing.Size(86, 20)
        Me.txHoraInicial1.TabIndex = 51
        '
        'lbStatusVendas
        '
        Me.lbStatusVendas.AutoSize = True
        Me.lbStatusVendas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbStatusVendas.Location = New System.Drawing.Point(13, 140)
        Me.lbStatusVendas.Name = "lbStatusVendas"
        Me.lbStatusVendas.Size = New System.Drawing.Size(195, 25)
        Me.lbStatusVendas.TabIndex = 54
        Me.lbStatusVendas.Text = "Status:FECHADO"
        '
        'btFechar
        '
        Me.btFechar.Location = New System.Drawing.Point(349, 130)
        Me.btFechar.Name = "btFechar"
        Me.btFechar.Size = New System.Drawing.Size(93, 35)
        Me.btFechar.TabIndex = 52
        Me.btFechar.Text = "Fechar Vendas"
        Me.btFechar.UseVisualStyleBackColor = True
        '
        'btAbrir
        '
        Me.btAbrir.Location = New System.Drawing.Point(236, 130)
        Me.btAbrir.Name = "btAbrir"
        Me.btAbrir.Size = New System.Drawing.Size(93, 35)
        Me.btAbrir.TabIndex = 51
        Me.btAbrir.Text = "Abrir Vendas"
        Me.btAbrir.UseVisualStyleBackColor = True
        '
        'chbVendasTeorema
        '
        Me.chbVendasTeorema.AutoSize = True
        Me.chbVendasTeorema.Location = New System.Drawing.Point(14, 19)
        Me.chbVendasTeorema.Name = "chbVendasTeorema"
        Me.chbVendasTeorema.Size = New System.Drawing.Size(148, 17)
        Me.chbVendasTeorema.TabIndex = 44
        Me.chbVendasTeorema.Text = "Habilitar Vendas Teorema"
        Me.chbVendasTeorema.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.Label120)
        Me.GroupBox7.Controls.Add(Me.Label119)
        Me.GroupBox7.Controls.Add(Me.txIdToken)
        Me.GroupBox7.Controls.Add(Me.txCSC)
        Me.GroupBox7.Controls.Add(Me.lbValidade)
        Me.GroupBox7.Controls.Add(Me.Label117)
        Me.GroupBox7.Controls.Add(Me.Label116)
        Me.GroupBox7.Controls.Add(Me.cbAmbiente)
        Me.GroupBox7.Controls.Add(Me.cbCertificado)
        Me.GroupBox7.Controls.Add(Me.chbNFCeCancelada)
        Me.GroupBox7.Location = New System.Drawing.Point(27, 137)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(618, 135)
        Me.GroupBox7.TabIndex = 43
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Verificações NFC-e"
        '
        'Label120
        '
        Me.Label120.AutoSize = True
        Me.Label120.Location = New System.Drawing.Point(294, 39)
        Me.Label120.Name = "Label120"
        Me.Label120.Size = New System.Drawing.Size(31, 13)
        Me.Label120.TabIndex = 52
        Me.Label120.Text = "CSC:"
        Me.Label120.Visible = False
        '
        'Label119
        '
        Me.Label119.AutoSize = True
        Me.Label119.Location = New System.Drawing.Point(223, 39)
        Me.Label119.Name = "Label119"
        Me.Label119.Size = New System.Drawing.Size(53, 13)
        Me.Label119.TabIndex = 51
        Me.Label119.Text = "Id Token:"
        Me.Label119.Visible = False
        '
        'txIdToken
        '
        Me.txIdToken.Location = New System.Drawing.Point(226, 55)
        Me.txIdToken.MaxLength = 6
        Me.txIdToken.Name = "txIdToken"
        Me.txIdToken.Size = New System.Drawing.Size(65, 20)
        Me.txIdToken.TabIndex = 50
        Me.txIdToken.Visible = False
        '
        'txCSC
        '
        Me.txCSC.Location = New System.Drawing.Point(297, 55)
        Me.txCSC.MaxLength = 36
        Me.txCSC.Name = "txCSC"
        Me.txCSC.Size = New System.Drawing.Size(295, 20)
        Me.txCSC.TabIndex = 49
        Me.txCSC.Visible = False
        '
        'lbValidade
        '
        Me.lbValidade.Location = New System.Drawing.Point(297, 81)
        Me.lbValidade.Name = "lbValidade"
        Me.lbValidade.Size = New System.Drawing.Size(295, 13)
        Me.lbValidade.TabIndex = 48
        Me.lbValidade.Text = "Data de validade:"
        Me.lbValidade.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label117
        '
        Me.Label117.AutoSize = True
        Me.Label117.Location = New System.Drawing.Point(16, 81)
        Me.Label117.Name = "Label117"
        Me.Label117.Size = New System.Drawing.Size(92, 13)
        Me.Label117.TabIndex = 47
        Me.Label117.Text = "Certificado Digital:"
        '
        'Label116
        '
        Me.Label116.AutoSize = True
        Me.Label116.Location = New System.Drawing.Point(16, 39)
        Me.Label116.Name = "Label116"
        Me.Label116.Size = New System.Drawing.Size(54, 13)
        Me.Label116.TabIndex = 46
        Me.Label116.Text = "Ambiente:"
        '
        'cbAmbiente
        '
        Me.cbAmbiente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbAmbiente.FormattingEnabled = True
        Me.cbAmbiente.Items.AddRange(New Object() {"1-Produção", "2-Homologação"})
        Me.cbAmbiente.Location = New System.Drawing.Point(16, 55)
        Me.cbAmbiente.Name = "cbAmbiente"
        Me.cbAmbiente.Size = New System.Drawing.Size(204, 21)
        Me.cbAmbiente.TabIndex = 45
        '
        'cbCertificado
        '
        Me.cbCertificado.FormattingEnabled = True
        Me.cbCertificado.Location = New System.Drawing.Point(16, 97)
        Me.cbCertificado.Name = "cbCertificado"
        Me.cbCertificado.Size = New System.Drawing.Size(577, 21)
        Me.cbCertificado.TabIndex = 44
        '
        'chbNFCeCancelada
        '
        Me.chbNFCeCancelada.AutoSize = True
        Me.chbNFCeCancelada.Location = New System.Drawing.Point(16, 19)
        Me.chbNFCeCancelada.Name = "chbNFCeCancelada"
        Me.chbNFCeCancelada.Size = New System.Drawing.Size(151, 17)
        Me.chbNFCeCancelada.TabIndex = 43
        Me.chbNFCeCancelada.Text = "Verificar NFC-e Cancelada"
        Me.chbNFCeCancelada.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.chbContabilidade)
        Me.GroupBox6.Controls.Add(Me.txLinkContabiliade)
        Me.GroupBox6.Location = New System.Drawing.Point(27, 22)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(620, 99)
        Me.GroupBox6.TabIndex = 42
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Diretórios para Contabilidade:"
        '
        'chbContabilidade
        '
        Me.chbContabilidade.AutoSize = True
        Me.chbContabilidade.Location = New System.Drawing.Point(17, 29)
        Me.chbContabilidade.Name = "chbContabilidade"
        Me.chbContabilidade.Size = New System.Drawing.Size(149, 17)
        Me.chbContabilidade.TabIndex = 42
        Me.chbContabilidade.Text = "Cópia para contabilidade?"
        Me.chbContabilidade.UseVisualStyleBackColor = True
        '
        'txLinkContabiliade
        '
        Me.txLinkContabiliade.Location = New System.Drawing.Point(17, 62)
        Me.txLinkContabiliade.Name = "txLinkContabiliade"
        Me.txLinkContabiliade.Size = New System.Drawing.Size(577, 20)
        Me.txLinkContabiliade.TabIndex = 0
        '
        'tmInicioImportacao
        '
        '
        'ntfMinimizar
        '
        Me.ntfMinimizar.Icon = CType(resources.GetObject("ntfMinimizar.Icon"), System.Drawing.Icon)
        Me.ntfMinimizar.Text = "Importador de Z"
        Me.ntfMinimizar.Visible = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.SystemColors.Control
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel4})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 666)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(749, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "Status:"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(45, 17)
        Me.ToolStripStatusLabel1.Text = "Status:"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(0, 17)
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(51, 17)
        Me.ToolStripStatusLabel4.Text = "Cupons:"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConfiguraçõesToolStripMenuItem, Me.SairToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(749, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ConfiguraçõesToolStripMenuItem
        '
        Me.ConfiguraçõesToolStripMenuItem.Name = "ConfiguraçõesToolStripMenuItem"
        Me.ConfiguraçõesToolStripMenuItem.Size = New System.Drawing.Size(96, 20)
        Me.ConfiguraçõesToolStripMenuItem.Text = "Configurações"
        '
        'SairToolStripMenuItem
        '
        Me.SairToolStripMenuItem.Name = "SairToolStripMenuItem"
        Me.SairToolStripMenuItem.Size = New System.Drawing.Size(38, 20)
        Me.SairToolStripMenuItem.Text = "Sair"
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Text = "NotifyIcon1"
        Me.NotifyIcon1.Visible = True
        '
        'tmMonitorPasta
        '
        '
        'Timer2
        '
        '
        'tmVendas
        '
        Me.tmVendas.Enabled = True
        Me.tmVendas.Interval = 5000
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(749, 688)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AMSoft - Importador Redução Z/NFC-e"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.grpFinanceiro.ResumeLayout(False)
        Me.grpFinanceiro.PerformLayout()
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        Me.chbCopiaContabilidade.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btDestinoNFCe As System.Windows.Forms.Button
    Friend WithEvents btDestinoZ As System.Windows.Forms.Button
    Friend WithEvents btDestinoVendas As System.Windows.Forms.Button
    Friend WithEvents btOrigemArquivos As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txDestinoNFCe As System.Windows.Forms.TextBox
    Friend WithEvents txDestinoZ As System.Windows.Forms.TextBox
    Friend WithEvents txDestinoVendas As System.Windows.Forms.TextBox
    Friend WithEvents txOrigemArquivos As System.Windows.Forms.TextBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents A2 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents A1 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents B1 As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents I1 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents I8 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents B8 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents A8 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents B2 As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents I2 As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents I7 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents B7 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents A7 As System.Windows.Forms.TextBox
    Friend WithEvents A3 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents B3 As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents I3 As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents I6 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents B6 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents A6 As System.Windows.Forms.TextBox
    Friend WithEvents A4 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents B4 As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents I4 As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents I5 As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents B5 As System.Windows.Forms.TextBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents A5 As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents transacao As System.Windows.Forms.TextBox
    Friend WithEvents mapa_numero As System.Windows.Forms.TextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents hora As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents naotributado As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents substituicao As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents isento As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Acrescimo As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents sangria As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents desconto As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents cancelamento As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Movimento As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents fabricante As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents impressora As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents loja As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents crz As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents cro As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents gt_final As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents gt_inicial As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents ticket_final As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents ticket_inicial As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents data_fiscal As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents caixa As System.Windows.Forms.TextBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents txTagSitProtocolo As System.Windows.Forms.TextBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents txDataProtocolo As System.Windows.Forms.TextBox
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents txProtocolo As System.Windows.Forms.TextBox
    Friend WithEvents txLinkQRCode As System.Windows.Forms.TextBox
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents txDigestValue As System.Windows.Forms.TextBox
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents txEmissao As System.Windows.Forms.TextBox
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents txChaveNFCe As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents tx_tag_protocolo As System.Windows.Forms.TextBox
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents btLocalExportacao As System.Windows.Forms.Button
    Friend WithEvents txNfceCaminhoExportacao As System.Windows.Forms.TextBox
    Friend WithEvents btExportarXml As System.Windows.Forms.Button
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents dtNfceFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtNfceDataIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents txNfceChave As System.Windows.Forms.TextBox
    Friend WithEvents txNfceNota As System.Windows.Forms.TextBox
    Friend WithEvents txNfceLoja As System.Windows.Forms.TextBox
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents nfce_Data As System.Windows.Forms.TextBox
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents nfce_Xml As System.Windows.Forms.TextBox
    Friend WithEvents nfce_Chave As System.Windows.Forms.TextBox
    Friend WithEvents nfce_Nota As System.Windows.Forms.TextBox
    Friend WithEvents nfce_Serie As System.Windows.Forms.TextBox
    Friend WithEvents nfce_Loja As System.Windows.Forms.TextBox
    Friend WithEvents lstDados As System.Windows.Forms.ListBox
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents txSenhaConfiguracao As System.Windows.Forms.TextBox
    Friend WithEvents btSairConfiguracao As System.Windows.Forms.Button
    Friend WithEvents chbHabilitaImportador As System.Windows.Forms.CheckBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents chbHabilitaData As System.Windows.Forms.CheckBox
    Friend WithEvents dtFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents btSalvar As System.Windows.Forms.Button
    Friend WithEvents lstArquivos As System.Windows.Forms.ListBox
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents txtVersao As System.Windows.Forms.TextBox
    Friend WithEvents chbXmlContabilidade As System.Windows.Forms.CheckBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents txExtensao As System.Windows.Forms.TextBox
    Friend WithEvents txTempo As System.Windows.Forms.TextBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents tmInicioImportacao As System.Windows.Forms.Timer
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ntfMinimizar As System.Windows.Forms.NotifyIcon
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents nfce_valor As System.Windows.Forms.TextBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents chbOnline As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ConfiguraçõesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SairToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btVisualizarXml As System.Windows.Forms.Button
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents chbmonitorpasta As System.Windows.Forms.CheckBox
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents btLocalizarBancoNfce As System.Windows.Forms.Button
    Friend WithEvents txConexaoSenhaNfce As System.Windows.Forms.TextBox
    Friend WithEvents txConexaoPortaNfce As System.Windows.Forms.TextBox
    Friend WithEvents txConexaoUsuarioNfce As System.Windows.Forms.TextBox
    Friend WithEvents txConexaoServidorNFce As System.Windows.Forms.TextBox
    Friend WithEvents txConexaoBancoNFce As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents rbConexaoODBC As System.Windows.Forms.RadioButton
    Friend WithEvents rbConexaoNormal As System.Windows.Forms.RadioButton
    Friend WithEvents cmdConexaoNormalSalvar As System.Windows.Forms.Button
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents btLocalizarBancoTeorema As System.Windows.Forms.Button
    Friend WithEvents txConexaoSenha As System.Windows.Forms.TextBox
    Friend WithEvents txConexaoPorta As System.Windows.Forms.TextBox
    Friend WithEvents txConexaoUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txConexaoServidor As System.Windows.Forms.TextBox
    Friend WithEvents txConexaoBanco As System.Windows.Forms.TextBox
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents grpFinanceiro As System.Windows.Forms.GroupBox
    Friend WithEvents chbRzRepetida As System.Windows.Forms.CheckBox
    Friend WithEvents chbImportaRz As System.Windows.Forms.CheckBox
    Friend WithEvents chbRzMenorZero As System.Windows.Forms.CheckBox
    Friend WithEvents chbRzValor As System.Windows.Forms.CheckBox
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents txtEcfPadrao As System.Windows.Forms.TextBox
    Friend WithEvents chbRzConsul As System.Windows.Forms.CheckBox
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents chbEmail As System.Windows.Forms.CheckBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents txContador As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents txAnexo As System.Windows.Forms.TextBox
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents txAssunto As System.Windows.Forms.TextBox
    Friend WithEvents btEnviar As System.Windows.Forms.Button
    Friend WithEvents chbSSL As System.Windows.Forms.CheckBox
    Friend WithEvents txMensagem As System.Windows.Forms.TextBox
    Friend WithEvents txDestinatario As System.Windows.Forms.TextBox
    Friend WithEvents txRemetente As System.Windows.Forms.TextBox
    Friend WithEvents txSenha As System.Windows.Forms.TextBox
    Friend WithEvents txUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txPorta As System.Windows.Forms.TextBox
    Friend WithEvents txServidorSMTP As System.Windows.Forms.TextBox
    Friend WithEvents tmMonitorPasta As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents txMonitorTempo As System.Windows.Forms.TextBox
    Friend WithEvents btEmailSalvar As System.Windows.Forms.Button
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents txSenhaMySql As System.Windows.Forms.TextBox
    Friend WithEvents txPortaMySql As System.Windows.Forms.TextBox
    Friend WithEvents txUsuarioMySql As System.Windows.Forms.TextBox
    Friend WithEvents txServidorMySql As System.Windows.Forms.TextBox
    Friend WithEvents txBancoMysql As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents LOJA_VENDAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NUMERO_DEVOLUCAO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PDV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DATA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TICKET_DEVOLUCAO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SEQUENCIA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PLU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCRICAO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTIDADE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VALOR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DEPARTAMENTO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MAKER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents REASON As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRANSACAO_VENDAS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chbImportarNFCe As System.Windows.Forms.CheckBox
    Friend WithEvents btDestinoScannTech As System.Windows.Forms.Button
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents txDestinoScannTech As System.Windows.Forms.TextBox
    Friend WithEvents chbScannTech As System.Windows.Forms.CheckBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label113 As System.Windows.Forms.Label
    Friend WithEvents txBancoEntregaFdb As System.Windows.Forms.TextBox
    Friend WithEvents chbEntrega As System.Windows.Forms.CheckBox
    Friend WithEvents btDestinoErro As Button
    Friend WithEvents Label114 As Label
    Friend WithEvents txDestinoErro As TextBox
    Friend WithEvents Label115 As Label
    Friend WithEvents txVesaoNFCe As TextBox
    Friend WithEvents chbCopiaContabilidade As TabPage
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents txLinkContabiliade As TextBox
    Friend WithEvents chbContabilidade As CheckBox
    Friend WithEvents chbRzRecarga As CheckBox
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents Label120 As Label
    Friend WithEvents Label119 As Label
    Friend WithEvents txIdToken As TextBox
    Friend WithEvents txCSC As TextBox
    Friend WithEvents lbValidade As Label
    Friend WithEvents Label117 As Label
    Friend WithEvents Label116 As Label
    Friend WithEvents cbAmbiente As ComboBox
    Friend WithEvents cbCertificado As ComboBox
    Friend WithEvents chbNFCeCancelada As CheckBox
    Friend WithEvents Button4 As Button
    Friend WithEvents Label118 As Label
    Friend WithEvents txCupomHoraFinal As TextBox
    Friend WithEvents txCupomHoraInicial As TextBox
    Friend WithEvents Label121 As Label
    Friend WithEvents Label122 As Label
    Friend WithEvents txNumeroArquivos As TextBox
    Friend WithEvents ToolStripStatusLabel4 As ToolStripStatusLabel
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents btAbrir As Button
    Friend WithEvents chbVendasTeorema As CheckBox
    Friend WithEvents btFechar As Button
    Friend WithEvents lbStatusVendas As Label
    Friend WithEvents tmVendas As Timer
    Friend WithEvents GroupBox11 As GroupBox
    Friend WithEvents chbDomingo As CheckBox
    Friend WithEvents chbSabado As CheckBox
    Friend WithEvents chbSexta As CheckBox
    Friend WithEvents chbQuinta As CheckBox
    Friend WithEvents chbQuarta As CheckBox
    Friend WithEvents chbTerca As CheckBox
    Friend WithEvents chbSegunda As CheckBox
    Friend WithEvents GroupBox10 As GroupBox
    Friend WithEvents Label125 As Label
    Friend WithEvents Label126 As Label
    Friend WithEvents txHoraFinal2 As TextBox
    Friend WithEvents txHoraInicial2 As TextBox
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents Label124 As Label
    Friend WithEvents Label123 As Label
    Friend WithEvents txHoraFinal1 As TextBox
    Friend WithEvents txHoraInicial1 As TextBox
    Friend WithEvents chbHabilitarVendas As CheckBox
    Friend WithEvents lbVendas As Label
    Friend WithEvents chbRzEmporium As CheckBox
    Friend WithEvents Button5 As Button
End Class
