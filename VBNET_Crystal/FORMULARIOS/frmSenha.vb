﻿Public Class frmSenha

    Private Sub btCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCancel.Click
        Me.Close()
    End Sub

    Private Sub btOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btOk.Click
        If txSenha.Text = Form1.txSenhaConfiguracao.Text Then
            Form1.HABILITAR()
            Me.Close()
        Else
            MsgBox("Senha invalida!", vbExclamation, "WBPort - Erro")
            'Me.Close()
        End If
    End Sub

    Private Sub txSenha_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txSenha.KeyPress
        If e.KeyChar = Chr(13) Then
            btOk_Click(Nothing, Nothing)
        End If
        If e.KeyChar = Chr(27) Then
            Me.Close()
        End If
    End Sub

End Class