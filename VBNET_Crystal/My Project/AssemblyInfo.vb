﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Importador NFC-e")>
<Assembly: AssemblyDescription("Importador NFC-e com envio para o contador")>
<Assembly: AssemblyCompany("AMSoft Soluções Inteligentes")>
<Assembly: AssemblyProduct("Importador NFC-e")>
<Assembly: AssemblyCopyright("Copyright © AMSoft 2020®")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("d7e875df-7443-4190-bbe2-7eb21d58f896")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.2021.01.14")>
<Assembly: AssemblyFileVersion("1.2021.01.14")>
